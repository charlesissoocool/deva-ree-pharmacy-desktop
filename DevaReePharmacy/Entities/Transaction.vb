﻿Public Class Transaction
    Inherits Entity

    Private _transaction_date As Date = Nothing
    Private _invoice_no As Long
    Private _transaction_type As OrderMode = OrderMode.Retail
    Private _customer As Customer = Nothing
    Private _staff As New User
    Private _agent As User = Nothing
    Private _credit As Credit = Nothing
    Private _dispatch_id As Integer = 0
    Private _transaction_items As List(Of Transaction_Item)
    Private _transaction_verified As Boolean = False
    Private _transaction_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property TransactionID As Integer
        Get
            TransactionID = _transaction_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property TransactionDate As Date
        Get
            TransactionDate = _transaction_date
        End Get
        Set(ByVal value As Date)
            _transaction_date = value
            _persisted = False
        End Set
    End Property

    Public Property InvoiceNo As Long
        Get
            InvoiceNo = _invoice_no
        End Get
        Set(ByVal value As Long)
            _invoice_no = value
            _persisted = False
        End Set
    End Property

    Public Property TransactionType As OrderMode
        Get
            TransactionType = _transaction_type
        End Get
        Set(ByVal value As OrderMode)
            _transaction_type = value
            _persisted = False
        End Set
    End Property

    Public ReadOnly Property Verified As Boolean
        Get
            Verified = _transaction_verified
        End Get
    End Property

    Public Property DispatchID As Integer
        Get
            DispatchID = _dispatch_id
        End Get
        Set(ByVal value As Integer)
            _persisted = False
            _dispatch_id = value
        End Set
    End Property

    Public Property Customer As Customer
        Get
            Customer = _customer
        End Get
        Set(ByVal value As Customer)
            _persisted = False
            _customer = value
        End Set
    End Property

    Public Property Agent As User
        Get
            Agent = _agent
        End Get
        Set(ByVal value As User)
            _persisted = False
            _agent = value
        End Set
    End Property

    Public Property Staff As User
        Get
            Staff = _staff
        End Get
        Set(ByVal value As User)
            If IsNothing(value) Then
                _staff = New User(module_session.current_user_profile.UserID)
            Else
                _staff = value
            End If
            _persisted = False
        End Set
    End Property

    Public Property Credit As Credit
        Get
            Credit = _credit
        End Get
        Set(ByVal value As Credit)
            _credit = value
            _persisted = False
        End Set
    End Property

    Public Sub AddItem(ByVal _new_item As Transaction_Item)

        LoadItem(_new_item)
        _persisted = False

    End Sub

    Private Sub LoadItem(ByRef _new_item As Transaction_Item)

        If IsNothing(_transaction_items) Then _transaction_items = New List(Of Transaction_Item)
        _transaction_items.Add(_new_item)

    End Sub

    Public Sub RemoveItem(ByVal item_id As Integer)

        For i = 0 To _transaction_items.Count - 1
            If _transaction_items(i).ItemID = item_id Then
                _transaction_items.RemoveAt(i)
                _persisted = False
                Exit For
            End If
        Next

    End Sub

    Public Sub RemoveAllItems()

        _transaction_items = New List(Of Transaction_Item)

    End Sub

    Public ReadOnly Property TransactionItems As List(Of Transaction_Item)
        Get
            TransactionItems = _transaction_items
        End Get
    End Property

    Public ReadOnly Property TotalAmount As Double
        Get
            If IsNothing(_transaction_items) Then
                TotalAmount = 0
            Else
                TotalAmount = 0
                For Each item In _transaction_items
                    TotalAmount += (item.ItemQuantity - item.ReturnQuantity) * (item.SellingPrice / item.Product.PackingSize)
                Next
            End If
        End Get
    End Property

    Protected Overrides Sub Clean()

        _transaction_date = Nothing
        _invoice_no = 0
        _transaction_type = OrderMode.Retail
        _customer = Nothing
        _staff = New User
        _agent = Nothing
        _credit = Nothing
        _dispatch_id = 0
        _transaction_verified = False
        _transaction_id = 0
        _transaction_items = New List(Of Transaction_Item)

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        _persisted = False
        Dim details = sql.GetTransactionDetail(id)
        If Not IsNothing(details) Then
            _transaction_id = id
            _transaction_date = Date.Parse(details("td_transaction_date"))
            _customer = If(details("td_customer_id") = 0, Nothing, New Customer(Val(details("td_customer_id"))))
            _staff = New User(details("td_staff_id"))
            _agent = If(details("td_agent_id") = 0, Nothing, New User(Val(details("td_agent_id"))))
            _credit = If(details("td_credit_id") = 0, Nothing, New Credit(Val(details("td_credit_id"))))
            _dispatch_id = details("td_dispatch_id")
            _transaction_verified = details("td_transaction_verified")
            _invoice_no = (details("td_invoice_no"))
            Select Case details("td_transaction_type")
                Case TR_RETAIL
                    _transaction_type = OrderMode.Retail
                Case TR_WHOLESALE
                    _transaction_type = OrderMode.Wholesale
                Case TR_DISTRIBUTION
                    _transaction_type = OrderMode.Distribution
            End Select
            _persisted = _staff.Synced
            If _persisted Then
                Dim items = sql.GetTransactionItemsId(id)
                If Not IsNothing(items) Then
                    For Each item In items
                        Dim _new_item = New Transaction_Item(item("item_id"))
                        LoadItem(_new_item)
                        _persisted = (_new_item.Synced And _persisted)
                    Next
                End If
            End If
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        Dim temp_transaction_type As String = If(_transaction_type = OrderMode.Retail, TR_RETAIL, If(_transaction_type = OrderMode.Wholesale, TR_WHOLESALE, TR_DISTRIBUTION))
        If temp_transaction_type = "" Then
            _persisted = False
            Persist = False
            Exit Function
        End If

        _persisted = True

        If Not _staff.Synced Then _persisted = _persisted And _staff.Persist
        If Not IsNothing(_agent) Then
            If Not _agent.Synced Then _persisted = _persisted And _agent.Persist
        End If
        If Not IsNothing(_customer) Then
            If Not _customer.Synced Then _persisted = _persisted And _customer.Persist
        End If
        If Not IsNothing(_credit) Then
            If Not _credit.Synced Then _persisted = _persisted And _credit.Persist
        End If

        If _persisted Then
            If _transaction_id = 0 Then
                _transaction_id = sql.CreateNewTransaction(temp_transaction_type, _invoice_no, _dispatch_id, If(IsNothing(_customer), 0, _customer.CustomerID), _staff.UserID, If(IsNothing(_agent), 0, _agent.UserID), If(IsNothing(_credit), 0, _credit.CreditID))
                _persisted = _transaction_id > 0
            Else
                _persisted = _persisted And sql.UpdateTransaction(_transaction_id, temp_transaction_type, _invoice_no, _dispatch_id, If(IsNothing(_customer), 0, _customer.CustomerID), _staff.UserID, If(IsNothing(_agent), 0, _agent.UserID), If(IsNothing(_credit), 0, _credit.CreditID), _transaction_date)
            End If
        End If

        If _persisted Then
            For Each item In _transaction_items
                item.TransactionID = _transaction_id
                _persisted = item.Persist() And _persisted
            Next
        End If

        Persist = If(_persisted, sql.ValidateTransaction(_transaction_id), _persisted)

    End Function
End Class
