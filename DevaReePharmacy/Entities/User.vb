﻿Public Class User
    Inherits Entity

    Protected _first_name As String = ""
    Protected _last_name As String = ""
    Protected _new_password As String = ""
    Protected _user_id As Integer = 0
    Protected _last_login As String = ""
    <VBFixedString(15)> Protected _user_name As String = ""
    <VBFixedString(30)> Protected _email As String = ""
    Protected _is_administrator As Boolean = False
    Protected _is_products As Boolean = False
    Protected _is_stocks As Boolean = False
    Protected _is_cashier As Boolean = False
    Protected _is_agent As Boolean = False
    Protected _is_finance As Boolean = False

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property UserID As Integer
        Get
            UserID = _user_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property NewPassword As String
        Get
            NewPassword = _new_password
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _new_password = value
        End Set
    End Property

    Public Property Username As String
        Get
            Username = _user_name
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _user_name = value
        End Set
    End Property

    Public Property FirstName As String
        Get
            FirstName = _first_name
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _first_name = value
        End Set
    End Property

    Public Property LastName As String
        Get
            LastName = _last_name
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _last_name = value
        End Set
    End Property

    Public ReadOnly Property FullName As String
        Get
            FullName = _first_name & Space(1) & _last_name
        End Get
    End Property

    Public ReadOnly Property InitialName As String
        Get
            InitialName = If(_first_name = "", "", _first_name.Substring(0, 1) & ". ") & _last_name
        End Get
    End Property

    Public Property Email As String
        Get
            Email = _email
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _email = value
        End Set
    End Property

    Public ReadOnly Property LastLogin As String
        Get
            LastLogin = _last_login
        End Get
    End Property

    Public Property Permission_Administator As Boolean
        Get
            Permission_Administator = _is_administrator
        End Get
        Set(ByVal value As Boolean)
            _is_administrator = value
            _persisted = False
        End Set
    End Property

    Public Property Permission_Stocks As Boolean
        Get
            Permission_Stocks = _is_stocks
        End Get
        Set(ByVal value As Boolean)
            _is_stocks = value
            _persisted = False
        End Set
    End Property

    Public Property Permission_Products As Boolean
        Get
            Permission_Products = _is_products
        End Get
        Set(ByVal value As Boolean)
            _is_products = value
            _persisted = False
        End Set
    End Property

    Public Property Permission_Cashier As Boolean
        Get
            Permission_Cashier = _is_cashier
        End Get
        Set(ByVal value As Boolean)
            _is_cashier = value
            _persisted = False
        End Set
    End Property

    Public Property Permission_Agent As Boolean
        Get
            Permission_Agent = _is_agent
        End Get
        Set(ByVal value As Boolean)
            _is_agent = value
            _persisted = False
        End Set
    End Property

    Public Property Permission_Finance As Boolean
        Get
            Permission_Finance = _is_finance
        End Get
        Set(ByVal value As Boolean)
            _is_finance = value
            _persisted = False
        End Set
    End Property

    Protected Overrides Sub Clean()

        _first_name = ""
        _last_name = ""
        _user_id = 0
        _new_password = Nothing
        _user_name = ""
        _last_login = ""
        _is_administrator = False
        _is_products = False
        _is_stocks = False
        _is_cashier = False
        _is_agent = False
        _is_finance = False
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim db_user = New class_db_user
        Dim db_login = New class_db_login
        Dim user_profile = db_user.GetUserCredential(id)
        If Not IsNothing(user_profile) Then
            _first_name = user_profile("first_name")
            _last_name = user_profile("last_name")
            _email = user_profile("email")
            _user_name = user_profile("username")
            _new_password = Nothing
            _user_id = id
            _last_login = db_login.GetLatestLogin(id)
            Dim permissions = db_user.GetUserPermissions(id)
            If Not IsNothing(permissions) Then
                For Each permission In permissions
                    Select Case permission("permission_type")
                        Case "Admin"
                            _is_administrator = True
                        Case "Products"
                            _is_products = True
                        Case "Stocks"
                            _is_stocks = True
                        Case "Cashier"
                            _is_cashier = True
                        Case "Agent"
                            _is_agent = True
                        Case "Finance"
                            _is_finance = True
                    End Select
                Next
            End If
            _persisted = True
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim db_user As New class_db_user
        If _user_id = 0 Then
            _user_id = db_user.CreateUserLogin(_user_name, _new_password)
        End If
        _persisted = db_user.UpdateUserProfileByID(_user_id, _first_name, _last_name, _email) And db_user.UpdateUserSecurityByID(_user_id, _user_name, _new_password)
        If _persisted Then
            _persisted = If(_is_administrator, db_user.AddUserPermission("Admin", _user_id), db_user.DeleteUserPermission("Admin", _user_id))
            _persisted = If(_is_stocks, db_user.AddUserPermission("Stocks", _user_id), db_user.DeleteUserPermission("Stocks", _user_id)) And _persisted
            _persisted = If(_is_products, db_user.AddUserPermission("Products", _user_id), db_user.DeleteUserPermission("Products", _user_id)) And _persisted
            _persisted = If(_is_cashier, db_user.AddUserPermission("Cashier", _user_id), db_user.DeleteUserPermission("Cashier", _user_id)) And _persisted
            _persisted = If(_is_agent, db_user.AddUserPermission("Agent", _user_id), db_user.DeleteUserPermission("Agent", _user_id)) And _persisted
            _persisted = If(_is_finance, db_user.AddUserPermission("Finance", _user_id), db_user.DeleteUserPermission("Finance", _user_id)) And _persisted
        End If
        Persist = _persisted

    End Function
End Class
