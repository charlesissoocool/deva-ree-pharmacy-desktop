﻿Public MustInherit Class Entity

    Protected _persisted As Boolean

    Protected MustOverride Sub Clean()
    Protected MustOverride Sub Fetch(ByVal id As Integer)
    MustOverride Function Persist() As Boolean

    Public Overloads ReadOnly Property Synced As Boolean
        Get
            Synced = _persisted
        End Get
    End Property

End Class
