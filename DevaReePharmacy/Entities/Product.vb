﻿Public Class Product
    Inherits Entity

    Private _product_category As Product_Category = Nothing
    <VBFixedString(50)> Private _product_name As String = ""
    <VBFixedString(25)> Private _product_description As String = ""
    <VBFixedString(50)> Private _product_code As String = ""
    Private _product_id As Integer = 0
    <VBFixedString(5)> Private _product_unit As String = ""
    <VBFixedString(50)> Private _product_brand As String = ""
    Private _packing_size As Integer = 0
    Private _pads_per_pack As Integer = 0

    Private _pad_price As ProductPrices = New ProductPrices(0, 0, 0)
    Private _unit_price As ProductPrices = New ProductPrices(0, 0, 0)

    Public WithEvents PackPrice As ProductPrices = New ProductPrices(0, 0, 0)

    Public Function Clone() As Product
        Clone = DirectCast(Me.MemberwiseClone(), Product)
    End Function

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property ProductID As Integer
        Get
            ProductID = _product_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property ProductCategory As Product_Category
        Get
            ProductCategory = _product_category
        End Get
        Set(ByVal value As Product_Category)
            _product_category = value
        End Set
    End Property

    Public Property ProductName As String
        Get
            ProductName = _product_name
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _product_name = value
        End Set
    End Property

    Public ReadOnly Property IsSoldInPacks As Boolean
        Get
            IsSoldInPacks = Not Me.PackingSize = 1
        End Get
    End Property

    Public ReadOnly Property IsSoldInPads As Boolean
        Get
            IsSoldInPads = Not Me.PadsPerPack = 1
        End Get
    End Property

    Public ReadOnly Property ProductCompleteName As String
        Get
            ProductCompleteName = _product_name & " " & _product_description & " (" & _product_brand & ")"
        End Get
    End Property

    Public ReadOnly Property ProductCompleteNameWithPacking(ByVal display_unit As ItemUnit) As String
        Get
            Dim piece_count As String
            If display_unit = ItemUnit.Pack Then
                piece_count = " " & _packing_size & "'s"
            ElseIf display_unit = ItemUnit.Pad Then
                piece_count = " " & (_packing_size / _pads_per_pack) & "'s"
            Else
                piece_count = " "
            End If
            ProductCompleteNameWithPacking = _product_name & " " & _product_description & piece_count & " (" & _product_brand & ")"
        End Get
    End Property

    Public Property ProductDescription As String
        Get
            ProductDescription = _product_description
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _product_description = value
        End Set
    End Property

    Public Property ProductCode As String
        Get
            ProductCode = _product_code
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _product_code = value
        End Set
    End Property

    Public Property ProductUnit As String
        Get
            ProductUnit = _product_unit
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _product_unit = value
        End Set
    End Property

    Public Property ProductBrand As String
        Get
            ProductBrand = _product_brand
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _product_brand = value
        End Set
    End Property

    Public Property PackingSize As Integer
        Get
            PackingSize = _packing_size
        End Get
        Set(ByVal value As Integer)
            _packing_size = value
            _persisted = False
        End Set
    End Property

    Public Property PadsPerPack As Integer
        Get
            PadsPerPack = _pads_per_pack
        End Get
        Set(ByVal value As Integer)
            _pads_per_pack = value
            _persisted = False
        End Set
    End Property

    Public ReadOnly Property PadPrice As ProductPrices
        Get
            PadPrice = _pad_price
        End Get
    End Property

    Public ReadOnly Property UnitPrice As ProductPrices
        Get
            UnitPrice = _unit_price
        End Get
    End Property

    Protected Overrides Sub Clean()

        _product_category = Nothing
        _product_name = ""
        _product_description = ""
        _product_code = ""
        _product_unit = ""
        _product_brand = ""
        PackPrice = New ProductPrices(0, 0, 0)
        _pad_price = New ProductPrices(0, 0, 0)
        _unit_price = New ProductPrices(0, 0, 0)
        _packing_size = 0
        _pads_per_pack = 0
        _product_id = 0
        _persisted = False

    End Sub

    Private Sub RecalculatePadAndUnitPrices() Handles PackPrice.RetailPriceChanged, PackPrice.WholesalePriceChanged, PackPrice.DistributionPriceChanged

        If IsNothing(PackPrice) Then
            _pad_price = Nothing
            _unit_price = Nothing
        Else
            _pad_price = New ProductPrices(RoundUp(PackPrice.Retail / _pads_per_pack, 2), RoundUp(PackPrice.Wholesale / _pads_per_pack, 2), RoundUp(PackPrice.Distribution / _pads_per_pack, 2))
            _unit_price = New ProductPrices(RoundUp(PackPrice.Retail / _packing_size, 2), RoundUp(PackPrice.Wholesale / _packing_size, 2), RoundUp(PackPrice.Distribution / _packing_size, 2))
        End If

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_products
        Dim result = sql.GetProductDetailByID(id)
        If Not IsNothing(result) Then
            _product_category = If(result("category_id") > 0, New Product_Category(result("category_id")), Nothing)
            _product_name = result("product_name_raw")
            _product_description = result("product_description")
            _product_code = result("product_code")
            _product_brand = result("product_brand")
            _product_unit = result("product_unit")
            _packing_size = Val(result("packing_size"))
            _pads_per_pack = Val(result("pads_per_pack"))
            PackPrice = New ProductPrices(result("retail_price"), result("wholesale_price"), result("distribution_price"))
            RecalculatePadAndUnitPrices
            _product_id = id
            _persisted = True
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_products
        If Not IsNothing(_product_category) Then
            _persisted = _product_category.Persist
        End If

        If _persisted Or IsNothing(_product_category) Then
            If _product_id = 0 Then
                _product_id = sql.CreateProduct(_product_name, _product_description, _product_code, _product_unit, _product_brand, PackPrice.Retail, PackPrice.Wholesale, PackPrice.Distribution, _packing_size, _pads_per_pack, If(IsNothing(_product_category), 0, _product_category.CategoryID))
                _persisted = _product_id > 0
            Else
                _persisted = sql.UpdateProductDetails(_product_id, _product_name, _product_description, _product_code, _product_unit, _product_brand, _packing_size, _pads_per_pack, If(IsNothing(_product_category), 0, _product_category.CategoryID)) And sql.UpdateProductPrices(_product_id, PackPrice.Retail, PackPrice.Wholesale, PackPrice.Distribution)
            End If
        End If

        Persist = _persisted

    End Function
End Class

Public Class ProductPrices

    Private _retail_price As Double
    Private _wholesale_price As Double
    Private _distribution_price As Double

    Public Event RetailPriceChanged()
    Public Event WholesalePriceChanged()
    Public Event DistributionPriceChanged()

    Public Sub New(ByVal retail_price As Double, ByVal wholesale_price As Double, ByVal distribution_price As Double)
        _retail_price = retail_price
        _wholesale_price = wholesale_price
        _distribution_price = distribution_price
    End Sub

    Public Property Retail As Double
        Get
            Retail = _retail_price
        End Get
        Set(ByVal value As Double)
            _retail_price = value
            RaiseEvent RetailPriceChanged()
        End Set
    End Property

    Public Property Wholesale As Double
        Get
            Wholesale = _wholesale_price
        End Get
        Set(ByVal value As Double)
            _wholesale_price = value
            RaiseEvent WholesalePriceChanged()
        End Set
    End Property

    Public Property Distribution As Double
        Get
            Distribution = _distribution_price
        End Get
        Set(ByVal value As Double)
            _distribution_price = value
            RaiseEvent DistributionPriceChanged()
        End Set
    End Property

End Class
