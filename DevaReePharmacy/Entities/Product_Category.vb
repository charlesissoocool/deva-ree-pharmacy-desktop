﻿Public Class Product_Category
    Inherits Entity

    <VBFixedString(20)> Private _category_name As String = ""
    Private _category_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property CategoryName As String
        Get
            CategoryName = _category_name
        End Get
        Set(ByVal value As String)
            _category_name = value
        End Set
    End Property

    Public Property CategoryID As Integer
        Get
            CategoryID = _category_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Protected Overrides Sub Clean()

        _category_name = ""
        _category_id = 0

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_products
        Dim result = sql.GetCategoryDetails(id)
        If Not IsNothing(result) Then
            _category_name = result("pc_category_name")
            _category_id = id
            _persisted = True
        End If

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_products
        If _category_id = 0 Then
            _category_id = sql.CreateNewCategory(_category_name)
            _persisted = _category_id > 0
        Else
            _persisted = sql.UpdateCategory(_category_id, _category_name)
        End If
        Persist = _persisted

    End Function
End Class
