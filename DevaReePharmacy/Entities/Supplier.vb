﻿Public Class Supplier
    Inherits Entity

    Private _supplier_name As String = ""
    Private _supplier_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property SupplierName As String
        Get
            SupplierName = _supplier_name
        End Get
        Set(ByVal value As String)
            If Not value = _supplier_name Then
                Dim sql As New class_db_stocks
                Dim test_id = sql.GetSupplierID(value)
                _supplier_id = test_id
                If test_id > 0 Then
                    Fetch(test_id)
                Else
                    _supplier_name = value
                End If
            End If
        End Set
    End Property

    Public Property SupplierID As Integer
        Get
            SupplierID = _supplier_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Protected Overrides Sub Clean()

        _supplier_name = ""
        _supplier_id = 0
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_stocks
        Dim supplier_details = sql.GetSupplierDetail(id)
        If Not IsNothing(supplier_details) Then
            _supplier_name = supplier_details("s_supplier_name")
            _supplier_id = id
            _persisted = True
        End If

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_stocks
        If _supplier_id = 0 Then
            _supplier_id = sql.AddSupplier(_supplier_name)
            _persisted = _supplier_id > 0
        Else
            _persisted = sql.UpdateSupplierDetail(_supplier_id, _supplier_name)
        End If
        Persist = _persisted

    End Function
End Class
