﻿Public Class Credit_Payment
    Inherits Entity

    Private _payment_amount As Double = 0
    Private _credit_id As Integer = 0
    Private _staff As User
    Private _payment_date As Date = Nothing
    Private _item_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property ItemID As Integer
        Get
            ItemID = _item_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property CreditID As Integer
        Get
            CreditID = _credit_id
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then Exit Property
            _persisted = False
            _credit_id = value
        End Set
    End Property

    Public Property Staff As User
        Get
            Staff = _staff
        End Get
        Set(ByVal value As User)
            If Not IsNothing(value) Then
                If Not value.Synced Then Exit Property
            End If
            _persisted = False
            _staff = value
        End Set
    End Property

    Public Property PaymentAmount As Double
        Get
            PaymentAmount = _payment_amount
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _payment_amount = value
        End Set
    End Property

    Public Property PaymentDate As Date
        Get
            PaymentDate = _payment_date
        End Get
        Set(ByVal value As Date)
            _payment_date = value
        End Set
    End Property

    Protected Overrides Sub Clean()

        _payment_amount = 0
        _credit_id = 0
        _staff = New User
        _payment_date = Nothing
        _item_id = 0
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim result = sql.GetCreditPaymentDetail(id)
        If Not IsNothing(result) Then
            _payment_amount = result("cp_payment_amount")
            _credit_id = result("cp_credit_id")
            _staff = New User(result("cp_staff_id"))
            _payment_date = Date.Parse(result("cp_payment_date"))
            _item_id = id
            _persisted = True
        End If
        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        If _item_id = 0 Then
            _item_id = sql.AddCreditPayment(_credit_id, _payment_amount, _staff.UserID)
            Dim result = sql.GetCreditPaymentDetail(_item_id)
            _payment_date = Date.Parse(result("cp_payment_date"))
            _persisted = _item_id > 0
        Else
            _persisted = sql.UpdateCreditPayment(_item_id, _payment_amount, _credit_id, _staff.UserID, _payment_date)
        End If

        Persist = _persisted

    End Function

End Class
