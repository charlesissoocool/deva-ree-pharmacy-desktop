﻿Public Class Town
    Inherits Entity

    <VBFixedString(25)> Private _town_name As String = ""
    Private _town_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property TownName As String
        Get
            TownName = _town_name
        End Get
        Set(ByVal value As String)
            _town_name = value
        End Set
    End Property

    Public Property TownID As Integer
        Get
            TownID = _town_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Protected Overrides Sub Clean()

        _town_name = ""
        _town_id = 0
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim result = sql.GetTownDetail(id)
        If Not IsNothing(result) Then
            _town_name = result("t_town_name")
            _town_id = id
            _persisted = True
        End If

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        If _town_id = 0 Then
            _town_id = sql.AddTown(_town_name)
            _persisted = _town_id > 0
        Else
            _persisted = sql.UpdateTown(_town_id, _town_name)
        End If
        Persist = _persisted

    End Function
End Class
