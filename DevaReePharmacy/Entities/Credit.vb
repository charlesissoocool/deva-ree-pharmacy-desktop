﻿Public Class Credit
    Inherits Entity

    Private _credit_amount As Double = 0
    Private _created_date As Date = Nothing
    Private _due_date As Date = Nothing
    Private _customer As Customer = Nothing
    Private _credit_payments As List(Of Credit_Payment)
    Private _credit_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public ReadOnly Property CreditPayments As List(Of Credit_Payment)
        Get
            CreditPayments = _credit_payments
        End Get
    End Property

    Public Property CreditAmount As Double
        Get
            CreditAmount = _credit_amount
        End Get
        Set(ByVal value As Double)
            _persisted = False
            _credit_amount = value
        End Set
    End Property

    Public Property CreatedDate As Date
        Get
            CreatedDate = _created_date
        End Get
        Set(ByVal value As Date)
            _persisted = False
            _created_date = value
        End Set
    End Property

    Public Property DueDate As Date
        Get
            DueDate = _due_date
        End Get
        Set(ByVal value As Date)
            _persisted = False
            _due_date = value
        End Set
    End Property

    Public ReadOnly Property Terms As String
        Get
            Terms = DateDiff(DateInterval.Day, _created_date, _due_date) & " day/s"
        End Get
    End Property

    Public Property Customer As Customer
        Get
            Customer = _customer
        End Get
        Set(ByVal value As Customer)
            _persisted = False
            _customer = value
        End Set
    End Property

    Public Property CreditID As Integer
        Get
            CreditID = _credit_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public ReadOnly Property TotalPaymentAmount() As Double
        Get
            TotalPaymentAmount = 0
            If Not IsNothing(_credit_payments) Then
                For Each _credit_payment In _credit_payments
                    TotalPaymentAmount += _credit_payment.PaymentAmount
                Next
            End If
        End Get
    End Property

    Protected Overrides Sub Clean()

        _credit_amount = 0
        _created_date = Nothing
        _due_date = Nothing
        _customer = Nothing
        _credit_id = 0
        _credit_payments = New List(Of Credit_Payment)
        _persisted = False

    End Sub

    Public Sub AddPaymentItem(ByVal _new_item As Credit_Payment)

        LoadPaymentItem(_new_item)
        _persisted = False

    End Sub

    Private Sub LoadPaymentItem(ByVal _new_item As Credit_Payment)

        If IsNothing(_credit_payments) Then _credit_payments = New List(Of Credit_Payment)
        _credit_payments.Add(_new_item)

    End Sub

    Public Sub RemovePaymentItem(ByVal item_id As Integer)

        For i = 0 To _credit_payments.Count - 1
            If _credit_payments(i).ItemID = item_id Then
                _credit_payments.RemoveAt(i)
                _persisted = False
                Exit For
            End If
        Next

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim result = sql.GetCreditDetails(id)
        If Not IsNothing(result) Then
            _credit_amount = result("cd_credit_amount")
            _created_date = Date.Parse(result("cd_created_date"))
            _due_date = Date.Parse(result("cd_due_date"))
            _customer = New Customer(result("cd_customer_id"))
            _credit_id = id
            Dim payments = sql.GetPaymentsForCredit(id)
            If Not IsNothing(payments) Then
                For Each payment In payments
                    _credit_payments.Add(New Credit_Payment(payment("cp_payment_id")))
                Next
            End If

            _persisted = True
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        If (_customer.CustomerID > 0 And Not _customer.Synced) Or Not _customer.Equals(New Customer) Then _persisted = _persisted And _customer.Persist
        If _credit_id = 0 Then
            _credit_id = sql.AddCredit(_credit_amount, _customer.CustomerID, _due_date)
            Dim result = sql.GetCreditDetails(_credit_id)
            _created_date = Date.Parse(result("cd_created_date"))
            _persisted = _credit_id > 0
        Else
            _persisted = sql.UpdateCredit(_credit_id, _credit_amount, _customer.CustomerID, _due_date, _created_date)
        End If

        If _persisted Then

            Dim items_in_db = sql.GetPaymentsForCredit(_credit_id)
            Dim to_add_items As New List(Of Integer)
            Dim in_db_items As New List(Of Integer)
            Dim in_obj_items As New List(Of Integer)
            If Not IsNothing(items_in_db) Then
                For Each dis_item In items_in_db
                    in_db_items.Add(dis_item("cp_payment_id"))
                Next
            End If
            If Not IsNothing(_credit_payments) Then
                If _credit_payments.Count > 0 Then
                    For Each payment_item In _credit_payments
                        in_obj_items.Add(payment_item.ItemID)
                    Next
                End If
            End If
            'Delete excess ids
            For Each item_id In in_db_items.Except(in_obj_items)
                _persisted = _persisted And sql.RemoveCreditPayment(item_id)
            Next
            'Add new ids
            If Not IsNothing(_credit_payments) then
                For Each payment_item In _credit_payments
                    payment_item.CreditID = _credit_id
                    If Not payment_item.Synced Then _persisted = _persisted And payment_item.Persist
                Next
            End If
        End If

        Persist = _persisted

    End Function
End Class
