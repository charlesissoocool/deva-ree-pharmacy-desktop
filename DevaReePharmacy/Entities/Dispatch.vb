﻿Public Class Dispatch
    Inherits Entity

    Private _dispatched_date As Date = Nothing
    Private _dispatch_type As OrderMode = OrderMode.Retail
    Private _dispatch_no As Long
    Private _staff As New User
    Private _agent As New User
    Private _stock_items As List(Of Dispatch_Item)
    Private _dispatch_transactions As List(Of Transaction)
    Private _dispatch_closed As Boolean = False
    Private _dispatch_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property DispatchID As Integer
        Get
            DispatchID = _dispatch_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property DispatchDate As Date
        Get
            DispatchDate = _dispatched_date
        End Get
        Set(ByVal value As Date)
            _dispatched_date = value
            _persisted = False
        End Set
    End Property

    Public Property DispatchType As OrderMode
        Get
            DispatchType = _dispatch_type
        End Get
        Set(ByVal value As OrderMode)
            _dispatch_type = value
            _persisted = False
        End Set
    End Property

    Public Property DispatchNo As Long
        Get
            DispatchNo = _dispatch_no
        End Get
        Set(ByVal value As Long)
            _dispatch_no = value
            _persisted = False
        End Set
    End Property

    Public Property Closed As Boolean
        Get
            Closed = _dispatch_closed
        End Get
        Set(ByVal value As Boolean)
            _dispatch_closed = value
            _persisted = False
        End Set
    End Property

    Public Property Agent As User
        Get
            Agent = _agent
        End Get
        Set(ByVal value As User)
            _persisted = False
            _agent = value
        End Set
    End Property

    Public Property Staff As User
        Get
            Staff = _staff
        End Get
        Set(ByVal value As User)
            If IsNothing(value) Then
                _staff = New User(module_session.current_user_profile.UserID)
            Else
                _staff = value
            End If
            _persisted = False
        End Set
    End Property

    Public Sub AddDispatchItem(ByVal _new_item As Dispatch_Item)

        LoadDispatchItem(_new_item)
        _persisted = False

    End Sub

    Private Sub LoadDispatchItem(ByVal _new_item As Dispatch_Item)

        If IsNothing(_stock_items) Then _stock_items = New List(Of Dispatch_Item)
        _stock_items.Add(_new_item)

    End Sub

    Public Sub RemoveDispatchItem(ByVal item_id As Integer)

        For i = 0 To _stock_items.Count - 1
            If _stock_items(i).ItemID = item_id Then
                _stock_items.RemoveAt(i)
                _persisted = False
                Exit For
            End If
        Next

    End Sub

    Public Sub RemoveAllItems()

        _stock_items = New List(Of Dispatch_Item)

    End Sub

    Public Sub AddTransactionItem(ByVal _new_item As Transaction)

        LoadTransactionItem(_new_item)
        _persisted = False

    End Sub

    Private Sub LoadTransactionItem(ByVal _new_item As Transaction)

        If IsNothing(_dispatch_transactions) Then _dispatch_transactions = New List(Of Transaction)
        _dispatch_transactions.Add(_new_item)

    End Sub

    Public Sub RemoveTransactionItem(ByVal item_id As Integer)

        For i = 0 To _dispatch_transactions.Count - 1
            If _dispatch_transactions(i).TransactionID = item_id Then
                _dispatch_transactions.RemoveAt(i)
                _persisted = False
                Exit For
            End If
        Next

    End Sub

    Public ReadOnly Property TransactionItems As List(Of Transaction)
        Get
            TransactionItems = _dispatch_transactions
        End Get
    End Property

    Protected Overrides Sub Clean()

        _dispatched_date = Nothing
        _dispatch_no = 0
        _staff = New User
        _agent = New User
        _stock_items = New List(Of Dispatch_Item)
        _dispatch_transactions = New List(Of Transaction)
        _dispatch_closed = False
        _dispatch_id = 0

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim result = sql.GetDispatchDetails(id)
        If Not IsNothing(result) Then
            _dispatch_no = Val(result("dd_dispatch_no"))
            _agent = New User(result("dd_agent_id"))
            _staff = New User(result("dd_staff_id"))
            _dispatch_closed = result("dd_closed") > 0
            _dispatched_date = Date.Parse(result("dd_dispatched_date"))
            _dispatch_id = id

            Dim results = sql.GetItemIDsFromDispatch(id)
            If Not IsNothing(results) Then
                For Each item_id In results
                    _stock_items.Add(New Dispatch_Item(item_id("di_item_id")))
                Next
            End If

            results = sql.GetDispatchTransactionIDs(id)
            If Not IsNothing(results) Then
                For Each transaction_id In results
                    _dispatch_transactions.Add(New Transaction(transaction_id("td_transaction_id")))
                Next
            End If
            _persisted = True
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        Dim temp_dispatch_type As String = If(_dispatch_type = OrderMode.Retail, TR_RETAIL, If(_dispatch_type = OrderMode.Wholesale, TR_WHOLESALE, TR_DISTRIBUTION))
        If temp_dispatch_type = "" Then
            _persisted = False
            Persist = False
            Exit Function
        End If

        If _dispatch_id = 0 Then
            _dispatch_id = sql.CreateNewDispatch(temp_dispatch_type, _dispatch_no, _agent.UserID, _staff.UserID, _dispatched_date)
            _persisted = _dispatch_id > 0
        Else
            _persisted = sql.UpdateDispatch(_dispatch_id, temp_dispatch_type, _dispatch_no, _agent.UserID, _staff.UserID, _dispatched_date)
        End If

        If _persisted Then

            Dim items_in_db = sql.GetItemIDsFromDispatch(_dispatch_id)
            Dim to_add_items As New List(Of Integer)
            Dim in_db_items As New List(Of Integer)
            Dim in_obj_items As New List(Of Integer)
            If Not IsNothing(items_in_db) Then
                For Each dis_item In items_in_db
                    in_db_items.Add(dis_item("di_item_id"))
                Next
            End If
            If Not IsNothing(_stock_items) Then
                For Each dis_item In _stock_items
                    in_obj_items.Add(dis_item.ItemID)
                Next
            End If
            'Delete excess ids
            For Each item_id In in_db_items.Except(in_obj_items)
                _persisted = _persisted And sql.RemoveDispatchItem(item_id)
            Next
            'Add new ids
            If Not IsNothing(_stock_items) Then
                For Each dis_item In _stock_items
                    dis_item.DispatchID = _dispatch_id
                    If Not dis_item.Synced Then _persisted = _persisted And dis_item.Persist
                Next
            End If

            Dim transaction_ids_db = sql.GetDispatchTransactionIDs(_dispatch_id)
            Dim to_add_transactions As New List(Of Integer)
            Dim in_db_transactions As New List(Of Integer)
            Dim in_obj_transactions As New List(Of Integer)
            If Not IsNothing(transaction_ids_db) Then
                For Each transaction_id In transaction_ids_db
                    in_db_transactions.Add(transaction_id("td_transaction_id"))
                Next
            End If
            If Not IsNothing(_dispatch_transactions) Then
                For Each transaction In _dispatch_transactions
                    in_obj_transactions.Add(transaction.TransactionID)
                Next
            End If

            'Delete excess ids
            For Each transaction_id In in_db_transactions.Except(in_obj_transactions)
                _persisted = _persisted And sql.RemoveTransactionFromDispatch(transaction_id)
            Next

            If Not IsNothing(_dispatch_transactions) Then
                For Each transaction_detail In _dispatch_transactions
                    'Add new ids
                    If in_obj_transactions.Except(in_db_transactions).Contains(transaction_detail.TransactionID) Then
                        transaction_detail.DispatchID = Me.DispatchID
                        _persisted = _persisted And transaction_detail.Persist
                    End If
                Next
            End If

            _persisted = _persisted And sql.SetDispatchClosedStatus(_dispatch_id, _dispatch_closed)
        End If

        Persist = _persisted

    End Function
End Class
