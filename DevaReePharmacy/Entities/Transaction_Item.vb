﻿Public Class Transaction_Item
    Inherits Entity

    Private _transaction_id As Integer = 0
    Private _stock As Stock = Nothing
    Private _product As New Product
    Private _item_quantity As Double = 0
    Private _display_unit As ItemUnit
    Private _return_entries As New List(Of Transaction_Item_Return)
    Private _selling_price As Double = 0
    Private _original_price As Double = 0
    Private _item_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property ItemID As Integer
        Get
            ItemID = _item_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public ReadOnly Property ReturnQuantity As Double
        Get
            ReturnQuantity = 0
            For Each return_item In _return_entries
                ReturnQuantity += return_item.ReturnQuantity
            Next
        End Get
    End Property

    Public Property TransactionID As Integer
        Get
            TransactionID = _transaction_id
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then Exit Property
            _persisted = False
            _transaction_id = value
        End Set
    End Property

    Public Property Stock As Stock
        Get
            Stock = _stock
        End Get
        Set(ByVal value As Stock)
            If Not IsNothing(value) Then
                If Not value.Synced Then Exit Property
            End If
            _persisted = False
            _stock = value
        End Set
    End Property

    Public Property Product As Product
        Get
            Product = _product
        End Get
        Set(ByVal value As Product)
            If IsNothing(value) Then Exit Property
            _persisted = False
            _product = value
        End Set
    End Property

    Public Property ItemQuantity As Double
        Get
            ItemQuantity = _item_quantity
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _item_quantity = value
        End Set
    End Property

    Public Property DisplayUnit As ItemUnit
        Get
            DisplayUnit = _display_unit
        End Get
        Set(ByVal value As ItemUnit)
            _display_unit = value
        End Set
    End Property

    Public Property SellingPrice As Double
        Get
            SellingPrice = _selling_price
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _selling_price = value
        End Set
    End Property

    Public Property OriginalPrice As Double
        Get
            OriginalPrice = _original_price
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _original_price = value
        End Set
    End Property

    Public ReadOnly Property ReturnEntries As List(Of Transaction_Item_Return)
        Get
            ReturnEntries = _return_entries
        End Get
    End Property

    Public Sub AddReturnItem(ByVal _new_item As Transaction_Item_Return)

        LoadReturnItem(_new_item)
        _persisted = False

    End Sub

    Private Sub LoadReturnItem(ByVal _new_item As Transaction_Item_Return)

        If IsNothing(_return_entries) Then _return_entries = New List(Of Transaction_Item_Return)
        _return_entries.Add(_new_item)

    End Sub

    Public Sub RemoveReturnItem(ByVal item_id As Integer)

        For i = 0 To _return_entries.Count - 1
            If _return_entries(i).ItemID = item_id Then
                _return_entries.RemoveAt(i)
                _persisted = False
                Exit For
            End If
        Next

    End Sub

    Protected Overrides Sub Clean()

        _transaction_id = 0
        _return_entries = New List(Of Transaction_Item_Return)
        _stock = Nothing
        _product = New Product
        _item_quantity = 0
        _selling_price = 0
        _item_id = 0
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim details = sql.GetTransactionItemDetail(id)
        If Not IsNothing(details) Then
            _item_id = id
            _transaction_id = details("transaction_id")
            _stock = If(Val(details("stock_id")) < 1, Nothing, New Stock(details("stock_id")))
            _product = New Product(details("product_id"))
            _item_quantity = details("item_quantity")
            _display_unit = If(details("display_unit") = "pack", ItemUnit.Pack, If(details("display_unit") = "pad", ItemUnit.Pad, ItemUnit.Unit))
            _selling_price = details("selling_price")
            _original_price = details("original_price")

            Dim results = sql.GetReturnEntriesForTransactionItem(id)
            If Not IsNothing(results) Then
                For Each result In results
                    _return_entries.Add(New Transaction_Item_Return(result("tir_return_id")))
                Next
            End If
            _persisted = If(IsNothing(_stock), True, _stock.Synced) And _product.Synced
        End If

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        _persisted = True
        If Not IsNothing(_stock) Then
            If Not _stock.Synced Then _persisted = _persisted And _stock.Persist
        End If

        If _persisted Then
            If _item_id = 0 Then
                _item_id = sql.AddItemToTransaction(_transaction_id, If(IsNothing(_stock), 0, _stock.StockID), _product.ProductID, _item_quantity, _display_unit, _selling_price, _original_price)
                _persisted = _item_id > 0
            Else
                _persisted = sql.UpdateItemInTransaction(_item_id, _transaction_id, If(IsNothing(_stock), 0, _stock.StockID), _product.ProductID, _item_quantity, _display_unit, _selling_price, _original_price)
            End If
        End If

        If _persisted Then
            Dim items_in_db = sql.GetReturnEntriesForTransactionItem(_item_id)
            Dim to_add_items As New List(Of Integer)
            Dim in_db_items As New List(Of Integer)
            Dim in_obj_items As New List(Of Integer)
            If Not IsNothing(items_in_db) Then
                For Each return_item In items_in_db
                    in_db_items.Add(return_item("tir_return_id"))
                Next
            End If
            If _return_entries.Count > 0 Then
                For Each return_item In _return_entries
                    in_obj_items.Add(return_item.ItemID)
                Next
            End If
            'Delete excess ids
            For Each return_item In in_db_items.Except(in_obj_items)
                _persisted = _persisted And sql.RemoveReturnEntry(return_item)
            Next
            'Add new ids
            For Each return_item In _return_entries
                return_item.TransactionItemID = _item_id
                If Not return_item.Synced Then _persisted = _persisted And return_item.Persist
            Next
        End If

        Persist = _persisted

    End Function

End Class
