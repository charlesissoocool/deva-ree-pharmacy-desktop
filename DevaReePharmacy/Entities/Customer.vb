﻿Public Class Customer
    Inherits Entity

    <VBFixedString(30)> Private _customer_name As String = ""
    Private _customer_address As String = ""
    Private _customer_town As Town = Nothing
    <VBFixedString(25)> Private _customer_contact As String = ""
    Private _date_registered As Date = Nothing
    Private _customer_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property CustomerID As Integer
        Get
            CustomerID = _customer_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property CustomerName As String
        Get
            CustomerName = _customer_name
        End Get
        Set(ByVal value As String)
            _customer_name = value
        End Set
    End Property

    Public Property Address As String
        Get
            Address = _customer_address
        End Get
        Set(ByVal value As String)
            _customer_address = value
        End Set
    End Property

    Public Property Town As Town
        Get
            Town = _customer_town
        End Get
        Set(ByVal value As Town)
            _customer_town = value
        End Set
    End Property

    Public Property Contact As String
        Get
            Contact = _customer_contact
        End Get
        Set(ByVal value As String)
            _customer_contact = value
        End Set
    End Property

    Public Property DateRegistered As Date
        Get
            DateRegistered = _date_registered
        End Get
        Set(ByVal value As Date)
            _date_registered = value
        End Set
    End Property

    Protected Overrides Sub Clean()

        _customer_name = ""
        _customer_address = ""
        _customer_town = Nothing
        _customer_contact = ""
        _date_registered = Nothing
        _customer_id = 0

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Dim sql As New class_db_transaction
        Clean()
        Dim result = sql.GetCustomerDetail(id)
        If Not IsNothing(result) Then
            _customer_name = CStr(result("c_name")).Replace("<br>", vbCrLf)
            _customer_address = result("c_address")
            _customer_contact = result("c_contact")
            _customer_town = New Town(Val(result("c_town_id")))
            _date_registered = Date.Parse(result("c_date_registered"))
            _customer_id = id
            _persisted = True
        End If

    End Sub

    Public Overrides Function Persist() As Boolean

        _customer_address.Replace(vbCrLf, "<br>")

        Dim sql As New class_db_transaction
        If _customer_id = 0 Then
            _customer_id = sql.AddCustomer(_customer_name, _customer_address, _customer_town.TownID, _customer_contact)
            _persisted = _customer_id > 0
        Else
            _persisted = sql.UpdateCustomer(_customer_id, _customer_name, _customer_address, _customer_town.TownID, _customer_contact)
        End If
        Persist = _persisted

    End Function
End Class
