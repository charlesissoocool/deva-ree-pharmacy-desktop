﻿Public Class Dispatch_Item
    Inherits Entity

    Private _dispatch_id As Integer = 0
    Private _stock As Stock = Nothing
    Private _item_quantity As Double = 0
    Private _display_unit As ItemUnit
    Private _sold_quantity As Double = 0
    Private _dispatch_price As Double = 0
    Private _item_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property ItemID As Integer
        Get
            ItemID = _item_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property DispatchID As Integer
        Get
            DispatchID = _dispatch_id
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then Exit Property
            _persisted = False
            _dispatch_id = value
        End Set
    End Property

    Public Property Stock As Stock
        Get
            Stock = _stock
        End Get
        Set(ByVal value As Stock)
            _persisted = False
            _stock = value
        End Set
    End Property

    Public Property ItemQuantity As Double
        Get
            ItemQuantity = _item_quantity
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _item_quantity = value
        End Set
    End Property

    Public Property DisplayUnit As ItemUnit
        Get
            DisplayUnit = _display_unit
        End Get
        Set(ByVal value As ItemUnit)
            _display_unit = value
        End Set
    End Property

    Public ReadOnly Property SoldQuantity As Double
        Get
            SoldQuantity = _sold_quantity
        End Get
    End Property

    Public Property DispatchPrice As Double
        Get
            DispatchPrice = _dispatch_price
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _dispatch_price = value
        End Set
    End Property

    Protected Overrides Sub Clean()

        _dispatch_id = 0
        _stock = Nothing
        _item_quantity = 0
        _sold_quantity = 0
        _dispatch_price = 0
        _item_id = 0
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim details = sql.GetDispatchItemDetail(id)
        If Not IsNothing(details) Then
            _stock = New Stock(details("di_stock_id"))
            _item_quantity = details("di_stock_quantity")
            _sold_quantity = details("qs_sold_quantity")
            _dispatch_id = details("di_dispatch_id")
            _item_id = id
            _persisted = True
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        If _item_id = 0 Then
            _item_id = sql.AddItemToDispatch(_dispatch_id, _item_quantity, _display_unit, _dispatch_price, If(IsNothing(_stock), 0, _stock.StockID))
            _persisted = _item_id > 0
        Else
            _persisted = sql.UpdateItemInDispatch(_item_id, _dispatch_id, _item_quantity, _display_unit, _dispatch_price, If(IsNothing(_stock), 0, _stock.StockID))
        End If

        Persist = _persisted

    End Function

End Class
