﻿Public Class Manifest
    Inherits Entity

    Private _manifest_id As Integer = 0
    Private _manifest_code As String = ""
    Private _staff As New User
    Private _supplier As New Supplier
    Private _entry_date As Date = Nothing
    Private _updated_date As Date = Nothing
    Private _stocks As List(Of Stock) = Nothing

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property ManifestID As Integer
        Get
            ManifestID = _manifest_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property ManifestCode As String
        Get
            ManifestCode = _manifest_code
        End Get
        Set(ByVal value As String)
            _manifest_code = value
        End Set
    End Property

    Public Property Staff As User
        Get
            Staff = _staff
        End Get
        Set(ByVal value As User)
            If IsNothing(value) Then
                _staff = New User(module_session.current_user_profile.UserID)
            Else
                _staff = value
            End If
            _persisted = False
        End Set
    End Property

    Public Property Supplier As Supplier
        Get
            Supplier = _supplier
        End Get
        Set(ByVal value As Supplier)
            _supplier = value
            _persisted = False
        End Set
    End Property

    Public Property EntryDate As Date
        Get
            EntryDate = _entry_date
        End Get
        Set(ByVal value As Date)
            _entry_date = value
            _persisted = False
        End Set
    End Property

    Public ReadOnly Property UpdatedDate As Date
        Get
            UpdatedDate = _updated_date
        End Get
    End Property

    Public ReadOnly Property Stocks As List(Of Stock)
        Get
            Stocks = _stocks
        End Get
    End Property

    Public Sub AddStock(ByVal _new_stock As Stock)

        LoadStock(_new_stock)
        _persisted = False

    End Sub

    Public Sub RemoveStock(ByVal stock_id As Integer)

        For i = 0 To _stocks.Count - 1
            If _stocks(i).StockID = stock_id Then
                _stocks.RemoveAt(i)
                _persisted = False
                If _stocks.Count = 0 Then _stocks = Nothing
                Exit For
            End If
        Next

    End Sub

    Public Sub RemoveAllStock()

        _stocks = Nothing

    End Sub

    Private Sub LoadStock(ByVal _new_stock As Stock)

        If IsNothing(_stocks) Then _stocks = New List(Of Stock)
        _stocks.Add(_new_stock)

    End Sub

    Protected Overrides Sub Clean()

        _manifest_id = 0
        _staff = New User
        _supplier = New Supplier
        _entry_date = Nothing
        _updated_date = Nothing
        _stocks = New List(Of Stock)

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_stocks
        Dim result = sql.GetManifestDetails(id)
        If Not IsNothing(result) Then
            _manifest_id = id
            _staff = New User(result("md_staff_id"))
            _supplier = New Supplier(result("md_supplier_id"))
            _persisted = _staff.UserID = result("md_staff_id") And _supplier.SupplierID = result("md_supplier_id")
            _entry_date = Date.Parse(result("md_entry_date"))
            _updated_date = result("md_updated_date")
            _manifest_code = result("md_manifest_code")
            Dim stocks = sql.GetManifestStocks(_manifest_id)
            For Each stock_detail In stocks
                Dim _stock As New Stock(stock_detail("ps_stock_id"))
                LoadStock(_stock)
                _persisted = (_stock.StockID = stock_detail("ps_stock_id")) And _persisted
            Next
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_stocks
        Dim new_manifest As Boolean = False

        _persisted = _supplier.Persist And _persisted

        If _manifest_id = 0 Then
            _manifest_id = sql.AddManifest(_staff.UserID, _manifest_code, _supplier.SupplierID, _entry_date)
            _persisted = _manifest_id > 0
            new_manifest = True
        Else
            _persisted = sql.UpdateManifest(_manifest_id, _staff.UserID, _manifest_code, _supplier.SupplierID) > 0
            _updated_date = sql.GetManifestUpdatedDate(_manifest_id)
        End If

        If IsNothing(_stocks) Then
            _persisted = RemoveAllStocksInDB()
        ElseIf _stocks.Count < 1 Then
            _persisted = RemoveAllStocksInDB()
        Else
            Dim stock_ids_db As New List(Of Integer)
            Dim stock_list = sql.GetManifestStocks(_manifest_id)
            If Not IsNothing(stock_list) Then
                For Each item In stock_list
                    stock_ids_db.Add(item("ps_stock_id"))
                Next
            End If
            For Each item As Stock In _stocks
                item.ManifestID = _manifest_id
                If item.StockID > 0 And stock_ids_db.Contains(item.StockID) Then
                    _persisted = item.Persist And _persisted
                    stock_ids_db.Remove(item.StockID)
                Else
                    If item.StockID <> 0 Then
                        _persisted = _persisted And sql.RemoveStock(item.StockID)
                    Else
                        _persisted = _persisted And item.Persist
                    End If
                End If
            Next
            For Each item In stock_ids_db
                _persisted = _persisted And sql.RemoveStock(item)
            Next
        End If
        Persist = _persisted

    End Function

    Private Function RemoveAllStocksInDB() As Boolean

        RemoveAllStocksInDB = True
        Dim sql As New class_db_stocks
        Dim stock_list = sql.GetManifestStocks(_manifest_id)
        If Not IsNothing(stock_list) Then
            For Each item In stock_list
                RemoveAllStocksInDB = RemoveAllStocksInDB And sql.RemoveStock(item("ps_stock_id"))
            Next
        End If

    End Function

End Class
