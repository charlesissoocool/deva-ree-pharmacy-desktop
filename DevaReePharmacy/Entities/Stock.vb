﻿Public Class Stock
    Inherits Entity

    Private _manifest_id As Integer = 0
    Private _stock_id As Integer = 0
    Private _product As Product = Nothing
    Private _supplier_price As Double = 0
    Private _stock_quantity As Double = 0
    <VBFixedString(50)> Private _lot_number As String = ""
    Private _expiry_date As Date = Nothing
    Private _status As String = ""
    Private _remaining_quantity As Double = 0
    Private _is_valid As Boolean = False
    Private _is_expired As Boolean = False

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property StockID As Integer
        Get
            StockID = _stock_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property Product As Product
        Get
            Product = _product
        End Get
        Set(ByVal value As Product)
            If value Is Nothing Then Exit Property
            _persisted = False
            _product = value
        End Set
    End Property

    Public Property SupplierPrice As Double
        Get
            SupplierPrice = _supplier_price
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _supplier_price = value
        End Set
    End Property

    Public Property StockQuantity As Double
        Get
            StockQuantity = _stock_quantity
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _stock_quantity = value
        End Set
    End Property

    Public ReadOnly Property RemainingQuantity As Double
        Get
            RemainingQuantity = _remaining_quantity
        End Get
    End Property


    Public Property LotNumber As String
        Get
            LotNumber = _lot_number
        End Get
        Set(ByVal value As String)
            If Trim(value) = "" Then Exit Property
            _persisted = False
            _lot_number = value
        End Set
    End Property

    Public Property ExpiryDate As Date
        Get
            ExpiryDate = _expiry_date
        End Get
        Set(ByVal value As Date)
            _persisted = False
            _expiry_date = value
        End Set
    End Property

    Public ReadOnly Property IsExpired As Boolean
        Get
            IsExpired = _is_expired
        End Get
    End Property

    Public ReadOnly Property Status As String
        Get
            Status = _status
        End Get
    End Property

    Public Property Valid As Boolean
        Get
            Valid = _is_valid
        End Get
        Set(ByVal value As Boolean)
            _persisted = False
            _is_valid = value
        End Set
    End Property

    Public Property ManifestID As Integer
        Get
            ManifestID = _manifest_id
        End Get
        Set(ByVal value As Integer)
            _persisted = False
            _manifest_id = value
        End Set

    End Property

    Protected Overrides Sub Clean()

        _manifest_id = 0
        _stock_id = 0
        _product = Nothing
        _supplier_price = 0
        _stock_quantity = 0
        _remaining_quantity = 0
        _lot_number = ""
        _expiry_date = Nothing
        _is_valid = False
        _is_expired = False
        _status = ""
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_stocks
        Dim stock = sql.GetStockDetail(id)
        If Not IsNothing(stock) Then
            _manifest_id = stock("manifest_id")
            _stock_id = id
            _product = New Product(stock("product_id"))
            _supplier_price = stock("supplier_price")
            _stock_quantity = stock("stock_quantity")
            _remaining_quantity = stock("quantity_available")
            _lot_number = stock("lot_number")
            _expiry_date = Date.Parse(stock("expiry_date"))
            _is_expired = CBool(stock("expired"))
            _status = stock("status")
            _is_valid = If(stock("status") = "Invalid", False, True)
            _persisted = _product.ProductID = stock("product_id")
        End If

        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_stocks
        If _stock_id = 0 Then
            _stock_id = sql.AddStock(_manifest_id, _product.ProductID, _stock_quantity, _supplier_price, _expiry_date, _lot_number)
            _persisted = _stock_id > 0
            If _persisted Then _remaining_quantity = _stock_quantity
        Else
            _persisted = sql.UpdateStock(_manifest_id, _stock_id, _product.ProductID, _stock_quantity, _supplier_price, _expiry_date, _lot_number) > 0
        End If
        If _persisted Then Fetch(_stock_id)
        Persist = _persisted

    End Function

End Class
