﻿Public Class Transaction_Item_Return
    Inherits Entity

    Private _transaction_item_id As Integer = 0
    Private _return_quantity As Double = 0
    Private _return_date As Date = Nothing
    Private _staff As New User
    Private _item_id As Integer = 0

    Public Sub New(Optional ByVal id As Integer = 0)

        If id > 0 Then
            Fetch(id)
        End If

    End Sub

    Public Property ItemID As Integer
        Get
            ItemID = _item_id
        End Get
        Set(ByVal value As Integer)
            Fetch(value)
        End Set
    End Property

    Public Property TransactionItemID As Integer
        Get
            TransactionItemID = _transaction_item_id
        End Get
        Set(ByVal value As Integer)
            If value < 1 Then Exit Property
            _persisted = False
            _transaction_item_id = value
        End Set
    End Property

    Public Property Staff As User
        Get
            Staff = _staff
        End Get
        Set(ByVal value As User)
            If IsNothing(value) Then Exit Property
            _persisted = False
            _staff = value
        End Set
    End Property

    Public Property ReturnQuantity As Double
        Get
            ReturnQuantity = _return_quantity
        End Get
        Set(ByVal value As Double)
            If value < 0 Then Exit Property
            _persisted = False
            _return_quantity = value
        End Set
    End Property

    Public Property ReturnDate As Date
        Get
            ReturnDate = _return_date
        End Get
        Set(ByVal value As Date)
            _persisted = False
            _return_date = value
        End Set
    End Property

    Protected Overrides Sub Clean()

        _transaction_item_id = 0
        _return_quantity = 0
        _return_date = Nothing
        _staff = New User
        _persisted = False

    End Sub

    Protected Overrides Sub Fetch(ByVal id As Integer)

        Clean()
        Dim sql As New class_db_transaction
        Dim result = sql.GetReturnEntryDetail(id)
        If Not IsNothing(result) Then
            _transaction_item_id = result("tir_item_id")
            _return_quantity = result("tir_return_quantity")
            _return_date = Date.Parse(result("tir_return_date"))
            _staff = New User(result("tir_staff_id"))
            _item_id = id
            _persisted = True
        End If
        If Not _persisted Then Clean()

    End Sub

    Public Overrides Function Persist() As Boolean

        Dim sql As New class_db_transaction
        _persisted = True

        If _item_id = 0 Then
            _item_id = sql.AddReturnEntry(_transaction_item_id, _return_quantity, _staff.UserID)
            Dim result = sql.GetReturnEntryDetail(_item_id)
            _return_date = Date.Parse(result("tir_return_date"))
            _persisted = _item_id > 0
        Else
            _persisted = sql.UpdateReturnEntry(_item_id, _transaction_item_id, _return_quantity, _staff.UserID, _return_date)
        End If

        Persist = _persisted

    End Function

End Class
