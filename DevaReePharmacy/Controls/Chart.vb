﻿Imports System.Drawing

Public Class Chart

    Private _min_y = 99999999, _max_y As Double = -99999999
    Private _x_coor As Double() = Nothing
    Private _y_coor As Dictionary(Of String, Double()) = Nothing
    Private _mouse_events As Dictionary(Of Integer, Dictionary(Of Integer, String)) = Nothing
    Private _values As Dictionary(Of String, Double()) = Nothing
    Private _x_labels As String() = Nothing
    Private _colors As Dictionary(Of String, Color) = Nothing
    Private _chart_title As String = "Chart"
    Private _tooltip As New ToolTip
    Private _value_pattern As String = "%value%"

    'Colors
    Private _default_colors As List(Of Color)

    'Theme
    Private _theme_grid_pen As New Pen(New SolidBrush(ChartLineColors.Grey), 1)
    Private _theme_bg_brush As New SolidBrush(ChartLineColors.DarkGrey)
    Private _theme_txt_brush As New SolidBrush(Color.White)
    Private _theme_points_brush As New SolidBrush(Color.White)
    Private _theme_points_font As New Font("Arial", 7)
    Private _theme_text_font As New Font("Arial", 9)
    Private _theme_coordinates_font As New Font("Arial", 9)
    Private _theme_coordinates_spacing As Single = 15
    Private _theme_coordinates_right_padding As Single = 20
    Private _theme_points_font_brush As New SolidBrush(Color.White)
    Private _theme_text_font_brush As New SolidBrush(Color.White)
    Private _theme_coordinates_font_brush As New SolidBrush(Color.White)
    Private _theme_zero_axis_pen As New Pen(New SolidBrush(ColorTranslator.FromHtml("#ffb1b0")), 2)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        _theme_grid_pen.DashPattern = {2, 2}
        _theme_zero_axis_pen.DashPattern = {2, 2}
        _default_colors = New List(Of Color)
        _default_colors.Add(ChartLineColors.BrightYellow)
        _default_colors.Add(ChartLineColors.Red)
        _default_colors.Add(ChartLineColors.Violet)
        _default_colors.Add(ChartLineColors.LightOrange)
        _default_colors.Add(ChartLineColors.DarkBlue)
        _default_colors.Add(ChartLineColors.Yellow)
        _default_colors.Add(ChartLineColors.Green)
        _default_colors.Add(ChartLineColors.Teal)
        _default_colors.Add(ChartLineColors.YellowGreen)
        _default_colors.Add(ChartLineColors.Blue)
        _default_colors.Add(ChartLineColors.Orange)
        _default_colors.Add(ChartLineColors.DarkPink)
        DrawText("No data.")

    End Sub

    Private Function PrepareChartArea() As Bitmap

        picture_draw_area.Refresh()
        picture_draw_area.Invalidate()
        Dim bmp As Bitmap
        If picture_draw_area.Height <= 0 Or picture_draw_area.Width <= 0 Then
            bmp = New Bitmap(1, 1)
        Else
            bmp = New Bitmap(picture_draw_area.Width, picture_draw_area.Height)
        End If

        Dim grp As Graphics = Graphics.FromImage(bmp)
        grp.Clear(Color.White)

        grp.FillRectangle(_theme_bg_brush, New Rectangle(New Point(0, 0), New Size(bmp.Width, bmp.Height)))
        PrepareChartArea = bmp

    End Function

    Private Sub DrawText(Optional ByVal msg As String = "No data")

        Dim bmp = PrepareChartArea()
        Dim grp As Graphics = Graphics.FromImage(bmp)
        Dim text_size = TextRenderer.MeasureText(msg, _theme_text_font)
        grp.TextRenderingHint = Drawing.Text.TextRenderingHint.AntiAliasGridFit
        grp.DrawString(msg, _theme_text_font, _theme_text_font_brush, (bmp.Width / 2) - (text_size.Width / 2), (bmp.Height / 2) - (text_size.Height / 2))
        picture_draw_area.Image = bmp

    End Sub

    Public Sub Clean()

        picture_draw_area.Image = Nothing

    End Sub

    Private Sub DrawChart()

        If IsNothing(_x_coor) Or IsNothing(_y_coor) Or IsNothing(_x_labels) Then
            DrawText("No data.")
        Else
            Dim prev_pnt As Point = Nothing
            Dim new_pnt As Point = Nothing
            Dim bmp = PrepareChartArea()
            Dim grp As Graphics = Graphics.FromImage(bmp)
            grp.TextRenderingHint = Drawing.Text.TextRenderingHint.AntiAliasGridFit
            Dim text_coordinate_size = TextRenderer.MeasureText("0,000,000.00", _theme_coordinates_font)
            Dim total_coordinates_display = Math.Floor((bmp.Height - _theme_coordinates_spacing) / (text_coordinate_size.Height + _theme_coordinates_spacing))
            Dim pen As New Pen(Color.Black, 1)

            _mouse_events = New Dictionary(Of Integer, Dictionary(Of Integer, String))

            If _min_y <= 0 Then 'Draw 0 line
                Dim y_plot As Double = bmp.Height * (_max_y / (_max_y - _min_y))
                grp.DrawLine(_theme_zero_axis_pen, New Point(0, y_plot), New Point(bmp.Width, y_plot))
            End If

            For draw_mode = 0 To 2
                For Each values As KeyValuePair(Of String, Double()) In _y_coor
                    pen.Color = _colors(values.Key)
                    prev_pnt = Nothing
                    For i = 0 To _x_coor.Count - 1
                        new_pnt = New Point(text_coordinate_size.Width + (((bmp.Width - text_coordinate_size.Width) - _theme_coordinates_right_padding) * _x_coor(i)), bmp.Height - (bmp.Height * values.Value(i)))
                        If _mouse_events.ContainsKey(new_pnt.X) Then
                            If Not _mouse_events(new_pnt.X).ContainsKey(new_pnt.Y) Then
                                _mouse_events(new_pnt.X)(new_pnt.Y) = values.Key & " @ " & _x_labels(i) & ": " & _value_pattern.Replace("%value%", FormatNumber(_values(values.Key)(i), 2, , , TriState.True))
                            End If
                        Else
                            _mouse_events(new_pnt.X) = New Dictionary(Of Integer, String)
                            _mouse_events(new_pnt.X)(new_pnt.Y) = values.Key & " @ " & _x_labels(i) & ": " & _value_pattern.Replace("%value%", FormatNumber(_values(values.Key)(i), 2, , , TriState.True))
                        End If
                        If i = 0 Then prev_pnt = new_pnt
                        If draw_mode = 0 Then 'Grid lines
                            grp.DrawLine(_theme_grid_pen, New Point(new_pnt.X, 0), New Point(new_pnt.X, bmp.Height))
                            grp.DrawString(_x_labels(i), _theme_coordinates_font, _theme_coordinates_font_brush, New Point(new_pnt.X - 5 - TextRenderer.MeasureText(_x_labels(i), _theme_coordinates_font).Width, bmp.Height - TextRenderer.MeasureText(_x_labels(i), _theme_coordinates_font).Height - 5))
                            For display = 0 To total_coordinates_display - 1
                                Dim grid_y As Double = _theme_coordinates_spacing + ((display / total_coordinates_display) * (bmp.Height - _theme_coordinates_spacing))
                                grp.DrawLine(_theme_grid_pen, New Point(0, grid_y), New Point(bmp.Width, grid_y))
                                grp.DrawString(_value_pattern.Replace("%value%", FormatNumber(_max_y - ((_max_y - _min_y) * (grid_y / bmp.Height)), 2, , , TriState.True)), _theme_coordinates_font, _theme_coordinates_font_brush, New Point(5, grid_y + 5))
                            Next
                        ElseIf draw_mode = 1 Then 'Value line
                            grp.DrawLine(pen, prev_pnt, new_pnt)
                        ElseIf draw_mode = 2 Then
                            grp.FillEllipse(_theme_points_brush, New Rectangle(new_pnt.X - 6, new_pnt.Y - 6, 12, 12))
                            grp.FillEllipse(New SolidBrush(_colors(values.Key)), New Rectangle(new_pnt.X - 4, new_pnt.Y - 4, 8, 8))
                        Else 'Value points
                            'grp.DrawString(SYS_CURRENCY & FormatNumber(_values(values.Key)(i), 2, , , TriState.True), _theme_points_font, _theme_points_font_brush, new_pnt)
                        End If
                        prev_pnt = new_pnt
                    Next
                Next
            Next
            grp.DrawString(_chart_title, _theme_text_font, _theme_text_font_brush, New Point((bmp.Width / 2) - (TextRenderer.MeasureText(_chart_title, _theme_text_font).Width / 2), 5))
            picture_draw_area.Image = bmp
        End If

    End Sub

    Public Sub RemoveAllChartData()

        _min_y = 99999999
        _max_y = -99999999
        _y_coor = Nothing
        _colors = Nothing
        _values = Nothing
        DrawText("No data.")

    End Sub

    Public Sub RemoveChartData(ByVal data_label As String)

        If Not IsNothing(_y_coor) Then
            If _y_coor.ContainsKey(data_label) Then
                _y_coor.Remove(data_label)
                _colors.Remove(data_label)
                _values.Remove(data_label)
                If _y_coor.Count < 1 Then
                    DrawText("No data.")
                Else
                    DrawChart()
                End If
            End If
        End If

    End Sub

    Public Sub SetTitle(ByVal title As String)

        _chart_title = title

    End Sub

    Public Sub SetValuePattern(ByVal pattern As String)

        _value_pattern = pattern

    End Sub

    Public Sub SetLabels(ByVal labels() As String)

        If Not IsNothing(labels) Then
            _x_labels = labels
            ReDim _x_coor(_x_labels.Count - 1)
            For i = 0 To _x_coor.Count - 1
                _x_coor(i) = i / (_x_coor.Count - 1)
            Next
        End If

    End Sub

    Public Sub AddChartData(ByVal values() As Double, Optional ByVal color As Color = Nothing, Optional ByVal data_label As String = Nothing, Optional ByVal draw As Boolean = True)

        If IsNothing(_x_labels) Then
            DrawText("Values cannot be added prior to labels.")
        Else
            If Not values.Count = _x_labels.Count Then
                DrawText("Invalid data.")
            Else
                If IsNothing(_y_coor) Then
                    _y_coor = New Dictionary(Of String, Double())
                End If
                If IsNothing(_colors) Then _colors = New Dictionary(Of String, Color)
                If IsNothing(_values) Then _values = New Dictionary(Of String, Double())
                If IsNothing(data_label) Then data_label = "Values " & (_y_coor.Count + 1)
                If _y_coor.ContainsKey(data_label) Then data_label = "Values " & (_y_coor.Count + 1)
                If color.A = 0 And color.B = 0 And color.R = 0 And color.G = 0 Then
                    For Each def_color As Color In _default_colors
                        If Not _colors.ContainsValue(def_color) Then color = def_color : Exit For
                    Next
                    If color.A = 0 And color.B = 0 And color.R = 0 And color.G = 0 Then color = _default_colors.ElementAt((_y_coor.Count + 1) Mod (_default_colors.Count - 1))
                End If

                Dim _old_min = _min_y
                Dim _old_max = _max_y

                For i = 0 To values.Count - 1
                    _max_y = If(values(i) > _max_y, values(i), _max_y)
                    _min_y = If(values(i) < _min_y, values(i), _min_y)
                Next

                If _min_y - ((_max_y - _min_y) * 0.2) < _old_min Or _max_y + ((_max_y - _min_y) * 0.2) > _old_max Then
                    _max_y += ((_max_y - _min_y) * 0.2)
                    If _min_y >= 0 Then _min_y = 0 Else _min_y -= ((_max_y - _min_y) * 0.2)
                End If

                If _max_y = _min_y Then _max_y = _min_y + 5000
                _values.Add(data_label, values.Clone())
                _colors.Add(data_label, color)
                _y_coor.Add(data_label, values)
                RecalculateCoordinates()
                If draw Then DrawChart()
            End If
        End If

    End Sub

    Private Sub RecalculateCoordinates()

        For Each key_values As KeyValuePair(Of String, Double()) In _values
            For i = 0 To key_values.Value.Count - 1
                _y_coor(key_values.Key)(i) = (key_values.Value(i) - _min_y) / (_max_y - _min_y)
            Next
        Next

    End Sub

    Private Sub Chart_ParentChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ParentChanged

        AddHandler Me.ParentForm.ResizeEnd, AddressOf Redraw
        AddHandler Me.ParentForm.SizeChanged, AddressOf Redraw

    End Sub

    Private Sub Chart_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        picture_draw_area.Left = 0
        picture_draw_area.Top = 0
        panel_legend.Left = 0
        panel_legend.Top = Me.ClientSize.Height - panel_legend.Height
        picture_draw_area.Height = panel_legend.Top
        picture_draw_area.Width = Me.ClientSize.Width
        panel_legend.Width = picture_draw_area.Width
        'DrawChart()

    End Sub

    Public Sub Redraw()

        DrawChart()

    End Sub

    Private Sub picture_draw_area_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picture_draw_area.MouseDown

        If Not IsNothing(_mouse_events) Then
            For x = e.X - 5 To e.X + 5
                If _mouse_events.ContainsKey(x) Then
                    For y = e.Y - 5 To e.Y + 5
                        If _mouse_events(x).ContainsKey(y) Then
                            _tooltip.Show(_mouse_events(x)(y), picture_draw_area)
                            Exit Sub
                        End If
                    Next
                End If
            Next
        End If

    End Sub

    Private Sub picture_draw_area_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picture_draw_area.MouseMove

        If Not IsNothing(_mouse_events) Then
            For x = e.X - 5 To e.X + 5
                If _mouse_events.ContainsKey(x) Then
                    For y = e.Y - 5 To e.Y + 5
                        If _mouse_events(x).ContainsKey(y) Then
                            Me.Cursor = Cursors.Hand
                            Exit Sub
                        End If
                    Next
                End If
            Next
            Me.Cursor = Cursors.Default
        End If

    End Sub

    Private Sub picture_draw_area_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picture_draw_area.Click

    End Sub
End Class

Module ChartLineColors

    Public ReadOnly Property DarkGrey As Color
        Get
            DarkGrey = ColorTranslator.FromHtml("#535364")
        End Get
    End Property

    Public ReadOnly Property Grey As Color
        Get
            Grey = ColorTranslator.FromHtml("#636372")
        End Get
    End Property

    Public ReadOnly Property Red As Color
        Get
            Red = ColorTranslator.FromHtml("#b81427")
        End Get
    End Property

    Public ReadOnly Property Orange As Color
        Get
            Orange = ColorTranslator.FromHtml("#ff9108")
        End Get
    End Property

    Public ReadOnly Property LightOrange As Color
        Get
            LightOrange = ColorTranslator.FromHtml("#fea832")
        End Get
    End Property

    Public ReadOnly Property Yellow As Color
        Get
            Yellow = ColorTranslator.FromHtml("#fcd538")
        End Get
    End Property

    Public ReadOnly Property BrightYellow As Color
        Get
            BrightYellow = ColorTranslator.FromHtml("#fff054")
        End Get
    End Property

    Public ReadOnly Property YellowGreen As Color
        Get
            YellowGreen = ColorTranslator.FromHtml("#c3e2a7")
        End Get
    End Property

    Public ReadOnly Property Green As Color
        Get
            Green = ColorTranslator.FromHtml("#118d4f")
        End Get
    End Property

    Public ReadOnly Property Teal As Color
        Get
            Teal = ColorTranslator.FromHtml("#2e9bd2")
        End Get
    End Property

    Public ReadOnly Property Blue As Color
        Get
            Blue = ColorTranslator.FromHtml("#6ebed7")
        End Get
    End Property

    Public ReadOnly Property DarkBlue As Color
        Get
            DarkBlue = ColorTranslator.FromHtml("#4030d3")
        End Get
    End Property

    Public ReadOnly Property Violet As Color
        Get
            Violet = ColorTranslator.FromHtml("#9431d2")
        End Get
    End Property

    Public ReadOnly Property DarkPink As Color
        Get
            DarkPink = ColorTranslator.FromHtml("#d02b82")
        End Get
    End Property

End Module