﻿Public Class PageNavigator

    Dim _page_count As Integer = 0
    Dim _current_page As Integer = 0
    Dim _total_items As Integer = 0
    Dim _page_size As Integer = SYS_PAGE_SIZE
    Dim _loopable As Boolean
    Dim _offset As Integer
    Dim _allow_left As Boolean = False
    Dim _allow_right As Boolean = False
    Dim _list_view As Control

    Public Event PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        _loopable = True

    End Sub

    Public Sub RefreshPage()

        If _current_page < 1 Then Exit Sub
        SetCurrentPage(_current_page)

    End Sub

    Public Property ListView As Control
        Get
            ListView = _list_view
        End Get
        Set(ByVal value As Control)
            If IsNothing(value) Then Exit Property
            _list_view = value
            AddHandler _list_view.KeyDown, AddressOf page_navigator_KeyDown
            AddHandler _list_view.GotFocus, AddressOf page_navigator_GotFocus
            AddHandler _list_view.LostFocus, AddressOf page_navigator_LostFocus
        End Set
    End Property

    Public Property Loopable As Boolean
        Get
            Loopable = _loopable
        End Get
        Set(ByVal value As Boolean)
            _loopable = value
        End Set

    End Property

    Public ReadOnly Property Offset As Integer
        Get
            Offset = _offset
        End Get
    End Property

    Public Property PageSize As Integer
        Get
            PageSize = _page_size
        End Get
        Set(ByVal value As Integer)

        End Set
    End Property

    Public Property TotalItems As Integer
        Get
            TotalItems = _total_items
        End Get
        Set(ByVal value As Integer)
            _total_items = value
            label_total_items.Text = _total_items & If(_total_items <= 1, " item", " items")
            _offset = 0
            If Not _total_items = 0 Then _page_count = Math.Ceiling(_total_items / _page_size) Else _page_count = 0
            SetCurrentPage(If(_current_page > 0, _current_page, 1))
        End Set
    End Property

    Private Sub SetCurrentPage(ByVal new_page As Integer)

        If _total_items = 0 Then
            _current_page = 0
            _page_count = 0
            Render()
            RaiseEvent PageChanged(_current_page, _offset, _page_size)
            Exit Sub
        End If

        _current_page = If(new_page > _page_count, _page_count, new_page)
        _current_page = If(new_page < 1, 1, new_page)
        If _current_page > _page_count Then _current_page = If(_loopable, _page_count, 1)
        If _current_page < 1 Then _current_page = If(_loopable, 1, _page_count)
        _allow_right = Not (_current_page = _page_count And Loopable)
        _allow_left = Not (_current_page = 1 And Loopable)
        Render()
        _offset = (_current_page - 1) * _page_size

        RaiseEvent PageChanged(_current_page, _offset, _page_size)

    End Sub

    Private Sub NextPage()

        SetCurrentPage(_current_page + 1)

    End Sub

    Private Sub PreviousPage()

        SetCurrentPage(_current_page - 1)

    End Sub

    Private Sub Render()

        label_page_guide.Text = "Page " & _current_page & " of " & _page_count
        If _total_items > 0 Then
            Dim _item_to, _item_from As Integer
            _item_from = ((_current_page - 1) * _page_size) + 1
            _item_to = _item_from + (_page_size - 1)
            If _item_to > _total_items Then _item_to = _total_items
            label_page_details.Text = "Showing " & _item_from & " - " & _item_to
            label_page_details.Visible = True
        Else
            label_page_details.Visible = False
        End If

        Dim total_width = PictureBox1.Width + label_page_details.Width + image_left_arrow.Width + label_page_guide.Width + image_right_arrow.Width + label_total_items.Width
        If total_width > Me.Width Then
            PictureBox1.Visible = False
            label_page_details.Visible = False
            label_total_items.Visible = False
            image_left_arrow.Left = 3
            image_right_arrow.Left = image_left_arrow.Right + 3
            label_page_guide.Left = Me.ClientSize.Width - 3 - label_page_guide.Width
        Else
            PictureBox1.Visible = True
            label_page_details.Visible = True
            label_total_items.Visible = True
            label_page_guide_TextChanged(Nothing, Nothing)
        End If

    End Sub

    Private Sub label_page_guide_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_page_guide.TextChanged

        If label_page_details.Visible Then
            label_page_guide.Left = (Me.ClientSize.Width / 2) - (label_page_guide.Width / 2)
            image_left_arrow.Left = label_page_guide.Left - 5 - image_left_arrow.Width
            image_right_arrow.Left = label_page_guide.Right + 5
        End If

    End Sub

    Private Sub page_navigator_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.GotFocus

        'image_left_arrow.Visible = True
        'image_right_arrow.Visible = True

    End Sub

    Private Sub page_navigator_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Left And _allow_left Then PreviousPage()
        If e.KeyCode = Keys.Right And _allow_right Then NextPage()

    End Sub

    Private Sub page_navigator_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus

        'image_left_arrow.Visible = False
        'image_right_arrow.Visible = False

    End Sub

    Private Sub page_navigator_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        Render()

    End Sub

    Private Sub label_page_guide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_page_guide.Click

        Dim new_quantity_form As New form_quantity
        Dim new_page As Integer = Val(new_quantity_form.OpenQuantityDialog(Me.ParentForm, "Go to page", _current_page, 1, False))
        SetCurrentPage(new_page)

    End Sub


    Private Sub image_left_arrow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles image_left_arrow.Click

        PreviousPage()

    End Sub

    Private Sub image_right_arrow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles image_right_arrow.Click

        NextPage()

    End Sub
End Class
