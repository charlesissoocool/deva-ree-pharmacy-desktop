﻿Public Class PriceTextBox

    Private _price As Double = 0.0
    Private _resizable As Boolean = True

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property PriceDescription As String
        Get
            PriceDescription = label_price_description.Text
        End Get
        Set(ByVal value As String)
            label_price_description.Text = value
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Resizable As Boolean
        Get
            Resizable = _resizable
        End Get
        Set(ByVal value As Boolean)
            _resizable = value
        End Set
    End Property

    Public Property Price As Double
        Get
            Price = _price
        End Get
        Set(ByVal value As Double)
            text_price.Text = value
            AssignNewPrice()
        End Set
    End Property

    Private Sub price_input_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseClick, label_price.MouseClick, label_price_description.MouseClick

        If text_price.Visible Then Exit Sub
        label_price.Visible = False
        text_price.Visible = True
        text_price.Focus()
        text_price.SelectAll()
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize, Me.Load

        label_price_description.Left = image_money.Right + 2
        label_price_description.Top = image_money.Top
        label_price.Top = image_money.Bottom - label_price.Height
        label_price.Left = label_price_description.Left
        text_price.Top = label_price_description.Bottom + 2
        text_price.Left = label_price.Left + 5
        label_price.Width = Me.ClientSize.Width - (image_money.Left + label_price.Left)
        text_price.Width = Me.ClientSize.Width - (image_money.Left + text_price.Left)

    End Sub

    Private Sub price_input_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        label_price.Text = SYS_CURRENCY & FormatNumber(_price, 2, , , TriState.True)

    End Sub

    Private Sub AssignNewPrice()

        _price = Val(text_price.Text)
        text_price.Visible = False
        label_price.Text = SYS_CURRENCY & FormatNumber(_price, 2, , , TriState.True)
        label_price.Visible = True
        Me.Cursor = Cursors.Hand

    End Sub

    Private Sub text_price_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_price.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            AssignNewPrice()
        ElseIf e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            text_price.Text = _price
            AssignNewPrice()
        End If

    End Sub

    Private Sub text_price_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_price.LostFocus

        AssignNewPrice()

    End Sub

    Private Sub text_price_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_price.TextChanged

        ValidateNumericTextBox(sender)

    End Sub

    Private Sub label_price_description_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_price_description.Click

    End Sub

    Private Sub label_price_description_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_price_description.TextChanged



    End Sub

    Private Sub label_price_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_price.TextChanged, label_price_description.TextChanged

        If Not _resizable Then Exit Sub

        If label_price_description.Width > label_price.Width Then
            Me.Width = label_price_description.Right + label_price_description.Left
        Else
            Me.Width = label_price.Right + label_price.Left
        End If

    End Sub
End Class
