﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PriceTextBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PriceTextBox))
        Me.label_price = New System.Windows.Forms.Label()
        Me.label_price_description = New System.Windows.Forms.Label()
        Me.text_price = New System.Windows.Forms.TextBox()
        Me.image_money = New System.Windows.Forms.PictureBox()
        CType(Me.image_money, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'label_price
        '
        Me.label_price.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_price.AutoSize = True
        Me.label_price.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_price.ForeColor = System.Drawing.Color.Black
        Me.label_price.Location = New System.Drawing.Point(51, 16)
        Me.label_price.Name = "label_price"
        Me.label_price.Size = New System.Drawing.Size(83, 37)
        Me.label_price.TabIndex = 16
        Me.label_price.Text = "P0.00"
        '
        'label_price_description
        '
        Me.label_price_description.AutoSize = True
        Me.label_price_description.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_price_description.ForeColor = System.Drawing.Color.Black
        Me.label_price_description.Location = New System.Drawing.Point(56, 2)
        Me.label_price_description.Name = "label_price_description"
        Me.label_price_description.Size = New System.Drawing.Size(41, 20)
        Me.label_price_description.TabIndex = 17
        Me.label_price_description.Text = "Price"
        '
        'text_price
        '
        Me.text_price.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_price.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_price.ForeColor = System.Drawing.Color.Black
        Me.text_price.Location = New System.Drawing.Point(59, 22)
        Me.text_price.Name = "text_price"
        Me.text_price.Size = New System.Drawing.Size(157, 29)
        Me.text_price.TabIndex = 18
        Me.text_price.Visible = False
        '
        'image_money
        '
        Me.image_money.Image = CType(resources.GetObject("image_money.Image"), System.Drawing.Image)
        Me.image_money.Location = New System.Drawing.Point(5, 3)
        Me.image_money.Name = "image_money"
        Me.image_money.Size = New System.Drawing.Size(48, 48)
        Me.image_money.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.image_money.TabIndex = 19
        Me.image_money.TabStop = False
        '
        'price_input
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.image_money)
        Me.Controls.Add(Me.label_price_description)
        Me.Controls.Add(Me.label_price)
        Me.Controls.Add(Me.text_price)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Name = "price_input"
        Me.Size = New System.Drawing.Size(219, 55)
        CType(Me.image_money, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents label_price As System.Windows.Forms.Label
    Friend WithEvents label_price_description As System.Windows.Forms.Label
    Friend WithEvents text_price As System.Windows.Forms.TextBox
    Friend WithEvents image_money As System.Windows.Forms.PictureBox

End Class
