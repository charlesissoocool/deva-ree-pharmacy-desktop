﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MobileListItem
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MobileListItem))
        Me.picture_icon = New System.Windows.Forms.PictureBox()
        Me.label_sub_text = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_text = New DevaReePharmacy.AntiAliasedLabel()
        CType(Me.picture_icon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picture_icon
        '
        Me.picture_icon.Image = CType(resources.GetObject("picture_icon.Image"), System.Drawing.Image)
        Me.picture_icon.Location = New System.Drawing.Point(6, 6)
        Me.picture_icon.Name = "picture_icon"
        Me.picture_icon.Size = New System.Drawing.Size(32, 32)
        Me.picture_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picture_icon.TabIndex = 2
        Me.picture_icon.TabStop = False
        '
        'label_sub_text
        '
        Me.label_sub_text.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_sub_text.AutoSize = False
        Me.label_sub_text.Font = New System.Drawing.Font("Open Sans Light", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_sub_text.ForeColor = System.Drawing.Color.Black
        Me.label_sub_text.Location = New System.Drawing.Point(43, 21)
        Me.label_sub_text.Name = "label_sub_text"
        Me.label_sub_text.Size = New System.Drawing.Size(311, 18)
        Me.label_sub_text.TabIndex = 1
        Me.label_sub_text.Text = "Subtext"
        Me.label_sub_text.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_sub_text.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_sub_text.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        '
        'label_text
        '
        Me.label_text.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_text.AutoAnimate = False
        Me.label_text.AutoSize = False
        Me.label_text.Font = New System.Drawing.Font("Open Sans", 10.0!)
        Me.label_text.ForeColor = System.Drawing.Color.Black
        Me.label_text.Location = New System.Drawing.Point(43, 5)
        Me.label_text.Name = "label_text"
        Me.label_text.Size = New System.Drawing.Size(311, 19)
        Me.label_text.TabIndex = 0
        Me.label_text.Text = "Label"
        Me.label_text.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_text.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_text.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        '
        'MobileListItem
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.picture_icon)
        Me.Controls.Add(Me.label_sub_text)
        Me.Controls.Add(Me.label_text)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.MinimumSize = New System.Drawing.Size(0, 45)
        Me.Name = "MobileListItem"
        Me.Size = New System.Drawing.Size(366, 45)
        CType(Me.picture_icon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents label_text As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_sub_text As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents picture_icon As System.Windows.Forms.PictureBox

End Class
