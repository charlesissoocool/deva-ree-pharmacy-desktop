﻿Imports System.IO
Imports System.Net

Public Class PDFGenerator

    Private _html As String
    Private _title As String = "Document"
    Private _orientation As PDF_Orientation = PDF_Orientation.Portrait
    Private _footer As String = ""
    Private _css As New Dictionary(Of String, String)

    Public Property HTML As String
        Get
            HTML = _html
        End Get
        Set(ByVal value As String)
            _html = value
        End Set
    End Property

    Public Property Title As String
        Get
            Title = _title
        End Get
        Set(ByVal value As String)
            _title = value
        End Set
    End Property

    Public Property Footer As String
        Get
            Footer = _footer
        End Get
        Set(ByVal value As String)
            _footer = value
        End Set
    End Property

    Public Property CSS As Dictionary(Of String, String)
        Get
            CSS = _css
        End Get
        Set(ByVal value As Dictionary(Of String, String))
            _css = value
        End Set
    End Property

    Public Property Orientation As PDF_Orientation
        Get
            Orientation = _orientation
        End Get
        Set(ByVal value As PDF_Orientation)
            _orientation = value
        End Set
    End Property

    Public Sub ShowPreview()

        Dim html_raw = GetRender()
        browser_preview.DocumentText = html_raw

    End Sub

    Private Function GetRender() As String

        Try

            Dim temp_html As String = ""
            Dim temp_css = New Dictionary(Of String, String)(_css)
            temp_css.Add("body", "font-family: 'Open Sans', 'Tahoma', 'Arial'; font-size: 10px")
            temp_css.Add(".title", "width: 100%; font-size: 25px")
            temp_css.Add(".info", "width: 100%; font-size: 10px; font-style: italic; text-align: left; display: block")

            temp_html &= "<style>"
            For Each css_values As KeyValuePair(Of String, String) In temp_css
                Dim identifier = css_values.Key
                temp_html &= identifier & "{" & css_values.Value & "}"
            Next
            temp_html &= "</style>"

            Dim logo_path As String = Application.StartupPath & "\pdf_logo.png"
            If File.Exists(logo_path) Then File.Delete(logo_path)
            SYS_LOGO.Save(logo_path)
            temp_html &= "<center><img src='" & WebUtility.HtmlEncode(logo_path) & "' style='display: block; max-height: 150px; height: 150px'></center>"

            If File.Exists(SYS_PDF_ADDITIONAL_HEADER) Then
                Dim f_stream As New System.IO.StreamReader(SYS_PDF_ADDITIONAL_HEADER.ToString)
                temp_html &= f_stream.ReadToEnd & "<br>"
                f_stream.Close()
            End If

            temp_html &= "<span class='title'><center><b>" & _title & "</b></center></span><br><br>"
            temp_html &= _html
            temp_html &= "<br><span class='info'>Generated on " & Date.Now().ToString(DTS_HUMAN_WITH_TIME) & " using " & SYS_CUSTOMER_NAME & " iPOS System</span><br>"
            GetRender = temp_html
        Catch ex As Exception
            GetRender = "Error generating preview. " & ex.Message
        End Try

    End Function

    Public Sub SavePrinterFriendly()

        Dim file_name = Application.StartupPath & "\pricelist-" & Date.Now().ToString(DATE_YYMMDDHHMMSSFILE) & ".html"
        Dim file_name_pdf = Application.StartupPath & "\pricelist-" & Date.Now().ToString(DATE_YYMMDDHHMMSSFILE) & ".pdf"

        Try
            Dim html_raw = GetRender()
            browser_preview.DocumentText = html_raw
            If File.Exists(file_name) Then File.Delete(file_name)
            Dim file_writer As New StreamWriter(file_name)
            file_writer.Write(html_raw)
            file_writer.Close()

            'Open file dialog to save PDF
            dialog_printer_friendly.Filter = "PDF (*.pdf)|*.pdf"
            If dialog_printer_friendly.ShowDialog(Me) = DialogResult.OK Then
                If File.Exists(dialog_printer_friendly.FileName) Then File.Delete(dialog_printer_friendly.FileName)
                Dim orientation As String = If(_orientation = PDF_Orientation.Portrait, "Portrait", "Landscape")
                Dim proc_call As New Process
                Dim proc_info As New ProcessStartInfo("""" & SYS_PDF_GENERATOR & """", If(Not SYS_PDF_GENERATOR_PARAMETERS = "", SYS_PDF_GENERATOR_PARAMETERS.ToString.Replace("<src_file>", file_name).Replace("<dest_file>", dialog_printer_friendly.FileName).Replace("<orientation>", orientation), "").Replace("<footer>", _footer))
                proc_info.WindowStyle = ProcessWindowStyle.Hidden
                proc_call.StartInfo = proc_info
                proc_call.Start()
                proc_call.WaitForExit()

                If Not File.Exists(dialog_printer_friendly.FileName) Then Throw New Exception("PDF file output missing.")
                File.Delete(file_name) 'Delete HTML file
                MsgBox("PDF has been saved.", MsgBoxStyle.Information)
            End If
            Me.Focus()
        Catch ex As Exception
            MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
        End Try

    End Sub

    Public Sub Print()
        Dim file_name = Application.StartupPath & "\pricelist-" & Date.Now().ToString(DATE_YYMMDDHHMMSSFILE) & ".html"
        Dim file_name_pdf = Application.StartupPath & "\pricelist-" & Date.Now().ToString(DATE_YYMMDDHHMMSSFILE) & ".pdf"

        Try
            Dim html_raw = GetRender()
            browser_preview.DocumentText = html_raw
            If File.Exists(file_name) Then File.Delete(file_name)
            Dim file_writer As New StreamWriter(file_name)
            file_writer.Write(html_raw)
            file_writer.Close()
            Dim printer_dialog As New PrintDialog
            If Not printer_dialog.ShowDialog() = DialogResult.Cancel Then
                Dim orientation As String = If(_orientation = PDF_Orientation.Portrait, "Portrait", "Landscape")
                Dim proc_call As New Process
                Dim proc_info As New ProcessStartInfo("""" & SYS_PDF_GENERATOR & """", If(Not SYS_PDF_GENERATOR_PARAMETERS = "", SYS_PDF_GENERATOR_PARAMETERS.ToString.Replace("<src_file>", file_name).Replace("<dest_file>", file_name_pdf).Replace("<orientation>", orientation).Replace("<footer>", _footer), ""))
                proc_info.WindowStyle = ProcessWindowStyle.Hidden
                proc_call.StartInfo = proc_info
                proc_call.Start()
                proc_call.WaitForExit()

                If Not File.Exists(file_name_pdf) Then Throw New Exception("PDF file output missing.")
                File.Delete(file_name) 'Delete HTML file
                'Print PDF file using file_name_pdf

                For i = 1 To printer_dialog.PrinterSettings.Copies
                    proc_call = New Process
                    proc_info = New ProcessStartInfo(SYS_PDF_PRINTER, If(Not SYS_PDF_PRINTER_PARAMETERS = "", SYS_PDF_PRINTER_PARAMETERS.ToString.Replace("<src_file>", file_name_pdf).Replace("<printer_name>", printer_dialog.PrinterSettings.PrinterName), ""))
                    proc_info.CreateNoWindow = True
                    proc_info.WindowStyle = ProcessWindowStyle.Hidden
                    proc_call.StartInfo = proc_info
                    proc_call.Start()
                    proc_call.WaitForExit()
                Next i
            End If
            Me.Focus()
        Catch ex As Exception
            MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
        Finally
            Try
                If File.Exists(file_name_pdf) Then File.Delete(file_name_pdf)
            Catch

            End Try
        End Try

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        browser_preview.DocumentText = "<div style='width: 100%; height: 100%; font-family: ""Arial""; font-size: 10px; display: table-cell; vertical-align: middle'><center>No Preview</center></div>"

    End Sub
End Class

Public Enum PDF_Orientation
    Landscape = 1
    Portrait = 0
End Enum
