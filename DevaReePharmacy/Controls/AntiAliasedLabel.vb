﻿Imports System.Drawing

Public Class AntiAliasedLabel

    Dim _auto_size As Boolean = True
    Dim _auto_ellipsis As Boolean = Not _auto_size
    Dim _draw_font As Font = New Font("Arial", "9")
    Dim _draw_hint As Drawing.Text.TextRenderingHint = Drawing.Text.TextRenderingHint.AntiAlias
    Dim _label As String = "Label"
    Dim _fore_color As Color = Color.Black
    Dim _alignment As TextAlignment = TextAlignment.Left
    Dim _vertical_alignment As TextVerticalAlignment = TextVerticalAlignment.Middle
    Dim _animate As Boolean = True
    Dim _last_point As Point
    Dim _vertical As Boolean = False

    Public Shadows Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event Click(ByVal sender As Object, ByVal e As System.EventArgs)

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Overrides Property Font As Font
        Get
            Font = _draw_font
        End Get
        Set(ByVal value As Font)
            _draw_font = value
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Overrides Property AutoSize As Boolean
        Get
            AutoSize = _auto_size
        End Get
        Set(ByVal value As Boolean)
            _auto_size = value
            _auto_ellipsis = Not _auto_size
            If _auto_size Then _animate = False
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property AutoEllipsis As Boolean
        Get
            AutoEllipsis = _auto_ellipsis
        End Get
        Set(ByVal value As Boolean)
            _auto_ellipsis = value
            If _auto_ellipsis Then _auto_size = False : _animate = False
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property AutoAnimate As Boolean
        Get
            AutoAnimate = _animate
        End Get
        Set(ByVal value As Boolean)
            _animate = value
            If _animate Then _auto_ellipsis = False
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Shadows Property ForeColor As Color
        Get
            ForeColor = _fore_color
        End Get
        Set(ByVal value As Color)
            _fore_color = value
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Overrides Property Text As String
        Get
            Text = _label
        End Get
        Set(ByVal value As String)
            _label = value
            Dim last_right = Me.Right
            Render()
            If _alignment = TextAlignment.Right Then Me.Left = Me.Left - (Me.Right - last_right)
            RaiseEvent TextChanged(Me, Nothing)
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property TextAlign As TextAlignment
        Get
            TextAlign = _alignment
        End Get
        Set(ByVal value As TextAlignment)
            _alignment = value
            Render()
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property TextVerticalAlign As TextVerticalAlignment
        Get
            TextVerticalAlign = _vertical_alignment
        End Get
        Set(ByVal value As TextVerticalAlignment)
            _vertical_alignment = value
            Render()
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property TextRender As System.Drawing.Text.TextRenderingHint
        Get
            TextRender = _draw_hint
        End Get
        Set(ByVal value As System.Drawing.Text.TextRenderingHint)
            _draw_hint = value
            Render()
        End Set
    End Property

    Public Property VerticalDirection As Boolean
        Get
            VerticalDirection = _vertical
        End Get
        Set(ByVal value As Boolean)
            _vertical = value
            Render()
        End Set
    End Property

    Private Sub Animate(ByVal new_point As Point)

        _last_point = new_point
        Dim bmp As Bitmap = New Bitmap(draw_area.Width, draw_area.Height)
        Dim grp As Graphics = Graphics.FromImage(bmp)
        grp.TextRenderingHint = _draw_hint
        grp.DrawString(_label, _draw_font, New SolidBrush(_fore_color), _last_point)
        draw_area.Image = bmp

    End Sub

    Private Function GetSubStringForWidth(ByVal text As String, ByVal font As Font, ByVal width As Long) As String

        If width > TextRenderer.MeasureText(text, font).Width Then
            GetSubStringForWidth = text
        Else
            Dim append As String = "..."
            For i = Len(text) To 0 Step -1
                If (TextRenderer.MeasureText(text.Substring(0, i) & append, font).Width <= width) Then
                    GetSubStringForWidth = text.Substring(0, i) & append
                    Exit Function
                End If
            Next
            GetSubStringForWidth = append
        End If

    End Function

    Private Sub Render()

        Try
            timer_animate.Enabled = False
            timer_animate_trigger.Enabled = False
            timer_render_trigger.Enabled = False
            Dim text_size = TextRenderer.MeasureText(_label, _draw_font)
            Dim label As String = _label

            If _auto_size Then
                Me.Width = text_size.Width
                Me.Height = text_size.Height
            ElseIf _auto_ellipsis And Not _animate Then
                label = GetSubStringForWidth(_label, _draw_font, Me.Width)
                Me.Height = text_size.Height
            End If
            draw_area.BackColor = Color.Transparent

            Dim bmp As Bitmap = New Bitmap(draw_area.Width, draw_area.Height)
            Dim grp As Graphics = Graphics.FromImage(bmp)
            grp.TextRenderingHint = _draw_hint
            Dim hor, ver As Integer
            Select Case _alignment
                Case TextAlignment.Left
                    hor = 0
                Case TextAlignment.Right
                    hor = Me.Width - text_size.Width
                Case TextAlignment.Center
                    hor = (Me.Width / 2) - (text_size.Width / 2)
            End Select
            Select Case _vertical_alignment
                Case TextVerticalAlignment.Bottom
                    ver = Me.Height - text_size.Height
                Case TextVerticalAlignment.Top
                    ver = 0
                Case TextVerticalAlignment.Middle
                    ver = (Me.Height / 2) - (text_size.Height / 2)
            End Select
            _last_point = New Point(hor, ver)
            timer_animate_trigger.Enabled = _animate And (text_size.Width > Me.Width)
            If _vertical Then
                grp.DrawString(label, _draw_font, New SolidBrush(_fore_color), _last_point, New StringFormat(StringFormatFlags.DirectionVertical))
            Else
                grp.DrawString(label, _draw_font, New SolidBrush(_fore_color), _last_point)
            End If
            draw_area.Image = bmp
        Catch ex As Exception
            draw_area.Image = Nothing
            timer_animate.Enabled = False
            timer_animate_trigger.Enabled = False
            timer_render_trigger.Enabled = False
        End Try

    End Sub

    Private Sub AntiAliasedLabel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Render()

    End Sub

    Private Sub AntiAliasedLabel_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        Render()

    End Sub

    Private Sub timer_animate_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_animate.Tick

        timer_animate.Enabled = False
        Dim target_left As Long
        Dim text_size = TextRenderer.MeasureText(_label, _draw_font)

        Select Case _alignment
            Case TextAlignment.Left
                target_left = -1 * ((text_size.Width - Me.Width) + draw_area.Margin.Left + draw_area.Margin.Right)
            Case TextAlignment.Right
                target_left = draw_area.Width
            Case TextAlignment.Center
                Exit Sub
        End Select

        If target_left <> _last_point.X Then
            Dim increment As Integer
            Dim move_x As Integer = 5
            If target_left > _last_point.X Then
                increment = If((target_left - _last_point.X >= move_x), move_x, (target_left - _last_point.X))
            Else
                increment = If((_last_point.X - target_left >= move_x), -1 * move_x, -1 * (_last_point.X - target_left))
            End If
            Dim new_point As New Point(_last_point.X + increment, _last_point.Y)
            Animate(new_point)
            timer_animate.Enabled = True
        Else
            timer_render_trigger.Enabled = True
        End If

    End Sub

    Private Sub timer_render_trigger_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_render_trigger.Tick

        timer_render_trigger.Enabled = False
        Render()

    End Sub

    Private Sub timer_animate_trigger_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_animate_trigger.Tick

        timer_animate_trigger.Enabled = False
        timer_animate.Enabled = True

    End Sub

    Private Sub draw_area_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles draw_area.Click

        RaiseEvent Click(sender, e)

    End Sub
End Class

Public Enum TextAlignment
    Left = 0
    Right = 1
    Center = 2
End Enum

Public Enum TextVerticalAlignment
    Bottom = 0
    Top = 1
    Middle = 2
End Enum