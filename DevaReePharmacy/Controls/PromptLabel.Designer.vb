﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PromptLabel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.label_message = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'label_message
        '
        Me.label_message.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_message.AutoEllipsis = True
        Me.label_message.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_message.Location = New System.Drawing.Point(13, 9)
        Me.label_message.Name = "label_message"
        Me.label_message.Size = New System.Drawing.Size(253, 48)
        Me.label_message.TabIndex = 1
        Me.label_message.Text = "--"
        Me.label_message.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PromptLabel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.label_message)
        Me.Name = "PromptLabel"
        Me.Size = New System.Drawing.Size(280, 66)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents label_message As System.Windows.Forms.Label

End Class
