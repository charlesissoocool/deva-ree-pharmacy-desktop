﻿Public Class RunningLabel

    Private _value As Double
    Private _show_decimal As Boolean = False
    Private _show_currency As Boolean = False
    Private _animate As Boolean = True
    Private _increment_value As Double = 25.25

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Title As String
        Get
            Title = label_title.Text
        End Get
        Set(ByVal value As String)
            label_title.Text = value
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Value As String
        Get
            Value = _value
        End Get
        Set(ByVal value As String)
            _value = value
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property HasDecimal As Boolean
        Get
            HasDecimal = _show_decimal
        End Get
        Set(ByVal value As Boolean)
            _show_decimal = value
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property ShowCurrency As Boolean
        Get
            ShowCurrency = _show_currency
        End Get
        Set(ByVal value As Boolean)
            _show_currency = value
            Render()
        End Set
    End Property

    <System.ComponentModel.DefaultValue(True), System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Animate As Boolean
        Get
            Animate = _animate
        End Get
        Set(ByVal value As Boolean)
            _animate = value
            Render()
        End Set
    End Property

    Private Sub Render()

        If _animate Then
            If label_value.Tag < _value Or label_value.Tag > _value Then
                _increment_value = If(label_value.Tag < _value, (_value - label_value.Tag) / 100, (label_value.Tag - _value) / 100)
                timer_text_refresher.Enabled = True
            Else
                label_value.Text = If(_show_currency, SYS_CURRENCY, "") & FormatNumber(_value, If(_show_decimal, 2, 0), , , TriState.True)
            End If
        Else
            label_value.Tag = _value
            label_value.Text = If(_show_currency, SYS_CURRENCY, "") & FormatNumber(_value, If(_show_decimal, 2, 0), , , TriState.True)
        End If

    End Sub

    Private Sub label_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_value.TextChanged, label_title.TextChanged

        Me.Width = If(label_title.Width > label_value.Width, (label_title.Left * 2) + label_title.Width, (label_value.Left * 2) + label_value.Width)

    End Sub
    
    Private Sub timer_text_refresher_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_text_refresher.Tick

        Dim increment As Double = 0
        If label_value.Tag < _value Then
            increment = If(_value - label_value.Tag > _increment_value, _increment_value, _value - label_value.Tag)
        ElseIf label_value.Tag > _value Then
            increment = If(_value - label_value.Tag < _increment_value, -1 * _increment_value, _value - label_value.Tag)
        Else
            timer_text_refresher.Enabled = False
            Exit Sub
        End If
        label_value.Tag += increment
        label_value.Text = If(_show_currency, SYS_CURRENCY, "") & FormatNumber(label_value.Tag, If(_show_decimal, 2, 0), , , TriState.True)

    End Sub
End Class