﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AntiAliasedLabel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.draw_area = New System.Windows.Forms.PictureBox()
        Me.timer_animate = New System.Windows.Forms.Timer(Me.components)
        Me.timer_render_trigger = New System.Windows.Forms.Timer(Me.components)
        Me.timer_animate_trigger = New System.Windows.Forms.Timer(Me.components)
        CType(Me.draw_area, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'draw_area
        '
        Me.draw_area.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.draw_area.Location = New System.Drawing.Point(0, 0)
        Me.draw_area.Name = "draw_area"
        Me.draw_area.Size = New System.Drawing.Size(170, 102)
        Me.draw_area.TabIndex = 0
        Me.draw_area.TabStop = False
        '
        'timer_animate
        '
        '
        'timer_render_trigger
        '
        Me.timer_render_trigger.Interval = 3000
        '
        'timer_animate_trigger
        '
        Me.timer_animate_trigger.Interval = 3000
        '
        'AntiAliasedLabel
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.draw_area)
        Me.DoubleBuffered = True
        Me.Name = "AntiAliasedLabel"
        Me.Size = New System.Drawing.Size(170, 102)
        CType(Me.draw_area, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents draw_area As System.Windows.Forms.PictureBox
    Friend WithEvents timer_animate As System.Windows.Forms.Timer
    Friend WithEvents timer_render_trigger As System.Windows.Forms.Timer
    Friend WithEvents timer_animate_trigger As System.Windows.Forms.Timer

End Class
