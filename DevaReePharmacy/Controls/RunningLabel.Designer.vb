﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RunningLabel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.timer_text_refresher = New System.Windows.Forms.Timer(Me.components)
        Me.label_title = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_value = New DevaReePharmacy.AntiAliasedLabel()
        Me.SuspendLayout()
        '
        'timer_text_refresher
        '
        Me.timer_text_refresher.Interval = 10
        '
        'label_title
        '
        Me.label_title.AutoEllipsis = False
        Me.label_title.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_title.ForeColor = System.Drawing.Color.White
        Me.label_title.Location = New System.Drawing.Point(9, 4)
        Me.label_title.Name = "label_title"
        Me.label_title.Size = New System.Drawing.Size(42, 22)
        Me.label_title.TabIndex = 0
        Me.label_title.Text = "Title"
        Me.label_title.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_title.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_title.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_title.VerticalDirection = False
        '
        'label_value
        '
        Me.label_value.AutoEllipsis = False
        Me.label_value.Font = New System.Drawing.Font("Open Sans", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_value.ForeColor = System.Drawing.Color.White
        Me.label_value.Location = New System.Drawing.Point(7, 21)
        Me.label_value.Name = "label_value"
        Me.label_value.Size = New System.Drawing.Size(40, 47)
        Me.label_value.TabIndex = 1
        Me.label_value.Text = "0"
        Me.label_value.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_value.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_value.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_value.VerticalDirection = False
        '
        'RunningLabel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.label_title)
        Me.Controls.Add(Me.label_value)
        Me.Name = "RunningLabel"
        Me.Size = New System.Drawing.Size(96, 74)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents timer_text_refresher As System.Windows.Forms.Timer
    Friend WithEvents label_title As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_value As DevaReePharmacy.AntiAliasedLabel

End Class
