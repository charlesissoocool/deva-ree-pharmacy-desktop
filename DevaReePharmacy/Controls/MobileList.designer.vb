﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MobileList
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.flow_list = New System.Windows.Forms.FlowLayoutPanel()
        Me.label_no_items = New DevaReePharmacy.AntiAliasedLabel()
        Me.SuspendLayout()
        '
        'flow_list
        '
        Me.flow_list.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flow_list.AutoScroll = True
        Me.flow_list.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.flow_list.Location = New System.Drawing.Point(0, 0)
        Me.flow_list.Name = "flow_list"
        Me.flow_list.Size = New System.Drawing.Size(289, 314)
        Me.flow_list.TabIndex = 0
        Me.flow_list.WrapContents = False
        '
        'label_no_items
        '
        Me.label_no_items.AutoEllipsis = False
        Me.label_no_items.Font = New System.Drawing.Font("Open Sans", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_no_items.ForeColor = System.Drawing.Color.Black
        Me.label_no_items.Location = New System.Drawing.Point(174, 135)
        Me.label_no_items.Name = "label_no_items"
        Me.label_no_items.Size = New System.Drawing.Size(75, 20)
        Me.label_no_items.TabIndex = 1
        Me.label_no_items.Text = "No items."
        Me.label_no_items.TextAlign = DevaReePharmacy.TextAlignment.Center
        Me.label_no_items.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_no_items.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_no_items.VerticalDirection = False
        '
        'MobileList
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Controls.Add(Me.label_no_items)
        Me.Controls.Add(Me.flow_list)
        Me.Name = "MobileList"
        Me.Size = New System.Drawing.Size(292, 314)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents flow_list As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents label_no_items As DevaReePharmacy.AntiAliasedLabel

End Class
