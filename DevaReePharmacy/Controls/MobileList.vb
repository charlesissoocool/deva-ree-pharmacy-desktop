﻿Public Class MobileList

    Private WithEvents _items As New MobileListItemCollection(Me)
    Private _image_list As ImageList
    Private _selected_index As Integer = -1
    Private _selector_color As Color = System.Drawing.ColorTranslator.FromHtml("#3399ff")
    Private _hover_selector_color As Color = System.Drawing.ColorTranslator.FromHtml("#0066cc")
    Private _hover_color As Color = Color.Silver
    Private _last_hover As Integer = -1

    Public Event ItemSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Protected Overrides Sub OnEnter(ByVal e As System.EventArgs)
        MyBase.OnEnter(e)
        If Not IsNothing(Me.SelectedItem) Then Me.SelectedItem.BackColor = _selector_color
    End Sub

    Protected Overrides Sub OnLeave(ByVal e As System.EventArgs)
        MyBase.OnLeave(e)
        If Not IsNothing(Me.SelectedItem) Then Me.SelectedItem.BackColor = Me.BackColor
    End Sub

    Private Sub MobileList_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyClass.MouseLeave

        If _last_hover > -1 Then ItemMouseLeave(Me.Items(_last_hover))

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        If _items.Count > 0 Then
            For i = 0 To _items.Count - 1
                _items.Item(i).Width = Me.ClientSize.Width
            Next
        End If
        label_no_items.Left = (Me.ClientSize.Width / 2) - (label_no_items.Width / 2)
        label_no_items.Top = (Me.ClientSize.Height / 2) - (label_no_items.Height / 2)

    End Sub

    Public ReadOnly Property Items As MobileListItemCollection
        Get
            Items = _items
        End Get
    End Property

    Public ReadOnly Property SelectedItem As MobileListItem
        Get
            If _selected_index < 0 Or _selected_index > Items.Count - 1 Then
                SelectedItem = Nothing
            Else
                SelectedItem = Items.Item(_selected_index)
            End If
        End Get
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property ImageList As ImageList
        Get
            ImageList = _image_list
        End Get
        Set(ByVal value As ImageList)
            _image_list = value
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property SelectedColor As Color
        Get
            SelectedColor = _selector_color
        End Get
        Set(ByVal value As Color)
            _selector_color = value
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property HoverSelectedColor As Color
        Get
            HoverSelectedColor = _hover_selector_color
        End Get
        Set(ByVal value As Color)
            _hover_selector_color = value
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property HoverColor As Color
        Get
            HoverColor = _hover_color
        End Get
        Set(ByVal value As Color)
            _hover_color = value
        End Set
    End Property

    Public Overrides Property BackColor As System.Drawing.Color
        Get
            Return MyBase.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            MyBase.BackColor = value
            flow_list.BackColor = value
        End Set
    End Property

    Private Sub RenderObjects()

        For Each list_obj As MobileListItem In _items.OfType(Of MobileListItem)()
            If Not flow_list.Controls.Contains(list_obj) Then
                flow_list.Controls.Add(list_obj)
            End If
        Next

    End Sub

    Private Sub ItemAdded(ByVal newItem As MobileListItem) Handles _items.ItemAdded

        newItem.TabStop = False
        newItem.Font = Me.Font
        newItem.ForeColor = Me.ForeColor
        flow_list.Controls.Add(newItem)
        AddHandler newItem.Clicked, Function(sender, e) Me.ItemClick(newItem)
        label_no_items.Visible = False
        'AddHandler newItem.MouseEnter, Function(sender, e) Me.ItemMouseEnter(newItem)
        'AddHandler newItem.MouseLeave, Function(sender, e) Me.ItemMouseLeave(newItem)
        'AddHandler newItem.ItemMouseOver, Function(sender, e) Me.ItemMouseEnter(newItem)

    End Sub

    Private Sub ItemRemoved(ByVal removedItem As MobileListItem) Handles _items.ItemRemoved

        For Each item As MobileListItem In flow_list.Controls.OfType(Of MobileListItem)()
            If item Is removedItem Then
                If item.Index = _selected_index Then _selected_index = -1
                flow_list.Controls.Remove(item)
            ElseIf _selected_index = item.Index And item.Index > removedItem.Index Then
                _selected_index -= 1
            End If
        Next
        label_no_items.Visible = Me.Items.Count = 0

    End Sub

    Private Function ItemClick(ByVal item As MobileListItem) As Boolean

        Me.Focus()
        ItemClick = True
        If _selected_index > -1 Then
            Items.Item(_selected_index).BackColor = Me.BackColor
            Items.Item(_selected_index).AnimateSubText = False
        End If
        _selected_index = item.Index
        Items.Item(_selected_index).BackColor = _selector_color
        Items.Item(_selected_index).AnimateSubText = True
        RaiseEvent ItemSelectionChanged(Me, Nothing)

    End Function

    Private Function ItemMouseEnter(ByVal item As MobileListItem) As Boolean

        ItemMouseEnter = True
        If Not _last_hover = item.Index Then
            If _last_hover >= 0 And _last_hover < Me.Items.Count Then ItemMouseLeave(Me.Items(_last_hover))
            _last_hover = item.Index
            If IsNothing(Me.SelectedItem) Then
                item.BackColor = Me.HoverColor
            Else
                If item.Index = Me.SelectedItem.Index Then
                    item.BackColor = Me.HoverSelectedColor
                Else
                    item.BackColor = Me.HoverColor
                End If
            End If
        End If

    End Function

    Private Function ItemMouseLeave(ByVal item As MobileListItem) As Boolean

        ItemMouseLeave = True
        If item Is Me.SelectedItem Then
            item.BackColor = Me.SelectedColor
        Else
            item.BackColor = Me.BackColor()
        End If

    End Function

    Private Sub ControlFontChanged() Handles Me.FontChanged

        If Not IsNothing(Me.Items) Then
            For Each list_item As MobileListItem In Me.Items
                list_item.Font = Me.Font
            Next
        End If

    End Sub

    Private Sub ControlForeColorChanged() Handles Me.ForeColorChanged

        If Not IsNothing(Me.Items) Then
            For Each list_item As MobileListItem In Me.Items
                list_item.ForeColor = Me.ForeColor
            Next
        End If

    End Sub

End Class

Public Class MobileListItemCollection
    Inherits CollectionBase

    Public Event ItemChanged()
    Public Event ItemAdded(ByVal newItem As MobileListItem)
    Public Event ItemRemoved(ByVal removedItem As MobileListItem)
    Private _mobile_list As MobileList

    Public Sub New(ByRef mobile_list As MobileList)

        _mobile_list = mobile_list

    End Sub

    'Public Sub Add(ByVal listItem As MobileListItem)

    '    listItem.Width = _mobile_list.ClientSize.Width
    '    listItem.AnimateSubText = False
    '    List.Add(listItem)
    '    listItem.SetIndex(List.Count - 1)
    '    AddHandler listItem.ImageKeyChanged, Function(sender, e) Me.ItemImageKeyChanged(listItem)
    '    RaiseEvent ItemChanged()
    '    RaiseEvent ItemAdded(listItem)

    'End Sub

    Public Function Add(ByVal text As String, Optional ByVal subText As String = "", Optional ByVal imageKey As String = "") As MobileListItem

        Dim new_item As New MobileListItem
        new_item.Width = _mobile_list.ClientSize.Width
        new_item.Text = text
        new_item.SubText = subText
        new_item.ImageKey = imageKey
        new_item.AnimateSubText = False
        List.Add(new_item)
        new_item.SetIndex(List.Count - 1)
        AddHandler new_item.ImageKeyChanged, Function(sender, e) Me.ItemImageKeyChanged(new_item)
        RaiseEvent ItemChanged()
        RaiseEvent ItemAdded(new_item)
        ItemImageKeyChanged(new_item)
        Add = new_item

    End Function

    Public Sub Remove(ByVal index As Integer)

        If index > Count - 1 Or index < 0 Then
            MsgBox("Index not valid!")
        Else
            RaiseEvent ItemChanged()
            RaiseEvent ItemRemoved(List.Item(index))
            List.RemoveAt(index)
            For i = 0 To List.Count - 1
                List.Item(i).SetIndex(i)
            Next
        End If

    End Sub

    Public Shadows Sub Clear()

        _mobile_list.flow_list.Controls.Clear()
        List.Clear()

    End Sub

    Public ReadOnly Property Item(ByVal index As Integer) As MobileListItem
        Get
            If index > List.Count - 1 Then
                Item = New MobileListItem
            Else
                Return CType(List.Item(index), MobileListItem)
            End If
        End Get
    End Property

    Private Function ItemImageKeyChanged(ByVal item As MobileListItem) As Boolean

        ItemImageKeyChanged = True
        If item.ImageKey = "" Then
            item.SetIcon(Nothing)
        Else
            If Not IsNothing(_mobile_list.ImageList) Then
                item.SetIcon(_mobile_list.ImageList.Images(item.ImageKey))
            End If
        End If

    End Function

End Class

