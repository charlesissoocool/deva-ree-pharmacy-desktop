﻿Public Class PromptLabel

    Private _mode_bg_colors(4) As Color
    Private _mode_fg_colors(4) As Color
    Private _current_mode As Integer = 0
    Public Enum PromptLabelMode
        Info = 0
        Success = 1
        Warning = 2
        Critical = 3
        Neutral = 4
    End Enum

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Mode As PromptLabelMode
        Get
            Mode = _current_mode
        End Get
        Set(ByVal value As PromptLabelMode)
            _current_mode = value
            Render()
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Message As String
        Get
            Message = label_message.Text
        End Get
        Set(ByVal value As String)
            label_message.Text = value
            Render()
        End Set
    End Property

    Private Sub Render()

        If _current_mode < 0 Or _current_mode > 4 Then _current_mode = 0
        Me.BackColor = _mode_bg_colors(_current_mode)
        label_message.ForeColor = _mode_fg_colors(_current_mode)
        AdjustObjects()
        Me.Visible = True

    End Sub

    Private Sub AdjustObjects()

        label_message.Left = 5
        label_message.Top = 5
        label_message.Width = Me.ClientSize.Width - (label_message.Left * 2)
        label_message.Height = Me.ClientSize.Height - (label_message.Top * 2)

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _mode_bg_colors(0) = module_colors.color_light_blue
        _mode_bg_colors(1) = module_colors.color_green
        _mode_bg_colors(2) = module_colors.color_yellow
        _mode_bg_colors(3) = module_colors.color_red
        _mode_bg_colors(4) = module_colors.color_gray
        _mode_fg_colors(0) = Color.White
        _mode_fg_colors(1) = Color.White
        _mode_fg_colors(2) = Color.Black
        _mode_fg_colors(3) = Color.White
        _mode_fg_colors(4) = Color.Black
        Me.BackColor = _mode_bg_colors(_current_mode)

    End Sub

    Private Sub prompt_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize

        label_message.Width = Me.Width - 10

    End Sub

    Private Sub label_message_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_message.Click

    End Sub

    Private Sub label_message_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_message.TextChanged

        Me.Height = (label_message.Top * 2) + label_message.Height

    End Sub
End Class
