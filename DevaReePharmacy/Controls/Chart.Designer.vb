﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chart
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picture_draw_area = New System.Windows.Forms.PictureBox()
        Me.panel_legend = New System.Windows.Forms.Panel()
        CType(Me.picture_draw_area, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picture_draw_area
        '
        Me.picture_draw_area.Location = New System.Drawing.Point(0, 0)
        Me.picture_draw_area.Name = "picture_draw_area"
        Me.picture_draw_area.Size = New System.Drawing.Size(470, 210)
        Me.picture_draw_area.TabIndex = 0
        Me.picture_draw_area.TabStop = False
        '
        'panel_legend
        '
        Me.panel_legend.Location = New System.Drawing.Point(0, 208)
        Me.panel_legend.Name = "panel_legend"
        Me.panel_legend.Size = New System.Drawing.Size(470, 28)
        Me.panel_legend.TabIndex = 1
        '
        'Chart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.panel_legend)
        Me.Controls.Add(Me.picture_draw_area)
        Me.Name = "Chart"
        Me.Size = New System.Drawing.Size(470, 239)
        CType(Me.picture_draw_area, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picture_draw_area As System.Windows.Forms.PictureBox
    Friend WithEvents panel_legend As System.Windows.Forms.Panel

End Class
