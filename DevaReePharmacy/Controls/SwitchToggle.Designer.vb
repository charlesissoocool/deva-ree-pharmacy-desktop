﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SwitchToggle
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.label_toggle = New System.Windows.Forms.Label()
        Me.image_toggle = New System.Windows.Forms.PictureBox()
        Me.label_status = New System.Windows.Forms.Label()
        CType(Me.image_toggle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'label_toggle
        '
        Me.label_toggle.AutoSize = True
        Me.label_toggle.Font = New System.Drawing.Font("Segoe UI Semibold", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_toggle.ForeColor = System.Drawing.Color.Black
        Me.label_toggle.Location = New System.Drawing.Point(57, 3)
        Me.label_toggle.Name = "label_toggle"
        Me.label_toggle.Size = New System.Drawing.Size(55, 20)
        Me.label_toggle.TabIndex = 14
        Me.label_toggle.Text = "Toggle"
        '
        'image_toggle
        '
        Me.image_toggle.Cursor = System.Windows.Forms.Cursors.Hand
        Me.image_toggle.Image = Global.DevaReePharmacy.My.Resources.Resources.toggle_on
        Me.image_toggle.Location = New System.Drawing.Point(3, 3)
        Me.image_toggle.Name = "image_toggle"
        Me.image_toggle.Size = New System.Drawing.Size(48, 48)
        Me.image_toggle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.image_toggle.TabIndex = 0
        Me.image_toggle.TabStop = False
        '
        'label_status
        '
        Me.label_status.AutoSize = True
        Me.label_status.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_status.ForeColor = System.Drawing.Color.Black
        Me.label_status.Location = New System.Drawing.Point(57, 23)
        Me.label_status.Name = "label_status"
        Me.label_status.Size = New System.Drawing.Size(23, 15)
        Me.label_status.TabIndex = 15
        Me.label_status.Text = "On"
        '
        'switch_toggle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.label_status)
        Me.Controls.Add(Me.label_toggle)
        Me.Controls.Add(Me.image_toggle)
        Me.Name = "switch_toggle"
        Me.Size = New System.Drawing.Size(247, 56)
        CType(Me.image_toggle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents image_toggle As System.Windows.Forms.PictureBox
    Friend WithEvents label_toggle As System.Windows.Forms.Label
    Friend WithEvents label_status As System.Windows.Forms.Label

End Class
