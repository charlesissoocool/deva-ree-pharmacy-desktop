﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PageNavigator
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PageNavigator))
        Me.label_page_guide = New System.Windows.Forms.Label()
        Me.label_total_items = New System.Windows.Forms.Label()
        Me.label_page_details = New System.Windows.Forms.Label()
        Me.image_right_arrow = New System.Windows.Forms.PictureBox()
        Me.image_left_arrow = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.image_right_arrow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.image_left_arrow, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'label_page_guide
        '
        Me.label_page_guide.AutoEllipsis = True
        Me.label_page_guide.AutoSize = True
        Me.label_page_guide.Cursor = System.Windows.Forms.Cursors.Hand
        Me.label_page_guide.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_page_guide.ForeColor = System.Drawing.Color.White
        Me.label_page_guide.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.label_page_guide.Location = New System.Drawing.Point(404, 10)
        Me.label_page_guide.Name = "label_page_guide"
        Me.label_page_guide.Size = New System.Drawing.Size(72, 18)
        Me.label_page_guide.TabIndex = 7
        Me.label_page_guide.Text = "Page 0 of 0"
        '
        'label_total_items
        '
        Me.label_total_items.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_total_items.AutoEllipsis = True
        Me.label_total_items.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.label_total_items.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total_items.ForeColor = System.Drawing.Color.White
        Me.label_total_items.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.label_total_items.Location = New System.Drawing.Point(543, 10)
        Me.label_total_items.Name = "label_total_items"
        Me.label_total_items.Size = New System.Drawing.Size(121, 17)
        Me.label_total_items.TabIndex = 11
        Me.label_total_items.Text = "0 items"
        Me.label_total_items.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label_page_details
        '
        Me.label_page_details.AutoEllipsis = True
        Me.label_page_details.AutoSize = True
        Me.label_page_details.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.label_page_details.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_page_details.ForeColor = System.Drawing.Color.White
        Me.label_page_details.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.label_page_details.Location = New System.Drawing.Point(36, 10)
        Me.label_page_details.Name = "label_page_details"
        Me.label_page_details.Size = New System.Drawing.Size(86, 18)
        Me.label_page_details.TabIndex = 12
        Me.label_page_details.Text = "Showing 0 - 0"
        Me.label_page_details.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'image_right_arrow
        '
        Me.image_right_arrow.Cursor = System.Windows.Forms.Cursors.Hand
        Me.image_right_arrow.Image = CType(resources.GetObject("image_right_arrow.Image"), System.Drawing.Image)
        Me.image_right_arrow.Location = New System.Drawing.Point(511, 6)
        Me.image_right_arrow.Name = "image_right_arrow"
        Me.image_right_arrow.Size = New System.Drawing.Size(26, 26)
        Me.image_right_arrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.image_right_arrow.TabIndex = 14
        Me.image_right_arrow.TabStop = False
        '
        'image_left_arrow
        '
        Me.image_left_arrow.Cursor = System.Windows.Forms.Cursors.Hand
        Me.image_left_arrow.Image = CType(resources.GetObject("image_left_arrow.Image"), System.Drawing.Image)
        Me.image_left_arrow.Location = New System.Drawing.Point(271, 6)
        Me.image_left_arrow.Name = "image_left_arrow"
        Me.image_left_arrow.Size = New System.Drawing.Size(26, 26)
        Me.image_left_arrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.image_left_arrow.TabIndex = 13
        Me.image_left_arrow.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(4, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(26, 26)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 10
        Me.PictureBox1.TabStop = False
        '
        'PageNavigator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.Controls.Add(Me.image_right_arrow)
        Me.Controls.Add(Me.image_left_arrow)
        Me.Controls.Add(Me.label_page_details)
        Me.Controls.Add(Me.label_total_items)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.label_page_guide)
        Me.Name = "PageNavigator"
        Me.Size = New System.Drawing.Size(675, 36)
        CType(Me.image_right_arrow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.image_left_arrow, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents label_page_guide As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents label_total_items As System.Windows.Forms.Label
    Friend WithEvents label_page_details As System.Windows.Forms.Label
    Friend WithEvents image_left_arrow As System.Windows.Forms.PictureBox
    Friend WithEvents image_right_arrow As System.Windows.Forms.PictureBox

End Class
