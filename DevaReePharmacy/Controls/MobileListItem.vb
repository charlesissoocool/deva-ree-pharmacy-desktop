﻿Public Class MobileListItem

    Private _image_key As String = ""
    Private _index As Integer = 0

    Private Sub MobileListItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click, label_text.Click, label_sub_text.Click, picture_icon.Click

        RaiseEvent Clicked(sender, Nothing)

    End Sub

    'Rendering and Initialization

    Public Sub AdjustObjects() Handles Me.Resize

        picture_icon.Top = (Me.ClientSize.Height / 2) - (picture_icon.Height / 2)
        label_text.Top = (Me.ClientSize.Height / 2) - (((label_text.Height + label_sub_text.Height)) / 2)
        label_sub_text.Top = label_text.Bottom

    End Sub

    Public Sub AdjustInterface() Handles Me.Load

        Me.Cursor = Cursors.Hand
        Me.BackColor = Me.Parent.BackColor
        Me.ForeColor = Me.Parent.ForeColor
        label_text.Font = Me.Font
        label_sub_text.Font = Me.Font

    End Sub

    'Events

    Public Shadows Event TextChanged()
    Public Shadows Event ImageKeyChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event Clicked(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Shadows Event ItemMouseOver(ByVal sender As Object, ByVal e As System.EventArgs)

    'Properties

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Shadows Property Text As String
        Get
            Text = label_text.Text
        End Get
        Set(ByVal value As String)
            label_text.Text = value
            AdjustObjects()
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Shadows Property SubText As String
        Get
            SubText = label_sub_text.Text
        End Get
        Set(ByVal value As String)
            label_sub_text.Text = value
            AdjustObjects()
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property HasIcon As Boolean
        Get
            HasIcon = picture_icon.Visible
        End Get
        Set(ByVal value As Boolean)
            picture_icon.Visible = value
            If picture_icon.Visible Then
                label_text.Left = picture_icon.Right + 3
            Else
                label_text.Left = picture_icon.Left
            End If
            label_text.Width = Me.ClientSize.Width - (label_text.Left + 11)
            label_sub_text.Left = label_text.Left
            label_sub_text.Width = label_text.Width
        End Set
    End Property

    Public ReadOnly Property Index As Integer
        Get
            Index = _index
        End Get
    End Property

    Public Property AnimateSubText As Boolean
        Get
            AnimateSubText = label_sub_text.AutoAnimate
        End Get
        Set(ByVal value As Boolean)
            label_sub_text.AutoAnimate = value
            label_sub_text.AutoEllipsis = Not value
        End Set
    End Property

    Public Property ImageKey As String
        Get
            ImageKey = _image_key
        End Get
        Set(ByVal value As String)
            _image_key = value
            RaiseEvent ImageKeyChanged(Me, Nothing)
        End Set
    End Property

    Public Sub SetIcon(ByVal icon As Image)

        picture_icon.Image = icon

    End Sub

    'Subs and Functions

    Public Sub SetIndex(ByVal index As Integer)
        _index = index
    End Sub

    Private Sub MobileListItem_MouseMove(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MouseMove, picture_icon.MouseMove, label_text.MouseMove, label_sub_text.MouseMove

        RaiseEvent ItemMouseOver(sender, e)

    End Sub

    Private Sub ChangeTextFont() Handles Me.FontChanged

        label_text.Font = New Font(Me.Font, FontStyle.Bold)
        label_sub_text.Font = New Font(Me.Font, FontStyle.Regular)
        Me.Height = label_sub_text.Bottom + label_text.Top
        AdjustObjects()

    End Sub

    Private Sub ChangeTextForeColor() Handles Me.ForeColorChanged

        label_text.ForeColor = Me.ForeColor
        label_sub_text.ForeColor = Me.ForeColor

    End Sub

End Class
