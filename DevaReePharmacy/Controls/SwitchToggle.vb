﻿Public Class SwitchToggle

    Private _toggle_stat As Boolean = True

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Overrides Property Text As String
        Get
            Text = label_toggle.Text
        End Get
        Set(ByVal value As String)
            label_toggle.Text = value
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property DisplayStatus As Boolean
        Get
            DisplayStatus = label_status.Visible
        End Get
        Set(ByVal value As Boolean)
            label_status.Visible = value
            AdjustObjects()
        End Set
    End Property

    <System.ComponentModel.Browsable(True), System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Visible)>
    Public Property Checked As Boolean
        Get
            Checked = _toggle_stat
        End Get
        Set(ByVal value As Boolean)
            _toggle_stat = value
            Dim toggle_image As Image = If(_toggle_stat, My.Resources.ResourceManager.GetObject("toggle_on"), My.Resources.ResourceManager.GetObject("toggle_off"))
            image_toggle.Image = toggle_image
            label_status.Text = If(_toggle_stat, "On", "Off")
        End Set
    End Property

    Private Sub label_toggle_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_toggle.TextChanged

        AdjustObjects()
        'Me.Width = label_toggle.Right + image_toggle.Left

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize, Me.Load

        image_toggle.Top = (Me.ClientSize.Height / 2) - (image_toggle.Height / 2)
        If label_status.Visible Then
            label_toggle.Top = (Me.ClientSize.Height / 2) - ((label_toggle.Height + label_status.Height) / 2)
            label_status.Top = label_toggle.Bottom
        Else
            label_toggle.Top = (Me.ClientSize.Height / 2) - (label_toggle.Height / 2)
        End If
        label_toggle.Left = image_toggle.Right + 5
        label_status.Left = label_toggle.Left

    End Sub

    Private Sub image_toggle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles image_toggle.Click

        Me.Checked = Not Me.Checked

    End Sub
End Class
