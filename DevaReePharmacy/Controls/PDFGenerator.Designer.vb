﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PDFGenerator
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.browser_preview = New System.Windows.Forms.WebBrowser()
        Me.dialog_printer_friendly = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'browser_preview
        '
        Me.browser_preview.Dock = System.Windows.Forms.DockStyle.Fill
        Me.browser_preview.Location = New System.Drawing.Point(0, 0)
        Me.browser_preview.MinimumSize = New System.Drawing.Size(20, 20)
        Me.browser_preview.Name = "browser_preview"
        Me.browser_preview.Size = New System.Drawing.Size(374, 282)
        Me.browser_preview.TabIndex = 0
        '
        'PDFGenerator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.browser_preview)
        Me.Name = "PDFGenerator"
        Me.Size = New System.Drawing.Size(374, 282)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents browser_preview As System.Windows.Forms.WebBrowser
    Friend WithEvents dialog_printer_friendly As System.Windows.Forms.SaveFileDialog

End Class
