﻿Imports System.IO

Module module_common

    Public Const DATE_YYMMDD = "%Y-%m-%d" '2014-03-18
    Public Const DATE_HUMAN = "%M %e, %Y" 'March 18, 2014
    Public Const DATE_HUMAN_WITH_TIME = DATE_HUMAN & " %h:%i %p" 'March 18, 2014 12:00 AM
    Public Const DATE_YYMMDDHHMMSS = "%Y-%m-%d %h:%i:%s %p" '2014-03-18 8:00:00
    Public Const DATE_YYMMDDHHMMSSFILE = "%Y-%m-%d-%h-%i-%s"

    'Date Object to String format
    Public Const DTS_YYYYMMDD = "yyyy-MM-dd"
    Public Const DTS_YYYYMMDDHHmmSS = "yyyy-MM-dd HH:mm:ss"
    Public Const DTS_HUMAN = "MMMM dd, yyyy"
    Public Const DTS_HUMAN_WITH_TIME = DTS_HUMAN & " hh:mm tt"

    Public Const TR_RETAIL = "Retail"
    Public Const TR_WHOLESALE = "Wholesale"
    Public Const TR_DISTRIBUTION = "Distribution"

    'System default config. These values will be replaced with the contents in config.ini."
    Public SYS_DB_HOST = ""
    Public SYS_DB_NAME = ""
    Public SYS_DB_USER = ""
    Public SYS_DB_PASS = ""
    Public SYS_CURRENCY = "P"
    Public SYS_ERROR_MESSAGE = "Ops! Something went wrong. Please try again. If the error persists, please contact system support."
    Public SYS_DEBUG = True
    Public SYS_DECIMAL_STOCKS = False
    Public SYS_PAGE_SIZE = 2
    Public SYS_CUSTOMER_NAME = "Demo"
    Public SYS_PDF_GENERATOR = ""
    Public SYS_PDF_GENERATOR_PARAMETERS = ""
    Public SYS_PDF_PRINTER = ""
    Public SYS_PDF_PRINTER_PARAMETERS = ""
    Public SYS_PDF_ADDITIONAL_HEADER = ""
    Public SYS_PACK_ALIAS = "pack"
    Public SYS_PAD_ALIAS = "pad"
    Public SYS_LOGO As Image = My.Resources.ipos_logo
    Public SYS_CRITICAL_STOCK_FLAG = 1000
    Public SYS_EXPIRY_STOCK_FLAG = 300
    Public SYS_CREDIT_DUE_FLAG = 100
    Public SYS_BARCODE_FONT = ""
    Public SYS_BARCODE_SIZE = 66
    Public SYS_BARCODE_DELIMITER = ""

    Public Sub ResizeListViewColumn(ByRef list_object As ListView, ByVal col_width() As Double)

        If col_width.Count <> list_object.Columns.Count Then Exit Sub
        Dim index As Integer = 0
        For Each column As ColumnHeader In list_object.Columns
            column.Width = (col_width(index) / 100) * list_object.Width
            index += 1
        Next

    End Sub

    Public Function ClearNumberFormat(ByVal formattedString As String) As String

        ClearNumberFormat = vbNullString
        For i = 1 To Len(formattedString)
            If IsNumeric(Mid(formattedString, i, 1)) Or Mid(formattedString, i, 1) = "." Then
                ClearNumberFormat = ClearNumberFormat & Mid(formattedString, i, 1)
            End If
        Next

    End Function

    Public Sub DumpVariable(ByVal dumpObject As Object, Optional ByVal dumpName As String = "object")

        'This function is for debugging purposes only. This will not be used in any of the system's data
        'processing.

        Debug.Print(vbCrLf & vbCrLf & "Starting dump on " & dumpName & "...")

        Try
            'Dictionary Array
            If dumpObject.GetType() Is GetType(Dictionary(Of String, Object)()) Then
                Debug.Print("Type: Dictionary Array")
                Dim loopIndex As Integer = 0
                Dim subLoopIndex As Integer = 0
                Dim content As String
                For Each objData As Dictionary(Of String, Object) In dumpObject
                    Debug.Print("Array [" & loopIndex & "]")
                    content = ""
                    subLoopIndex = 0
                    For Each data As Object In objData
                        If IsDBNull(data) Then data = "NULL" 'Change to string NULL to avoid errors in printing

                        If Not content = "" Then
                            content = content & vbCrLf
                        End If

                        If IsDBNull(data.Value) Then
                            content = content & vbTab & "[" & data.Key() & "] => NULL"
                        Else
                            content = content & vbTab & "[" & data.Key() & "] => " & data.Value
                        End If

                        subLoopIndex = subLoopIndex + 1
                    Next
                    Debug.Print(content)
                    loopIndex = loopIndex + 1
                Next
            ElseIf dumpObject.GetType() Is GetType(Dictionary(Of String, Object)) Then
                Debug.Print("Type: Dictionary")
                Dim content As String = ""
                Dim subLoopIndex As Integer = 0
                For Each data As Object In dumpObject
                    If IsDBNull(data) Then data = "NULL" 'Change to string NULL to avoid errors in printing

                    If Not content = "" Then
                        content = content & vbCrLf
                    End If

                    If IsDBNull(data.Value) Then
                        content = content & vbTab & "[" & data.Key() & "] => NULL"
                    Else
                        content = content & vbTab & "[" & data.Key() & "] => " & data.Value
                    End If

                    subLoopIndex = subLoopIndex + 1
                Next
                Debug.Print(content)
            Else
                Debug.Print("Unrecognized object type. Dump aborted.")
            End If
        Catch ex As Exception
            Debug.Print("Error: " & ex.Message & ". Dump aborted.")
        End Try

        Debug.Print("Dump ended." & vbCrLf & vbCrLf)

    End Sub

    Public Sub ValidateNumericTextBox(ByVal textObject As TextBox, Optional ByVal allowDecimal As Boolean = True, Optional ByVal allowSigns As Boolean = False, Optional ByVal allowPercent As Boolean = False)

        If Trim(textObject.Text) = "" Then Exit Sub

        Dim encounteredPeriod As Boolean = False
        Dim newText As String = ""
        Dim cursorPosition As Integer = textObject.SelectionStart

        For i As Integer = 1 To Len(textObject.Text)
            If Not IsNumeric(Mid(textObject.Text, i, 1)) And Not Mid(textObject.Text, i, 1) = "." And Not (i = 1 And allowSigns And (Mid(textObject.Text, i, 1) = "-" Or Mid(textObject.Text, i, 1) = "+")) And Not (i = Len(textObject.Text) And allowPercent And Mid(textObject.Text, i, 1) = "%") Then
                If cursorPosition <= i Then cursorPosition = cursorPosition - 1
                Continue For
                'newText = newText & Mid(textObject.Text, i, 1)
            Else
                If Mid(textObject.Text, i, 1) = "." Then
                    If Not allowDecimal Then Continue For
                    If encounteredPeriod = True Then Continue For
                    newText = newText & "."
                    encounteredPeriod = True
                Else
                    newText = newText & Mid(textObject.Text, i, 1)
                End If
            End If
        Next

        textObject.Text = newText
        If Not Trim(newText) = "" And cursorPosition >= 0 Then textObject.SelectionStart = cursorPosition

    End Sub

    Public Function InputQuantity(ByVal parentForm As Form, Optional ByVal title As String = "Quantity", Optional ByVal defaultValue As Double = 0, Optional ByVal increment As Double = 1) As Double

        Dim quantity_form As New form_quantity
        InputQuantity = quantity_form.OpenQuantityDialog(parentForm, title, defaultValue, increment)

    End Function

    Public Function ReadConfigFile() As Boolean

        Try
            If File.Exists("config.ini") Then
                Dim f_hdl As New StreamReader("config.ini")
                While Not f_hdl.EndOfStream
                    Dim line = f_hdl.ReadLine
                    If Left(line, 1) = "#" Then Continue While
                    Dim separator() As Char = {"="}
                    Dim chunks() = line.Split(separator, 2)
                    chunks(UBound(chunks)) = chunks.Last.Replace("<app_path>", Application.StartupPath)
                    Select Case chunks.First
                        Case "db_name"
                            SYS_DB_NAME = chunks.Last
                        Case "db_host"
                            SYS_DB_HOST = chunks.Last
                        Case "db_pass"
                            SYS_DB_PASS = chunks.Last
                        Case "db_user"
                            SYS_DB_USER = chunks.Last
                        Case "page_size"
                            SYS_PAGE_SIZE = Val(chunks.Last)
                        Case "currency"
                            SYS_CURRENCY = chunks.Last
                        Case "debug_mode"
                            SYS_DEBUG = chunks.Last.ToLower = "true"
                        Case "decimal_stocks"
                            SYS_DECIMAL_STOCKS = chunks.Last.ToLower = "true"
                        Case "customer"
                            SYS_CUSTOMER_NAME = chunks.Last
                        Case "pdf_converter"
                            SYS_PDF_GENERATOR = chunks.Last
                        Case "pdf_converter_parameters"
                            SYS_PDF_GENERATOR_PARAMETERS = chunks.Last
                        Case "pdf_printer"
                            SYS_PDF_PRINTER = chunks.Last
                        Case "pdf_printer_parameters"
                            SYS_PDF_PRINTER_PARAMETERS = chunks.Last
                        Case "pdf_additional_header"
                            SYS_PDF_ADDITIONAL_HEADER = chunks.Last
                        Case "pack_alias"
                            SYS_PACK_ALIAS = chunks.Last
                        Case "pad_alias"
                            SYS_PAD_ALIAS = chunks.Last
                        Case "critical_stock_flag"
                            SYS_CRITICAL_STOCK_FLAG = CInt(Val(chunks.Last))
                        Case "expiry_stock_flag"
                            SYS_EXPIRY_STOCK_FLAG = CInt(Val(chunks.Last))
                        Case "credit_due_flag"
                            SYS_CREDIT_DUE_FLAG = CInt(Val(chunks.Last))
                        Case "barcode_font"
                            SYS_BARCODE_FONT = chunks.Last
                        Case "barcode_size"
                            SYS_BARCODE_SIZE = chunks.Last
                        Case "barcode_delimiter"
                            SYS_BARCODE_DELIMITER = chunks.Last
                    End Select
                End While
                f_hdl.Close()
                If File.Exists("logo.png") Then SYS_LOGO = Image.FromFile("logo.png")
                ReadConfigFile = True
            Else
                Throw New Exception("File not found.")
            End If
        Catch ex As Exception
            ReadConfigFile = False
        End Try

    End Function

    Public Sub InvalidateTextBox(ByRef text_box_obj As TextBox)

        text_box_obj.BackColor = module_colors.color_red
        text_box_obj.ForeColor = Color.White

    End Sub

    Public Sub ValidateTextBox(ByRef text_box_obj As TextBox)

        text_box_obj.BackColor = Color.White
        text_box_obj.ForeColor = Color.Black

    End Sub

    Public Function RoundUp(ByVal value As Double, ByVal decimals As Integer) As Double

        Return Math.Ceiling(value * (10 ^ decimals)) / (10 ^ decimals)

    End Function

    Public Function IsFontInstalled(ByVal FontName As String) As Boolean
        Using TestFont As Font = New Font(FontName, 10)
            Return CBool(String.Compare(FontName, TestFont.OriginalFontName, StringComparison.InvariantCultureIgnoreCase) = 0)
        End Using
    End Function

End Module

Public Enum OrderMode
    NoMode = 0
    Retail = 1
    Wholesale = 2
    Distribution = 3
End Enum

Public Enum ItemUnit
    NoUnit = 0
    Unit = 1
    Pack = 2
    Pad = 3
End Enum

Public Enum OrderType
    NoType = 0
    Transaction = 1
    Dispatch = 2
End Enum
