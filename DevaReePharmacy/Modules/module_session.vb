﻿Module module_session

    Public current_user_profile As User = Nothing

    Public Sub InitiateSessionVariables()

        ClearSessionVariables()

    End Sub

    Public Sub ClearSessionVariables()

        current_user_profile = New User

    End Sub

End Module
