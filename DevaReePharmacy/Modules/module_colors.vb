﻿Imports System.Drawing

Module module_colors

    Public color_light_red As Color = System.Drawing.ColorTranslator.FromHtml("#e74d3d")
    Public color_red As Color = System.Drawing.ColorTranslator.FromHtml("#b81427")
    Public color_pink As Color = System.Drawing.ColorTranslator.FromHtml("#fe6464")
    Public color_light_blue As Color = System.Drawing.ColorTranslator.FromHtml("#6ebed7")
    Public color_dark_blue As Color = System.Drawing.ColorTranslator.FromHtml("#2a80b9")
    Public color_blue As Color = System.Drawing.ColorTranslator.FromHtml("#445ca2")
    Public color_yellow As Color = System.Drawing.ColorTranslator.FromHtml("#f2c411")
    Public color_green As Color = System.Drawing.ColorTranslator.FromHtml("#63b448")
    Public color_light_green As Color = System.Drawing.ColorTranslator.FromHtml("#71bb58")
    Public color_pale_green As Color = System.Drawing.ColorTranslator.FromHtml("#c3e2a7")
    Public color_gray As Color = System.Drawing.ColorTranslator.FromHtml("#696969")
    Public color_black As Color = System.Drawing.ColorTranslator.FromHtml("#202020")
    Public color_cool_gray As Color = System.Drawing.ColorTranslator.FromHtml("#3c3f43")
    Public color_dark_gray As Color = System.Drawing.ColorTranslator.FromHtml("#2d3033")

    Public color_button_blue As Color = System.Drawing.ColorTranslator.FromHtml("#298bc1")
    Public color_button_green As Color = System.Drawing.ColorTranslator.FromHtml("#5db0a5")

    Public Const PM_NEUTRAL = 4
    Public Const PM_INFO = 0
    Public Const PM_SUCCESS = 1
    Public Const PM_WARNING = 2
    Public Const PM_CRITICAL = 3

End Module
