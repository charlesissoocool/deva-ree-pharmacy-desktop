﻿Imports System.IO
Imports MySql.Data.MySqlClient

Module module_db_connection

    Public db_connection As MySqlConnection
    Public db_ready As Boolean
    Private _db_die As Boolean = False

    Public ReadOnly Property Connected As Boolean
        Get
            Connected = If(db_connection.State = ConnectionState.Open And db_connection.Ping, True, False)
        End Get
    End Property

    Private Sub ConnectionStatusChanged(ByVal sender As Object, ByVal e As System.Data.StateChangeEventArgs)

        db_ready = e.CurrentState = ConnectionState.Open

        If (e.OriginalState = ConnectionState.Open) And (e.CurrentState = ConnectionState.Closed Or e.CurrentState = ConnectionState.Broken) And Application.OpenForms.Count > 0 And Not _db_die Then
            OpenConnectionNotice()
        End If

    End Sub

    Public Sub DisconnectDatabase()

        Try
            _db_die = True
            db_connection.Close()
            db_connection.Dispose()
        Catch ex As Exception

        End Try

    End Sub

    Public Sub ConnectToDatabase()

        db_connection = New MySqlConnection
        Try
            AddHandler db_connection.StateChange, AddressOf ConnectionStatusChanged
            db_connection.ConnectionString = "Server=" & SYS_DB_HOST & ";Uid=" & SYS_DB_USER & ";Pwd=" & SYS_DB_PASS & ";Database=" & SYS_DB_NAME & ";AllowUserVariables=true;"
            db_connection.Open()
            If db_connection.Ping Then CloseConnectionNotice()

        Catch connectionError As MySql.Data.MySqlClient.MySqlException
            Debug.Print("Failed to connect to database. Reason: " & connectionError.Message & ".")
            'MsgBox("" & connectionError.Message & "")
            OpenConnectionNotice()

        End Try
    End Sub

    Private Sub CloseConnectionNotice()

        For Each open_form In Application.OpenForms()
            If TypeOf open_form Is form_connection_notice Then
                open_form.Visible = False
                open_form.ForceClose()
            End If
        Next

    End Sub

    Private Sub OpenConnectionNotice()

        If Application.OpenForms().OfType(Of form_connection_notice).Any() Then
            Application.OpenForms().OfType(Of form_connection_notice).First.Focus()
        Else
            Dim notice_form As New form_connection_notice
            notice_form.ShowDialog()
        End If

    End Sub

End Module
