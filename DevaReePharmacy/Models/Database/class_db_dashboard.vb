﻿Public Class class_db_dashboard
    Inherits class_db

    Public Function GetCriticalProductsCount() As Integer

        Dim result = SelectQuery("SELECT count(*) as total_critical FROM view_product_details vpd LEFT JOIN view_product_stocks_inventory vps ON vps.product_id = vpd.product_id WHERE vps.stock_quantity <= " & SYS_CRITICAL_STOCK_FLAG & " OR ISNULL(stock_quantity)")
        If IsNothing(result) Then
            GetCriticalProductsCount = 0
        Else
            GetCriticalProductsCount = Val(result.First()("total_critical"))
        End If

    End Function

    Public Function GetCriticalProducts(Optional ByVal start As Integer = 0, Optional ByVal limit As Integer = 50) As Dictionary(Of String, Object)()

        GetCriticalProducts = SelectQuery("SELECT vpd.product_name, vps.stock_quantity FROM view_product_details vpd LEFT JOIN view_product_stocks_inventory vps ON vpd.product_id = vps.product_id WHERE stock_quantity <= " & SYS_CRITICAL_STOCK_FLAG & " OR ISNULL(stock_quantity) LIMIT " & start & "," & limit)
        
    End Function

    Public Function GetExpiringProductCount() As Integer

        Dim result = SelectQuery("SELECT count(*) as total_expiring FROM view_product_expiry WHERE DATEDIFF(expiry_date, DATE(NOW())) <= " & SYS_EXPIRY_STOCK_FLAG)
        If IsNothing(result) Then
            GetExpiringProductCount = 0
        Else
            GetExpiringProductCount = Val(result.First()("total_expiring"))
        End If

    End Function

    Public Function GetExpiringProducts(Optional ByVal start As Integer = 0, Optional ByVal limit As Integer = 50) As Dictionary(Of String, Object)()

        GetExpiringProducts = SelectQuery("SELECT *, DATE_FORMAT(expiry_date, '" & DATE_HUMAN & "') as expiry_date_formatted FROM view_product_expiry WHERE DATEDIFF(expiry_date, DATE(NOW())) <= " & SYS_EXPIRY_STOCK_FLAG & " LIMIT " & start & "," & limit)

    End Function

    Public Function GetCreditDues(Optional ByVal start As Integer = 0, Optional ByVal limit As Integer = 50) As Dictionary(Of String, Object)()

        GetCreditDues = SelectQuery("SELECT customer_name, credit_id, (credit_amount - total_payed) as total_balance, DATEDIFF(DATE(due_date), DATE(NOW())) as due_date_days FROM view_credit_details WHERE DATEDIFF(DATE(due_date), DATE(NOW())) < " & SYS_CREDIT_DUE_FLAG & " AND credit_amount > total_payed ORDER BY DATEDIFF(DATE(due_date), DATE(NOW())) ASC LIMIT " & start & "," & limit)

    End Function

    Public Function GetCreditDuesCount() As Integer

        Dim result = SelectQuery("SELECT count(*) as total_dues FROM view_credit_details WHERE DATEDIFF(DATE(due_date), DATE(NOW())) < " & SYS_CREDIT_DUE_FLAG & " AND credit_amount > total_payed")
        If IsNothing(result) Then
            GetCreditDuesCount = 0
        Else
            GetCreditDuesCount = result.First()("total_dues")
        End If

    End Function

    Public Function GetProductStocks(Optional ByVal search_tag As String = Nothing, Optional ByVal product_filters() As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1, Optional ByVal sort_field As String = "entry_date", Optional ByVal sort_dir As String = "DESC") As Dictionary(Of String, Object)()

        Dim query As String = "SELECT vpd.*, vps.stock_quantity FROM view_product_details vpd LEFT JOIN view_product_stocks_inventory vps ON vpd.product_id = vps.product_id"
        Dim condition As String = ""
        If Not IsNothing(search_tag) Then condition = " WHERE (vpd.product_name LIKE '%" & search_tag & "%' OR vpd.product_code LIKE '%" & search_tag & "%')"
        If Not IsNothing(product_filters) Then
            Dim filter As String = ""
            For Each product_filter In product_filters
                filter = filter & If(filter = "", "'" & product_filter & "'", ",'" & product_filter & "'")
            Next
            If condition = "" Then
                condition &= " WHERE vpd.product_code IN (" & filter & ")"
            Else
                condition &= " AND vpd.product_code IN (" & filter & ")"
            End If
        End If
        query = query & condition
        query &= " ORDER BY " & sort_field & " " & sort_dir
        If Not (offset = 0 And count = 0) Then query &= " LIMIT " & offset & ", " & count
        GetProductStocks = SelectQuery(query)

    End Function

    Public Function GetProductStocksCount(Optional ByVal search_tag As String = Nothing, Optional ByVal product_filters() As String = Nothing) As Integer

        Dim query As String = "SELECT count(*) as total_count FROM view_product_details vpd LEFT JOIN view_product_stocks_inventory vps ON vpd.product_id = vps.product_id"
        Dim condition As String = ""
        If Not IsNothing(search_tag) Then condition = " WHERE (vpd.product_name LIKE '%" & search_tag & "%' OR vpd.product_code LIKE '%" & search_tag & "%')"
        If Not IsNothing(product_filters) Then
            Dim filter As String = ""
            For Each product_filter In product_filters
                filter = filter & If(filter = "", "'" & product_filter & "'", ",'" & product_filter & "'")
            Next
            If condition = "" Then
                condition &= " WHERE vpd.product_code IN (" & filter & ")"
            Else
                condition &= " AND vpd.product_code IN (" & filter & ")"
            End If
        End If
        query = query & condition
        Dim result = SelectQuery(query)
        If IsNothing(result) Then
            GetProductStocksCount = 0
        Else
            GetProductStocksCount = result.First()("total_count")
        End If

    End Function

    Public Function GetTotalTransactionsByStaff(ByVal staff_id As Integer) As Integer

        Dim transactions = SelectQuery("SELECT COUNT(*) as total_transaction FROM transaction_details WHERE td_staff_id = " & staff_id & " AND DATE(td_transaction_date) = DATE(NOW())")
        If IsNothing(transactions) Then
            GetTotalTransactionsByStaff = 0
        Else
            GetTotalTransactionsByStaff = transactions.First()("total_transaction")
        End If

    End Function

    Public Function GetTotalTransactions() As Integer

        Dim transactions = SelectQuery("SELECT COUNT(*) as total_transaction FROM transaction_details WHERE DATE(td_transaction_date) = DATE(NOW())")
        If IsNothing(transactions) Then
            GetTotalTransactions = 0
        Else
            GetTotalTransactions = transactions.First()("total_transaction")
        End If

    End Function

    Public Function GetHourlyLossForToday() As Dictionary(Of String, Object)()

        GetHourlyLossForToday = SelectQuery("SELECT HOUR(td.td_transaction_date) as hour, SUM(( (CEIL((ti.ti_selling_price / pd.pd_packing_size) * 100) / 100) - (CEIL((ps.ps_supplier_price / pd.pd_packing_size) * 100)) / 100) * ti.ti_item_quantity) as loss FROM transaction_items ti INNER JOIN transaction_details td ON td.td_transaction_id = ti.ti_transaction_id INNER JOIN product_stocks ps ON ps.ps_stock_id = ti.ti_stock_id INNER JOIN product_details pd ON pd.pd_product_id = ti.ti_product_id WHERE DATE(td.td_transaction_date) = DATE(NOW()) AND (ti.ti_selling_price / pd.pd_packing_size) < (ps.ps_supplier_price / pd.pd_packing_size) GROUP BY HOUR(td.td_transaction_date)")

    End Function

    Public Function GetHourlyProfitForToday() As Dictionary(Of String, Object)()

        GetHourlyProfitForToday = SelectQuery("SELECT HOUR(td.td_transaction_date) as hour, SUM(( (CEIL((ti.ti_selling_price / pd.pd_packing_size) * 100) / 100) - (CEIL((ps.ps_supplier_price / pd.pd_packing_size) * 100) / 100) ) * ti.ti_item_quantity) as profit FROM transaction_items ti INNER JOIN transaction_details td ON td.td_transaction_id = ti.ti_transaction_id INNER JOIN product_stocks ps ON ps.ps_stock_id = ti.ti_stock_id INNER JOIN product_details pd ON pd.pd_product_id = ti.ti_product_id WHERE DATE(td.td_transaction_date) = DATE(NOW()) AND (ti.ti_selling_price / pd.pd_packing_size) > (ps.ps_supplier_price / pd.pd_packing_size) GROUP BY HOUR(td.td_transaction_date)")

    End Function

    Public Function GetHourlyGrossForToday() As Dictionary(Of String, Object)()

        GetHourlyGrossForToday = SelectQuery("SELECT HOUR(td.td_transaction_date) as hour, SUM((CEIL((ti.ti_selling_price / pd.pd_packing_size) * 100) / 100) * ti.ti_item_quantity) as gross FROM transaction_items ti INNER JOIN transaction_details td ON td.td_transaction_id = ti.ti_transaction_id INNER JOIN product_details pd ON pd.pd_product_id = ti.ti_product_id WHERE DATE(td.td_transaction_date) = DATE(NOW()) GROUP BY HOUR(td.td_transaction_date)")

    End Function

    Public Function GetTotalGrossDaily(Optional ByVal staff_id As Integer = 0, Optional ByVal report_date As Date = Nothing) As Double

        Dim temp_date As String = If(report_date = Date.MinValue, "NOW()", "'" & report_date.ToString(DTS_YYYYMMDD) & "'")
        Dim temp_staff As String = If(staff_id = 0, "", " AND td_staff_id = " & staff_id)
        Dim query = "SELECT SUM((CEIL((ti.ti_selling_price / pd.pd_packing_size) * 100) / 100) * ti.ti_item_quantity) as gross FROM transaction_items ti INNER JOIN transaction_details td ON td.td_transaction_id = ti.ti_transaction_id INNER JOIN product_details pd ON pd.pd_product_id = ti.ti_product_id WHERE DATE(td.td_transaction_date) = DATE(" & temp_date & ") " & temp_staff
        Dim result = SelectQuery(query)
        If IsNothing(result) Then
            GetTotalGrossDaily = 0
        Else
            GetTotalGrossDaily = Val(result.First()("gross"))
        End If

    End Function

    Public Function GetTotalCash(Optional ByVal staff_id As Integer = 0, Optional ByVal report_date As Date = Nothing) As Double

        Dim daily_gross = GetTotalGrossDaily(staff_id, report_date)
        Dim temp_date As String = If(report_date = Date.MinValue, "NOW()", "'" & report_date.ToString(DTS_YYYYMMDD) & "'")
        Dim temp_staff As String = If(staff_id = 0, "", " AND td_staff_id = " & staff_id)
        Dim result = SelectQuery("SELECT IFNULL(SUM(cd_credit_amount), 0) as total_credit FROM credit_details cd LEFT JOIN transaction_details td ON td.td_credit_id = cd.cd_credit_id WHERE DATE(cd_created_date) = " & temp_date & temp_staff)
        If IsNothing(result) Then
            GetTotalCash = daily_gross
        Else
            GetTotalCash = daily_gross - result.First()("total_credit")
        End If
        temp_staff = If(staff_id = 0, "", " AND cp_staff_id = " & staff_id)
        result = SelectQuery("SELECT IFNULL(SUM(cp_payment_amount), 0) as total_payments FROM credit_payments WHERE DATE(cp_payment_date) = " & temp_date & temp_staff)
        If Not IsNothing(result) Then
            GetTotalCash = GetTotalCash + result.First()("total_payments")
        End If

    End Function

End Class
