﻿Public Class class_db_user
    Inherits class_db

    Private _user_permissions() As String = {"Admin", "Cashier", "Stocks", "Products", "Agent", "Finance"}

    Public Function AuthenticateUserByID(ByVal user_id As Integer, ByVal user_pass As String) As Boolean

        EscapeStr(user_pass)
        Dim user_credential = SelectQuery("SELECT * FROM view_staff_credentials WHERE staff_id = " & user_id & " AND password = SHA1('" & user_pass & "')")
        AuthenticateUserByID = If(IsNothing(user_credential), False, True)

    End Function

    Public Function GetImplodedUserPermissions(ByVal user_id As Integer) As String

        GetImplodedUserPermissions = ""
        Dim results = GetUserPermissions(user_id)
        If Not IsNothing(results) Then
            For Each result In results
                GetImplodedUserPermissions &= If(GetImplodedUserPermissions = "", result("permission_type"), ", " & result("permission_type"))
            Next
        End If

    End Function

    Public Function GetUserPermissions(ByVal user_id As Integer) As Dictionary(Of String, Object)()

        Dim user_permissions = SelectQuery("SELECT permission_type FROM view_staff_permissions WHERE staff_id = " & user_id)
        If Not IsNothing(user_permissions) Then
            GetUserPermissions = user_permissions
        Else
            GetUserPermissions = Nothing
        End If

    End Function

    Public Function DoesUserHavePermission(ByVal permission_type As String, ByVal user_id As Integer) As Boolean
        EscapeStr(permission_type)
        If Not _user_permissions.Contains(permission_type) Then
            DoesUserHavePermission = False
        Else
            Dim result = SelectQuery("SELECT count(*) as does_exist FROM view_staff_permissions WHERE permission_type = '" & permission_type & "' AND staff_id = " & user_id)
            If IsNothing(result) Then
                DoesUserHavePermission = False
            Else
                DoesUserHavePermission = result.First()("does_exist") > 0
            End If
        End If
    End Function

    Public Function DeleteUserPermission(ByVal permission_type As String, ByVal user_id As Integer) As Boolean

        EscapeStr(permission_type)
        If Not _user_permissions.Contains(permission_type) Then
            DeleteUserPermission = False
        Else
            If Not DoesUserHavePermission(permission_type, user_id) Then
                DeleteUserPermission = True
            Else
                DeleteUserPermission = DoQuery("DELETE FROM staff_permissions WHERE sp_staff_id = " & user_id & " AND sp_permission_type = '" & permission_type & "' LIMIT 1") > 0
            End If
        End If

    End Function

    Public Function AddUserPermission(ByVal permission_type As String, ByVal user_id As Integer, Optional ByVal grant_date As Date = Nothing) As Boolean

        EscapeStr(permission_type)
        If Not _user_permissions.Contains(permission_type) Then
            AddUserPermission = False
        Else
            If DoesUserHavePermission(permission_type, user_id) Then
                AddUserPermission = True
            Else
                Dim temp_date As String = If(grant_date = Date.MinValue, "NOW()", "'" & grant_date.ToString(DTS_YYYYMMDD) & "'")
                AddUserPermission = DoQuery("INSERT INTO staff_permissions (sp_staff_id, sp_permission_type, sp_date_granted) VALUES (" & user_id & ", '" & permission_type & "', " & temp_date & ")") > 0
            End If
        End If

    End Function

    Public Function GetUserListCount(Optional ByVal search_tag As String = Nothing) As Integer

        EscapeStr(search_tag)
        Dim query As String = "SELECT count(*) as user_count FROM view_staff_credentials"
        If Not IsNothing(search_tag) Then query &= " WHERE first_name LIKE '%" & search_tag & "%' OR last_name LIKE '%" & search_tag & "%'"
        Dim result = SelectQuery(query)
        If IsNothing(result) Then
            GetUserListCount = 0
        Else
            GetUserListCount = result.First()("user_count")
        End If

    End Function

    Public Function GetUserList(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1) As Dictionary(Of String, Object)()

        EscapeStr(search_tag)
        count = If(count = -1, SYS_PAGE_SIZE, count)
        Dim query As String = "SELECT * FROM view_staff_credentials"
        If Not IsNothing(search_tag) Then query &= " WHERE first_name LIKE '%" & search_tag & "%' OR last_name LIKE '%" & search_tag & "%'"
        If Not (offset = 0 And count = 0) Then query &= " LIMIT " & offset & ", " & count
        Dim user_list = SelectQuery(query)
        If Not IsNothing(user_list) Then
            GetUserList = user_list
        Else
            GetUserList = Nothing
        End If

    End Function

    Public Function GetUserCredential(ByVal user_id As Integer) As Dictionary(Of String, Object)

        Dim user_credential = SelectQuery("SELECT * FROM view_staff_credentials WHERE staff_id = " & user_id)
        GetUserCredential = If(IsNothing(user_credential), Nothing, user_credential.First())

    End Function

    Public Function GetUserIDByCredential(ByVal user_name As String, ByVal user_pass As String) As Integer

        EscapeStr(user_name)
        EscapeStr(user_pass)
        Dim user_id = SelectQuery("SELECT * FROM view_staff_credentials WHERE username = '" & user_name & "' AND password = SHA1('" & user_pass & "') LIMIT 1")
        If Not IsNothing(user_id) Then
            GetUserIDByCredential = user_id.First()("staff_id")
        Else
            GetUserIDByCredential = 0
        End If

    End Function

    Public Function GetUserIDByUsername(ByVal user_name As String) As Integer

        EscapeStr(user_name)
        Dim user_id = SelectQuery("SELECT staff_id FROM view_staff_credentials WHERE username = '" & user_name & "'")
        If IsNothing(user_id) Then
            GetUserIDByUsername = 0
        Else
            GetUserIDByUsername = user_id.First()("staff_id")
        End If

    End Function

    Public Function IsUserActivated(ByVal user_id As Integer) As Boolean

        Dim user_credential = SelectQuery("SELECT * FROM view_staff_credentials WHERE staff_id = " & user_id & " AND NOT ISNULL(first_name) AND NOT ISNULL(last_name)")
        If Not IsNothing(user_credential) Then
            Dim first_item = user_credential.First()
            IsUserActivated = Not IsDBNull(first_item("first_name")) And Not IsDBNull(first_item("last_name"))
        Else
            IsUserActivated = False
        End If

    End Function

    Public Function UpdateUserSecurityByID(ByVal user_id As Integer, ByVal username As String, Optional ByVal password As String = Nothing) As Boolean

        EscapeStr(username)
        EscapeStr(password)
        Dim query As String

        If IsNothing(password) Then
            query = "UPDATE staff_login SET sl_username = '" & username & "' WHERE sl_staff_id = " & user_id
        Else
            query = "UPDATE staff_login SET sl_username = '" & username & "', sl_password = SHA1('" & password & "') WHERE sl_staff_id = " & user_id
        End If
        UpdateUserSecurityByID = DoQuery(query) > 0

    End Function

    Public Function UpdateUserProfileByID(ByVal user_id As Integer, ByVal first_name As String, ByVal last_name As String, ByVal email As String) As Boolean

        EscapeStr(first_name)
        EscapeStr(last_name)
        EscapeStr(email)
        Dim query As String = ""
        If IsUserActivated(user_id) Then
            query = "UPDATE staff_profile SET sp_first_name = '" & first_name & "', sp_last_name = '" & last_name & "', sp_email = '" & email & "' WHERE sp_staff_id = " & user_id
        Else
            query = "INSERT INTO staff_profile (sp_first_name, sp_last_name, sp_email, sp_date_registered, sp_staff_id) VALUES ('" & first_name & "', '" & last_name & "', '" & email & "', NOW(), " & user_id & ")"
        End If

        UpdateUserProfileByID = DoQuery(query) > 0

    End Function

    Public Function CreateUserLogin(ByVal user_name As String, ByVal password As String) As Integer

        EscapeStr(user_name)
        EscapeStr(password)
        CreateUserLogin = DoQuery("INSERT INTO staff_login (sl_username, sl_password) VALUES ('" & user_name & "','" & password & "')")

    End Function

    Public Function DeactivateUser(ByVal user_id As Integer) As Boolean

        DeactivateUser = DoQuery("DELETE FROM staff_profile WHERE sp_staff_id = " & user_id) > 0

    End Function

    Public Function IsUserNameTaken(ByVal user_name As String) As Boolean

        EscapeStr(user_name)
        Dim result = SelectQuery("SELECT count(*) as does_exist FROM staff_login WHERE sl_username = '" & user_name & "'")
        If IsNothing(result) Then
            IsUserNameTaken = False
        Else
            IsUserNameTaken = Val(result.First()("does_exist")) > 0
        End If

    End Function

    Public Function IsEmailTaken(ByVal email As String) As Boolean

        EscapeStr(email)
        Dim result = SelectQuery("SELECT count(*) as does_exist FROM view_staff_credentials WHERE email = '" & email & "'")
        If IsNothing(result) Then
            IsEmailTaken = False
        Else
            IsEmailTaken = Val(result.First()("does_exist")) > 0
        End If
    End Function

    Function GetAgentList() As Dictionary(Of String, Object)()

        Dim results = SelectQuery("SELECT * FROM view_agent_list")
        If IsNothing(results) Then
            GetAgentList = Nothing
        Else
            GetAgentList = results
        End If

    End Function

    Public Function GetAgentIDByFullName(ByVal full_name As String) As Integer

        EscapeStr(full_name)
        Dim agent_info = SelectQuery("SELECT staff_id FROM view_agent_list WHERE CONCAT(first_name, ' ', last_name) = '" & full_name & "'")
        If IsNothing(agent_info) Then
            GetAgentIDByFullName = 0
        Else
            GetAgentIDByFullName = Val(agent_info.First()("staff_id"))
        End If

    End Function

End Class
