﻿Imports MySql.Data.MySqlClient

Public MustInherit Class class_db

    Protected PAGE_SIZE = SYS_PAGE_SIZE

    Protected Function SelectQuery(ByVal queryString As String) As Dictionary(Of String, Object)()

        If SYS_DEBUG Then Debug.Print("SQL: " & queryString)
        Dim dbCommand As MySqlCommand

        Try
            dbCommand = New MySqlCommand(queryString, module_db_connection.db_connection)
            Dim dbReader = dbCommand.ExecuteReader()
            Dim queryResult() As Dictionary(Of String, Object) = Nothing
            Dim queryRow As Integer = 0
            'Convert database result to dictionary array
            While dbReader.Read
                ReDim Preserve queryResult(queryRow)
                queryResult(queryRow) = New Dictionary(Of String, Object)
                For i = 0 To dbReader.FieldCount - 1 'Loop through all columns
                    If IsDBNull(dbReader.GetValue(i)) Then
                        Select Case dbReader.GetFieldType(i)
                            Case GetType(Integer), GetType(Double)
                                queryResult(queryRow).Add(dbReader.GetName(i).ToString(), 0)
                            Case GetType(Date)
                                queryResult(queryRow).Add(dbReader.GetName(i).ToString(), Nothing)
                            Case Else
                                queryResult(queryRow).Add(dbReader.GetName(i).ToString(), "")
                        End Select
                    Else
                        queryResult(queryRow).Add(dbReader.GetName(i).ToString(), dbReader.GetValue(i))
                    End If
                Next
                queryRow = queryRow + 1
            End While
            dbReader.Close()
            SelectQuery = queryResult

            'If Not IsNothing(SelectQuery) Then

            '    'Clean objects of null values
            '    Dim cache_query() As Dictionary(Of String, Object) = Nothing
            '    Dim new_index As Integer

            '    For Each dbData As Dictionary(Of String, Object) In SelectQuery
            '        If Not HasNullValues(dbData) Then RemoveNullValues(dbData)
            '        new_index = If(IsNothing(cache_query), 0, UBound(cache_query) + 1)
            '        ReDim Preserve cache_query(new_index)
            '        cache_query(new_index) = dbData
            '    Next
            '    SelectQuery = cache_query

            'End If

        Catch selectQueryError As Exception
            If SYS_DEBUG Then Debug.Print("Query failure. '" & queryString & "'. " & selectQueryError.Message)
            If Not module_db_connection.db_connection.Ping() Then module_db_connection.db_connection.Close()
            SelectQuery = Nothing
        End Try

    End Function

    Private Function HasNullValues(ByVal dict_obj As Dictionary(Of String, Object)) As Boolean

        For Each value As Object In dict_obj
            If IsDBNull(value) Then HasNullValues = True : Exit Function
        Next

        HasNullValues = False

    End Function

    Private Sub RemoveNullValues(ByRef dict_obj As Dictionary(Of String, Object))

        Dim new_obj As New Dictionary(Of String, Object)

        For Each values As KeyValuePair(Of String, Object) In dict_obj
            If IsDBNull(values.Value) Then
                If TypeOf values.Value Is Integer Or TypeOf values.Value Is Double Then
                    new_obj.Add(values.Key, 0)
                Else
                    new_obj.Add(values.Key, "")
                End If
            Else
                new_obj.Add(values.Key, values.Value)
            End If
        Next

        dict_obj = new_obj

    End Sub

    Protected Function DoQuery(ByVal queryString As String) As Integer

        Dim dbCommand As MySqlCommand
        Try
            dbCommand = New MySqlCommand(queryString, module_db_connection.db_connection)
            DoQuery = dbCommand.ExecuteNonQuery()
            If Not dbCommand.LastInsertedId = 0 Then DoQuery = dbCommand.LastInsertedId
        Catch doQueryError As Exception
            If SYS_DEBUG Then Debug.Print("Query failure. '" & queryString & "'. " & doQueryError.Message)
            DoQuery = 0
        End Try

        If SYS_DEBUG Then Debug.Print("Query: " & queryString & vbCrLf & "Return: " & DoQuery)


    End Function

    Public Sub DumpVariable(ByVal dumpObject As Object, Optional ByVal dumpName As String = "object")

        'This function is for debugging purposes only. This will not be used in any of the system's data
        'processing.

        Debug.Print(vbCrLf & vbCrLf & "Starting dump on " & dumpName & "...")

        Try
            'Dictionary Array
            If dumpObject.GetType() Is GetType(Dictionary(Of String, Object)()) Then
                Debug.Print("Type: Dictionary Array")
                Dim loopIndex As Integer = 0
                Dim subLoopIndex As Integer = 0
                Dim content As String
                For Each objData As Dictionary(Of String, Object) In dumpObject
                    Debug.Print("Array [" & loopIndex & "]")
                    content = ""
                    subLoopIndex = 0
                    For Each data As Object In objData
                        If IsDBNull(data) Then data = "NULL" 'Change to string NULL to avoid errors in printing

                        If Not content = "" Then
                            content = content & vbCrLf
                        End If

                        If IsDBNull(data.Value) Then
                            content = content & vbTab & "[" & data.Key() & "] => NULL"
                        Else
                            content = content & vbTab & "[" & data.Key() & "] => " & data.Value
                        End If

                        subLoopIndex = subLoopIndex + 1
                    Next
                    Debug.Print(content)
                    loopIndex = loopIndex + 1
                Next
            ElseIf dumpObject.GetType() Is GetType(Dictionary(Of String, Object)) Then
                Debug.Print("Type: Dictionary")
                Dim content As String = ""
                Dim subLoopIndex As Integer = 0
                For Each data As Object In dumpObject
                    If IsDBNull(data) Then data = "NULL" 'Change to string NULL to avoid errors in printing

                    If Not content = "" Then
                        content = content & vbCrLf
                    End If

                    If IsDBNull(data.Value) Then
                        content = content & vbTab & "[" & data.Key() & "] => NULL"
                    Else
                        content = content & vbTab & "[" & data.Key() & "] => " & data.Value
                    End If

                    subLoopIndex = subLoopIndex + 1
                Next
                Debug.Print(content)
            Else
                Debug.Print("Unrecognized object type. Dump aborted.")
            End If
        Catch ex As Exception
            Debug.Print("Error: " & ex.Message & ". Dump aborted.")
        End Try

        Debug.Print("Dump ended." & vbCrLf & vbCrLf)

    End Sub

    Public Sub EscapeStr(ByRef str As String)

        If Not IsNothing(str) Then
            str = str.Replace("'", "\'")
        End If

    End Sub

End Class
