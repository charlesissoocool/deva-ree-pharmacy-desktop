﻿Public Class class_db_transaction
    Inherits class_db

    Public Function GetStockDetailForProductCode(ByVal product_code As String) As Dictionary(Of String, Object)

        EscapeStr(product_code)
        Dim result = SelectQuery("SELECT product_id, product_name, stock_quantity FROM view_product_stocks_inventory WHERE product_code = '" & product_code & "'")
        If IsNothing(result) Then
            GetStockDetailForProductCode = Nothing
        Else
            GetStockDetailForProductCode = result.First()
        End If

    End Function

    Public Function CreateNewTransaction(ByVal transaction_type As String, ByVal invoice_no As Long, Optional ByVal dispatch_id As Integer = 0, Optional ByVal customer_id As Integer = 0, Optional ByVal staff_id As Integer = 0, Optional ByVal agent_id As Integer = 0, Optional ByVal credit_id As Integer = 0, Optional ByVal transact_date As Date = Nothing) As Integer

        EscapeStr(transaction_type)
        If Not transaction_type = TR_RETAIL And Not transaction_type = TR_WHOLESALE And Not transaction_type = TR_DISTRIBUTION Then
            CreateNewTransaction = 0
            Exit Function
        End If

        Dim temp_dispatch As String = If(dispatch_id = 0, "NULL", dispatch_id)
        Dim temp_customer As String = If(customer_id = 0, "NULL", customer_id)
        Dim temp_agent As String = If(agent_id = 0, "NULL", agent_id)
        Dim temp_credit As String = If(credit_id = 0, "NULL", credit_id)
        If staff_id = 0 Then staff_id = module_session.current_user_profile.UserID
        Dim temp_date As String = If(transact_date = Date.MinValue, "NOW()", "'" & transact_date.ToString(DTS_YYYYMMDD) & "'")
        CreateNewTransaction = DoQuery("INSERT INTO transaction_details (td_transaction_type, td_dispatch_id, td_customer_id, td_staff_id, td_agent_id, td_credit_id, td_transaction_date, td_invoice_no) VALUES ('" & transaction_type & "', " & temp_dispatch & ", " & temp_customer & ", " & staff_id & "," & temp_agent & "," & temp_credit & "," & temp_date & "," & invoice_no & ")")

    End Function

    Public Function UpdateTransaction(ByVal transaction_id As Integer, ByVal transaction_type As String, ByVal invoice_no As Long, Optional ByVal dispatch_id As Integer = 0, Optional ByVal customer_id As Integer = 0, Optional ByVal staff_id As Integer = 0, Optional ByVal agent_id As Integer = 0, Optional ByVal credit_id As Integer = 0, Optional ByVal transact_date As Date = Nothing) As Boolean

        EscapeStr(transaction_type)
        If Not transaction_type = TR_RETAIL And Not transaction_type = TR_WHOLESALE And Not transaction_type = TR_DISTRIBUTION Then
            UpdateTransaction = False
            Exit Function
        End If

        Dim temp_dispatch As String = If(dispatch_id = 0, "NULL", dispatch_id)
        Dim temp_customer As String = If(customer_id = 0, "NULL", customer_id)
        Dim temp_agent As String = If(agent_id = 0, "NULL", agent_id)
        Dim temp_credit As String = If(credit_id = 0, "NULL", credit_id)
        If staff_id = 0 Then staff_id = module_session.current_user_profile.UserID
        Dim temp_date As String = If(transact_date = Date.MinValue, "NOW()", "'" & transact_date.ToString(DTS_YYYYMMDD) & "'")
        UpdateTransaction = DoQuery("UPDATE transaction_details SET td_transaction_type = '" & transaction_type & "', td_transaction_date = " & temp_date & ", td_invoice_no = " & invoice_no & ", td_dispatch_id = " & temp_dispatch & ", td_customer_id = " & temp_customer & ", td_staff_id = " & staff_id & ", td_agent_id = " & temp_agent & ", td_credit_id = " & temp_credit & ", td_transaction_date = " & temp_date & " WHERE td_transaction_id = " & transaction_id) > 0

    End Function

    Public Function GetTransactionCount(Optional ByVal staff_list As List(Of Integer) = Nothing) As Integer

        Dim temp_staff As String = ""
        If Not IsNothing(staff_list) Then
            For Each staff_id In staff_list
                If Not temp_staff = "" Then temp_staff &= ","
                temp_staff &= staff_id
            Next
        End If

        Dim query As String = "SELECT count(*) as transaction_count FROM transaction_details"
        If Not temp_staff = "" Then query &= " WHERE td_staff_id IN (" & temp_staff & ")"
        Dim result = SelectQuery(query)
        If IsNothing(result) Then
            GetTransactionCount = 0
        Else
            GetTransactionCount = Val(result.First()("transaction_count"))
        End If

    End Function

    Public Function GetTransactionList(Optional ByVal staff_list As List(Of Integer) = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1, Optional ByVal sort_field As String = "td_transaction_date", Optional ByVal sort_dir As String = "DESC") As Dictionary(Of String, Object)()

        EscapeStr(sort_field)
        EscapeStr(sort_dir)
        Dim temp_staff As String = ""
        If Not IsNothing(staff_list) Then
            For Each staff_id In staff_list
                If Not temp_staff = "" Then temp_staff &= ","
                temp_staff &= staff_id
            Next
        End If
        Dim query As String = "SELECT td.td_invoice_no, td.td_transaction_type, IFNULL(c.c_name, 'Guest') as c_name, IFNULL(cc.c_name, '--') as debtor_name, IFNULL(cd.cd_credit_amount, 0) as credit_amount, IFNULL(asl.sl_username, '--') as asl_username, sl.sl_username, IF(ISNULL(sp.sp_date_registered), CONCAT('ID: ', sl.sl_username), CONCAT(sp.sp_first_name, ' ', sp.sp_last_name)) as sp_staff_name,  td.td_transaction_verified, td.td_transaction_date, td.td_transaction_id FROM transaction_details td LEFT JOIN customers c ON c.c_customer_id = td.td_customer_id LEFT JOIN credit_details cd ON cd.cd_credit_id = td.td_credit_id LEFT JOIN customers cc ON cc.c_customer_id = cd.cd_customer_id INNER JOIN staff_login sl ON sl.sl_staff_id = td.td_staff_id LEFT JOIN staff_profile sp ON sp.sp_staff_id = sl.sl_staff_id LEFT JOIN staff_login asl ON asl.sl_staff_id = td_agent_id"
        If Not temp_staff = "" Then query &= " WHERE td.td_staff_id IN (" & temp_staff & ")"
        query &= " ORDER BY " & sort_field & " " & sort_dir
        query &= " LIMIT " & offset & ", " & If(count = -1, SYS_PAGE_SIZE, count)
        GetTransactionList = SelectQuery(query)

    End Function

    Public Function GetTransactionDetail(ByVal transaction_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM transaction_details WHERE td_transaction_id = " & transaction_id)
        If IsNothing(result) Then
            GetTransactionDetail = Nothing
        Else
            GetTransactionDetail = result.First()
        End If

    End Function

    Public Function GetTransactionDetailByInvoiceID(ByVal invoice_no As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM transaction_details WHERE td_invoice_no = " & invoice_no)
        If IsNothing(result) Then
            GetTransactionDetailByInvoiceID = Nothing
        Else
            GetTransactionDetailByInvoiceID = result.First()
        End If

    End Function

    Public Function GetTransactionItemsId(ByVal transaction_id As Integer) As Dictionary(Of String, Object)()

        GetTransactionItemsId = SelectQuery("SELECT item_id FROM view_transaction_items WHERE transaction_id = " & transaction_id)

    End Function

    Public Function ValidateItemForTransaction(ByVal product_id As Integer, ByVal quantity As Double) As Dictionary(Of String, Object)()

        Dim stocks = SelectQuery("SELECT stock_id, product_id, quantity_available FROM view_product_stocks_for_release WHERE product_id = " & product_id)
        Dim stock_ids() As Dictionary(Of String, Object) = Nothing
        If Not IsNothing(stocks) Then
            For Each stock In stocks
                Dim stock_id As New Dictionary(Of String, Object)
                stock_id.Add("stock_id", stock("stock_id"))
                stock_id.Add("product_id", stock("product_id"))
                If Val(stock("quantity_available")) >= quantity Then
                    stock_id.Add("quantity", quantity)
                Else
                    stock_id.Add("quantity", stock("quantity_available"))
                End If
                quantity -= stock_id("quantity")
                If IsNothing(stock_ids) Then
                    Array.Resize(stock_ids, 1)
                Else
                    Array.Resize(stock_ids, stock_ids.Count + 1)
                End If
                stock_ids(stock_ids.Count - 1) = stock_id
                If (quantity <= 0) Then Exit For
            Next
            If quantity > 0 Then
                Dim stock_id = New Dictionary(Of String, Object)
                stock_id.Add("stock_id", "0")
                stock_id.Add("quantity", quantity)
                Array.Resize(stock_ids, stock_ids.Count + 1)
                stock_ids(stock_ids.Count - 1) = stock_id
            End If
        Else
            Dim stock_id As New Dictionary(Of String, Object)
            stock_id.Add("stock_id", "0")
            stock_id.Add("quantity", quantity)
            Array.Resize(stock_ids, 1)
            stock_ids(stock_ids.Count - 1) = stock_id
        End If
        ValidateItemForTransaction = stock_ids

    End Function

    Public Function AddItemToTransaction(ByVal transaction_id As Integer, ByVal stock_id As Integer, ByVal product_id As Integer, ByVal quantity As Double, ByVal display_unit As ItemUnit, ByVal selling_price As Double, ByVal original_price As Double) As Integer

        Dim query As String
        Dim temp_display_unit As String = If(display_unit = ItemUnit.Pack, "pack", If(display_unit = ItemUnit.Pad, "pad", "unit"))
        If stock_id = 0 Then
            query = "INSERT INTO transaction_items (ti_transaction_id, ti_stock_id, ti_product_id, ti_item_quantity, ti_display_unit, ti_selling_price, ti_original_price) VALUES (" & transaction_id & ", NULL, " & product_id & ", " & quantity & ", '" & temp_display_unit & "', " & selling_price & "," & original_price & ")"
        Else
            query = "INSERT INTO transaction_items (ti_transaction_id, ti_stock_id, ti_product_id, ti_item_quantity, ti_display_unit, ti_selling_price, ti_original_price) VALUES (" & transaction_id & ", " & stock_id & ", " & product_id & ", " & quantity & ", '" & temp_display_unit & "', " & selling_price & "," & original_price & ")"
        End If
        AddItemToTransaction = DoQuery(query)

    End Function

    Public Function UpdateItemInTransaction(ByVal item_id As Integer, ByVal transaction_id As Integer, ByVal stock_id As Integer, ByVal product_id As Integer, ByVal quantity As Double, ByVal display_unit As ItemUnit, ByVal selling_price As Double, ByVal original_price As Double) As Boolean

        Dim temp_stock = If(stock_id = 0, "NULL", stock_id)
        Dim temp_display_unit As String = If(display_unit = ItemUnit.Pack, "pack", If(display_unit = ItemUnit.Pad, "pad", "unit"))
        UpdateItemInTransaction = DoQuery("UPDATE transaction_items SET " & _
            "ti_transaction_id = " & transaction_id & "," & _
            "ti_stock_id = " & temp_stock & "," & _
            "ti_product_id = " & product_id & "," & _
            "ti_item_quantity = " & quantity & "," & _
            "ti_display_unit = '" & temp_display_unit & "'," & _
            "ti_selling_price = " & selling_price & "," & _
            "ti_original_price = " & original_price & _
            " WHERE ti_item_id = " & item_id) > 0

    End Function

    Public Function ValidateTransaction(ByVal transaction_id As Integer) As Boolean

        ValidateTransaction = DoQuery("UPDATE transaction_details SET td_transaction_verified = 1 WHERE td_transaction_id = " & transaction_id) > 0

    End Function

    Public Function GetTransactionItemDetail(ByVal item_id As Integer) As Dictionary(Of String, Object)

        Dim results = SelectQuery("SELECT * FROM view_transaction_items WHERE item_id = " & item_id)
        If IsNothing(results) Then
            GetTransactionItemDetail = Nothing
        Else
            GetTransactionItemDetail = results.First()
        End If

    End Function

    Public Function GetTotalStocksAvailable(ByVal product_id As Integer) As Double

        Dim stocks = SelectQuery("SELECT SUM(quantity_available) as quantity_available FROM view_product_stocks_for_release WHERE product_id = " & product_id & " GROUP BY product_id")
        If IsNothing(stocks) Then
            GetTotalStocksAvailable = Nothing
        Else
            GetTotalStocksAvailable = Val(stocks.First()("quantity_available"))
        End If

    End Function

    Public Function CreateNewDistribution(ByVal agent_id As Integer, Optional ByVal staff_id As Integer = 0) As Integer

        If staff_id = 0 Then staff_id = module_session.current_user_profile.UserID
        CreateNewDistribution = DoQuery("INSERT INTO distribution_details (dd_agent_id, dd_staff_id, dd_distribution_date) VALUES (" & agent_id & "," & staff_id & ", NOW())")

    End Function

    Public Function SetDistributionItemSold(ByVal item_id As Integer, ByVal customer_id As Integer, ByVal selling_price As Double, ByVal quantity As Double, Optional ByVal sold_date As Date = Nothing) As Boolean

        Dim temp As String = If(sold_date = Date.MinValue, "NOW()", "'" & sold_date.ToString(DTS_YYYYMMDD) & "'")
        SetDistributionItemSold = DoQuery("INSERT INTO distribution_items_sold (dis_item_id, dis_selling_price, dis_item_quantity, dis_customer_id, dis_sold_date) VALUES (" & item_id & "," & selling_price & "," & quantity & "," & customer_id & "," & temp & ")")

    End Function

    Public Function ValidateDistribution(ByVal distribution_id As Integer) As Boolean

        ValidateDistribution = DoQuery("UPDATE distribution_details SET dd_distribution_verified = 1 WHERE dd_distribution_id = " & distribution_id) > 0

    End Function

    Public Function CloseDistribution(ByVal distribution_id As Integer) As Boolean

        CloseDistribution = DoQuery("UPDATE distribution_details SET dd_distribution_closed = 1 WHERE dd_distribution_id = " & distribution_id) > 0

    End Function

    Public Function GetOpenDistributionIDByAgent(ByVal agent_id As Integer) As Integer

        Dim result = SelectQuery("SELECT dd_distribution_id FROM distribution_details WHERE dd_agent_id = " & agent_id & " AND dd_distribution_closed = 0")
        If IsNothing(result) Then
            GetOpenDistributionIDByAgent = 0
        Else
            GetOpenDistributionIDByAgent = result.First()("dd_distribution_id")
        End If

    End Function

    Public Function GetItemsFromDistribution(ByVal distribution_id As Integer) As Dictionary(Of String, Object)()

        GetItemsFromDistribution = SelectQuery("SELECT di.di_item_id as item_id, di.di_stock_id as stock_id, vpsi.product_name, di.di_item_quantity as item_quantity, ifnull(dis.sold_quantity, 0) as sold_quantity, vpsi.product_unit FROM distribution_items di LEFT JOIN (SELECT SUM(dis_item_quantity) as sold_quantity, dis_item_id FROM distribution_items_sold)dis ON dis.dis_item_id = di.di_item_id INNER JOIN view_product_stocks_in vpsi ON vpsi.stock_id = di.di_stock_id WHERE di.di_distribution_id = " & distribution_id)

    End Function

    Public Function GetSuitableStocksForProduct(ByVal product_id As Integer, ByVal total_quantity As Double) As Dictionary(Of String, Object)()

        GetSuitableStocksForProduct = SelectQuery("SELECT stock_id, quantity_available, @total := @total + quantity_available AS sum FROM (view_product_stocks_for_release, (select @total := 0) t) WHERE product_id = " & product_id & " AND quantity_available > 0 AND @total < " & total_quantity)

    End Function

    Public Function GetStockProductID(ByVal stock_id As Integer) As Integer

        Dim result = SelectQuery("SELECT ps_product_id FROM product_stocks WHERE ps_stock_id = " & stock_id)
        If IsNothing(result) Then
            GetStockProductID = 0
        Else
            GetStockProductID = result.First()("ps_product_id")
        End If

    End Function

    Public Function IsInvoiceTaken(ByVal invoice_no As Integer) As Boolean

        Dim result = SelectQuery("SELECT count(*) as does_exist FROM transaction_details WHERE td_invoice_no = " & invoice_no)
        If IsNothing(result) Then
            IsInvoiceTaken = False
        Else
            IsInvoiceTaken = result.First()("does_exist") > 0
        End If

    End Function

    Public Function IsDispatchNoTaken(ByVal dispatch_no As Integer) As Boolean

        Dim result = SelectQuery("SELECT count(*) as does_exist FROM dispatch_details WHERE dd_dispatch_no = " & dispatch_no)
        If IsNothing(result) Then
            IsDispatchNoTaken = False
        Else
            IsDispatchNoTaken = result.First()("does_exist") > 0
        End If

    End Function

    Public Function GetRecommendedDispatchNo() As Long

        Dim result = SelectQuery("SELECT IFNULL(MAX(dd_dispatch_no), 0) + 1 as dispatch_no FROM dispatch_details")
        If IsNothing(result) Then
            GetRecommendedDispatchNo = 0
        Else
            GetRecommendedDispatchNo = result.First()("dispatch_no")
        End If

    End Function

    Public Function GetRecommendedInvoiceNo() As Long

        Dim result = SelectQuery("SELECT IFNULL(MAX(td_invoice_no), 0) + 1 as invoice_no FROM transaction_details")
        If IsNothing(result) Then
            GetRecommendedInvoiceNo = 0
        Else
            GetRecommendedInvoiceNo = result.First()("invoice_no")
        End If

    End Function

    Public Function AddReturnEntry(ByVal transaction_item_id As Integer, ByVal return_quantity As Double, ByVal user_id As Integer) As Integer

        Dim item_detail = GetTransactionItemDetail(transaction_item_id)
        If IsNothing(item_detail) Then
            AddReturnEntry = False
        Else
            If return_quantity > (item_detail("item_quantity") - item_detail("return_quantity")) Then
                AddReturnEntry = False
            Else
                AddReturnEntry = DoQuery("INSERT INTO transaction_items_returned (tir_item_id, tir_return_quantity, tir_return_date, tir_staff_id) VALUES (" & transaction_item_id & ", " & return_quantity & ", NOW(), " & user_id & ")")
            End If
        End If

    End Function

    Public Function UpdateReturnEntry(ByVal item_id As Integer, ByVal transaction_item_id As Integer, ByVal return_quantity As Double, ByVal user_id As Integer, Optional ByVal return_date As Date = Nothing) As Boolean

        Dim query = "UPDATE transaction_items_returned SET tir_return_quantity = " & return_quantity & ", tir_staff_id = " & user_id
        If Not return_date = Date.MinValue Then query &= ", tir_return_date = '" & return_date.ToString(DTS_YYYYMMDDHHmmSS) & "'"
        query &= " WHERE tir_return_id = " & item_id
        UpdateReturnEntry = DoQuery(query) > 0

    End Function

    Public Function RemoveReturnEntry(ByVal item_id As Integer) As Boolean

        RemoveReturnEntry = DoQuery("DELETE FROM transaction_items_returned WHERE tir_return_id = " & item_id) > 0

    End Function

    Public Function GetReturnEntryDetail(ByVal item_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM transaction_items_returned WHERE tir_return_id = " & item_id)
        If IsNothing(result) Then
            GetReturnEntryDetail = Nothing
        Else
            GetReturnEntryDetail = result.First()
        End If

    End Function

    Public Function GetReturnEntriesForTransactionItem(ByVal transaction_item_id As Integer) As Dictionary(Of String, Object)()

        GetReturnEntriesForTransactionItem = SelectQuery("SELECT * FROM transaction_items_returned WHERE tir_item_id = " & transaction_item_id)

    End Function

    Public Function AddTown(ByVal town_name As String) As Integer

        EscapeStr(town_name)
        AddTown = DoQuery("INSERT INTO towns (t_town_name) VALUES ('" & town_name & "')")

    End Function

    Public Function UpdateTown(ByVal town_id As Integer, ByVal town_name As String) As Boolean

        EscapeStr(town_name)
        UpdateTown = DoQuery("UPDATE towns SET t_town_name = '" & town_name & "' WHERE t_town_id = " & town_id) > 0

    End Function

    Public Function GetTownDetail(ByVal town_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM towns WHERE t_town_id = " & town_id)
        If IsNothing(result) Then
            GetTownDetail = Nothing
        Else
            GetTownDetail = result.First()
        End If

    End Function

    Public Function DoesTownExist(ByVal town_name As String) As Boolean

        EscapeStr(town_name)
        Dim result = SelectQuery("SELECT count(*) as does_exist from towns WHERE t_town_name = '" & town_name & "'")
        If IsNothing(result) Then
            DoesTownExist = False
        Else
            DoesTownExist = result.First()("does_exist") > 0
        End If

    End Function

    Public Function GetTownList() As Dictionary(Of String, Object)()

        GetTownList = SelectQuery("SELECT * FROM towns")

    End Function

    Public Function GetTownIDByName(ByVal town_name As String) As Integer

        EscapeStr(town_name)
        Dim result = SelectQuery("SELECT t_town_id FROM towns WHERE t_town_name = '" & town_name & "'")
        If IsNothing(result) Then
            GetTownIDByName = 0
        Else
            GetTownIDByName = result.First()("t_town_id")
        End If

    End Function

    Public Function AddCustomer(ByVal customer_name As String, ByVal customer_address As String, ByVal town_id As Integer, ByVal customer_contact As String, Optional ByVal date_registered As Date = Nothing) As Integer

        EscapeStr(customer_name)
        EscapeStr(customer_address)
        EscapeStr(customer_contact)
        Dim temp_date As String = If(date_registered = Date.MinValue, "NOW()", "'" & date_registered.ToString(DTS_YYYYMMDDHHmmSS) & "'")
        Dim query As String = "INSERT INTO customers (c_name, c_address, c_town_id, c_contact, c_date_registered) VALUES ('" & customer_name & "', '" & customer_address & "', " & town_id & ", '" & customer_contact & "', " & temp_date & ")"
        AddCustomer = DoQuery(query)

    End Function

    Public Function UpdateCustomer(ByVal customer_id As Integer, ByVal customer_name As String, ByVal customer_address As String, ByVal town_id As Integer, ByVal customer_contact As String) As Boolean

        EscapeStr(customer_name)
        EscapeStr(customer_address)
        EscapeStr(customer_contact)
        UpdateCustomer = DoQuery("UPDATE customers SET c_name = '" & customer_name & "', c_address = '" & customer_address & "', c_town_id = " & town_id & ", c_contact = '" & customer_contact & "' WHERE c_customer_id = " & customer_id) > 0

    End Function

    Public Function GetCustomerDetail(ByVal customer_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM customers WHERE c_customer_id = " & customer_id)
        If IsNothing(result) Then
            GetCustomerDetail = Nothing
        Else
            GetCustomerDetail = result.First()
        End If

    End Function

    Public Function GetCustomerIDByName(ByVal customer_name As String) As Integer

        EscapeStr(customer_name)
        Dim result = SelectQuery("SELECT c_customer_id FROM customers WHERE c_name = '" & customer_name & "'")
        If IsNothing(result) Then
            GetCustomerIDByName = 0
        Else
            GetCustomerIDByName = result.First()("c_customer_id")
        End If

    End Function

    Public Function GetCustomerList() As Dictionary(Of String, Object)()

        GetCustomerList = SelectQuery("SELECT c_name, c_customer_id FROM customers")

    End Function

    Public Function CreateNewDispatch(ByVal dispatch_type As String, ByVal dispatch_no As Long, ByVal agent_id As Integer, ByVal staff_id As Integer, Optional ByVal dispatch_date As Date = Nothing) As Integer

        EscapeStr(dispatch_type)
        Dim temp_date As String = If(dispatch_date = Date.MinValue, "NOW()", "'" & dispatch_date.ToString(DTS_YYYYMMDDHHmmSS) & "'")
        CreateNewDispatch = DoQuery("INSERT INTO dispatch_details (dd_dispatch_type, dd_dispatch_no, dd_agent_id, dd_staff_id, dd_dispatched_date) VALUES ('" & dispatch_type & "', " & dispatch_no & ", " & agent_id & ", " & staff_id & ", " & temp_date & ")")

    End Function

    Public Function UpdateDispatch(ByVal dispatch_id As Integer, ByVal dispatch_type As String, ByVal dispatch_no As Long, ByVal agent_id As Integer, ByVal staff_id As Integer, Optional ByVal dispatch_date As Date = Nothing) As Boolean

        EscapeStr(dispatch_type)
        Dim query As String = "UPDATE dispatch_details SET dd_dispatch_type = '" & dispatch_type & "', dd_dispatch_no = " & dispatch_no & ", dd_agent_id = " & agent_id & ", dd_staff_id = " & staff_id
        If Not dispatch_date = Date.MinValue Then query &= ", dd_dispatched_date = '" & dispatch_date.ToString(DTS_YYYYMMDDHHmmSS) & "'"
        query &= " WHERE dd_dispatch_id = " & dispatch_id
        UpdateDispatch = DoQuery(query) > 0

    End Function

    Public Function GetDispatchDetails(ByVal dispatch_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM dispatch_details WHERE dd_dispatch_id = " & dispatch_id)
        If IsNothing(result) Then
            GetDispatchDetails = Nothing
        Else
            GetDispatchDetails = result.First()
        End If

    End Function

    Public Function SetDispatchClosedStatus(ByVal dispatch_id As Integer, ByVal closed_status As Boolean) As Boolean

        SetDispatchClosedStatus = DoQuery("UPDATE dispatch_details SET dd_closed = " & If(closed_status, "1", "0") & " WHERE dd_dispatch_id = " & dispatch_id) > 0

    End Function

    Public Function GetDispatchTransactionIDs(ByVal dispatch_id As Integer) As Dictionary(Of String, Object)()

        GetDispatchTransactionIDs = SelectQuery("SELECT td_transaction_id FROM transaction_details WHERE td_dispatch_id = " & dispatch_id)

    End Function

    Public Function AddTransactionIDtoDispatch(ByVal dispatch_id As Integer, ByVal transaction_id As Integer) As Integer

        AddTransactionIDtoDispatch = DoQuery("UPDATE transaction_details SET td_dispatch_id = " & dispatch_id & " WHERE td_transaction_id = " & transaction_id & " LIMIT 1") > 0

    End Function

    Public Function RemoveTransactionFromDispatch(ByVal transaction_id As Integer) As Boolean

        RemoveTransactionFromDispatch = DoQuery("UPDATE transaction_details SET td_dispatch_id = NULL WHERE td_transaction_id = " & transaction_id & " LIMIT 1") > 0

    End Function

    Public Function GetDispatchItemDetail(ByVal item_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT di_dispatch_id, di.di_stock_id, di.di_stock_quantity, IFNULL(qs.quantity_sold, 0) as qs_sold_quantity, di.di_item_id FROM dispatch_items di LEFT JOIN (SELECT SUM(ti_item_quantity) as quantity_sold, ti.ti_stock_id, td.td_dispatch_id FROM transaction_items ti INNER JOIN transaction_details td ON td.td_transaction_id = ti.ti_transaction_id WHERE NOT ISNULL(td.td_dispatch_id) AND NOT ISNULL(ti.ti_stock_id) GROUP BY ti.ti_stock_id, td.td_dispatch_id)qs ON qs.td_dispatch_id = di.di_dispatch_id AND qs.ti_stock_id = di.di_stock_id WHERE di.di_item_id = " & item_id)
        If IsNothing(result) Then
            GetDispatchItemDetail = Nothing
        Else
            GetDispatchItemDetail = result.First()
        End If

    End Function

    Public Function AddItemToDispatch(ByVal dispatch_id As Integer, ByVal stock_quantity As Double, ByVal display_unit As ItemUnit, ByVal dispatch_price As Double, Optional ByVal stock_id As Integer = 0) As Integer

        Dim temp_stock_id = If(stock_id = 0, "NULL", stock_id)
        Dim temp_display_unit As String = If(display_unit = ItemUnit.Pack, "pack", If(display_unit = ItemUnit.Pad, "pad", "unit"))
        AddItemToDispatch = DoQuery("INSERT INTO dispatch_items (di_dispatch_id, di_stock_id, di_stock_quantity, di_display_unit, di_dispatch_price) VALUES (" & dispatch_id & ", " & temp_stock_id & ", " & stock_quantity & ", '" & temp_display_unit & "', " & dispatch_price & ")")

    End Function

    Public Function UpdateItemInDispatch(ByVal item_id As Integer, ByVal dispatch_id As Integer, ByVal stock_quantity As Double, ByVal display_unit As ItemUnit, ByVal dispatch_price As Double, Optional ByVal stock_id As Integer = 0) As Boolean

        Dim temp_stock_id = If(stock_id = 0, "NULL", stock_id)
        Dim temp_display_unit As String = If(display_unit = ItemUnit.Pack, "pack", If(display_unit = ItemUnit.Pad, "pad", "unit"))
        UpdateItemInDispatch = DoQuery("UPDATE dispatch_items SET di_dispatch_id = " & dispatch_id & ", di_stock_id = " & temp_stock_id & ", di_stock_quantity = " & stock_quantity & ", di_display_unit = '" & temp_display_unit & "', di_dispatch_price = " & dispatch_price & " WHERE di_item_id = " & item_id) > 0

    End Function

    Public Function RemoveDispatchItem(ByVal item_id As Integer) As Boolean

        RemoveDispatchItem = DoQuery("DELETE FROM dispatch_items WHERE di_item_id = " & item_id) > 0

    End Function

    Public Function GetItemIDsFromDispatch(ByVal dispatch_id As Integer) As Dictionary(Of String, Object)()

        GetItemIDsFromDispatch = SelectQuery("SELECT di_item_id FROM dispatch_items WHERE di_dispatch_id = " & dispatch_id)

    End Function

    Public Function AddCredit(ByVal credit_amount As Double, ByVal customer_id As Integer, ByVal due_date As Date, Optional ByVal created_date As Date = Nothing) As Integer

        Dim temp_created_date As String = If(created_date = Date.MinValue, "NOW()", "'" & created_date.ToString(DTS_YYYYMMDDHHmmSS) & "'")
        AddCredit = DoQuery("INSERT INTO credit_details (cd_credit_amount, cd_created_date, cd_due_date, cd_customer_id) VALUES (" & credit_amount & ", " & temp_created_date & ", '" & due_date.ToString(DTS_YYYYMMDDHHmmSS) & "', " & customer_id & ")")

    End Function

    Public Function UpdateCredit(ByVal credit_id As Integer, ByVal credit_amount As Double, ByVal customer_id As Integer, ByVal due_date As Date, Optional ByVal created_date As Date = Nothing) As Boolean

        Dim query As String = "UPDATE credit_details SET cd_credit_amount = " & credit_amount & ", cd_due_date = '" & due_date.ToString(DTS_YYYYMMDDHHmmSS) & "', cd_customer_id = " & customer_id
        If Not created_date = Date.MinValue Then query &= ", cd_created_date = '" & created_date.ToString(DTS_YYYYMMDDHHmmSS) & "'"
        query &= " WHERE cd_credit_id = " & credit_id
        UpdateCredit = DoQuery(query) > 0

    End Function

    Public Function GetCreditDetails(ByVal credit_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM credit_details WHERE cd_credit_id = " & credit_id)
        If IsNothing(result) Then
            GetCreditDetails = Nothing
        Else
            GetCreditDetails = result.First()
        End If

    End Function

    Public Function GetPaymentsForCredit(ByVal credit_id As Integer) As Dictionary(Of String, Object)()

        GetPaymentsForCredit = SelectQuery("SELECT * FROM credit_payments WHERE cp_credit_id = " & credit_id & " ORDER BY cp_payment_date DESC")

    End Function

    Public Function AddCreditPayment(ByVal credit_id As Integer, ByVal payment_amount As Double, ByVal staff_id As Integer) As Integer

        AddCreditPayment = DoQuery("INSERT INTO credit_payments (cp_payment_amount, cp_credit_id, cp_staff_id, cp_payment_date) VALUES (" & payment_amount & ", " & credit_id & ", " & staff_id & ", NOW())")

    End Function

    Public Function UpdateCreditPayment(ByVal credit_payment_id As Integer, ByVal payment_amount As Double, ByVal credit_id As Integer, ByVal staff_id As Integer, Optional ByVal payment_date As Date = Nothing) As Boolean

        UpdateCreditPayment = DoQuery("UPDATE credit_payments SET cp_payment_amount = " & payment_amount & ", cp_credit_id = " & credit_id & ", cp_staff_id = " & staff_id & If(payment_date = Date.MinValue, ", cp_payment_date = '" & payment_date.ToString(DTS_YYYYMMDDHHmmSS) & "'", "") & " WHERE cp_payment_id = " & credit_payment_id) > 0

    End Function

    Public Function RemoveCreditPayment(ByVal credit_payment_id As Integer) As Boolean

        RemoveCreditPayment = DoQuery("DELETE FROM credit_payments WHERE cp_payment_id = " & credit_payment_id) > 0

    End Function

    Public Function GetCreditPaymentDetail(ByVal credit_payment_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM credit_payments WHERE cp_payment_id = " & credit_payment_id)
        If IsNothing(result) Then
            GetCreditPaymentDetail = Nothing
        Else
            GetCreditPaymentDetail = result.First()
        End If

    End Function

    Public Function GetCreditPaymentsList(ByVal credit_id As Integer) As Dictionary(Of String, Object)()

        GetCreditPaymentsList = SelectQuery("SELECT cp.*, sl.sl_username FROM credit_payments cp INNER JOIN staff_login sl ON sl.sl_staff_id = cp.cp_staff_id WHERE cp.cp_credit_id = " & credit_id)

    End Function

    Public Function GetCustomerReportList(Optional ByVal customer_name As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1, Optional ByVal sort_field As String = "customer_name", Optional ByVal sort_dir As String = "ASC", Optional ByVal exclude_clean As Boolean = False) As Dictionary(Of String, Object)()

        EscapeStr(customer_name)
        EscapeStr(sort_field)
        EscapeStr(sort_dir)
        Dim query As String = "SELECT * FROM view_customer_list"
        If Not IsNothing(customer_name) Then query &= " WHERE customer_name LIKE '%" & customer_name & "%'"
        If exclude_clean Then
            If Not IsNothing(customer_name) Then query &= " AND" Else query &= " WHERE"
            query &= " credit_amount > total_payed"
        End If
        count = If(count = -1, SYS_PAGE_SIZE, count)
        query &= " ORDER BY " & sort_field & " " & sort_dir & " LIMIT " & offset & ", " & count
        GetCustomerReportList = SelectQuery(query)

    End Function

    Public Function GetCustomerReportListCount(Optional ByVal customer_name As String = Nothing, Optional ByVal exclude_clean As Boolean = False) As Integer

        EscapeStr(customer_name)
        Dim query As String = "SELECT count(*) as list_count FROM view_customer_list"
        If Not IsNothing(customer_name) Then query &= " WHERE customer_name LIKE '%" & customer_name & "%'"
        If exclude_clean Then
            If Not IsNothing(customer_name) Then query &= " AND" Else query &= " WHERE"
            query &= " credit_amount > total_payed"
        End If
        Dim result = SelectQuery(query)
        If IsNothing(result) Then
            GetCustomerReportListCount = 0
        Else
            GetCustomerReportListCount = result.First()("list_count")
        End If

    End Function

    Public Function GetCustomerTransactionsListCount(ByVal customer_id As Integer) As Integer

        Dim result = SelectQuery("SELECT count(*) as transaction_count FROM transaction_details td LEFT JOIN view_credit_details vcd ON vcd.credit_id = td.td_credit_id WHERE td.td_customer_id = " & customer_id & " OR vcd.customer_id = " & customer_id)
        If IsNothing(result) Then
            GetCustomerTransactionsListCount = 0
        Else
            GetCustomerTransactionsListCount = result.First()("transaction_count")
        End If

    End Function

    Public Function GetCustomerTransactionsList(ByVal customer_id As Integer, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1, Optional ByVal sort_field As String = "td.td_invoice_no", Optional ByVal sort_dir As String = "ASC") As Dictionary(Of String, Object)()

        EscapeStr(sort_dir)
        EscapeStr(sort_field)
        Dim query As String = "SELECT td.td_invoice_no as invoice_no, vcd.credit_id, IFNULL(vcd.customer_name, '--') as debtor_name, IFNULL(vcd.credit_amount, 0) as credit_amount, IFNULL(vcd.total_payed, 0) as total_payed, vcd.due_date, IF(vcd.due_date < DATE(NOW()), 1, 0) as is_due FROM transaction_details td LEFT JOIN view_credit_details vcd ON vcd.credit_id = td.td_credit_id WHERE td.td_customer_id = " & customer_id & " OR vcd.customer_id = " & customer_id
        count = If(count = -1, SYS_PAGE_SIZE, count)
        query &= " ORDER BY " & sort_field & " " & sort_dir & " LIMIT " & offset & ", " & count
        GetCustomerTransactionsList = SelectQuery(query)

    End Function

    Public Function GetTransactionListWithCredits(Optional ByVal include_payed As Boolean = False) As Dictionary(Of String, Object)()

        Dim query As String

        If include_payed Then
            query = "SELECT td.*, IFNULL(c.c_name, '') as c_name FROM transaction_details td LEFT JOIN customers c ON c.c_customer_id = td.td_customer_id WHERE NOT ISNULL(td.td_credit_id)"
        Else
            query = "SELECT td.*, IFNULL(c.c_name, '') as c_name FROM transaction_details td LEFT JOIN customers c ON c.c_customer_id = td.td_customer_id INNER JOIN view_credit_details vcd ON vcd.credit_id = td.td_credit_id WHERE vcd.credit_amount > vcd.total_payed"
        End If
        GetTransactionListWithCredits = SelectQuery(query)

    End Function

    Public Function VoidTransaction(ByVal transaction_id As Integer) As Boolean

        Dim transaction_detail = GetTransactionDetail(transaction_id)
        VoidTransaction = DoQuery("DELETE FROM transaction_details WHERE td_transaction_id = " & transaction_id) > 0
        If VoidTransaction And (Val(transaction_detail("td_credit_id")) > 0) Then
            VoidTransaction = DoQuery("DELETE FROM credit_details WHERE cd_credit_id = " & Val(transaction_detail("td_credit_id"))) > 0
        End If

    End Function

    Public Function GetTransactionIDByCriteria(ByVal customer_id As Integer, ByVal date_transacted As Date, ByVal invoice_no As Long) As Integer

        Dim result = SelectQuery("SELECT td_transaction_id FROM transaction_details WHERE td_customer_id = " & customer_id & " AND DATE(td_transaction_date) = '" & date_transacted.ToString(DTS_YYYYMMDD) & "' AND td_invoice_no = " & invoice_no)
        If IsNothing(result) Then
            GetTransactionIDByCriteria = 0
        Else
            GetTransactionIDByCriteria = result.First()("td_transaction_id")
        End If

    End Function
End Class
