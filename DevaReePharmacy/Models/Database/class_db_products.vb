﻿Public Class class_db_products
    Inherits class_db

    Public Function CreateNewCategory(ByVal category_name As String) As Integer

        EscapeStr(category_name)
        CreateNewCategory = DoQuery("INSERT INTO product_categories (pc_category_name) VALUES ('" & category_name & "')")

    End Function

    Public Function UpdateCategory(ByVal category_id As Integer, ByVal category_name As String) As Boolean

        EscapeStr(category_name)
        UpdateCategory = DoQuery("UPDATE product_categories SET pc_category_name = '" & category_name & "' WHERE pc_category_id = " & category_id) > 0

    End Function

    Public Function GetCategoryDetails(ByVal category_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM product_categories WHERE pc_category_id = " & category_id)
        If IsNothing(result) Then
            GetCategoryDetails = Nothing
        Else
            GetCategoryDetails = result.First()
        End If

    End Function

    Public Function DeleteProduct(ByVal product_id As Integer) As Boolean

        DeleteProduct = DoQuery("DELETE FROM product_details WHERE pd_product_id = " & product_id) > 0

    End Function

    Public Function GetProductNames(Optional ByVal search_tag As String = "") As Dictionary(Of String, Object)()

        EscapeStr(search_tag)
        Dim query As String
        query = "SELECT category_name, product_name, product_id FROM view_product_details" & If(search_tag = "", "", " WHERE product_name LIKE '%" & search_tag & "%'")
        GetProductNames = SelectQuery(query)

    End Function

    Public Function GetProductIDByCode(ByVal product_code As String) As Integer

        EscapeStr(product_code)
        Dim result = SelectQuery("SELECT product_id FROM view_product_details WHERE product_code = '" & product_code & "'")
        If IsNothing(result) Then
            GetProductIDByCode = 0
        Else
            GetProductIDByCode = result.First()("product_id")
        End If

    End Function

    Public Function GetProductIDByName(ByVal product_name As String) As Integer

        EscapeStr(product_name)
        Dim result = SelectQuery("SELECT product_id FROM view_product_details WHERE product_name = '" & product_name & "'")
        If IsNothing(result) Then
            GetProductIDByName = 0
        Else
            GetProductIDByName = result.First()("product_id")
        End If

    End Function

    Public Function GetProductName(ByVal product_id As Integer) As String

        Dim result = SelectQuery("SELECT product_name FROM view_product_details WHERE product_id = " & product_id)
        If IsNothing(result) Then
            GetProductName = Nothing
        Else
            GetProductName = result.First()("product_name")
        End If

    End Function

    Public Function GetProductCount(Optional ByVal search_tag As String = Nothing) As Integer

        EscapeStr(search_tag)
        Dim result = SelectQuery(If(IsNothing(search_tag), "SELECT COUNT(*) as product_count FROM view_product_details", "SELECT COUNT(*) as product_count FROM view_product_details WHERE product_name LIKE '%" & search_tag & "%' OR product_code = '" & search_tag & "' OR product_unit = '" & search_tag & "'"))
        If IsNothing(result) Then
            GetProductCount = 0
        Else
            GetProductCount = Val(result.First()("product_count"))
        End If

    End Function

    Public Function GetProductListAndDetails(ByVal product_ids As List(Of Integer)) As Dictionary(Of String, Object)()

        Dim id_filter As String = ""
        For Each product_id In product_ids
            id_filter &= If(id_filter = "", product_id, ", " & product_id)
        Next

        Dim query As String = "SELECT pd.*, IFNULL(vps.stock_quantity, 0) as stock_quantity FROM view_product_details pd LEFT JOIN view_product_stocks_inventory vps ON vps.product_id = pd.product_id WHERE pd.product_id IN (" & id_filter & ")"
        GetProductListAndDetails = SelectQuery(query)

    End Function

    Public Function GetProductListAndDetails(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1, Optional ByVal sort_field As String = "category_name", Optional ByVal sort_dir As String = "ASC") As Dictionary(Of String, Object)()

        EscapeStr(search_tag)
        count = If(count = -1, SYS_PAGE_SIZE, count)
        Dim query As String = If(IsNothing(search_tag), "SELECT pd.*, IFNULL(vps.stock_quantity, 0) as stock_quantity FROM view_product_details pd LEFT JOIN view_product_stocks_inventory vps ON vps.product_id = pd.product_id", "SELECT pd.*, IFNULL(vps.stock_quantity, 0) as stock_quantity FROM view_product_details pd LEFT JOIN view_product_stocks_inventory vps ON vps.product_id = pd.product_id WHERE pd.product_name LIKE '%" & search_tag & "%' OR pd.product_code = '" & search_tag & "' OR pd.product_unit = '" & search_tag & "'")
        query &= " ORDER BY " & sort_field & " " & sort_dir
        If sort_field = "category_name" Then query &= ", product_name ASC"
        If Not (offset = 0 And count = 0) Then query &= " LIMIT " & offset & ", " & count
        GetProductListAndDetails = SelectQuery(query)

    End Function

    Public Function CreateProduct(ByVal product_name As String, ByVal product_description As String, ByVal product_code As String, ByVal product_unit As String, ByVal product_brand As String, ByVal retail_price As Double, ByVal wholesale_price As Double, ByVal distribution_price As Double, ByVal packing_size As Integer, ByVal pads_per_pack As Integer, Optional ByVal category_id As Integer = 0) As Integer

        EscapeStr(product_name)
        EscapeStr(product_description)
        EscapeStr(product_code)
        EscapeStr(product_unit)
        EscapeStr(product_brand)
        If Trim(product_unit) = SYS_PACK_ALIAS Or Trim(product_unit) = SYS_PAD_ALIAS Then
            CreateProduct = 0
        Else
            Dim brand_id As Integer
            Dim unit_id As Integer
            Dim temp_category_id As String = If(category_id = 0, "NULL", category_id)
            brand_id = GetBrandID(product_brand)
            unit_id = GetUnitID(product_unit)
            If brand_id = 0 Then brand_id = CreateBrand(product_brand)
            If unit_id = 0 Then unit_id = CreateUnit(product_unit)
            If brand_id = 0 Or unit_id = 0 Then CreateProduct = 0 : Exit Function

            Dim product_id = DoQuery("INSERT INTO product_details (pd_category_id, pd_product_name, pd_product_description, pd_product_code, pd_brand_id, pd_product_unit_id, pd_product_retail_price, pd_product_wholesale_price, pd_product_distribution_price, pd_packing_size, pd_pads_per_pack) VALUES (" & temp_category_id & ",'" & product_name & "', '" & product_description & "', '" & product_code & "', " & brand_id & ", " & unit_id & ", " & retail_price & ", " & wholesale_price & ", " & distribution_price & "," & packing_size & "," & pads_per_pack & ")")
            CreateProduct = product_id
        End If


    End Function

    Public Function UpdateProductDetails(ByVal product_id As Integer, Optional ByVal product_name As String = Nothing, Optional ByVal product_description As String = Nothing, Optional ByVal product_code As String = Nothing, Optional ByVal product_unit As String = Nothing, Optional ByVal product_brand As String = Nothing, Optional ByVal packing_size As Integer = 0, Optional ByVal pads_per_pack As Integer = 0, Optional ByVal category_id As Integer = 0) As Boolean

        EscapeStr(product_name)
        EscapeStr(product_description)
        EscapeStr(product_code)
        EscapeStr(product_unit)
        EscapeStr(product_brand)
        Dim temp_category_id As String = If(category_id = 0, "NULL", category_id)
        Dim query As String = "UPDATE product_details SET"
        Dim update As String = " pd_category_id = " & temp_category_id
        If Not IsNothing(product_name) Then update &= ", pd_product_name = '" & product_name & "'"
        If Not IsNothing(product_description) Then update &= If(update = "", " pd_product_description = '" & product_description & "'", ", pd_product_description = '" & product_description & "'")
        If Not IsNothing(product_code) Then update &= If(update = "", " pd_product_code = '" & product_code & "'", ", pd_product_code = '" & product_code & "'")
        If packing_size > 0 Then update &= If(update = "", " pd_packing_size = " & packing_size, ", pd_packing_size = " & packing_size)
        If pads_per_pack > 0 Then update &= If(update = "", " pd_pads_per_pack = " & pads_per_pack, ", pd_pads_per_pack = " & pads_per_pack)
        query &= update & " WHERE pd_product_id = " & product_id
        UpdateProductDetails = DoQuery(query) > 0
        If Not IsNothing(product_unit) Then UpdateProductDetails = UpdateProductDetails And UpdateProductUnit(product_id, product_unit)
        If Not IsNothing(product_brand) Then UpdateProductDetails = UpdateProductDetails And UpdateProductBrand(product_id, product_brand)

    End Function

    Public Function UpdateProductBrand(ByVal product_id As Integer, ByVal product_brand As String) As Boolean

        EscapeStr(product_brand)
        product_brand = Trim(product_brand)
        Dim query As String = "SELECT pb_brand_id FROM product_brand WHERE pb_brand_name = '" & product_brand & "'"
        Dim result = SelectQuery(query)
        Dim product_brand_id As Integer
        If IsNothing(result) Then
            query = "INSERT INTO product_brand (pb_brand_name) VALUES ('" & product_brand & "')"
            product_brand_id = DoQuery(query)
        Else
            product_brand_id = result.First()("pb_brand_id")
        End If
        query = "UPDATE product_details SET pd_brand_id = " & product_brand_id & " WHERE pd_product_id = " & product_id
        UpdateProductBrand = DoQuery(query) > 0

    End Function

    Public Function UpdateProductUnit(ByVal product_id As Integer, ByVal product_unit As String) As Boolean

        EscapeStr(product_unit)
        If Trim(product_unit) = SYS_PACK_ALIAS Or Trim(product_unit) = SYS_PAD_ALIAS Then
            UpdateProductUnit = False
        Else
            product_unit = Trim(product_unit)
            Dim query As String = "SELECT pu_product_unit_id FROM product_unit WHERE pu_product_unit = '" & product_unit & "'"
            Dim result = SelectQuery(query)
            Dim product_unit_id As Integer
            If IsNothing(result) Then
                query = "INSERT INTO product_unit (pu_product_unit) VALUES ('" & product_unit & "')"
                product_unit_id = DoQuery(query)
            Else
                product_unit_id = result.First()("pu_product_unit_id")
            End If
            query = "UPDATE product_details SET pd_product_unit_id = " & product_unit_id & " WHERE pd_product_id = " & product_id
            UpdateProductUnit = DoQuery(query) > 0
        End If


    End Function

    Public Function UpdateProductPrices(ByVal product_id As Integer, Optional ByVal retail_price As Double = 0, Optional ByVal wholesale_price As Double = 0, Optional ByVal distribution_price As Double = 0) As Boolean

        Dim query As String = "UPDATE product_details SET"
        Dim update As String = ""
        If retail_price > 0 Then update &= " pd_product_retail_price = " & retail_price
        If wholesale_price > 0 Then update &= If(update = "", " pd_product_wholesale_price = " & wholesale_price, ", pd_product_wholesale_price = " & wholesale_price)
        If distribution_price > 0 Then update &= If(update = "", " pd_product_distribution_price = " & distribution_price, ", pd_product_distribution_price = " & distribution_price)
        If update = "" Then UpdateProductPrices = True
        query &= update & " WHERE pd_product_id = " & product_id
        UpdateProductPrices = DoQuery(query) > 0

    End Function

    Public Function CreateBrand(ByVal brand_name As String) As Integer

        EscapeStr(brand_name)
        CreateBrand = DoQuery("INSERT INTO product_brand (pb_brand_name) VALUES('" & brand_name & "')")

    End Function

    Public Function CreateUnit(ByVal product_unit As String) As Integer

        EscapeStr(product_unit)
        CreateUnit = DoQuery("INSERT INTO product_unit (pu_product_unit) VALUES ('" & product_unit & "')")

    End Function

    Public Function GetBrandID(ByVal brand_name As String) As Integer

        EscapeStr(brand_name)
        Dim result = SelectQuery("SELECT pb_brand_id FROM product_brand WHERE pb_brand_name = '" & brand_name & "'")
        If IsNothing(result) Then
            GetBrandID = 0
        Else
            GetBrandID = result.First()("pb_brand_id")
        End If

    End Function

    Public Function GetUnitID(ByVal product_unit As String) As Integer

        EscapeStr(product_unit)
        Dim result = SelectQuery("SELECT pu_product_unit_id FROM product_unit WHERE pu_product_unit = '" & product_unit & "'")
        If IsNothing(result) Then
            GetUnitID = 0
        Else
            GetUnitID = result.First()("pu_product_unit_id")
        End If

    End Function

    Public Function GetBrandList() As Dictionary(Of String, Object)()

        GetBrandList = SelectQuery("SELECT * FROM product_brand")

    End Function

    Public Function GetUnitList() As Dictionary(Of String, Object)()

        GetUnitList = SelectQuery("SELECT * FROM product_unit")

    End Function

    Public Function DoesProductHaveStocks(ByVal product_id As Integer) As Boolean

        Dim result = SelectQuery("SELECT count(*) as has_stocks FROM product_stocks WHERE ps_product_id = " & product_id)
        If IsNothing(result) Then
            DoesProductHaveStocks = False
        Else
            DoesProductHaveStocks = result.First()("has_stocks") > 0
        End If

    End Function

    Public Function DoesProductCategoryExist(ByVal category_name As String) As Boolean

        EscapeStr(category_name)
        Dim result = SelectQuery("SELECT count(*) does_exist FROM product_categories WHERE pc_category_name = '" & category_name & "'")
        If IsNothing(result) Then
            DoesProductCategoryExist = False
        Else
            DoesProductCategoryExist = result.First()("does_exist") > 0
        End If

    End Function

    Public Function IsProductCodeTaken(ByVal product_code As String) As Boolean

        EscapeStr(product_code)
        Dim results = SelectQuery("SELECT count(*) as is_taken FROM product_details WHERE pd_product_code = '" & product_code & "'")
        If IsNothing(results) Then
            IsProductCodeTaken = False
        Else
            IsProductCodeTaken = results.First()("is_taken") > 0
        End If

    End Function

    Public Function IsProductInFlow(ByVal product_id As Integer) As Boolean

        Dim result = SelectQuery("SELECT count(*) as in_manifest FROM product_stocks WHERE ps_product_id = " & product_id)
        If IsNothing(result) Then
            IsProductInFlow = False
        Else
            IsProductInFlow = result.First()("in_manifest") > 0
        End If

        If Not IsProductInFlow Then
            result = SelectQuery("SELECT count(*) as is_transacted FROM transaction_items WHERE ti_product_id = " & product_id)
            If IsNothing(result) Then
                IsProductInFlow = False
            Else
                IsProductInFlow = result.First()("is_transacted") > 0
            End If
        End If

        If Not IsProductInFlow Then
            result = SelectQuery("SELECT count(*) as is_distributed FROM distribution_items WHERE di_product_id = " & product_id)
            If IsNothing(result) Then
                IsProductInFlow = False
            Else
                IsProductInFlow = result.First()("is_distributed") > 0
            End If
        End If

        If Not IsProductInFlow Then
            result = SelectQuery("SELECT count(*) as is_requested FROM delivery_request_items WHERE dri_product_id = " & product_id)
            If IsNothing(result) Then
                IsProductInFlow = False
            Else
                IsProductInFlow = result.First()("is_requested") > 0
            End If
        End If

    End Function

    Public Function GetProductDetailByID(ByVal product_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM view_product_details WHERE product_id = " & product_id)
        If IsNothing(result) Then
            GetProductDetailByID = Nothing
        Else
            GetProductDetailByID = result.First()
        End If

    End Function

    Public Function GetProductCategoryList() As Dictionary(Of String, Object)()

        GetProductCategoryList = SelectQuery("SELECT * FROM product_categories")

    End Function

    Public Function GetCategoryIDByName(ByVal category_name As String) As Integer

        EscapeStr(category_name)
        Dim result = SelectQuery("SELECT pc_category_id FROM product_categories WHERE pc_category_name = '" & category_name & "'")
        If IsNothing(result) Then
            GetCategoryIDByName = 0
        Else
            GetCategoryIDByName = result.First()("pc_category_id")
        End If

    End Function

End Class
