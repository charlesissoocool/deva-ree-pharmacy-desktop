﻿Public Class class_db_login
    Inherits class_db

    Public Function IsUserActive(ByVal user_id As Integer) As Boolean

        Dim result = SelectQuery("SELECT is_active FROM view_staff_credentials WHERE staff_id = " & user_id)
        If IsNothing(result) Then
            IsUserActive = False
        Else
            IsUserActive = If(result.First()("is_active") = 1, True, False)
        End If

    End Function

    Public Function GetUserLoginHistory(ByVal user_id As Integer) As Dictionary(Of String, Object)()

        GetUserLoginHistory = SelectQuery("SELECT DATE_FORMAT(slh_log_time, '" & DATE_HUMAN_WITH_TIME & "') as slh_log_time_formatted, slh_log_type FROM staff_login_history WHERE slh_staff_id = " & user_id & " ORDER BY slh_log_time DESC")

    End Function

    Public Function GetLatestLogin(ByVal user_id As Integer, Optional ByVal date_format As String = module_common.DATE_YYMMDDHHMMSS) As String

        EscapeStr(date_format)
        Dim last_login = SelectQuery("SELECT DATE_FORMAT(slh_log_time, '" & date_format & "') as slh_log_time, slh_log_type FROM staff_login_history WHERE slh_staff_id = " & user_id & " AND slh_log_type = 'In' ORDER BY slh_log_time DESC LIMIT 1")
        If Not IsNothing(last_login) Then
            GetLatestLogin = last_login.First()("slh_log_time")
        Else
            GetLatestLogin = "Never"
        End If

    End Function

    Public Function AddUserLog(ByVal log_type As String, Optional ByVal staff_id As Integer = 0, Optional ByVal log_time As Date = Nothing) As Boolean

        EscapeStr(log_type)
        If staff_id <= 0 Then
            If module_session.current_user_profile.UserID = 0 Then
                AddUserLog = False
                Exit Function
            Else
                staff_id = module_session.current_user_profile.UserID
            End If
        End If

        Dim temp_date As String = If(log_time = New Date, "NOW()", "'" & log_time.ToString(DTS_YYYYMMDDHHmmSS) & "'")
        If Not log_type = "In" And Not log_type = "Out" Then
            AddUserLog = False
        Else
            AddUserLog = DoQuery("INSERT INTO staff_login_history (slh_staff_id, slh_log_time, slh_log_type) VALUES (" & staff_id & ", " & temp_date & ", '" & log_type & "')") > 0
        End If

    End Function

    Public Function TestQuery() As Dictionary(Of String, Object)()

        TestQuery = SelectQuery("SELECT *, DATE_FORMAT(entry_date, '" & DATE_HUMAN_WITH_TIME & "') as entry_date_formatted, DATE_FORMAT(expiry_date, '" & DATE_HUMAN & "') as expiry_date_formatted FROM view_product_stocks_in")

    End Function

End Class
