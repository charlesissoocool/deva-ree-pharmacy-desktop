﻿Public Class class_db_stocks
    Inherits class_db

    Public Function GetStockListCount(Optional ByVal search_tag As String = Nothing) As Integer

        EscapeStr(search_tag)
        Dim query As String = "SELECT count(*) as stock_count FROM view_product_stocks_in"
        If Not IsNothing(search_tag) Then query &= " WHERE product_name LIKE '%" & search_tag & "%'"
        Dim result = SelectQuery(query)
        If IsNothing(result) Then
            GetStockListCount = 0
        Else
            GetStockListCount = result.First()("stock_count")
        End If

    End Function

    Public Function GetStockList(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1, Optional ByVal sort_field As String = "entry_date", Optional ByVal sort_dir As String = "DESC") As Dictionary(Of String, Object)()

        EscapeStr(search_tag)
        EscapeStr(sort_field)
        EscapeStr(sort_dir)
        count = If(count = -1, SYS_PAGE_SIZE, count)
        Dim query As String = "SELECT *, DATE_FORMAT(entry_date, '" & DATE_HUMAN_WITH_TIME & "') as entry_date_formatted, DATE_FORMAT(expiry_date, '" & DATE_HUMAN & "') as expiry_date_formatted FROM view_product_stocks_in"
        If Not IsNothing(search_tag) Then query &= " WHERE product_name LIKE '%" & search_tag & "%'"
        query &= " ORDER BY " & sort_field & " " & sort_dir
        If Not (offset = 0 And count = 0) Then query &= " LIMIT " & offset & ", " & count
        GetStockList = SelectQuery(query)

    End Function

    Public Function ValidateStock(ByVal stock_id As Integer) As Boolean

        ValidateStock = DoQuery("UPDATE product_stocks SET ps_is_valid = 1 WHERE ps_stock_id = " & stock_id) > 0

    End Function

    Public Function InvalidateStock(ByVal stock_id As Integer) As Boolean

        InvalidateStock = DoQuery("UPDATE product_stocks SET ps_is_valid = 0 WHERE ps_stock_id = " & stock_id) > 0

    End Function

    Public Function AddStock(ByVal manifest_id As Integer, ByVal product_id As Integer, ByVal stock_quantity As Double, ByVal supplier_price As Double, ByVal expiry_date As Date, ByVal lot_number As String) As Integer

        EscapeStr(lot_number)
        AddStock = DoQuery("INSERT INTO product_stocks (ps_manifest_id, ps_product_id, ps_stock_quantity, ps_supplier_price, ps_expiry_date, ps_lot_number) VALUES (" & manifest_id & ", " & product_id & ", " & stock_quantity & ", " & supplier_price & ", '" & expiry_date.ToString("yyyy-MM-dd") & "', '" & lot_number & "')")

    End Function

    Public Function UpdateStock(ByVal manifest_id As Integer, ByVal stock_id As Integer, ByVal product_id As Integer, ByVal stock_quantity As Double, ByVal supplier_price As Double, ByVal expiry_date As Date, ByVal lot_number As String) As Integer

        EscapeStr(lot_number)
        UpdateStock = DoQuery("UPDATE product_stocks SET ps_manifest_id = " & manifest_id & ", ps_product_id = " & product_id & ", ps_stock_quantity = " & stock_quantity & ", ps_supplier_price = " & supplier_price & ", ps_expiry_date = '" & expiry_date.ToString("yyyy-MM-dd") & "', ps_lot_number = '" & lot_number & "' WHERE ps_stock_id = " & stock_id)

    End Function

    Public Function RemoveStock(ByVal stock_id As Integer) As Boolean

        RemoveStock = DoQuery("DELETE FROM product_stocks WHERE ps_stock_id = " & stock_id) > 0

    End Function

    Public Function GetStockDetail(ByVal stock_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT vpsi.*, IFNULL(vpsfr.quantity_available, 0) as quantity_available FROM view_product_stocks_in vpsi LEFT JOIN view_product_stocks_for_release vpsfr ON vpsi.stock_id = vpsfr.stock_id WHERE vpsi.stock_id = " & stock_id)
        If IsNothing(result) Then
            GetStockDetail = Nothing
        Else
            GetStockDetail = result.First()
        End If

    End Function

    Public Function AddManifest(ByVal staff_id As Integer, ByVal manifest_code As String, ByVal supplier_id As Integer, Optional ByVal entry_date As Date = Nothing) As Integer

        EscapeStr(manifest_code)
        Dim temp_date As String = If(entry_date = Date.MinValue, "NOW()", "'" & entry_date.ToString(DTS_YYYYMMDDHHmmSS) & "'")
        Dim query As String = "INSERT INTO manifest_details (md_staff_id, md_manifest_code, md_supplier_id, md_entry_date) VALUES (" & staff_id & ", '" & manifest_code & "', " & supplier_id & ", " & temp_date & ")"
        AddManifest = DoQuery(query)

    End Function

    Public Function UpdateManifest(ByVal manifest_id As Integer, ByVal staff_id As Integer, ByVal manifest_code As String, ByVal supplier_id As Integer, Optional ByVal entry_date As Date = Nothing) As Integer

        EscapeStr(manifest_code)
        Dim query As String = "UPDATE manifest_details SET md_staff_id = " & staff_id & ", md_manifest_code = '" & manifest_code & "', md_supplier_id = " & supplier_id & If(entry_date = Date.MinValue, "", ", md_entry_date = " & entry_date.ToString(DTS_YYYYMMDDHHmmSS)) & ", md_updated_date = NOW() WHERE md_manifest_id = " & manifest_id
        UpdateManifest = DoQuery(query)

    End Function

    Public Function GetManifestUpdatedDate(ByVal manifest_id As Integer) As Date

        Dim result = SelectQuery("SELECT md_updated_date FROM manifest_details WHERE md_manifest_id = " & manifest_id)
        If IsNothing(result) Then
            GetManifestUpdatedDate = Nothing
        Else
            GetManifestUpdatedDate = result.First()("md_updated_date")
        End If

    End Function

    Public Function IsManifestCodeTaken(ByVal manifest_code As String) As Boolean

        EscapeStr(manifest_code)
        Dim results = SelectQuery("SELECT count(*) as is_taken FROM manifest_details WHERE md_manifest_code = '" & manifest_code & "'")
        If IsNothing(results) Then
            IsManifestCodeTaken = False
        Else
            IsManifestCodeTaken = results.First()("is_taken") > 0
        End If

    End Function

    Public Function GetManifestDetails(ByVal manifest_id As Integer) As Dictionary(Of String, Object)

        Dim query As String = "SELECT * FROM manifest_details WHERE md_manifest_id = " & manifest_id
        Dim results = SelectQuery(query)
        If IsNothing(results) Then
            GetManifestDetails = Nothing
        Else
            GetManifestDetails = results.First()
        End If

    End Function

    Public Function GetManifestIDByCodeAndSupplier(ByVal supplier_id As Integer, ByVal manifest_code As String) As Integer

        EscapeStr(manifest_code)
        Dim result = SelectQuery("SELECT md_manifest_id FROM manifest_details WHERE md_supplier_id = " & supplier_id & " AND md_manifest_code = '" & manifest_code & "'")
        If IsNothing(result) Then
            GetManifestIDByCodeAndSupplier = 0
        Else
            GetManifestIDByCodeAndSupplier = result.First()("md_manifest_id")
        End If

    End Function

    Public Function GetManifestDetailsUnderSupplier(ByVal supplier_id As Integer) As Dictionary(Of String, Object)()

        GetManifestDetailsUnderSupplier = SelectQuery("SELECT * FROM manifest_details WHERE md_supplier_id = " & supplier_id)

    End Function

    Public Function GetManifestStocks(ByVal manifest_id As Integer) As Dictionary(Of String, Object)()

        GetManifestStocks = SelectQuery("SELECT * FROM product_stocks WHERE ps_manifest_id = " & manifest_id)

    End Function

    Public Function DoesLotNumberExists(ByVal lot_number As String) As Boolean

        EscapeStr(lot_number)
        Dim result = SelectQuery("SELECT count(*) as does_exist FROM view_product_stocks_in WHERE lot_number = '" & lot_number & "'")
        If IsNothing(result) Then
            DoesLotNumberExists = False
        Else
            DoesLotNumberExists = Val(result.First()("does_exist")) > 0
        End If

    End Function

    Public Function GetSuppliers(Optional ByVal search_tag As String = "") As Dictionary(Of String, Object)()

        EscapeStr(search_tag)
        Dim query As String = "SELECT * FROM suppliers"
        If Not search_tag = "" Then query &= " WHERE s_supplier_name LIKE '%" & search_tag & "%'"
        Dim results = SelectQuery(query)
        GetSuppliers = results

    End Function

    Public Function GetSupplierDetail(ByVal supplier_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM suppliers WHERE s_supplier_id = " & supplier_id)
        If IsNothing(result) Then
            GetSupplierDetail = Nothing
        Else
            GetSupplierDetail = result.First()
        End If

    End Function

    Public Function GetSupplierID(ByVal supplier_name As String) As Integer

        EscapeStr(supplier_name)
        Dim result = SelectQuery("SELECT s_supplier_id FROM suppliers WHERE s_supplier_name = '" & supplier_name & "'")
        If IsNothing(result) Then
            GetSupplierID = 0
        Else
            GetSupplierID = result.First()("s_supplier_id")
        End If

    End Function

    Public Function UpdateSupplierDetail(ByVal supplier_id As Integer, ByVal supplier_name As String) As Boolean

        UpdateSupplierDetail = DoQuery("UPDATE suppliers SET s_supplier_name = '" & supplier_name & "' WHERE s_supplier_id = " & supplier_id) > 0

    End Function

    Public Function AddSupplier(ByVal supplier_name As String) As Integer

        EscapeStr(supplier_name)
        AddSupplier = DoQuery("INSERT INTO suppliers (s_supplier_name) VALUES ('" & supplier_name & "')")

    End Function

    Public Function GetInventoryForProducts(ByVal product_ids As List(Of Integer)) As Dictionary(Of String, Object)()

        Dim id_filter As String = ""
        For Each product_id In product_ids
            id_filter &= If(id_filter = "", product_id, ", " & product_id)
        Next

        Dim query = "SELECT * FROM view_product_stocks_inventory WHERE product_id IN (" & id_filter & ") ORDER BY product_name ASC"
        GetInventoryForProducts = SelectQuery(query)

    End Function

    Public Function AddStockMigrate(ByVal stock_id As Integer, ByVal stock_quantity As Double, ByVal staff_id As Integer, ByVal migrate_date As Date, ByVal reason As String) As Integer

        EscapeStr(reason)
        AddStockMigrate = DoQuery("INSERT INTO migrated_stocks (ms_stock_id, ms_stock_quantity, ms_staff_id, ms_migrate_date, ms_reason) VALUES (" & stock_id & ", " & stock_quantity & ", " & staff_id & ", '" & migrate_date.ToString(DTS_YYYYMMDDHHmmSS) & "', '" & reason & "')")

    End Function

    Public Function GetStockMigrateDetails(ByVal migrate_id As Integer) As Dictionary(Of String, Object)

        Dim result = SelectQuery("SELECT * FROM migrated_stocks WHERE ms_migrate_id = " & migrate_id)
        If IsNothing(result) Then
            GetStockMigrateDetails = Nothing
        Else
            GetStockMigrateDetails = result.First()
        End If

    End Function

    Public Function UpdateStockMigrateDetails(ByVal migrate_id As Integer, ByVal stock_id As Integer, ByVal stock_quantity As Double, ByVal staff_id As Integer, Optional ByVal migrate_date As Date = Nothing, Optional ByVal reason As String = Nothing) As Boolean

        EscapeStr(reason)
        Dim query = "UPDATE migrate_details SET ms_stock_id = " & stock_id & ", ms_stock_quantity = " & stock_quantity & ", ms_staff_id = " & staff_id
        query &= If(Not migrate_date = Date.MinValue, ", ms_migrate_date = '" & migrate_date.ToString(DTS_YYYYMMDDHHmmSS) & "'", "")
        query &= If(IsNothing(reason), ", ms_reason = '" & reason & "'", "")
        query &= " WHERE ms_migrate_id = " & migrate_id
        UpdateStockMigrateDetails = DoQuery(query) > 0

    End Function

    Public Function IsStockInFlow(ByVal stock_id As Integer) As Boolean

        IsStockInFlow = False
        Dim result = SelectQuery("SELECT count(*) as is_distributed FROM distribution_items WHERE di_stock_id = " & stock_id)
        If Not IsNothing(result) Then IsStockInFlow = result.First()("is_distributed") > 0

        If Not IsStockInFlow Then
            result = SelectQuery("SELECT count(*) as is_transacted FROM transaction_items WHERE ti_stock_id = " & stock_id)
            If Not IsNothing(result) Then IsStockInFlow = result.First()("is_transacted") > 0
        End If

        If Not IsStockInFlow Then
            result = SelectQuery("SELECT count(*) as is_migrated FROM migrated_stocks WHERE ms_stock_id = " & stock_id)
            If Not IsNothing(result) Then IsStockInFlow = result.First()("is_migrated") > 0
        End If

    End Function

    Public Function GetManifestsWithinDate(Optional ByVal from_date As Date = Nothing, Optional ByVal to_date As Date = Nothing)

        Dim start_date As String = If(from_date = Date.MinValue, "NOW()", "'" & from_date.ToString(DTS_YYYYMMDD) & "'")
        Dim end_date As String = If(to_date = Date.MinValue, "NOW()", "'" & to_date.ToString(DTS_YYYYMMDD) & "'")

        GetManifestsWithinDate = SelectQuery("SELECT * FROM manifest_details WHERE md_entry_date >= " & start_date & " AND md_entry_date <= " & end_date)

    End Function

    Public Function GetProductStockHistory(ByVal product_id As Integer) As Dictionary(Of String, Object)()

        GetProductStockHistory = SelectQuery("SELECT vpsi.*, IFNULL(vpsfr.quantity_available, 0) as remaining_quantity FROM view_product_stocks_in vpsi LEFT JOIN view_product_stocks_for_release vpsfr ON vpsi.stock_id = vpsfr.stock_id WHERE vpsi.product_id = " & product_id)

    End Function

    Public Function GetStockTransactionHistory(ByVal stock_id As Integer) As Dictionary(Of String, Object)()

        GetStockTransactionHistory = SelectQuery("SELECT IF(ISNULL(c_customer_id), 'Guest', CONCAT(c.c_first_name, ' ', c.c_last_name)) as customer_name , td.td_invoice_no as invoice_no, td.td_transaction_date as transaction_date, ti.ti_item_quantity as transacted_quantity FROM transaction_items ti INNER JOIN transaction_details td ON td.td_transaction_id = ti.ti_transaction_id LEFT JOIN customers c ON c.c_customer_id = td.td_customer_id WHERE ti.ti_stock_id = " & stock_id)

    End Function

End Class
