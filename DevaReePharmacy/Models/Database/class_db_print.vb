﻿Public Class class_db_print
    Inherits class_db

    Public Function GetProductIDListGroupedByCategory(Optional ByVal filter_id As List(Of Integer) = Nothing) As Dictionary(Of String, Object)()

        Dim temp_filter As String = ""
        If Not IsNothing(filter_id) Then
            For Each id In filter_id
                If Not temp_filter = "" Then temp_filter &= ","
                temp_filter &= id
            Next
            temp_filter = " WHERE product_id IN (" & temp_filter & ")"
        End If
        GetProductIDListGroupedByCategory = SelectQuery("SELECT product_id FROM view_product_details" & temp_filter & " ORDER BY category_name ASC, product_name ASC")

    End Function

End Class
