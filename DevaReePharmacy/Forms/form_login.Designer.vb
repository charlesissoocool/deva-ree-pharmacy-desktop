﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_login))
        Me.group_login = New System.Windows.Forms.GroupBox()
        Me.prompt_login = New DevaReePharmacy.PromptLabel()
        Me.panel_login = New System.Windows.Forms.Panel()
        Me.panel_button = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_login = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.text_user_name = New System.Windows.Forms.TextBox()
        Me.text_password = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.image_logo = New System.Windows.Forms.PictureBox()
        Me.label_copyright = New System.Windows.Forms.Label()
        Me.button_about = New System.Windows.Forms.Button()
        Me.button_quit = New System.Windows.Forms.Button()
        Me.image_icon = New System.Windows.Forms.PictureBox()
        Me.label_last_updated = New DevaReePharmacy.AntiAliasedLabel()
        Me.group_login.SuspendLayout()
        Me.panel_login.SuspendLayout()
        Me.panel_button.SuspendLayout()
        CType(Me.image_logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.image_icon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'group_login
        '
        Me.group_login.Controls.Add(Me.prompt_login)
        Me.group_login.Controls.Add(Me.panel_login)
        Me.group_login.Controls.Add(Me.image_logo)
        Me.group_login.Location = New System.Drawing.Point(282, 12)
        Me.group_login.Name = "group_login"
        Me.group_login.Size = New System.Drawing.Size(411, 430)
        Me.group_login.TabIndex = 1
        Me.group_login.TabStop = False
        '
        'prompt_login
        '
        Me.prompt_login.BackColor = System.Drawing.Color.FromArgb(CType(CType(110, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.prompt_login.Location = New System.Drawing.Point(19, 163)
        Me.prompt_login.Message = "Please enter login credentials"
        Me.prompt_login.Mode = DevaReePharmacy.PromptLabel.PromptLabelMode.Info
        Me.prompt_login.Name = "prompt_login"
        Me.prompt_login.Size = New System.Drawing.Size(368, 41)
        Me.prompt_login.TabIndex = 9
        Me.prompt_login.Visible = False
        '
        'panel_login
        '
        Me.panel_login.Controls.Add(Me.panel_button)
        Me.panel_login.Controls.Add(Me.Label1)
        Me.panel_login.Controls.Add(Me.text_user_name)
        Me.panel_login.Controls.Add(Me.text_password)
        Me.panel_login.Controls.Add(Me.Label2)
        Me.panel_login.Location = New System.Drawing.Point(19, 210)
        Me.panel_login.Name = "panel_login"
        Me.panel_login.Size = New System.Drawing.Size(368, 209)
        Me.panel_login.TabIndex = 2
        '
        'panel_button
        '
        Me.panel_button.Controls.Add(Me.button_cancel)
        Me.panel_button.Controls.Add(Me.button_login)
        Me.panel_button.Location = New System.Drawing.Point(93, 123)
        Me.panel_button.Name = "panel_button"
        Me.panel_button.Size = New System.Drawing.Size(194, 73)
        Me.panel_button.TabIndex = 2
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.Enabled = False
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(94, 0)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(97, 75)
        Me.button_cancel.TabIndex = 3
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_login
        '
        Me.button_login.BackColor = System.Drawing.Color.Transparent
        Me.button_login.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_login.FlatAppearance.BorderSize = 0
        Me.button_login.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_login.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_login.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_login.Image = CType(resources.GetObject("button_login.Image"), System.Drawing.Image)
        Me.button_login.Location = New System.Drawing.Point(0, 0)
        Me.button_login.Name = "button_login"
        Me.button_login.Size = New System.Drawing.Size(97, 75)
        Me.button_login.TabIndex = 2
        Me.button_login.Text = "Login"
        Me.button_login.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_login.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(2, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Username"
        '
        'text_user_name
        '
        Me.text_user_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_user_name.ForeColor = System.Drawing.Color.Black
        Me.text_user_name.Location = New System.Drawing.Point(6, 36)
        Me.text_user_name.Name = "text_user_name"
        Me.text_user_name.Size = New System.Drawing.Size(356, 29)
        Me.text_user_name.TabIndex = 0
        '
        'text_password
        '
        Me.text_password.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_password.ForeColor = System.Drawing.Color.Black
        Me.text_password.Location = New System.Drawing.Point(7, 88)
        Me.text_password.Name = "text_password"
        Me.text_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.text_password.Size = New System.Drawing.Size(355, 29)
        Me.text_password.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(2, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 21)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Password"
        '
        'image_logo
        '
        Me.image_logo.Location = New System.Drawing.Point(45, 17)
        Me.image_logo.Name = "image_logo"
        Me.image_logo.Size = New System.Drawing.Size(285, 140)
        Me.image_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.image_logo.TabIndex = 8
        Me.image_logo.TabStop = False
        '
        'label_copyright
        '
        Me.label_copyright.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.label_copyright.AutoSize = True
        Me.label_copyright.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_copyright.Location = New System.Drawing.Point(434, 458)
        Me.label_copyright.Name = "label_copyright"
        Me.label_copyright.Size = New System.Drawing.Size(136, 51)
        Me.label_copyright.TabIndex = 2
        Me.label_copyright.Text = "Customized for Demo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " © Copyright 2015" & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.label_copyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_about
        '
        Me.button_about.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_about.BackColor = System.Drawing.Color.Transparent
        Me.button_about.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_about.FlatAppearance.BorderSize = 0
        Me.button_about.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_about.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_about.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_about.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_about.Image = CType(resources.GetObject("button_about.Image"), System.Drawing.Image)
        Me.button_about.Location = New System.Drawing.Point(847, 1)
        Me.button_about.Name = "button_about"
        Me.button_about.Size = New System.Drawing.Size(34, 36)
        Me.button_about.TabIndex = 4
        Me.button_about.TabStop = False
        Me.button_about.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_about.UseVisualStyleBackColor = True
        '
        'button_quit
        '
        Me.button_quit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_quit.BackColor = System.Drawing.Color.Transparent
        Me.button_quit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_quit.FlatAppearance.BorderSize = 0
        Me.button_quit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_quit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_quit.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_quit.Image = CType(resources.GetObject("button_quit.Image"), System.Drawing.Image)
        Me.button_quit.Location = New System.Drawing.Point(884, 1)
        Me.button_quit.Name = "button_quit"
        Me.button_quit.Size = New System.Drawing.Size(34, 36)
        Me.button_quit.TabIndex = 5
        Me.button_quit.TabStop = False
        Me.button_quit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_quit.UseVisualStyleBackColor = True
        '
        'image_icon
        '
        Me.image_icon.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.image_icon.Image = CType(resources.GetObject("image_icon.Image"), System.Drawing.Image)
        Me.image_icon.Location = New System.Drawing.Point(452, 405)
        Me.image_icon.Name = "image_icon"
        Me.image_icon.Size = New System.Drawing.Size(100, 50)
        Me.image_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.image_icon.TabIndex = 7
        Me.image_icon.TabStop = False
        '
        'label_last_updated
        '
        Me.label_last_updated.AutoEllipsis = False
        Me.label_last_updated.Font = New System.Drawing.Font("Open Sans", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_last_updated.ForeColor = System.Drawing.Color.Black
        Me.label_last_updated.Location = New System.Drawing.Point(8, 5)
        Me.label_last_updated.Name = "label_last_updated"
        Me.label_last_updated.Size = New System.Drawing.Size(86, 15)
        Me.label_last_updated.TabIndex = 6
        Me.label_last_updated.Text = "Last Updated: -"
        Me.label_last_updated.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_last_updated.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_last_updated.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_last_updated.VerticalDirection = False
        '
        'form_login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(923, 504)
        Me.ControlBox = False
        Me.Controls.Add(Me.image_icon)
        Me.Controls.Add(Me.label_last_updated)
        Me.Controls.Add(Me.button_about)
        Me.Controls.Add(Me.button_quit)
        Me.Controls.Add(Me.label_copyright)
        Me.Controls.Add(Me.group_login)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "form_login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Deva Ree Pharma"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.group_login.ResumeLayout(False)
        Me.panel_login.ResumeLayout(False)
        Me.panel_login.PerformLayout()
        Me.panel_button.ResumeLayout(False)
        CType(Me.image_logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.image_icon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents group_login As System.Windows.Forms.GroupBox
    Friend WithEvents image_logo As System.Windows.Forms.PictureBox
    Friend WithEvents panel_login As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents button_login As System.Windows.Forms.Button
    Friend WithEvents text_user_name As System.Windows.Forms.TextBox
    Friend WithEvents text_password As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents prompt_login As DevaReePharmacy.PromptLabel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents panel_button As System.Windows.Forms.Panel
    Friend WithEvents label_copyright As System.Windows.Forms.Label
    Friend WithEvents button_quit As System.Windows.Forms.Button
    Friend WithEvents button_about As System.Windows.Forms.Button
    Friend WithEvents label_last_updated As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents image_icon As System.Windows.Forms.PictureBox

End Class
