﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_staff_management
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_staff_management))
        Me.list_user = New System.Windows.Forms.ListView()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.text_search = New System.Windows.Forms.TextBox()
        Me.panel_controls = New System.Windows.Forms.Panel()
        Me.button_resort = New System.Windows.Forms.Button()
        Me.button_activate_user = New System.Windows.Forms.Button()
        Me.button_deactivate_user = New System.Windows.Forms.Button()
        Me.button_edit_user = New System.Windows.Forms.Button()
        Me.button_add_user = New System.Windows.Forms.Button()
        Me.page_navigator = New DevaReePharmacy.PageNavigator()
        Me.panel_controls.SuspendLayout()
        Me.SuspendLayout()
        '
        'list_user
        '
        Me.list_user.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_user.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.list_user.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader2, Me.ColumnHeader1})
        Me.list_user.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_user.ForeColor = System.Drawing.Color.Black
        Me.list_user.FullRowSelect = True
        Me.list_user.GridLines = True
        Me.list_user.Location = New System.Drawing.Point(0, 122)
        Me.list_user.Name = "list_user"
        Me.list_user.Size = New System.Drawing.Size(860, 295)
        Me.list_user.TabIndex = 8
        Me.list_user.TabStop = False
        Me.list_user.UseCompatibleStateImageBehavior = False
        Me.list_user.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Username"
        Me.ColumnHeader6.Width = 122
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Full Name"
        Me.ColumnHeader7.Width = 134
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Permissions"
        Me.ColumnHeader2.Width = 146
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Account Status"
        Me.ColumnHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.ColumnHeader1.Width = 177
        '
        'text_search
        '
        Me.text_search.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_search.ForeColor = System.Drawing.Color.Black
        Me.text_search.Location = New System.Drawing.Point(0, 96)
        Me.text_search.Name = "text_search"
        Me.text_search.Size = New System.Drawing.Size(860, 29)
        Me.text_search.TabIndex = 13
        '
        'panel_controls
        '
        Me.panel_controls.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_controls.BackColor = System.Drawing.Color.Silver
        Me.panel_controls.Controls.Add(Me.button_resort)
        Me.panel_controls.Controls.Add(Me.button_activate_user)
        Me.panel_controls.Controls.Add(Me.button_deactivate_user)
        Me.panel_controls.Controls.Add(Me.button_edit_user)
        Me.panel_controls.Controls.Add(Me.button_add_user)
        Me.panel_controls.Location = New System.Drawing.Point(0, 0)
        Me.panel_controls.Name = "panel_controls"
        Me.panel_controls.Size = New System.Drawing.Size(860, 60)
        Me.panel_controls.TabIndex = 14
        '
        'button_resort
        '
        Me.button_resort.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_resort.BackColor = System.Drawing.Color.Transparent
        Me.button_resort.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_resort.FlatAppearance.BorderSize = 0
        Me.button_resort.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_resort.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_resort.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_resort.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_resort.ForeColor = System.Drawing.Color.White
        Me.button_resort.Image = CType(resources.GetObject("button_resort.Image"), System.Drawing.Image)
        Me.button_resort.Location = New System.Drawing.Point(739, 0)
        Me.button_resort.Name = "button_resort"
        Me.button_resort.Size = New System.Drawing.Size(121, 60)
        Me.button_resort.TabIndex = 17
        Me.button_resort.TabStop = False
        Me.button_resort.Text = "Re-sort Users"
        Me.button_resort.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_resort.UseVisualStyleBackColor = False
        '
        'button_activate_user
        '
        Me.button_activate_user.BackColor = System.Drawing.Color.Transparent
        Me.button_activate_user.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_activate_user.Enabled = False
        Me.button_activate_user.FlatAppearance.BorderSize = 0
        Me.button_activate_user.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_activate_user.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_activate_user.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_activate_user.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_activate_user.ForeColor = System.Drawing.Color.White
        Me.button_activate_user.Image = CType(resources.GetObject("button_activate_user.Image"), System.Drawing.Image)
        Me.button_activate_user.Location = New System.Drawing.Point(267, 0)
        Me.button_activate_user.Name = "button_activate_user"
        Me.button_activate_user.Size = New System.Drawing.Size(83, 60)
        Me.button_activate_user.TabIndex = 12
        Me.button_activate_user.TabStop = False
        Me.button_activate_user.Text = "Activate"
        Me.button_activate_user.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_activate_user.UseVisualStyleBackColor = False
        '
        'button_deactivate_user
        '
        Me.button_deactivate_user.BackColor = System.Drawing.Color.Transparent
        Me.button_deactivate_user.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_deactivate_user.Enabled = False
        Me.button_deactivate_user.FlatAppearance.BorderSize = 0
        Me.button_deactivate_user.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_deactivate_user.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_deactivate_user.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_deactivate_user.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_deactivate_user.ForeColor = System.Drawing.Color.White
        Me.button_deactivate_user.Image = CType(resources.GetObject("button_deactivate_user.Image"), System.Drawing.Image)
        Me.button_deactivate_user.Location = New System.Drawing.Point(178, 0)
        Me.button_deactivate_user.Name = "button_deactivate_user"
        Me.button_deactivate_user.Size = New System.Drawing.Size(83, 60)
        Me.button_deactivate_user.TabIndex = 11
        Me.button_deactivate_user.TabStop = False
        Me.button_deactivate_user.Text = "Deactivate"
        Me.button_deactivate_user.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_deactivate_user.UseVisualStyleBackColor = False
        '
        'button_edit_user
        '
        Me.button_edit_user.BackColor = System.Drawing.Color.Transparent
        Me.button_edit_user.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_edit_user.Enabled = False
        Me.button_edit_user.FlatAppearance.BorderSize = 0
        Me.button_edit_user.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_edit_user.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_edit_user.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_edit_user.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_edit_user.ForeColor = System.Drawing.Color.White
        Me.button_edit_user.Image = CType(resources.GetObject("button_edit_user.Image"), System.Drawing.Image)
        Me.button_edit_user.Location = New System.Drawing.Point(89, 0)
        Me.button_edit_user.Name = "button_edit_user"
        Me.button_edit_user.Size = New System.Drawing.Size(83, 60)
        Me.button_edit_user.TabIndex = 10
        Me.button_edit_user.TabStop = False
        Me.button_edit_user.Text = "Edit User"
        Me.button_edit_user.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_edit_user.UseVisualStyleBackColor = False
        '
        'button_add_user
        '
        Me.button_add_user.BackColor = System.Drawing.Color.Transparent
        Me.button_add_user.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_user.FlatAppearance.BorderSize = 0
        Me.button_add_user.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_add_user.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_add_user.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_user.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_user.ForeColor = System.Drawing.Color.White
        Me.button_add_user.Image = CType(resources.GetObject("button_add_user.Image"), System.Drawing.Image)
        Me.button_add_user.Location = New System.Drawing.Point(0, 0)
        Me.button_add_user.Name = "button_add_user"
        Me.button_add_user.Size = New System.Drawing.Size(83, 60)
        Me.button_add_user.TabIndex = 9
        Me.button_add_user.TabStop = False
        Me.button_add_user.Text = "Add User"
        Me.button_add_user.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_user.UseVisualStyleBackColor = False
        '
        'page_navigator
        '
        Me.page_navigator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.page_navigator.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator.ListView = Nothing
        Me.page_navigator.Location = New System.Drawing.Point(0, 59)
        Me.page_navigator.Loopable = True
        Me.page_navigator.Name = "page_navigator"
        Me.page_navigator.PageSize = 2
        Me.page_navigator.Size = New System.Drawing.Size(860, 37)
        Me.page_navigator.TabIndex = 15
        Me.page_navigator.TotalItems = 0
        '
        'form_staff_management
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(860, 417)
        Me.Controls.Add(Me.page_navigator)
        Me.Controls.Add(Me.panel_controls)
        Me.Controls.Add(Me.text_search)
        Me.Controls.Add(Me.list_user)
        Me.Name = "form_staff_management"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Staff Management"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_controls.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents list_user As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents text_search As System.Windows.Forms.TextBox
    Friend WithEvents panel_controls As System.Windows.Forms.Panel
    Friend WithEvents button_edit_user As System.Windows.Forms.Button
    Friend WithEvents button_add_user As System.Windows.Forms.Button
    Friend WithEvents button_deactivate_user As System.Windows.Forms.Button
    Friend WithEvents button_activate_user As System.Windows.Forms.Button
    Friend WithEvents page_navigator As DevaReePharmacy.PageNavigator
    Friend WithEvents button_resort As System.Windows.Forms.Button
End Class
