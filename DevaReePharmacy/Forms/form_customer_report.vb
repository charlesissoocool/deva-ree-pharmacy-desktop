﻿Public Class form_customer_report

    Private _current_offset As Integer = 0
    Private sql As New class_db_transaction
    Private _customer_col_width() As Double = {20, 10, 10, 20, 20, 20}
    Private _customer_transactions_col_width() As Double = {20, 20, 20, 20, 20}
    Private _customer_payments_col_width() As Double = {20, 40, 40}
    Private _customer_search_tag As String = Nothing

    Private _current_offset_transaction As Integer
    Private _current_sort_field_transaction As String
    Private _current_sort_order_transaction As String
    Private _transaction_column_sort As New Dictionary(Of Integer, String)
    Private _selected_customer_id As Integer = 0

    Private _customer_exclude_cleared As Boolean = False

    Private Sub list_credited_transactions_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles list_credited_transactions.ColumnClick

        If _transaction_column_sort.ContainsKey(e.Column) Then
            If _current_sort_field_transaction = _transaction_column_sort(e.Column) Then
                _current_sort_order_transaction = If(_current_sort_order_transaction = "ASC", "DESC", "ASC")
            Else
                _current_sort_field_transaction = _transaction_column_sort(e.Column)
            End If
            page_navigator_transaction.TotalItems = sql.GetCustomerTransactionsListCount(_selected_customer_id)
        End If

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        _transaction_column_sort.Add(0, "td.td_invoice_no")
        _transaction_column_sort.Add(1, "vcd.customer_name")
        _transaction_column_sort.Add(2, "vcd.credit_id")
        _transaction_column_sort.Add(3, "vcd.credit_amount")
        _transaction_column_sort.Add(4, "vcd.due_date")
        'panel_total_amount.BackColor = module_colors.color_green
        'panel_credit_amount.BackColor = module_colors.color_red
        label_total_payed_amount.ForeColor = module_colors.color_light_green
        label_total_balance_amount.ForeColor = module_colors.color_pink
        label_total_credit_amount.ForeColor = module_colors.color_yellow
        page_navigator_customer.TotalItems = sql.GetCustomerReportListCount(_customer_search_tag, _customer_exclude_cleared)
        page_navigator_customer.ListView = list_customers
        panel_controls.BackColor = module_colors.color_gray

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        'panel_controls.BackColor = module_colors.color_gray
        ''list_customers.Height = (Me.ClientSize.Height / 2) - list_customers.Top
        ''panel_header.Top = list_customers.
        list_credited_transactions.Left = list_customers.Right
        list_credited_transactions.Width = Me.ClientSize.Width - list_customers.Width
        list_credited_transactions.Top = panel_transaction_header.Bottom
        list_credited_transactions.Height = (Me.ClientSize.Height - panel_payed_amount.Height - panel_transaction_header.Bottom - panel_transaction_header.Height) / 2
        label_no_transaction.Left = list_credited_transactions.Left + ((list_credited_transactions.Width / 2) - (label_no_transaction.Width / 2))
        label_no_transaction.Top = list_credited_transactions.Top + ((list_credited_transactions.Height / 2) - (label_no_transaction.Height / 2))
        panel_payment_header.Left = list_credit_payments.Left
        panel_payment_header.Top = list_credited_transactions.Bottom
        panel_payment_header.Width = list_credited_transactions.Width
        list_credit_payments.Left = list_credited_transactions.Left
        list_credit_payments.Width = list_credited_transactions.Width
        list_credit_payments.Top = panel_payment_header.Bottom
        list_credit_payments.Height = list_credited_transactions.Height
        label_no_payments.Left = list_credit_payments.Left + ((list_credit_payments.Width / 2) - (label_no_payments.Width / 2))
        label_no_payments.Top = list_credit_payments.Top + ((list_credit_payments.Height / 2) - (label_no_payments.Height / 2))
        panel_balance_amount.Width = (list_credit_payments.Width / 3)
        panel_payed_amount.Width = panel_balance_amount.Width
        panel_credit_amount.Width = panel_balance_amount.Width
        panel_balance_amount.Left = Me.ClientSize.Width - panel_balance_amount.Width
        panel_balance_amount.Top = Me.ClientSize.Height - panel_payed_amount.Height
        panel_payed_amount.Left = panel_balance_amount.Left - panel_payed_amount.Width
        panel_payed_amount.Top = panel_balance_amount.Top
        panel_credit_amount.Left = panel_payed_amount.Left - panel_credit_amount.Width
        panel_credit_amount.Top = panel_balance_amount.Top
        ResizeListViewColumn(list_credited_transactions, _customer_transactions_col_width)
        ResizeListViewColumn(list_credit_payments, _customer_payments_col_width)
        page_navigator_customer.Width = list_customers.Width

    End Sub

    Private Sub SelectCustomer(ByVal customer_id As Integer, Optional ByVal offset As Integer = 0, Optional ByVal sort_field As String = "td.td_invoice_no", Optional ByVal sort_order As String = "ASC")

        Dim customer_detail As New Customer(customer_id)
        If IsNothing(customer_detail) Or customer_detail.CustomerID = 0 Then Exit Sub
        list_credited_transactions.Items.Clear()
        list_credit_payments.Items.Clear()
        list_credited_transactions_ItemSelectionChanged(Nothing, Nothing)
        label_customer_name.Text = customer_detail.CustomerName
        label_total_balance_amount.Text = "0.00"
        label_total_payed_amount.Text = "0.00"
        _current_offset_transaction = offset
        _current_sort_field_transaction = sort_field
        _current_sort_order_transaction = sort_order
        Dim results = sql.GetCustomerTransactionsList(customer_id, offset, , sort_field, sort_order)
        If Not IsNothing(results) Then
            For Each result In results
                Dim new_item As ListViewItem = list_credited_transactions.Items.Add(result("invoice_no").ToString().PadLeft(10, "0"))
                new_item.UseItemStyleForSubItems = False
                new_item.SubItems.Add(result("debtor_name"))
                If result("credit_id") = 0 Then
                    new_item.SubItems.Add("--")
                Else
                    new_item.SubItems.Add(result("credit_id").ToString().PadLeft(10, "0"))
                End If
                new_item.SubItems.Add(SYS_CURRENCY & FormatNumber(result("credit_amount"), 2, , , TriState.True))
                Dim new_sub As New ListViewItem.ListViewSubItem
                new_sub.Text = If(IsNothing(result("due_date")), "--", Date.Parse(result("due_date")).ToString(DTS_HUMAN))
                If (result("is_due")) And result("credit_amount") > result("total_payed") Then
                    new_sub.Font = New Font(list_credit_payments.Font, FontStyle.Bold)
                    new_sub.ForeColor = module_colors.color_red
                    new_item.ImageKey = "invalid"
                ElseIf result("credit_amount") <= result("total_payed") Then
                    new_item.ImageKey = "valid"
                Else
                    new_item.ImageKey = "active"
                    new_sub.ForeColor = Color.Black
                End If
                new_item.SubItems.Add(new_sub)
                new_item.Tag = result("credit_id")
            Next
        End If
        label_cart_count.Text = "Transactions (" & list_credited_transactions.Items.Count & " item/s)"
        list_credited_transactions.Tag = customer_id

    End Sub

    Private Sub SelectCredit(ByVal credit_id As Integer)

        list_credit_payments.Items.Clear()
        Dim credit_detail As New Credit(credit_id)
        If Not IsNothing(credit_detail.CreditPayments) Then
            For Each payments In credit_detail.CreditPayments
                Dim new_item As ListViewItem = list_credit_payments.Items.Add(SYS_CURRENCY & FormatNumber(payments.PaymentAmount, 2, , , TriState.True))
                new_item.SubItems.Add(payments.PaymentDate.ToString(DTS_HUMAN_WITH_TIME))
                new_item.SubItems.Add(payments.Staff.FullName)
            Next
            label_credit_payments.Text = "Payments for Credit #" & credit_id.ToString().PadLeft(10, "0")
        Else
            label_credit_payments.Text = "Payments for Credit"
        End If
        label_total_balance_amount.Text = FormatNumber(credit_detail.CreditAmount - credit_detail.TotalPaymentAmount, 2, , , TriState.True)
        label_total_payed_amount.Text = FormatNumber(credit_detail.TotalPaymentAmount, 2, , , TriState.True)
        label_total_credit_amount.Text = FormatNumber(credit_detail.CreditAmount, 2, , , TriState.True)
        list_credit_payments.Tag = credit_id

    End Sub

    Private Sub LoadCustomerReportList(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1)

        Dim results = sql.GetCustomerReportList(search_tag, offset, count, , , _customer_exclude_cleared)
        list_customers.Items.Clear()
        'list_credited_transactions.Items.Clear()
        'list_credit_payments.Items.Clear()
        If Not IsNothing(results) Then
            For Each result In results
                Dim new_item As MobileListItem = list_customers.Items.Add(result("customer_name") & " (" & result("transaction_count") & ")")
                If result("total_payed") < result("credit_amount") Then
                    new_item.SubText = SYS_CURRENCY & FormatNumber(result("credit_amount") - result("total_payed"), 2, , , TriState.True) & " Remaining balance"
                    new_item.ImageKey = "invalid"
                Else
                    new_item.SubText = "Cleared"
                    new_item.ImageKey = "valid"
                End If
                new_item.Tag = result("customer_id")
            Next
        End If

    End Sub

    Private Sub page_navigator_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator_customer.PageChanged

        LoadCustomerReportList(_customer_search_tag, offset, page_count)

    End Sub

    Private Sub list_customers_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles list_customers.ItemSelectionChanged

        If Not IsNothing(list_customers.SelectedItem) Then
            _current_offset_transaction = 0
            _current_sort_field_transaction = "td.td_invoice_no"
            _current_sort_order_transaction = "ASC"
            _selected_customer_id = Val(list_customers.SelectedItem.Tag)
            page_navigator_transaction.TotalItems = sql.GetCustomerTransactionsListCount(_selected_customer_id)
            list_credited_transactions.Visible = True
        Else
            list_credited_transactions.Visible = False
            label_customer_name.Text = "--"
        End If
        label_no_transaction.Visible = Not list_credited_transactions.Visible

    End Sub

    Private Sub list_credited_transactions_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles list_credited_transactions.ItemSelectionChanged

        If list_credited_transactions.SelectedItems.Count > 0 Then
            SelectCredit(Val(list_credited_transactions.SelectedItems(0).Tag))
            list_credit_payments.Visible = True
        Else
            label_credit_payments.Text = "Credit Payments"
            label_no_payments.Text = "No selected transaction."
            list_credit_payments.Visible = False
        End If
        label_no_payments.Visible = Not list_credit_payments.Visible
        panel_balance_amount.Visible = list_credit_payments.Visible
        panel_payed_amount.Visible = list_credit_payments.Visible
        panel_credit_amount.Visible = list_credit_payments.Visible

    End Sub

    Private Sub button_add_payment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_payment.Click

        Dim form_add_payment As New form_credit_payment
        form_add_payment.OpenCreditPayment(Me)
        page_navigator_customer.TotalItems = sql.GetCustomerReportListCount(_customer_search_tag, _customer_exclude_cleared)
        If Val(list_credited_transactions.Tag) > 0 Then SelectCustomer(Val(list_credited_transactions.Tag))
        If Val(list_credit_payments.Tag) > 0 Then SelectCredit(Val(list_credit_payments.Tag))

    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) And Not Trim(text_search.Text) = "" Then
            e.Handled = True
            _customer_search_tag = text_search.Text
            page_navigator_customer.TotalItems = sql.GetCustomerReportListCount(_customer_search_tag, _customer_exclude_cleared)
        ElseIf e.KeyChar = ChrW(Keys.Escape) Or (e.KeyChar = ChrW(Keys.Back) And Trim(text_search.Text) = "") Then
            e.Handled = True
            _customer_search_tag = Nothing
            text_search.Text = ""
            page_navigator_customer.TotalItems = sql.GetCustomerReportListCount(_customer_search_tag, _customer_exclude_cleared)
        End If

    End Sub

    Private Sub button_open_filter_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles button_open_filter.MouseDown

        menu_filter.Show(button_open_filter, e.Location)

    End Sub

    Private Sub menu_exclude_filter_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles menu_exclude_filter.CheckStateChanged

        _customer_exclude_cleared = menu_exclude_filter.Checked
        page_navigator_customer.TotalItems = sql.GetCustomerReportListCount(_customer_search_tag, _customer_exclude_cleared)

    End Sub

    Private Sub button_verify_transaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_verify_transaction.Click

        Dim customer_selector As New form_customer_selector
        If Not IsNothing(customer_selector.OpenCustomerSelectorDialog(Me, False)) Then
            page_navigator_customer.TotalItems = sql.GetCustomerReportListCount(_customer_search_tag, _customer_exclude_cleared)
        End If

    End Sub

    Private Sub page_navigator_transaction_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator_transaction.PageChanged

        SelectCustomer(_selected_customer_id, offset, _current_sort_field_transaction, _current_sort_order_transaction)

    End Sub

    Private Sub list_customers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_customers.Load

    End Sub

    Private Sub list_customers_ItemSelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_customers.ItemSelectionChanged

    End Sub

    Private Sub page_navigator_transaction_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles page_navigator_transaction.Load

    End Sub
End Class