﻿Public Class form_stock_management

    Dim sql As New class_db_stocks
    Dim col_width() As Double = {25, 15, 15, 15, 15, 15}
    Private _column_sort As New Dictionary(Of Integer, String)
    Private _current_search_tag As String = Nothing
    Private _current_offset As Integer
    Private _current_sort_field As String = "entry_date"
    Private _current_sort_order As String = "DESC"

    Private Sub AdjustInterface() Handles Me.Load

        panel_header.BackColor = module_colors.color_gray
        page_navigator.TotalItems = sql.GetStockListCount()
        page_navigator.ListView = list_stocks
        _column_sort.Add(0, "product_name")
        _column_sort.Add(2, "expiry_date")

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        ResizeListViewColumn(list_stocks, col_width)

    End Sub

    Private Function GetGroupCount(ByVal group_name As String) As Integer

        GetGroupCount = 0
        For Each group As ListViewGroup In list_stocks.Groups
            If group.Header = group_name Then GetGroupCount += 1
        Next

    End Function


    Private Sub LoadStockList(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0)

        Dim sql_user As New class_db_user

        list_stocks.Items.Clear()
        list_stocks.Groups.Clear()
        Dim result = sql.GetStockList(search_tag, offset, , _current_sort_field, _current_sort_order)
        _current_search_tag = search_tag
        _current_offset = offset
        Dim last_group_name As String = ""
        Dim last_group_key As String = ""
        Dim group_key As String = ""
        If Not IsNothing(result) Then
            For Each stock In result
                Dim staff_uploader = New User(Val(stock("staff_id")))
                Dim group_name As String = "#" & stock("manifest_code") & " (" & stock("supplier_name") & ") | " & stock("entry_date_formatted")
                If group_name = last_group_name Then
                    group_key = last_group_key
                Else
                    group_key = group_name & (GetGroupCount(group_name) + 1)
                    list_stocks.Groups.Add(group_key, group_name)
                    last_group_key = group_key
                    last_group_name = group_name
                End If
                'Dim group As ListViewGroup
                'If Not IsNothing(last_group) Then
                '    If last_group.Header = group_name Then
                '        group = last_group
                '    Else
                '        list_stocks.Groups.Add(last_group)
                '        group = New ListViewGroup(group_name)
                '    End If
                'Else
                '    group = New ListViewGroup(group_name)
                'End If

                Dim product_detail As New Product(stock("product_id"))
                Dim row_item As New ListViewItem
                row_item.Text = stock("product_name")
                row_item.Group = list_stocks.Groups(group_key)
                row_item.SubItems.Add(stock("lot_number"))
                row_item.SubItems.Add(stock("expiry_date_formatted"))
                row_item.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(stock("supplier_price")), , , , TriState.True) & " / " & If(product_detail.IsSoldInPacks, SYS_PACK_ALIAS, product_detail.ProductUnit))
                If product_detail.IsSoldInPacks Then
                    row_item.SubItems.Add(FormatNumber(stock("stock_quantity"), 0, , , TriState.True) & " " & product_detail.ProductUnit & " (" & (Math.Round(stock("stock_quantity") / product_detail.PackingSize, 0)) & " " & SYS_PACK_ALIAS & ")")
                Else
                    row_item.SubItems.Add(FormatNumber(stock("stock_quantity"), 0, , , TriState.True) & " " & product_detail.ProductUnit)
                End If
                Dim status = stock("status")
                row_item.SubItems.Add(stock("status"))
                Select Case status
                    Case "Expired"
                        row_item.BackColor = module_colors.color_red
                        row_item.ForeColor = Color.White
                    Case "Sold Out", "Invalid"
                        row_item.BackColor = module_colors.color_yellow
                        row_item.ForeColor = Color.Black
                    Case Else
                        row_item.BackColor = Color.White
                        row_item.ForeColor = Color.Black
                End Select
                row_item.Tag = stock("stock_id")
                list_stocks.Items.Add(row_item)
                'last_group = group
            Next
        End If

    End Sub

    Private Function DoesGroupExist(ByVal group_name As String) As Boolean

        For Each group As ListViewGroup In list_stocks.Groups
            If group.Name = group_name Then DoesGroupExist = True : Exit Function
        Next

        DoesGroupExist = False

    End Function

    Private Sub InvalidateStock(ByVal sender As Object, ByVal e As System.EventArgs)

        If sql.InvalidateStock(Val(sender.tag)) Then LoadStockList(_current_search_tag, _current_offset) Else MessageBox.Show("Failed to invalidate item.")

    End Sub

    Private Sub ValidateStock(ByVal sender As Object, ByVal e As System.EventArgs)

        If sql.ValidateStock(Val(sender.tag)) Then LoadStockList(_current_search_tag, _current_offset) Else MessageBox.Show("Failed to validate item.")

    End Sub

    Private Sub page_navigator_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator.PageChanged

        LoadStockList(_current_search_tag, offset)

    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            _current_search_tag = text_search.Text
            page_navigator.TotalItems = sql.GetStockListCount(_current_search_tag)
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            _current_search_tag = Nothing
            page_navigator.TotalItems = sql.GetStockListCount()
        End If

    End Sub

    Private Sub text_search_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_search.TextChanged

    End Sub

    Private Sub button_new_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim stock_editor As New form_stock_editor
        stock_editor.OpenStockEditor(Me)
        LoadStockList(_current_search_tag, _current_offset)

    End Sub

    Private Sub button_add_manifest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_manifest.Click

        Dim manifest_editor As New form_manifest_editor
        Dim manifest_details = manifest_editor.OpenManifestEditor(Me)
        If Not IsNothing(manifest_details) Then
            If Not manifest_details.Persist() Then
                MsgBox("Failed to add manifest.")
            Else
                RefreshPage()
                MsgBox("Manifest successfully added.", MsgBoxStyle.Information)
            End If
        End If

    End Sub

    Public Sub RefreshPage()

        page_navigator.TotalItems = sql.GetStockListCount()

    End Sub

    Private Sub list_stocks_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles list_stocks.ColumnClick

        If _column_sort.ContainsKey(e.Column) Then
            If _current_sort_field = _column_sort(e.Column) Then
                _current_sort_order = If(_current_sort_order = "ASC", "DESC", "ASC")
            Else
                _current_sort_field = _column_sort(e.Column)
            End If
            page_navigator.TotalItems = sql.GetStockListCount
        End If

    End Sub

    Private Sub list_stocks_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_stocks.DoubleClick

        If list_stocks.SelectedItems.Count > 0 Then
            button_edit_manifest.PerformClick()
        End If

    End Sub

    Private Sub list_stocks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_stocks.SelectedIndexChanged

        button_inspect_manifest.Enabled = list_stocks.SelectedItems.Count > 0
        button_edit_manifest.Enabled = button_inspect_manifest.Enabled And module_session.current_user_profile.Permission_Administator

    End Sub

    Private Sub button_inspect_manifest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_inspect_manifest.Click

        If list_stocks.SelectedItems.Count < 1 Then
            button_inspect_manifest.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            Dim sql_stock As New class_db_stocks
            Dim manifest_details As New Manifest(sql_stock.GetStockDetail(Val(sel_row.Tag))("manifest_id"))
            Dim manifest_editor As New form_manifest_editor
            manifest_editor.OpenManifestEditor(Me, manifest_details, True)
        End If

    End Sub

    Private Sub button_edit_manifest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_edit_manifest.Click

        If list_stocks.SelectedItems.Count < 1 Then
            button_edit_manifest.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            Dim sql_stock As New class_db_stocks
            Dim manifest_details As New Manifest(sql_stock.GetStockDetail(Val(sel_row.Tag))("manifest_id"))
            Dim manifest_editor As New form_manifest_editor
            manifest_details = manifest_editor.OpenManifestEditor(Me, manifest_details, False)
            If Not IsNothing(manifest_details) Then
                If manifest_details.Persist() Then
                    page_navigator.TotalItems = sql.GetStockListCount()
                    MsgBox("Manifest successfully edited.", MsgBoxStyle.Information)
                Else
                    MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                End If
            End If
        End If

    End Sub

    Private Sub button_resort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_resort.Click

        Dim sort_key = "manifest_id"
        If _current_sort_field = sort_key Then
            _current_sort_order = If(_current_sort_order = "ASC", "DESC", "ASC")
        Else
            _current_sort_field = sort_key
        End If
        page_navigator.TotalItems = sql.GetStockListCount

    End Sub

    Private Sub button_print_stock_list_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_print_stock_list.Click
        Dim price_list_generator As New form_stock_list_generator
        price_list_generator.ShowDialog(Me)
    End Sub
End Class