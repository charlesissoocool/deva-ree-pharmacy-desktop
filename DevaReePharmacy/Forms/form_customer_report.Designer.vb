﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_customer_report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_customer_report))
        Me.panel_transaction_header = New System.Windows.Forms.Panel()
        Me.list_credited_transactions = New System.Windows.Forms.ListView()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.imagelist_small_icons = New System.Windows.Forms.ImageList(Me.components)
        Me.list_credit_payments = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.panel_controls = New System.Windows.Forms.Panel()
        Me.button_add_payment = New System.Windows.Forms.Button()
        Me.button_verify_transaction = New System.Windows.Forms.Button()
        Me.panel_payed_amount = New System.Windows.Forms.Panel()
        Me.panel_balance_amount = New System.Windows.Forms.Panel()
        Me.imagelist_large_icons = New System.Windows.Forms.ImageList(Me.components)
        Me.panel_payment_header = New System.Windows.Forms.Panel()
        Me.text_search = New System.Windows.Forms.TextBox()
        Me.button_open_filter = New System.Windows.Forms.Button()
        Me.menu_filter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.menu_exclude_filter = New System.Windows.Forms.ToolStripMenuItem()
        Me.panel_credit_amount = New System.Windows.Forms.Panel()
        Me.page_navigator_transaction = New DevaReePharmacy.PageNavigator()
        Me.label_total_credit = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_credit_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_no_payments = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_no_transaction = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_credit_payments = New DevaReePharmacy.AntiAliasedLabel()
        Me.list_customers = New DevaReePharmacy.MobileList()
        Me.label_total_payed = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_payed_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_balance = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_balance_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.page_navigator_customer = New DevaReePharmacy.PageNavigator()
        Me.label_customer_name = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_cart_count = New DevaReePharmacy.AntiAliasedLabel()
        Me.panel_transaction_header.SuspendLayout()
        Me.panel_controls.SuspendLayout()
        Me.panel_payed_amount.SuspendLayout()
        Me.panel_balance_amount.SuspendLayout()
        Me.panel_payment_header.SuspendLayout()
        Me.menu_filter.SuspendLayout()
        Me.panel_credit_amount.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_transaction_header
        '
        Me.panel_transaction_header.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_transaction_header.BackColor = System.Drawing.Color.Gold
        Me.panel_transaction_header.Controls.Add(Me.label_customer_name)
        Me.panel_transaction_header.Controls.Add(Me.label_cart_count)
        Me.panel_transaction_header.Location = New System.Drawing.Point(267, 100)
        Me.panel_transaction_header.Name = "panel_transaction_header"
        Me.panel_transaction_header.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.panel_transaction_header.Size = New System.Drawing.Size(601, 37)
        Me.panel_transaction_header.TabIndex = 21
        '
        'list_credited_transactions
        '
        Me.list_credited_transactions.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.list_credited_transactions.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.list_credited_transactions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader4, Me.ColumnHeader10, Me.ColumnHeader8, Me.ColumnHeader9})
        Me.list_credited_transactions.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_credited_transactions.FullRowSelect = True
        Me.list_credited_transactions.GridLines = True
        Me.list_credited_transactions.HideSelection = False
        Me.list_credited_transactions.Location = New System.Drawing.Point(273, 141)
        Me.list_credited_transactions.MultiSelect = False
        Me.list_credited_transactions.Name = "list_credited_transactions"
        Me.list_credited_transactions.Size = New System.Drawing.Size(601, 92)
        Me.list_credited_transactions.SmallImageList = Me.imagelist_small_icons
        Me.list_credited_transactions.TabIndex = 6
        Me.list_credited_transactions.UseCompatibleStateImageBehavior = False
        Me.list_credited_transactions.View = System.Windows.Forms.View.Details
        Me.list_credited_transactions.Visible = False
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "D.R. No."
        Me.ColumnHeader7.Width = 130
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Credit Billed To"
        Me.ColumnHeader4.Width = 148
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Credit No."
        Me.ColumnHeader10.Width = 116
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Total Credit"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Credit Due"
        Me.ColumnHeader9.Width = 91
        '
        'imagelist_small_icons
        '
        Me.imagelist_small_icons.ImageStream = CType(resources.GetObject("imagelist_small_icons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imagelist_small_icons.TransparentColor = System.Drawing.Color.Transparent
        Me.imagelist_small_icons.Images.SetKeyName(0, "invalid")
        Me.imagelist_small_icons.Images.SetKeyName(1, "valid")
        Me.imagelist_small_icons.Images.SetKeyName(2, "active")
        '
        'list_credit_payments
        '
        Me.list_credit_payments.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.list_credit_payments.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.list_credit_payments.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.list_credit_payments.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_credit_payments.FullRowSelect = True
        Me.list_credit_payments.GridLines = True
        Me.list_credit_payments.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.list_credit_payments.HideSelection = False
        Me.list_credit_payments.Location = New System.Drawing.Point(267, 330)
        Me.list_credit_payments.MultiSelect = False
        Me.list_credit_payments.Name = "list_credit_payments"
        Me.list_credit_payments.Size = New System.Drawing.Size(601, 108)
        Me.list_credit_payments.TabIndex = 7
        Me.list_credit_payments.UseCompatibleStateImageBehavior = False
        Me.list_credit_payments.View = System.Windows.Forms.View.Details
        Me.list_credit_payments.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Amount"
        Me.ColumnHeader1.Width = 111
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Date"
        Me.ColumnHeader2.Width = 107
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Received by"
        Me.ColumnHeader3.Width = 97
        '
        'panel_controls
        '
        Me.panel_controls.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_controls.BackColor = System.Drawing.Color.Silver
        Me.panel_controls.Controls.Add(Me.button_add_payment)
        Me.panel_controls.Controls.Add(Me.button_verify_transaction)
        Me.panel_controls.Location = New System.Drawing.Point(0, 0)
        Me.panel_controls.Name = "panel_controls"
        Me.panel_controls.Size = New System.Drawing.Size(868, 63)
        Me.panel_controls.TabIndex = 27
        '
        'button_add_payment
        '
        Me.button_add_payment.BackColor = System.Drawing.Color.Transparent
        Me.button_add_payment.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_payment.FlatAppearance.BorderSize = 0
        Me.button_add_payment.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_add_payment.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_add_payment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_payment.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_payment.ForeColor = System.Drawing.Color.White
        Me.button_add_payment.Image = CType(resources.GetObject("button_add_payment.Image"), System.Drawing.Image)
        Me.button_add_payment.Location = New System.Drawing.Point(103, 2)
        Me.button_add_payment.Name = "button_add_payment"
        Me.button_add_payment.Size = New System.Drawing.Size(102, 60)
        Me.button_add_payment.TabIndex = 2
        Me.button_add_payment.Text = "Credit Payment"
        Me.button_add_payment.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_add_payment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_payment.UseVisualStyleBackColor = False
        '
        'button_verify_transaction
        '
        Me.button_verify_transaction.BackColor = System.Drawing.Color.Transparent
        Me.button_verify_transaction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_verify_transaction.FlatAppearance.BorderSize = 0
        Me.button_verify_transaction.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_verify_transaction.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_verify_transaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_verify_transaction.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_verify_transaction.ForeColor = System.Drawing.Color.White
        Me.button_verify_transaction.Image = CType(resources.GetObject("button_verify_transaction.Image"), System.Drawing.Image)
        Me.button_verify_transaction.Location = New System.Drawing.Point(0, 2)
        Me.button_verify_transaction.Name = "button_verify_transaction"
        Me.button_verify_transaction.Size = New System.Drawing.Size(108, 60)
        Me.button_verify_transaction.TabIndex = 1
        Me.button_verify_transaction.Text = "Add Customer"
        Me.button_verify_transaction.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_verify_transaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_verify_transaction.UseVisualStyleBackColor = False
        '
        'panel_payed_amount
        '
        Me.panel_payed_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_payed_amount.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.panel_payed_amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel_payed_amount.Controls.Add(Me.label_total_payed)
        Me.panel_payed_amount.Controls.Add(Me.label_total_payed_amount)
        Me.panel_payed_amount.Location = New System.Drawing.Point(484, 441)
        Me.panel_payed_amount.Name = "panel_payed_amount"
        Me.panel_payed_amount.Size = New System.Drawing.Size(174, 74)
        Me.panel_payed_amount.TabIndex = 28
        Me.panel_payed_amount.Visible = False
        '
        'panel_balance_amount
        '
        Me.panel_balance_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_balance_amount.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.panel_balance_amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel_balance_amount.Controls.Add(Me.label_total_balance)
        Me.panel_balance_amount.Controls.Add(Me.label_total_balance_amount)
        Me.panel_balance_amount.Location = New System.Drawing.Point(659, 441)
        Me.panel_balance_amount.Name = "panel_balance_amount"
        Me.panel_balance_amount.Size = New System.Drawing.Size(194, 74)
        Me.panel_balance_amount.TabIndex = 29
        Me.panel_balance_amount.Visible = False
        '
        'imagelist_large_icons
        '
        Me.imagelist_large_icons.ImageStream = CType(resources.GetObject("imagelist_large_icons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imagelist_large_icons.TransparentColor = System.Drawing.Color.Transparent
        Me.imagelist_large_icons.Images.SetKeyName(0, "invalid")
        Me.imagelist_large_icons.Images.SetKeyName(1, "valid")
        '
        'panel_payment_header
        '
        Me.panel_payment_header.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_payment_header.BackColor = System.Drawing.Color.Gold
        Me.panel_payment_header.Controls.Add(Me.label_credit_payments)
        Me.panel_payment_header.Location = New System.Drawing.Point(267, 287)
        Me.panel_payment_header.Name = "panel_payment_header"
        Me.panel_payment_header.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.panel_payment_header.Size = New System.Drawing.Size(601, 37)
        Me.panel_payment_header.TabIndex = 31
        '
        'text_search
        '
        Me.text_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_search.ForeColor = System.Drawing.Color.Black
        Me.text_search.Location = New System.Drawing.Point(0, 100)
        Me.text_search.Name = "text_search"
        Me.text_search.Size = New System.Drawing.Size(267, 29)
        Me.text_search.TabIndex = 3
        '
        'button_open_filter
        '
        Me.button_open_filter.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_open_filter.FlatAppearance.BorderSize = 0
        Me.button_open_filter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_open_filter.Image = CType(resources.GetObject("button_open_filter.Image"), System.Drawing.Image)
        Me.button_open_filter.Location = New System.Drawing.Point(239, 101)
        Me.button_open_filter.Name = "button_open_filter"
        Me.button_open_filter.Size = New System.Drawing.Size(25, 25)
        Me.button_open_filter.TabIndex = 4
        Me.button_open_filter.UseVisualStyleBackColor = True
        '
        'menu_filter
        '
        Me.menu_filter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menu_exclude_filter})
        Me.menu_filter.Name = "menu_filter"
        Me.menu_filter.Size = New System.Drawing.Size(214, 26)
        Me.menu_filter.Text = "Filter"
        '
        'menu_exclude_filter
        '
        Me.menu_exclude_filter.CheckOnClick = True
        Me.menu_exclude_filter.Name = "menu_exclude_filter"
        Me.menu_exclude_filter.Size = New System.Drawing.Size(213, 22)
        Me.menu_exclude_filter.Text = "Exclude cleared customers"
        '
        'panel_credit_amount
        '
        Me.panel_credit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_credit_amount.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.panel_credit_amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel_credit_amount.Controls.Add(Me.label_total_credit)
        Me.panel_credit_amount.Controls.Add(Me.label_total_credit_amount)
        Me.panel_credit_amount.Location = New System.Drawing.Point(297, 441)
        Me.panel_credit_amount.Name = "panel_credit_amount"
        Me.panel_credit_amount.Size = New System.Drawing.Size(181, 74)
        Me.panel_credit_amount.TabIndex = 35
        Me.panel_credit_amount.Visible = False
        '
        'page_navigator_transaction
        '
        Me.page_navigator_transaction.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.page_navigator_transaction.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator_transaction.ListView = Nothing
        Me.page_navigator_transaction.Location = New System.Drawing.Point(268, 63)
        Me.page_navigator_transaction.Loopable = True
        Me.page_navigator_transaction.Name = "page_navigator_transaction"
        Me.page_navigator_transaction.PageSize = 2
        Me.page_navigator_transaction.Size = New System.Drawing.Size(600, 37)
        Me.page_navigator_transaction.TabIndex = 36
        Me.page_navigator_transaction.TotalItems = 0
        '
        'label_total_credit
        '
        Me.label_total_credit.AutoEllipsis = False
        Me.label_total_credit.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total_credit.ForeColor = System.Drawing.Color.White
        Me.label_total_credit.Location = New System.Drawing.Point(4, 3)
        Me.label_total_credit.Name = "label_total_credit"
        Me.label_total_credit.Size = New System.Drawing.Size(119, 22)
        Me.label_total_credit.TabIndex = 20
        Me.label_total_credit.TabStop = False
        Me.label_total_credit.Text = "Credit Balance"
        Me.label_total_credit.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_total_credit.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_credit.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_credit.VerticalDirection = False
        '
        'label_total_credit_amount
        '
        Me.label_total_credit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_total_credit_amount.AutoEllipsis = False
        Me.label_total_credit_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_total_credit_amount.ForeColor = System.Drawing.Color.White
        Me.label_total_credit_amount.Location = New System.Drawing.Point(64, 10)
        Me.label_total_credit_amount.Name = "label_total_credit_amount"
        Me.label_total_credit_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_total_credit_amount.TabIndex = 19
        Me.label_total_credit_amount.TabStop = False
        Me.label_total_credit_amount.Text = "0.00"
        Me.label_total_credit_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_total_credit_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_credit_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_credit_amount.VerticalDirection = False
        '
        'label_no_payments
        '
        Me.label_no_payments.AutoEllipsis = False
        Me.label_no_payments.Font = New System.Drawing.Font("Open Sans", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_no_payments.ForeColor = System.Drawing.Color.Black
        Me.label_no_payments.Location = New System.Drawing.Point(552, 360)
        Me.label_no_payments.Name = "label_no_payments"
        Me.label_no_payments.Size = New System.Drawing.Size(143, 17)
        Me.label_no_payments.TabIndex = 34
        Me.label_no_payments.Text = "No selected transaction."
        Me.label_no_payments.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_no_payments.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_no_payments.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_no_payments.VerticalDirection = False
        '
        'label_no_transaction
        '
        Me.label_no_transaction.AutoEllipsis = False
        Me.label_no_transaction.Font = New System.Drawing.Font("Open Sans", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_no_transaction.ForeColor = System.Drawing.Color.Black
        Me.label_no_transaction.Location = New System.Drawing.Point(533, 184)
        Me.label_no_transaction.Name = "label_no_transaction"
        Me.label_no_transaction.Size = New System.Drawing.Size(103, 17)
        Me.label_no_transaction.TabIndex = 33
        Me.label_no_transaction.Text = "No transaction/s."
        Me.label_no_transaction.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_no_transaction.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_no_transaction.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_no_transaction.VerticalDirection = False
        '
        'label_credit_payments
        '
        Me.label_credit_payments.AutoEllipsis = False
        Me.label_credit_payments.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_credit_payments.ForeColor = System.Drawing.Color.Black
        Me.label_credit_payments.Location = New System.Drawing.Point(14, 10)
        Me.label_credit_payments.Name = "label_credit_payments"
        Me.label_credit_payments.Size = New System.Drawing.Size(133, 19)
        Me.label_credit_payments.TabIndex = 17
        Me.label_credit_payments.TabStop = False
        Me.label_credit_payments.Text = "Payments for Credit"
        Me.label_credit_payments.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_credit_payments.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_credit_payments.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_credit_payments.VerticalDirection = False
        '
        'list_customers
        '
        Me.list_customers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.list_customers.BackColor = System.Drawing.Color.White
        Me.list_customers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.list_customers.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_customers.HoverColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.list_customers.HoverSelectedColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.list_customers.ImageList = Me.imagelist_large_icons
        Me.list_customers.Location = New System.Drawing.Point(0, 128)
        Me.list_customers.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.list_customers.Name = "list_customers"
        Me.list_customers.SelectedColor = System.Drawing.Color.AliceBlue
        Me.list_customers.Size = New System.Drawing.Size(267, 387)
        Me.list_customers.TabIndex = 5
        '
        'label_total_payed
        '
        Me.label_total_payed.AutoEllipsis = False
        Me.label_total_payed.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total_payed.ForeColor = System.Drawing.Color.White
        Me.label_total_payed.Location = New System.Drawing.Point(4, 3)
        Me.label_total_payed.Name = "label_total_payed"
        Me.label_total_payed.Size = New System.Drawing.Size(126, 22)
        Me.label_total_payed.TabIndex = 20
        Me.label_total_payed.TabStop = False
        Me.label_total_payed.Text = "Settled Balance"
        Me.label_total_payed.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_total_payed.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_payed.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_payed.VerticalDirection = False
        '
        'label_total_payed_amount
        '
        Me.label_total_payed_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_total_payed_amount.AutoEllipsis = False
        Me.label_total_payed_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_total_payed_amount.ForeColor = System.Drawing.Color.White
        Me.label_total_payed_amount.Location = New System.Drawing.Point(57, 10)
        Me.label_total_payed_amount.Name = "label_total_payed_amount"
        Me.label_total_payed_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_total_payed_amount.TabIndex = 19
        Me.label_total_payed_amount.TabStop = False
        Me.label_total_payed_amount.Text = "0.00"
        Me.label_total_payed_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_total_payed_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_payed_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_payed_amount.VerticalDirection = False
        '
        'label_total_balance
        '
        Me.label_total_balance.AutoEllipsis = False
        Me.label_total_balance.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total_balance.ForeColor = System.Drawing.Color.White
        Me.label_total_balance.Location = New System.Drawing.Point(4, 3)
        Me.label_total_balance.Name = "label_total_balance"
        Me.label_total_balance.Size = New System.Drawing.Size(153, 22)
        Me.label_total_balance.TabIndex = 20
        Me.label_total_balance.TabStop = False
        Me.label_total_balance.Text = "Remaining Balance"
        Me.label_total_balance.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_total_balance.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_balance.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_balance.VerticalDirection = False
        '
        'label_total_balance_amount
        '
        Me.label_total_balance_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_total_balance_amount.AutoEllipsis = False
        Me.label_total_balance_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_total_balance_amount.ForeColor = System.Drawing.Color.White
        Me.label_total_balance_amount.Location = New System.Drawing.Point(77, 10)
        Me.label_total_balance_amount.Name = "label_total_balance_amount"
        Me.label_total_balance_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_total_balance_amount.TabIndex = 19
        Me.label_total_balance_amount.TabStop = False
        Me.label_total_balance_amount.Text = "0.00"
        Me.label_total_balance_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_total_balance_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_balance_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_balance_amount.VerticalDirection = False
        '
        'page_navigator_customer
        '
        Me.page_navigator_customer.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator_customer.ListView = Nothing
        Me.page_navigator_customer.Location = New System.Drawing.Point(0, 63)
        Me.page_navigator_customer.Loopable = True
        Me.page_navigator_customer.Name = "page_navigator_customer"
        Me.page_navigator_customer.PageSize = 2
        Me.page_navigator_customer.Size = New System.Drawing.Size(267, 37)
        Me.page_navigator_customer.TabIndex = 23
        Me.page_navigator_customer.TotalItems = 0
        '
        'label_customer_name
        '
        Me.label_customer_name.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_customer_name.AutoEllipsis = False
        Me.label_customer_name.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_customer_name.ForeColor = System.Drawing.Color.Black
        Me.label_customer_name.Location = New System.Drawing.Point(568, 9)
        Me.label_customer_name.Name = "label_customer_name"
        Me.label_customer_name.Size = New System.Drawing.Size(19, 19)
        Me.label_customer_name.TabIndex = 18
        Me.label_customer_name.TabStop = False
        Me.label_customer_name.Text = "--"
        Me.label_customer_name.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_customer_name.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_customer_name.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_customer_name.VerticalDirection = False
        '
        'label_cart_count
        '
        Me.label_cart_count.AutoEllipsis = False
        Me.label_cart_count.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_cart_count.ForeColor = System.Drawing.Color.Black
        Me.label_cart_count.Location = New System.Drawing.Point(14, 9)
        Me.label_cart_count.Name = "label_cart_count"
        Me.label_cart_count.Size = New System.Drawing.Size(88, 19)
        Me.label_cart_count.TabIndex = 17
        Me.label_cart_count.TabStop = False
        Me.label_cart_count.Text = "Transactions"
        Me.label_cart_count.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_cart_count.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_cart_count.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_cart_count.VerticalDirection = False
        '
        'form_customer_report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(866, 515)
        Me.Controls.Add(Me.page_navigator_transaction)
        Me.Controls.Add(Me.panel_credit_amount)
        Me.Controls.Add(Me.label_no_payments)
        Me.Controls.Add(Me.label_no_transaction)
        Me.Controls.Add(Me.panel_payment_header)
        Me.Controls.Add(Me.list_credit_payments)
        Me.Controls.Add(Me.list_customers)
        Me.Controls.Add(Me.panel_payed_amount)
        Me.Controls.Add(Me.panel_balance_amount)
        Me.Controls.Add(Me.page_navigator_customer)
        Me.Controls.Add(Me.list_credited_transactions)
        Me.Controls.Add(Me.panel_transaction_header)
        Me.Controls.Add(Me.button_open_filter)
        Me.Controls.Add(Me.text_search)
        Me.Controls.Add(Me.panel_controls)
        Me.MinimizeBox = False
        Me.Name = "form_customer_report"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Customer Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_transaction_header.ResumeLayout(False)
        Me.panel_transaction_header.PerformLayout()
        Me.panel_controls.ResumeLayout(False)
        Me.panel_payed_amount.ResumeLayout(False)
        Me.panel_payed_amount.PerformLayout()
        Me.panel_balance_amount.ResumeLayout(False)
        Me.panel_balance_amount.PerformLayout()
        Me.panel_payment_header.ResumeLayout(False)
        Me.panel_payment_header.PerformLayout()
        Me.menu_filter.ResumeLayout(False)
        Me.panel_credit_amount.ResumeLayout(False)
        Me.panel_credit_amount.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panel_transaction_header As System.Windows.Forms.Panel
    Friend WithEvents label_cart_count As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents list_credited_transactions As System.Windows.Forms.ListView
    Friend WithEvents page_navigator_customer As DevaReePharmacy.PageNavigator
    Friend WithEvents list_credit_payments As System.Windows.Forms.ListView
    Friend WithEvents label_credit_payments As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_customer_name As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_controls As System.Windows.Forms.Panel
    Friend WithEvents imagelist_small_icons As System.Windows.Forms.ImageList
    Friend WithEvents panel_payed_amount As System.Windows.Forms.Panel
    Friend WithEvents label_total_payed As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_total_payed_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_balance_amount As System.Windows.Forms.Panel
    Friend WithEvents label_total_balance As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_total_balance_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_verify_transaction As System.Windows.Forms.Button
    Friend WithEvents button_add_payment As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents list_customers As DevaReePharmacy.MobileList
    Friend WithEvents imagelist_large_icons As System.Windows.Forms.ImageList
    Friend WithEvents panel_payment_header As System.Windows.Forms.Panel
    Friend WithEvents text_search As System.Windows.Forms.TextBox
    Friend WithEvents label_no_transaction As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_no_payments As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents button_open_filter As System.Windows.Forms.Button
    Friend WithEvents menu_filter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents menu_exclude_filter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents panel_credit_amount As System.Windows.Forms.Panel
    Friend WithEvents label_total_credit As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_total_credit_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents page_navigator_transaction As DevaReePharmacy.PageNavigator
End Class
