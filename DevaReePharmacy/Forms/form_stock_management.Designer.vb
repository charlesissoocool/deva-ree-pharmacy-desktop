﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_stock_management
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_stock_management))
        Me.text_search = New System.Windows.Forms.TextBox()
        Me.panel_header = New System.Windows.Forms.Panel()
        Me.button_print_stock_list = New System.Windows.Forms.Button()
        Me.button_resort = New System.Windows.Forms.Button()
        Me.button_edit_manifest = New System.Windows.Forms.Button()
        Me.button_inspect_manifest = New System.Windows.Forms.Button()
        Me.button_add_manifest = New System.Windows.Forms.Button()
        Me.list_stocks = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.page_navigator = New DevaReePharmacy.PageNavigator()
        Me.panel_header.SuspendLayout()
        Me.SuspendLayout()
        '
        'text_search
        '
        Me.text_search.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_search.ForeColor = System.Drawing.Color.Black
        Me.text_search.Location = New System.Drawing.Point(0, 59)
        Me.text_search.Name = "text_search"
        Me.text_search.Size = New System.Drawing.Size(815, 29)
        Me.text_search.TabIndex = 12
        '
        'panel_header
        '
        Me.panel_header.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_header.BackColor = System.Drawing.Color.Silver
        Me.panel_header.Controls.Add(Me.button_print_stock_list)
        Me.panel_header.Controls.Add(Me.button_resort)
        Me.panel_header.Controls.Add(Me.button_edit_manifest)
        Me.panel_header.Controls.Add(Me.button_inspect_manifest)
        Me.panel_header.Controls.Add(Me.button_add_manifest)
        Me.panel_header.Location = New System.Drawing.Point(0, 0)
        Me.panel_header.Name = "panel_header"
        Me.panel_header.Size = New System.Drawing.Size(815, 60)
        Me.panel_header.TabIndex = 13
        '
        'button_print_stock_list
        '
        Me.button_print_stock_list.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_print_stock_list.BackColor = System.Drawing.Color.Transparent
        Me.button_print_stock_list.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_print_stock_list.FlatAppearance.BorderSize = 0
        Me.button_print_stock_list.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_print_stock_list.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_print_stock_list.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_print_stock_list.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_print_stock_list.ForeColor = System.Drawing.Color.White
        Me.button_print_stock_list.Image = CType(resources.GetObject("button_print_stock_list.Image"), System.Drawing.Image)
        Me.button_print_stock_list.Location = New System.Drawing.Point(717, 0)
        Me.button_print_stock_list.Name = "button_print_stock_list"
        Me.button_print_stock_list.Size = New System.Drawing.Size(96, 60)
        Me.button_print_stock_list.TabIndex = 17
        Me.button_print_stock_list.TabStop = False
        Me.button_print_stock_list.Text = "Print Stock List"
        Me.button_print_stock_list.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_print_stock_list.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_print_stock_list.UseVisualStyleBackColor = False
        '
        'button_resort
        '
        Me.button_resort.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_resort.BackColor = System.Drawing.Color.Transparent
        Me.button_resort.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_resort.FlatAppearance.BorderSize = 0
        Me.button_resort.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_resort.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_resort.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_resort.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_resort.ForeColor = System.Drawing.Color.White
        Me.button_resort.Image = CType(resources.GetObject("button_resort.Image"), System.Drawing.Image)
        Me.button_resort.Location = New System.Drawing.Point(600, 0)
        Me.button_resort.Name = "button_resort"
        Me.button_resort.Size = New System.Drawing.Size(121, 60)
        Me.button_resort.TabIndex = 16
        Me.button_resort.TabStop = False
        Me.button_resort.Text = "Re-sort Manifests"
        Me.button_resort.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_resort.UseVisualStyleBackColor = False
        '
        'button_edit_manifest
        '
        Me.button_edit_manifest.BackColor = System.Drawing.Color.Transparent
        Me.button_edit_manifest.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_edit_manifest.Enabled = False
        Me.button_edit_manifest.FlatAppearance.BorderSize = 0
        Me.button_edit_manifest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_edit_manifest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_edit_manifest.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_edit_manifest.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_edit_manifest.ForeColor = System.Drawing.Color.White
        Me.button_edit_manifest.Image = CType(resources.GetObject("button_edit_manifest.Image"), System.Drawing.Image)
        Me.button_edit_manifest.Location = New System.Drawing.Point(96, 0)
        Me.button_edit_manifest.Name = "button_edit_manifest"
        Me.button_edit_manifest.Size = New System.Drawing.Size(105, 60)
        Me.button_edit_manifest.TabIndex = 14
        Me.button_edit_manifest.TabStop = False
        Me.button_edit_manifest.Text = "Edit Manifest"
        Me.button_edit_manifest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_edit_manifest.UseVisualStyleBackColor = False
        '
        'button_inspect_manifest
        '
        Me.button_inspect_manifest.BackColor = System.Drawing.Color.Transparent
        Me.button_inspect_manifest.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_inspect_manifest.Enabled = False
        Me.button_inspect_manifest.FlatAppearance.BorderSize = 0
        Me.button_inspect_manifest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_inspect_manifest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_inspect_manifest.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_inspect_manifest.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_inspect_manifest.ForeColor = System.Drawing.Color.White
        Me.button_inspect_manifest.Image = CType(resources.GetObject("button_inspect_manifest.Image"), System.Drawing.Image)
        Me.button_inspect_manifest.Location = New System.Drawing.Point(196, 0)
        Me.button_inspect_manifest.Name = "button_inspect_manifest"
        Me.button_inspect_manifest.Size = New System.Drawing.Size(105, 60)
        Me.button_inspect_manifest.TabIndex = 13
        Me.button_inspect_manifest.TabStop = False
        Me.button_inspect_manifest.Text = "Inspect Manifest"
        Me.button_inspect_manifest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_inspect_manifest.UseVisualStyleBackColor = False
        '
        'button_add_manifest
        '
        Me.button_add_manifest.BackColor = System.Drawing.Color.Transparent
        Me.button_add_manifest.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_manifest.FlatAppearance.BorderSize = 0
        Me.button_add_manifest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_add_manifest.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_add_manifest.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_manifest.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_manifest.ForeColor = System.Drawing.Color.White
        Me.button_add_manifest.Image = CType(resources.GetObject("button_add_manifest.Image"), System.Drawing.Image)
        Me.button_add_manifest.Location = New System.Drawing.Point(0, 0)
        Me.button_add_manifest.Name = "button_add_manifest"
        Me.button_add_manifest.Size = New System.Drawing.Size(99, 60)
        Me.button_add_manifest.TabIndex = 12
        Me.button_add_manifest.TabStop = False
        Me.button_add_manifest.Text = "Add Manifest"
        Me.button_add_manifest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_manifest.UseVisualStyleBackColor = False
        '
        'list_stocks
        '
        Me.list_stocks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_stocks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader7, Me.ColumnHeader4, Me.ColumnHeader6})
        Me.list_stocks.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_stocks.FullRowSelect = True
        Me.list_stocks.GridLines = True
        Me.list_stocks.Location = New System.Drawing.Point(0, 125)
        Me.list_stocks.Name = "list_stocks"
        Me.list_stocks.Size = New System.Drawing.Size(815, 228)
        Me.list_stocks.TabIndex = 15
        Me.list_stocks.TabStop = False
        Me.list_stocks.UseCompatibleStateImageBehavior = False
        Me.list_stocks.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product Name"
        Me.ColumnHeader1.Width = 131
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Lot / Batch No."
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Expiry Date"
        Me.ColumnHeader3.Width = 93
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Supplier Price"
        Me.ColumnHeader7.Width = 111
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Quantity"
        Me.ColumnHeader4.Width = 85
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Status"
        '
        'page_navigator
        '
        Me.page_navigator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.page_navigator.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator.ListView = Nothing
        Me.page_navigator.Location = New System.Drawing.Point(0, 88)
        Me.page_navigator.Loopable = True
        Me.page_navigator.Name = "page_navigator"
        Me.page_navigator.PageSize = 2
        Me.page_navigator.Size = New System.Drawing.Size(815, 37)
        Me.page_navigator.TabIndex = 14
        Me.page_navigator.TotalItems = 0
        '
        'form_stock_management
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(815, 353)
        Me.Controls.Add(Me.list_stocks)
        Me.Controls.Add(Me.page_navigator)
        Me.Controls.Add(Me.text_search)
        Me.Controls.Add(Me.panel_header)
        Me.Name = "form_stock_management"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock Management"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_header.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents page_navigator As DevaReePharmacy.PageNavigator
    Friend WithEvents text_search As System.Windows.Forms.TextBox
    Friend WithEvents panel_header As System.Windows.Forms.Panel
    Friend WithEvents list_stocks As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_inspect_manifest As System.Windows.Forms.Button
    Friend WithEvents button_add_manifest As System.Windows.Forms.Button
    Friend WithEvents button_edit_manifest As System.Windows.Forms.Button
    Friend WithEvents button_resort As System.Windows.Forms.Button
    Friend WithEvents button_print_stock_list As System.Windows.Forms.Button
End Class
