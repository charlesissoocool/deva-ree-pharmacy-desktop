﻿Public Class form_distribution

    Dim _agent_staff_id As Integer = 0
    Dim sql As New class_db_transaction
    Private _col_width() As Double = {50, 15, 35}
    Dim _current_distribution As Integer = 0

    Private Sub AdjustInterface() Handles Me.Load

        panel_cashier_info.BackColor = module_colors.color_yellow
        panel_distribution_menu.BackColor = module_colors.color_light_blue
        panel_remittance_menu.BackColor = module_colors.color_light_blue
        button_inventory.BackColor = module_colors.color_button_blue
        button_quantity.BackColor = module_colors.color_button_blue
        button_place_distribution.BackColor = module_colors.color_button_blue
        button_remove_item.BackColor = module_colors.color_button_blue
        button_remit.BackColor = module_colors.color_button_blue
        button_close_distribution.BackColor = module_colors.color_button_blue
        Dim agent_list = sql.GetAgentList()
        Dim autocomplete As New AutoCompleteStringCollection

        If IsNothing(agent_list) Then
            MessageBox.Show("No agents are registered into the system! Please contact system administrator.")
            Me.Close()
        Else
            For Each agent In agent_list
                autocomplete.Add(agent("first_name") & " " & agent("last_name"))
            Next
            text_agent_name.AutoCompleteCustomSource = autocomplete
        End If
        Timer_distribution.Enabled = True
        label_cashier_name.Text = module_session.current_user_profile.FirstName & " " & module_session.current_user_profile.LastName
    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        text_code.Top = Me.ClientSize.Height - 5 - text_code.Height
        label_code.Top = text_code.Top - 5 - label_code.Height
        list_products.Height = label_code.Top - 5 - list_products.Top
        panel_remittance_menu.Left = panel_distribution_menu.Left
        panel_remittance_menu.Top = panel_distribution_menu.Top
        ResizeListViewColumn(list_products, _col_width)

    End Sub

    Private Sub LoadDistribution(ByVal distribution_id As Integer)

        list_products.Items.Clear()
        Dim items = sql.GetItemsFromDistribution(distribution_id)
        If Not IsNothing(items) Then
            For Each item In items
                Dim new_item As ListViewItem = list_products.Items.Add(item("product_name"))
                new_item.SubItems.Add(item("sold_quantity") & " / " & item("item_quantity"))
                new_item.SubItems.Add(item("product_unit"))
                If Val(item("sold_quantity")) = Val(item("item_quantity")) Then
                    new_item.BackColor = module_colors.color_red
                    new_item.ForeColor = Color.White
                Else
                    new_item.BackColor = Color.White
                    new_item.ForeColor = Color.Black
                End If
            Next
        End If

    End Sub

    Private Sub ReloadInterface()

        text_agent_name.Text = ""

    End Sub

    Private Sub ResetDistribution(Optional ByVal confirm As Boolean = True)

        Dim ask = Nothing
        If list_products.Items.Count > 0 Then
            ask = MessageBox.Show("Do you want to cancel current distribution and proceed on making a new one?", "New Distribution", MessageBoxButtons.YesNo)
        ElseIf confirm Then
            ask = MessageBox.Show("Are you sure you want to create a new distribution?", "New Distribution", MessageBoxButtons.YesNo)
        End If

        If Not IsNothing(ask) And Not ask = DialogResult.Yes Then Exit Sub

        list_products.Items.Clear()
        text_agent_name.Text = ""
        _agent_staff_id = 0

    End Sub

    Private Sub button_inventory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_inventory.Click

        If sender Is button_inventory Then
            Dim inventory_form As New form_inventory
            Dim product_id = inventory_form.OpenInventory(Me)
            'Dim prodResult = sql.GetProductDetailByID(product_id)
            Dim prodResult = Nothing
            If Not IsNothing(prodResult) Then

                Dim prod_details = sql.GetProductDetailByID(prodResult("product_id"))
                For Each listRow As ListViewItem In list_products.Items
                    If prod_details("product_id") = listRow.Tag Then
                        listRow.SubItems(1).Text = Val(listRow.SubItems(1).Text) + 1
                        'extended_price = prod_details("retail_price")
                        'listRow.SubItems(4).Text = Val(listRow.SubItems(4).Text) + extended_price
                        'Total_Amount()
                        Exit Sub
                    End If
                Next

                prod_details = sql.GetProductDetailByID(prodResult("product_id"))
                Dim NewItem As ListViewItem = list_products.Items.Add(prodResult("product_name"))
                NewItem.SubItems.Add(("1"))
                NewItem.SubItems.Add(prod_details("product_unit"))
                'NewItem.SubItems.Add(prod_details("retail_price"))
                NewItem.Tag = prodResult("product_id")
            End If
            text_code.Text = ""
            'Total_Amount()
        End If
    End Sub

    Private Sub text_code_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_code.KeyPress
        Dim extended_priceCount As Integer = 1
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            Dim prodResult = sql.GetStockDetailForProductCode(text_code.Text)
            If Not IsNothing(prodResult) Then
                Dim prod_details = sql.GetProductDetailByID(prodResult("product_id"))

                For Each listRow As ListViewItem In list_products.Items
                    If prod_details("product_id") = listRow.Tag Then
                        listRow.SubItems(1).Text = Val(listRow.SubItems(1).Text) + 1
                        'If _transaction_mode = TR_WHOLESALE Then
                        '    extended_price = prod_details("wholesale_price")
                        'ElseIf _transaction_mode = TR_RETAIL Then
                        '    extended_price = prod_details("retail_price")
                        'End If
                        'listRow.SubItems(3).Text = Val(listRow.SubItems(3).Text) + extended_price
                        'Total_Amount()
                        text_code.Text = ""
                        Exit Sub
                    End If
                Next

                Dim NewItem As ListViewItem = list_products.Items.Add(prodResult("product_name"))
                NewItem.SubItems.Add(("1"))
                NewItem.SubItems.Add(prod_details("product_unit"))

                'If _transaction_mode = TR_WHOLESALE Then
                '    NewItem.SubItems.Add(prod_details("wholesale_price"))
                '    extended_price = prod_details("wholesale_price")
                '    NewItem.SubItems.Add(extended_price)
                'ElseIf _transaction_mode = TR_RETAIL Then
                '    NewItem.SubItems.Add(prod_details("retail_price"))
                '    extended_price = prod_details("retail_price")
                '    NewItem.SubItems.Add(extended_price)
                'End If

                NewItem.Tag = prodResult("product_id")
                text_code.Text = ""
            Else
                MsgBox("Item not exists!", MsgBoxStyle.Critical)
            End If
            e.Handled = True
            text_code.Text = ""
            'Total_Amount()
        End If

        If list_products.Items.Count > 0 Then
            'button_transaction_mode.Visible = False
            'button_cancel_transaction.visible = Not button_transaction_mode.Visible
        End If
    End Sub

    Private Sub button_remove_item_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_remove_item.Click

        If list_products.SelectedItems.Count < 1 Then MsgBox("Please select item to remove!", MsgBoxStyle.Critical) : Exit Sub
        Dim ask = MessageBox.Show("Are you sure you want to remove selected item?", "Remove Item", MessageBoxButtons.YesNo)
        If ask = DialogResult.Yes Then list_products.SelectedItems(0).Remove()

    End Sub

    Private Sub button_quantity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_quantity.Click

        If list_products.SelectedItems.Count = 0 Then
            MsgBox("Please select item to change quantity!", MsgBoxStyle.Critical)
            Exit Sub
        End If

        'Dim form_inventory As New form_inventory
        Dim current_quantity As Integer = list_products.Items(list_products.SelectedItems(0).Index).SubItems(1).Text

        Dim new_quantity As Double = InputQuantity(Me, "Quantity", current_quantity, increment:=1)
        If Not IsNumeric(new_quantity) Or Val(new_quantity) <= 0 Then Exit Sub
        list_products.Items(list_products.SelectedItems(0).Index).SubItems(1).Text = new_quantity

    End Sub

    Private Sub button_place_distribution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_place_distribution.Click

        If list_products.Items.Count < 1 Then
            MessageBox.Show("No items on cart!")
            Exit Sub
        Else
            Dim ask = MessageBox.Show("Are you sure you want to place distribution?", "Place Distribution", MessageBoxButtons.YesNo)
            If Not ask = DialogResult.Yes Then Exit Sub
        End If

        'Validate stocks first
        Dim product_validation As New Dictionary(Of String, Object)
        Dim index As Integer
        For Each list_row As ListViewItem In list_products.Items
            index = Val(list_row.Tag)
            If product_validation.ContainsKey(index) Then
                product_validation(index) += Val(list_row.SubItems(1).Text)
            Else
                product_validation.Add(index, Val(list_row.SubItems(1).Text))
            End If
        Next

        Dim sql As New class_db_transaction
        Dim has_exceeded As Boolean = False
        For Each product As KeyValuePair(Of String, Object) In product_validation
            Dim quantity_available = sql.GetTotalStocksAvailable(Val(product.Key))
            If quantity_available < Val(product.Value) Then
                For Each list_row As ListViewItem In list_products.Items
                    If Val(list_row.Tag) = Val(product.Key) Then list_row.BackColor = module_colors.color_red : has_exceeded = True
                Next
            End If
        Next

        If has_exceeded Then
            MsgBox("Some items have exceeded stock limits. Transaction cannot be completed.") ' Temporary
            Dim ask = MessageBox.Show("Some items have exceeded stock limit. Proceeding will need administrator authorization. Do you wish to continue?", "Stock Limit Exceeded", MessageBoxButtons.YesNo)
            If Not ask = DialogResult.Yes Then
                ResetDistribution()
                Exit Sub
            End If
            If Not module_session.current_user_profile.Permission_Stocks Then
                Dim login_form As New form_user_login
                Do While True
                    Dim _user_id = login_form.ShowLoginDialog(Me, "Please enter username with permission to override stocks.")
                    If _user_id = 0 Then
                        ask = MessageBox.Show("Do you want to cancel your current transaction?", "Cancel Transaction", MessageBoxButtons.YesNo)
                        If ask = DialogResult.Yes Then
                            ResetDistribution()
                            Exit Sub
                        Else
                            Continue Do
                        End If
                    End If
                    Dim db_user As New class_db_user
                    If db_user.DoesUserHavePermission("Stocks", _user_id) Then
                        Exit Do
                    Else
                        ask = MessageBox.Show("The user does not have enough permission to handle this request. Would you like to try again?", "Permission Denied", MessageBoxButtons.YesNo)
                        If ask = DialogResult.Yes Then
                            Continue Do
                        Else
                            ResetDistribution()
                            Exit Sub
                        End If
                    End If
                Loop
            End If
        End If

        Dim distribution_id As Integer = sql.CreateNewDistribution(_agent_staff_id)
        If distribution_id = 0 Then MessageBox.Show("A transaction error occurred. If the problem persists, please contact administrator.") : Exit Sub

        For Each list_row As ListViewItem In list_products.Items
            Dim stock_vals = sql.ValidateItemForTransaction(Val(list_row.Tag), Val(list_row.SubItems(1).Text))
            For Each stock_val In stock_vals
                If Not sql.AddItemToDistribution(distribution_id, stock_val("stock_id"), stock_val("product_id"), stock_val("quantity")) <> 0 Then
                    MessageBox.Show("A transaction error occurred. If the problem persists, please contact administrator.")
                    Exit Sub
                End If
            Next
        Next

        If sql.ValidateDistribution(distribution_id) = 0 Then MessageBox.Show("Transaction verification failed! Please contact administrator immediately. (Ref. No: #" & distribution_id & ")")
        MessageBox.Show("Transaction complete. Press Enter/Return to start a new one.")
        ResetDistribution(False)

    End Sub

    Private Sub Timer_distribution_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer_distribution.Tick
        Label2.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
    End Sub

    Private Sub CheckAgentName()

        Dim staff_id = sql.GetAgentIDByFullName(text_agent_name.Text)
        If Not staff_id = 0 Then
            If Not staff_id = _agent_staff_id Then
                _agent_staff_id = staff_id
                list_products.Items.Clear()
                Dim distribution_id = sql.GetOpenDistributionIDByAgent(_agent_staff_id)
                label_distribution_id.Text = "Distribution Ref #: " & distribution_id
                panel_distribution_menu.Visible = (distribution_id = 0)
                panel_distribution_menu.Enabled = panel_distribution_menu.Visible
                panel_remittance_menu.Visible = Not panel_distribution_menu.Visible
                panel_remittance_menu.Enabled = panel_remittance_menu.Visible
                text_code.Enabled = panel_distribution_menu.Enabled
                If Not distribution_id = 0 Then
                    LoadDistribution(distribution_id)
                    _current_distribution = distribution_id
                Else
                    _current_distribution = 0
                End If
            End If
        Else
            _agent_staff_id = 0
            list_products.Items.Clear()
            panel_distribution_menu.Enabled = False
            panel_remittance_menu.Enabled = False
            panel_distribution_menu.Visible = True
            panel_remittance_menu.Visible = False
            text_code.Enabled = False
        End If

    End Sub

    Private Sub text_agent_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_agent_name.TextChanged

        CheckAgentName()

    End Sub

    Private Sub button_close_distribution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_close_distribution.Click

        Dim ask = MsgBox("Unremitted items will be returned to stocks. Are you sure you want to close this distribution?", MsgBoxStyle.YesNo)
        If Not ask = DialogResult.Yes Then Exit Sub

        If Not sql.CloseDistribution(_current_distribution) Then
            MsgBox("Failed to close distribution.")
        Else
            ReloadInterface()
        End If

    End Sub
End Class