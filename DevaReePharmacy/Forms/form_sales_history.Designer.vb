﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_sales_history
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_sales_history))
        Me.panel_header = New System.Windows.Forms.Panel()
        Me.label_invoice_no = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_return_count = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_cart_count = New DevaReePharmacy.AntiAliasedLabel()
        Me.list_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader17 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.list_transactions = New System.Windows.Forms.ListView()
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader18 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.imagelist_small_icons = New System.Windows.Forms.ImageList(Me.components)
        Me.list_returns = New System.Windows.Forms.ListView()
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.panel_controls = New System.Windows.Forms.Panel()
        Me.button_change_credit = New System.Windows.Forms.Button()
        Me.button_return = New System.Windows.Forms.Button()
        Me.button_change_agent = New System.Windows.Forms.Button()
        Me.button_change_customer = New System.Windows.Forms.Button()
        Me.button_reprint_dr = New System.Windows.Forms.Button()
        Me.button_verify_transaction = New System.Windows.Forms.Button()
        Me.panel_total_amount = New System.Windows.Forms.Panel()
        Me.label_total = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.panel_credit_amount = New System.Windows.Forms.Panel()
        Me.label_credit = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_credit_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.page_navigator = New DevaReePharmacy.PageNavigator()
        Me.button_void_transaction = New System.Windows.Forms.Button()
        Me.panel_header.SuspendLayout()
        Me.panel_controls.SuspendLayout()
        Me.panel_total_amount.SuspendLayout()
        Me.panel_credit_amount.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_header
        '
        Me.panel_header.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_header.BackColor = System.Drawing.Color.Gold
        Me.panel_header.Controls.Add(Me.label_invoice_no)
        Me.panel_header.Controls.Add(Me.label_return_count)
        Me.panel_header.Controls.Add(Me.label_cart_count)
        Me.panel_header.Location = New System.Drawing.Point(0, 213)
        Me.panel_header.Name = "panel_header"
        Me.panel_header.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.panel_header.Size = New System.Drawing.Size(868, 37)
        Me.panel_header.TabIndex = 21
        '
        'label_invoice_no
        '
        Me.label_invoice_no.AutoEllipsis = False
        Me.label_invoice_no.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_invoice_no.ForeColor = System.Drawing.Color.Black
        Me.label_invoice_no.Location = New System.Drawing.Point(352, 9)
        Me.label_invoice_no.Name = "label_invoice_no"
        Me.label_invoice_no.Size = New System.Drawing.Size(73, 19)
        Me.label_invoice_no.TabIndex = 18
        Me.label_invoice_no.TabStop = False
        Me.label_invoice_no.Text = "D.R. No.: 0"
        Me.label_invoice_no.TextAlign = DevaReePharmacy.TextAlignment.Center
        Me.label_invoice_no.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_invoice_no.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_invoice_no.VerticalDirection = False
        '
        'label_return_count
        '
        Me.label_return_count.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_return_count.AutoEllipsis = False
        Me.label_return_count.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_return_count.ForeColor = System.Drawing.Color.Black
        Me.label_return_count.Location = New System.Drawing.Point(739, 9)
        Me.label_return_count.Name = "label_return_count"
        Me.label_return_count.Size = New System.Drawing.Size(126, 19)
        Me.label_return_count.TabIndex = 17
        Me.label_return_count.TabStop = False
        Me.label_return_count.Text = "Returned (0 items)"
        Me.label_return_count.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_return_count.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_return_count.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_return_count.VerticalDirection = False
        '
        'label_cart_count
        '
        Me.label_cart_count.AutoEllipsis = False
        Me.label_cart_count.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_cart_count.ForeColor = System.Drawing.Color.Black
        Me.label_cart_count.Location = New System.Drawing.Point(14, 9)
        Me.label_cart_count.Name = "label_cart_count"
        Me.label_cart_count.Size = New System.Drawing.Size(94, 19)
        Me.label_cart_count.TabIndex = 17
        Me.label_cart_count.TabStop = False
        Me.label_cart_count.Text = "Cart (0 items)"
        Me.label_cart_count.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_cart_count.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_cart_count.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_cart_count.VerticalDirection = False
        '
        'list_products
        '
        Me.list_products.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.list_products.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.list_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader17, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.list_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_products.FullRowSelect = True
        Me.list_products.GridLines = True
        Me.list_products.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.list_products.HideSelection = False
        Me.list_products.Location = New System.Drawing.Point(0, 249)
        Me.list_products.MultiSelect = False
        Me.list_products.Name = "list_products"
        Me.list_products.Size = New System.Drawing.Size(405, 182)
        Me.list_products.TabIndex = 22
        Me.list_products.UseCompatibleStateImageBehavior = False
        Me.list_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product"
        Me.ColumnHeader1.Width = 142
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Lot Number"
        Me.ColumnHeader17.Width = 76
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Quantity"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader2.Width = 89
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Unit"
        Me.ColumnHeader3.Width = 65
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Price"
        Me.ColumnHeader4.Width = 122
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Extended Price"
        Me.ColumnHeader5.Width = 91
        '
        'list_transactions
        '
        Me.list_transactions.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.list_transactions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_transactions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader16, Me.ColumnHeader6, Me.ColumnHeader14, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader18, Me.ColumnHeader9, Me.ColumnHeader10})
        Me.list_transactions.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_transactions.FullRowSelect = True
        Me.list_transactions.GridLines = True
        Me.list_transactions.HideSelection = False
        Me.list_transactions.Location = New System.Drawing.Point(0, 99)
        Me.list_transactions.MultiSelect = False
        Me.list_transactions.Name = "list_transactions"
        Me.list_transactions.Size = New System.Drawing.Size(868, 115)
        Me.list_transactions.SmallImageList = Me.imagelist_small_icons
        Me.list_transactions.TabIndex = 24
        Me.list_transactions.UseCompatibleStateImageBehavior = False
        Me.list_transactions.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "D.R. No."
        Me.ColumnHeader16.Width = 135
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Transaction Type"
        Me.ColumnHeader6.Width = 150
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Credit Billed"
        Me.ColumnHeader14.Width = 120
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Customer"
        Me.ColumnHeader7.Width = 135
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Cashier"
        Me.ColumnHeader8.Width = 126
        '
        'ColumnHeader18
        '
        Me.ColumnHeader18.Text = "Agent"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Status"
        Me.ColumnHeader9.Width = 114
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Date Transacted"
        Me.ColumnHeader10.Width = 147
        '
        'imagelist_small_icons
        '
        Me.imagelist_small_icons.ImageStream = CType(resources.GetObject("imagelist_small_icons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imagelist_small_icons.TransparentColor = System.Drawing.Color.Transparent
        Me.imagelist_small_icons.Images.SetKeyName(0, "invalid")
        Me.imagelist_small_icons.Images.SetKeyName(1, "valid")
        '
        'list_returns
        '
        Me.list_returns.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.list_returns.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.list_returns.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader15})
        Me.list_returns.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_returns.FullRowSelect = True
        Me.list_returns.GridLines = True
        Me.list_returns.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.list_returns.HideSelection = False
        Me.list_returns.Location = New System.Drawing.Point(405, 249)
        Me.list_returns.MultiSelect = False
        Me.list_returns.Name = "list_returns"
        Me.list_returns.Size = New System.Drawing.Size(460, 108)
        Me.list_returns.TabIndex = 26
        Me.list_returns.UseCompatibleStateImageBehavior = False
        Me.list_returns.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Product"
        Me.ColumnHeader11.Width = 222
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Quantity"
        Me.ColumnHeader12.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader12.Width = 54
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Unit"
        Me.ColumnHeader13.Width = 48
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Refund Balance"
        Me.ColumnHeader15.Width = 104
        '
        'panel_controls
        '
        Me.panel_controls.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_controls.BackColor = System.Drawing.Color.Silver
        Me.panel_controls.Controls.Add(Me.button_void_transaction)
        Me.panel_controls.Controls.Add(Me.button_change_credit)
        Me.panel_controls.Controls.Add(Me.button_return)
        Me.panel_controls.Controls.Add(Me.button_change_agent)
        Me.panel_controls.Controls.Add(Me.button_change_customer)
        Me.panel_controls.Controls.Add(Me.button_reprint_dr)
        Me.panel_controls.Controls.Add(Me.button_verify_transaction)
        Me.panel_controls.Location = New System.Drawing.Point(0, 0)
        Me.panel_controls.Name = "panel_controls"
        Me.panel_controls.Size = New System.Drawing.Size(868, 63)
        Me.panel_controls.TabIndex = 27
        '
        'button_change_credit
        '
        Me.button_change_credit.BackColor = System.Drawing.Color.Transparent
        Me.button_change_credit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_change_credit.Enabled = False
        Me.button_change_credit.FlatAppearance.BorderSize = 0
        Me.button_change_credit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_change_credit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_change_credit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_change_credit.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_change_credit.ForeColor = System.Drawing.Color.White
        Me.button_change_credit.Image = CType(resources.GetObject("button_change_credit.Image"), System.Drawing.Image)
        Me.button_change_credit.Location = New System.Drawing.Point(419, 2)
        Me.button_change_credit.Name = "button_change_credit"
        Me.button_change_credit.Size = New System.Drawing.Size(108, 60)
        Me.button_change_credit.TabIndex = 15
        Me.button_change_credit.TabStop = False
        Me.button_change_credit.Text = "Change Credit"
        Me.button_change_credit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_change_credit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_change_credit.UseVisualStyleBackColor = False
        '
        'button_return
        '
        Me.button_return.BackColor = System.Drawing.Color.Transparent
        Me.button_return.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_return.Enabled = False
        Me.button_return.FlatAppearance.BorderSize = 0
        Me.button_return.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_return.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_return.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_return.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_return.ForeColor = System.Drawing.Color.White
        Me.button_return.Image = CType(resources.GetObject("button_return.Image"), System.Drawing.Image)
        Me.button_return.Location = New System.Drawing.Point(511, 3)
        Me.button_return.Name = "button_return"
        Me.button_return.Size = New System.Drawing.Size(108, 60)
        Me.button_return.TabIndex = 14
        Me.button_return.TabStop = False
        Me.button_return.Text = "Return Item"
        Me.button_return.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_return.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_return.UseVisualStyleBackColor = False
        '
        'button_change_agent
        '
        Me.button_change_agent.BackColor = System.Drawing.Color.Transparent
        Me.button_change_agent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_change_agent.Enabled = False
        Me.button_change_agent.FlatAppearance.BorderSize = 0
        Me.button_change_agent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_change_agent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_change_agent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_change_agent.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_change_agent.ForeColor = System.Drawing.Color.White
        Me.button_change_agent.Image = CType(resources.GetObject("button_change_agent.Image"), System.Drawing.Image)
        Me.button_change_agent.Location = New System.Drawing.Point(320, 2)
        Me.button_change_agent.Name = "button_change_agent"
        Me.button_change_agent.Size = New System.Drawing.Size(108, 60)
        Me.button_change_agent.TabIndex = 13
        Me.button_change_agent.TabStop = False
        Me.button_change_agent.Text = "Change Agent"
        Me.button_change_agent.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_change_agent.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_change_agent.UseVisualStyleBackColor = False
        '
        'button_change_customer
        '
        Me.button_change_customer.BackColor = System.Drawing.Color.Transparent
        Me.button_change_customer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_change_customer.Enabled = False
        Me.button_change_customer.FlatAppearance.BorderSize = 0
        Me.button_change_customer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_change_customer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_change_customer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_change_customer.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_change_customer.ForeColor = System.Drawing.Color.White
        Me.button_change_customer.Image = CType(resources.GetObject("button_change_customer.Image"), System.Drawing.Image)
        Me.button_change_customer.Location = New System.Drawing.Point(211, 2)
        Me.button_change_customer.Name = "button_change_customer"
        Me.button_change_customer.Size = New System.Drawing.Size(108, 60)
        Me.button_change_customer.TabIndex = 12
        Me.button_change_customer.TabStop = False
        Me.button_change_customer.Text = "Change Customer"
        Me.button_change_customer.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_change_customer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_change_customer.UseVisualStyleBackColor = False
        '
        'button_reprint_dr
        '
        Me.button_reprint_dr.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_reprint_dr.BackColor = System.Drawing.Color.Transparent
        Me.button_reprint_dr.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_reprint_dr.Enabled = False
        Me.button_reprint_dr.FlatAppearance.BorderSize = 0
        Me.button_reprint_dr.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_reprint_dr.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_reprint_dr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_reprint_dr.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_reprint_dr.ForeColor = System.Drawing.Color.White
        Me.button_reprint_dr.Image = CType(resources.GetObject("button_reprint_dr.Image"), System.Drawing.Image)
        Me.button_reprint_dr.Location = New System.Drawing.Point(782, 2)
        Me.button_reprint_dr.Name = "button_reprint_dr"
        Me.button_reprint_dr.Size = New System.Drawing.Size(83, 60)
        Me.button_reprint_dr.TabIndex = 11
        Me.button_reprint_dr.TabStop = False
        Me.button_reprint_dr.Text = "Reprint DR"
        Me.button_reprint_dr.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_reprint_dr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_reprint_dr.UseVisualStyleBackColor = False
        '
        'button_verify_transaction
        '
        Me.button_verify_transaction.BackColor = System.Drawing.Color.Transparent
        Me.button_verify_transaction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_verify_transaction.Enabled = False
        Me.button_verify_transaction.FlatAppearance.BorderSize = 0
        Me.button_verify_transaction.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_verify_transaction.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_verify_transaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_verify_transaction.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_verify_transaction.ForeColor = System.Drawing.Color.White
        Me.button_verify_transaction.Image = CType(resources.GetObject("button_verify_transaction.Image"), System.Drawing.Image)
        Me.button_verify_transaction.Location = New System.Drawing.Point(0, 2)
        Me.button_verify_transaction.Name = "button_verify_transaction"
        Me.button_verify_transaction.Size = New System.Drawing.Size(108, 60)
        Me.button_verify_transaction.TabIndex = 10
        Me.button_verify_transaction.TabStop = False
        Me.button_verify_transaction.Text = "Verify Transaction"
        Me.button_verify_transaction.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_verify_transaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_verify_transaction.UseVisualStyleBackColor = False
        '
        'panel_total_amount
        '
        Me.panel_total_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_total_amount.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.panel_total_amount.Controls.Add(Me.label_total)
        Me.panel_total_amount.Controls.Add(Me.label_total_amount)
        Me.panel_total_amount.Location = New System.Drawing.Point(405, 357)
        Me.panel_total_amount.Name = "panel_total_amount"
        Me.panel_total_amount.Size = New System.Drawing.Size(226, 74)
        Me.panel_total_amount.TabIndex = 28
        '
        'label_total
        '
        Me.label_total.AutoEllipsis = False
        Me.label_total.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total.ForeColor = System.Drawing.Color.White
        Me.label_total.Location = New System.Drawing.Point(4, 3)
        Me.label_total.Name = "label_total"
        Me.label_total.Size = New System.Drawing.Size(48, 22)
        Me.label_total.TabIndex = 20
        Me.label_total.TabStop = False
        Me.label_total.Text = "Total"
        Me.label_total.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_total.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total.VerticalDirection = False
        '
        'label_total_amount
        '
        Me.label_total_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_total_amount.AutoEllipsis = False
        Me.label_total_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_total_amount.ForeColor = System.Drawing.Color.White
        Me.label_total_amount.Location = New System.Drawing.Point(111, 12)
        Me.label_total_amount.Name = "label_total_amount"
        Me.label_total_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_total_amount.TabIndex = 19
        Me.label_total_amount.TabStop = False
        Me.label_total_amount.Text = "0.00"
        Me.label_total_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_total_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_amount.VerticalDirection = False
        '
        'panel_credit_amount
        '
        Me.panel_credit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_credit_amount.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.panel_credit_amount.Controls.Add(Me.label_credit)
        Me.panel_credit_amount.Controls.Add(Me.label_credit_amount)
        Me.panel_credit_amount.Location = New System.Drawing.Point(633, 357)
        Me.panel_credit_amount.Name = "panel_credit_amount"
        Me.panel_credit_amount.Size = New System.Drawing.Size(235, 74)
        Me.panel_credit_amount.TabIndex = 29
        '
        'label_credit
        '
        Me.label_credit.AutoEllipsis = False
        Me.label_credit.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_credit.ForeColor = System.Drawing.Color.White
        Me.label_credit.Location = New System.Drawing.Point(4, 3)
        Me.label_credit.Name = "label_credit"
        Me.label_credit.Size = New System.Drawing.Size(119, 22)
        Me.label_credit.TabIndex = 20
        Me.label_credit.TabStop = False
        Me.label_credit.Text = "Credit Balance"
        Me.label_credit.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_credit.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_credit.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_credit.VerticalDirection = False
        '
        'label_credit_amount
        '
        Me.label_credit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_credit_amount.AutoEllipsis = False
        Me.label_credit_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_credit_amount.ForeColor = System.Drawing.Color.White
        Me.label_credit_amount.Location = New System.Drawing.Point(120, 12)
        Me.label_credit_amount.Name = "label_credit_amount"
        Me.label_credit_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_credit_amount.TabIndex = 19
        Me.label_credit_amount.TabStop = False
        Me.label_credit_amount.Text = "0.00"
        Me.label_credit_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_credit_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_credit_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_credit_amount.VerticalDirection = False
        '
        'page_navigator
        '
        Me.page_navigator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.page_navigator.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator.ListView = Nothing
        Me.page_navigator.Location = New System.Drawing.Point(0, 63)
        Me.page_navigator.Loopable = True
        Me.page_navigator.Name = "page_navigator"
        Me.page_navigator.PageSize = 2
        Me.page_navigator.Size = New System.Drawing.Size(868, 37)
        Me.page_navigator.TabIndex = 23
        Me.page_navigator.TotalItems = 0
        '
        'button_void_transaction
        '
        Me.button_void_transaction.BackColor = System.Drawing.Color.Transparent
        Me.button_void_transaction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_void_transaction.Enabled = False
        Me.button_void_transaction.FlatAppearance.BorderSize = 0
        Me.button_void_transaction.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_void_transaction.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_void_transaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_void_transaction.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_void_transaction.ForeColor = System.Drawing.Color.White
        Me.button_void_transaction.Image = CType(resources.GetObject("button_void_transaction.Image"), System.Drawing.Image)
        Me.button_void_transaction.Location = New System.Drawing.Point(105, 2)
        Me.button_void_transaction.Name = "button_void_transaction"
        Me.button_void_transaction.Size = New System.Drawing.Size(108, 60)
        Me.button_void_transaction.TabIndex = 16
        Me.button_void_transaction.TabStop = False
        Me.button_void_transaction.Text = "Void Transaction"
        Me.button_void_transaction.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_void_transaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_void_transaction.UseVisualStyleBackColor = False
        '
        'form_sales_history
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(866, 431)
        Me.Controls.Add(Me.panel_total_amount)
        Me.Controls.Add(Me.panel_credit_amount)
        Me.Controls.Add(Me.panel_controls)
        Me.Controls.Add(Me.list_returns)
        Me.Controls.Add(Me.list_transactions)
        Me.Controls.Add(Me.page_navigator)
        Me.Controls.Add(Me.list_products)
        Me.Controls.Add(Me.panel_header)
        Me.MinimizeBox = False
        Me.Name = "form_sales_history"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Sales History"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_header.ResumeLayout(False)
        Me.panel_header.PerformLayout()
        Me.panel_controls.ResumeLayout(False)
        Me.panel_total_amount.ResumeLayout(False)
        Me.panel_total_amount.PerformLayout()
        Me.panel_credit_amount.ResumeLayout(False)
        Me.panel_credit_amount.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panel_header As System.Windows.Forms.Panel
    Friend WithEvents label_cart_count As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents list_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents page_navigator As DevaReePharmacy.PageNavigator
    Friend WithEvents list_transactions As System.Windows.Forms.ListView
    Friend WithEvents list_returns As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents label_return_count As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_invoice_no As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_controls As System.Windows.Forms.Panel
    Friend WithEvents button_verify_transaction As System.Windows.Forms.Button
    Friend WithEvents imagelist_small_icons As System.Windows.Forms.ImageList
    Friend WithEvents button_reprint_dr As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_change_customer As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader18 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_change_agent As System.Windows.Forms.Button
    Friend WithEvents panel_total_amount As System.Windows.Forms.Panel
    Friend WithEvents label_total As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_total_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_credit_amount As System.Windows.Forms.Panel
    Friend WithEvents label_credit As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_credit_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents button_return As System.Windows.Forms.Button
    Friend WithEvents button_change_credit As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_void_transaction As System.Windows.Forms.Button
End Class
