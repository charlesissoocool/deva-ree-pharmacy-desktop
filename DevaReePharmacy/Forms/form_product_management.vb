﻿Public Class form_product_management

    Dim sql As New class_db_products
    Dim col_width() As Double = {20, 5, 5, 15, 15, 15, 5, 5, 15}
    Dim _current_search_tag As String = Nothing
    Dim _current_offset As Integer
    Private _column_sort As New Dictionary(Of Integer, String)
    Private _current_sort_field As String = "category_name"
    Private _current_sort_order As String = "ASC"

    Private Sub form_product_management_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        'added 07/17/2015 by Aljun shortcut to close the form
        If e.KeyCode = Keys.F12 Then
            Me.Close()
        End If
    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        panel_header.BackColor = module_colors.color_gray
        page_navigator.TotalItems = sql.GetProductCount()
        page_navigator.ListView = list_products
        list_products.Columns(6).Text = SYS_PACK_ALIAS & " Size"
        list_products.Columns(7).Text = SYS_PAD_ALIAS & " per " & SYS_PACK_ALIAS
        _column_sort.Add(0, "product_name")

        Me.KeyPreview = True
    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        ResizeListViewColumn(list_products, col_width)

    End Sub

    Private Function GetGroupCount(ByVal group_name As String) As Integer

        GetGroupCount = 0
        For Each group As ListViewGroup In list_products.Groups
            If group.Header = group_name Then GetGroupCount += 1
        Next

    End Function

    Private Sub LoadProductList(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0)

        list_products.Items.Clear()
        Dim products = sql.GetProductListAndDetails(search_tag, offset, , _current_sort_field, _current_sort_order)
        _current_search_tag = search_tag
        _current_offset = offset
        If Not IsNothing(products) Then
            Dim sub_item As ListViewItem.ListViewSubItem
            Dim last_group_key = "", assign_key = "", group_key As String
            Dim list_group As New ListViewGroup
            For Each product In products
                group_key = product("category_id")
                If Not last_group_key = group_key Then
                    assign_key = group_key & "-" & (GetGroupCount(product("category_name")) + 1)
                    list_group = list_products.Groups.Add(assign_key, product("category_name"))
                    last_group_key = group_key
                End If
                Dim new_row As New ListViewItem
                new_row.Group = list_products.Groups(assign_key)
                new_row.Text = product("product_name")
                new_row.UseItemStyleForSubItems = False
                new_row.SubItems.Add(product("product_code"))
                new_row.SubItems.Add(product("product_unit"))
                new_row.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(product("retail_price")), 2, , , TriState.True))
                new_row.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(product("wholesale_price")), 2, , , TriState.True))
                new_row.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(product("distribution_price")), 2, , , TriState.True))
                sub_item = New ListViewItem.ListViewSubItem
                If product("packing_size") = 0 Then
                    sub_item.ForeColor = module_colors.color_red
                    sub_item.Font = New Font(sub_item.Font, FontStyle.Bold)
                End If
                sub_item.Text = product("packing_size")
                new_row.SubItems.Add(sub_item)
                sub_item = New ListViewItem.ListViewSubItem
                If product("pads_per_pack") = 0 Then
                    sub_item.ForeColor = module_colors.color_red
                    sub_item.Font = New Font(sub_item.Font, FontStyle.Bold)
                End If
                sub_item.Text = product("pads_per_pack")
                new_row.SubItems.Add(sub_item)
                If product("packing_size") > 1 Then
                    new_row.SubItems.Add(FormatNumber(product("stock_quantity"), 0, , , TriState.True) & " " & product("product_unit") & " (" & (Math.Round(product("stock_quantity") / product("packing_size"), 0)) & " " & SYS_PACK_ALIAS & ")")
                Else
                    new_row.SubItems.Add(FormatNumber(product("stock_quantity"), 0, , , TriState.True) & " " & product("product_unit"))
                End If
                new_row.Tag = product("product_id")
                list_products.Items.Add(new_row)
            Next
        End If

    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            _current_search_tag = text_search.Text
            page_navigator.TotalItems = sql.GetProductCount(_current_search_tag)
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            _current_search_tag = Nothing
            text_search.Text = ""
            page_navigator.TotalItems = sql.GetProductCount(_current_search_tag)
        End If

    End Sub

    Private Sub list_products_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        If e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            text_search.Text = ""
            _current_search_tag = Nothing
            page_navigator.TotalItems = sql.GetProductCount()
        End If

    End Sub

    Private Sub button_add_product_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_product.Click

        Dim product_editor As New form_product_editor
        Dim product_details = product_editor.OpenProductEditor(Me)
        If Not IsNothing(product_details) Then
            If product_details.Persist() Then
                page_navigator.TotalItems = sql.GetProductCount
                MsgBox("Product successfully added.", MsgBoxStyle.Information)
            Else
                MsgBox("Failed to add product.", MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub page_navigator_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator.PageChanged

        LoadProductList(_current_search_tag, offset)

    End Sub

    Private Sub button_edit_product_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_edit_product.Click

        If list_products.SelectedItems.Count < 1 Then
            button_edit_product.Enabled = False
        Else
            Dim product_editor As New form_product_editor
            Dim product_details As New Product(Val(list_products.SelectedItems(0).Tag))
            product_details = product_editor.OpenProductEditor(Me, product_details)
            If Not IsNothing(product_details) Then
                If product_details.Persist() Then
                    page_navigator.TotalItems = sql.GetProductCount()
                Else
                    MsgBox("Failed to update product.", MsgBoxStyle.Critical)
                End If
            End If
        End If
    End Sub

    Private Sub list_products_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles list_products.ColumnClick

        If _column_sort.ContainsKey(e.Column) Then
            If _current_sort_field = _column_sort(e.Column) Then
                _current_sort_order = If(_current_sort_order = "ASC", "DESC", "ASC")
            Else
                _current_sort_field = _column_sort(e.Column)
            End If
            page_navigator.TotalItems = sql.GetProductCount
        End If

    End Sub

    Private Sub list_products_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_products.DoubleClick

        If button_edit_product.Enabled Then button_edit_product.PerformClick()

    End Sub

    Private Sub list_products_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_products.SelectedIndexChanged

        button_edit_product.Enabled = list_products.SelectedItems.Count > 0
        button_delete_product.Enabled = button_edit_product.Enabled

    End Sub

    Private Sub button_delete_product_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_delete_product.Click

        If list_products.SelectedItems.Count < 1 Then
            button_delete_product.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_products.SelectedItems(0)
            If sql.IsProductInFlow(Val(sel_row.Tag)) Then
                MsgBox("Cannot delete product as it is already in stock.", MsgBoxStyle.Exclamation)
            Else
                Dim ask = MsgBox("Are you sure you want to delete this product?", MsgBoxStyle.YesNo)
                If ask = DialogResult.Yes Then
                    If Not sql.DeleteProduct(Val(sel_row.Tag)) Then
                        MsgBox("Failed to delete product.", MsgBoxStyle.Critical)
                    Else
                        page_navigator.TotalItems = sql.GetProductCount()
                    End If
                End If
            End If
        End If


    End Sub

    Private Sub button_print_price_list_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_print_price_list.Click

        Dim price_list_generator As New form_price_list_generator
        price_list_generator.ShowDialog(Me)

    End Sub

    Private Sub button_resort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_resort.Click

        Dim sort_key = "category_name"
        If _current_sort_field = sort_key Then
            _current_sort_order = If(_current_sort_order = "ASC", "DESC", "ASC")
        Else
            _current_sort_field = sort_key
        End If
        page_navigator.TotalItems = sql.GetProductCount

    End Sub
End Class