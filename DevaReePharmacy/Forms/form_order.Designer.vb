﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_order
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_order))
        Me.list_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.imagelist_small_icons = New System.Windows.Forms.ImageList(Me.components)
        Me.panel_amounts = New System.Windows.Forms.Panel()
        Me.panel_credit = New System.Windows.Forms.Panel()
        Me.panel_change_amount = New System.Windows.Forms.Panel()
        Me.panel_tendered_amount = New System.Windows.Forms.Panel()
        Me.panel_total_amount = New System.Windows.Forms.Panel()
        Me.panel_controls = New System.Windows.Forms.Panel()
        Me.image_sold = New System.Windows.Forms.PictureBox()
        Me.panel_main_controls = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.button_new_dispatch = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.button_new_transaction = New System.Windows.Forms.Button()
        Me.button_use_admin = New System.Windows.Forms.Button()
        Me.button_cancel_order = New System.Windows.Forms.Button()
        Me.button_place_order = New System.Windows.Forms.Button()
        Me.panel_cart_controls = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.button_set_customer = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.button_set_credit = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.button_set_reference_no = New System.Windows.Forms.Button()
        Me.button_price_inquiry = New System.Windows.Forms.Button()
        Me.button_change_unit = New System.Windows.Forms.Button()
        Me.button_void_item = New System.Windows.Forms.Button()
        Me.button_set_price = New System.Windows.Forms.Button()
        Me.button_set_quantity = New System.Windows.Forms.Button()
        Me.button_browse = New System.Windows.Forms.Button()
        Me.panel_item_detail = New System.Windows.Forms.Panel()
        Me.panel_header = New System.Windows.Forms.Panel()
        Me.timer_console = New System.Windows.Forms.Timer(Me.components)
        Me.timer_time = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.button_reprint_dr = New System.Windows.Forms.Button()
        Me.label_order_mode = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_customer_name = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_order_type = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_reference_no = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_cart_count = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_time = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_credit = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_credit_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_credit_amount_currency = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_change = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_change_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_change_amount_currency = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_tendered = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_tendered_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_tendered_amount_currency = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_amount = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_total_amount_currency = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_product_name = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_product_price = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_currency = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_console = New DevaReePharmacy.AntiAliasedLabel()
        Me.panel_amounts.SuspendLayout()
        Me.panel_credit.SuspendLayout()
        Me.panel_change_amount.SuspendLayout()
        Me.panel_tendered_amount.SuspendLayout()
        Me.panel_total_amount.SuspendLayout()
        Me.panel_controls.SuspendLayout()
        CType(Me.image_sold, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel_main_controls.SuspendLayout()
        Me.panel_cart_controls.SuspendLayout()
        Me.panel_item_detail.SuspendLayout()
        Me.panel_header.SuspendLayout()
        Me.SuspendLayout()
        '
        'list_products
        '
        Me.list_products.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.list_products.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.list_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_products.FullRowSelect = True
        Me.list_products.GridLines = True
        Me.list_products.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.list_products.HideSelection = False
        Me.list_products.Location = New System.Drawing.Point(0, 59)
        Me.list_products.Margin = New System.Windows.Forms.Padding(0)
        Me.list_products.MultiSelect = False
        Me.list_products.Name = "list_products"
        Me.list_products.Size = New System.Drawing.Size(862, 571)
        Me.list_products.SmallImageList = Me.imagelist_small_icons
        Me.list_products.TabIndex = 7
        Me.list_products.UseCompatibleStateImageBehavior = False
        Me.list_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product"
        Me.ColumnHeader1.Width = 222
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Quantity"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader2.Width = 89
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Unit"
        Me.ColumnHeader3.Width = 65
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Price"
        Me.ColumnHeader4.Width = 122
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Extended Price"
        Me.ColumnHeader5.Width = 91
        '
        'imagelist_small_icons
        '
        Me.imagelist_small_icons.ImageStream = CType(resources.GetObject("imagelist_small_icons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imagelist_small_icons.TransparentColor = System.Drawing.Color.Transparent
        Me.imagelist_small_icons.Images.SetKeyName(0, "box")
        Me.imagelist_small_icons.Images.SetKeyName(1, "pad")
        Me.imagelist_small_icons.Images.SetKeyName(2, "pcs")
        Me.imagelist_small_icons.Images.SetKeyName(3, "invalid")
        '
        'panel_amounts
        '
        Me.panel_amounts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_amounts.Controls.Add(Me.panel_credit)
        Me.panel_amounts.Controls.Add(Me.panel_change_amount)
        Me.panel_amounts.Controls.Add(Me.panel_tendered_amount)
        Me.panel_amounts.Controls.Add(Me.panel_total_amount)
        Me.panel_amounts.Location = New System.Drawing.Point(0, 544)
        Me.panel_amounts.Name = "panel_amounts"
        Me.panel_amounts.Size = New System.Drawing.Size(407, 220)
        Me.panel_amounts.TabIndex = 18
        '
        'panel_credit
        '
        Me.panel_credit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_credit.BackColor = System.Drawing.Color.Transparent
        Me.panel_credit.Controls.Add(Me.label_credit)
        Me.panel_credit.Controls.Add(Me.label_credit_amount)
        Me.panel_credit.Controls.Add(Me.label_credit_amount_currency)
        Me.panel_credit.Location = New System.Drawing.Point(1, 55)
        Me.panel_credit.Name = "panel_credit"
        Me.panel_credit.Size = New System.Drawing.Size(405, 55)
        Me.panel_credit.TabIndex = 18
        '
        'panel_change_amount
        '
        Me.panel_change_amount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_change_amount.BackColor = System.Drawing.Color.Transparent
        Me.panel_change_amount.Controls.Add(Me.label_change)
        Me.panel_change_amount.Controls.Add(Me.label_change_amount)
        Me.panel_change_amount.Controls.Add(Me.label_change_amount_currency)
        Me.panel_change_amount.Location = New System.Drawing.Point(0, 165)
        Me.panel_change_amount.Name = "panel_change_amount"
        Me.panel_change_amount.Size = New System.Drawing.Size(405, 55)
        Me.panel_change_amount.TabIndex = 17
        '
        'panel_tendered_amount
        '
        Me.panel_tendered_amount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_tendered_amount.BackColor = System.Drawing.Color.Transparent
        Me.panel_tendered_amount.Controls.Add(Me.label_tendered)
        Me.panel_tendered_amount.Controls.Add(Me.label_tendered_amount)
        Me.panel_tendered_amount.Controls.Add(Me.label_tendered_amount_currency)
        Me.panel_tendered_amount.Location = New System.Drawing.Point(0, 110)
        Me.panel_tendered_amount.Name = "panel_tendered_amount"
        Me.panel_tendered_amount.Size = New System.Drawing.Size(405, 55)
        Me.panel_tendered_amount.TabIndex = 16
        '
        'panel_total_amount
        '
        Me.panel_total_amount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_total_amount.BackColor = System.Drawing.Color.Transparent
        Me.panel_total_amount.Controls.Add(Me.label_total)
        Me.panel_total_amount.Controls.Add(Me.label_total_amount)
        Me.panel_total_amount.Controls.Add(Me.label_total_amount_currency)
        Me.panel_total_amount.Location = New System.Drawing.Point(1, 0)
        Me.panel_total_amount.Name = "panel_total_amount"
        Me.panel_total_amount.Size = New System.Drawing.Size(405, 55)
        Me.panel_total_amount.TabIndex = 15
        '
        'panel_controls
        '
        Me.panel_controls.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_controls.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(60, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.panel_controls.Controls.Add(Me.image_sold)
        Me.panel_controls.Controls.Add(Me.panel_main_controls)
        Me.panel_controls.Controls.Add(Me.label_time)
        Me.panel_controls.Controls.Add(Me.panel_amounts)
        Me.panel_controls.Controls.Add(Me.panel_cart_controls)
        Me.panel_controls.Location = New System.Drawing.Point(861, 0)
        Me.panel_controls.Name = "panel_controls"
        Me.panel_controls.Size = New System.Drawing.Size(407, 766)
        Me.panel_controls.TabIndex = 19
        '
        'image_sold
        '
        Me.image_sold.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.image_sold.Image = CType(resources.GetObject("image_sold.Image"), System.Drawing.Image)
        Me.image_sold.Location = New System.Drawing.Point(190, 490)
        Me.image_sold.Name = "image_sold"
        Me.image_sold.Size = New System.Drawing.Size(32, 32)
        Me.image_sold.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.image_sold.TabIndex = 40
        Me.image_sold.TabStop = False
        Me.image_sold.Visible = False
        '
        'panel_main_controls
        '
        Me.panel_main_controls.Controls.Add(Me.Label1)
        Me.panel_main_controls.Controls.Add(Me.button_reprint_dr)
        Me.panel_main_controls.Controls.Add(Me.Label17)
        Me.panel_main_controls.Controls.Add(Me.button_new_dispatch)
        Me.panel_main_controls.Controls.Add(Me.Label14)
        Me.panel_main_controls.Controls.Add(Me.Label11)
        Me.panel_main_controls.Controls.Add(Me.Label10)
        Me.panel_main_controls.Controls.Add(Me.Label9)
        Me.panel_main_controls.Controls.Add(Me.button_new_transaction)
        Me.panel_main_controls.Controls.Add(Me.button_use_admin)
        Me.panel_main_controls.Controls.Add(Me.button_cancel_order)
        Me.panel_main_controls.Controls.Add(Me.button_place_order)
        Me.panel_main_controls.Location = New System.Drawing.Point(34, 29)
        Me.panel_main_controls.Name = "panel_main_controls"
        Me.panel_main_controls.Size = New System.Drawing.Size(346, 160)
        Me.panel_main_controls.TabIndex = 37
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label17.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(196, 3)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 24)
        Me.Label17.TabIndex = 56
        Me.Label17.Text = "Cl+D"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_new_dispatch
        '
        Me.button_new_dispatch.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_new_dispatch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_new_dispatch.Enabled = False
        Me.button_new_dispatch.FlatAppearance.BorderSize = 0
        Me.button_new_dispatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_new_dispatch.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_new_dispatch.ForeColor = System.Drawing.Color.White
        Me.button_new_dispatch.Image = CType(resources.GetObject("button_new_dispatch.Image"), System.Drawing.Image)
        Me.button_new_dispatch.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_new_dispatch.Location = New System.Drawing.Point(117, 2)
        Me.button_new_dispatch.Name = "button_new_dispatch"
        Me.button_new_dispatch.Size = New System.Drawing.Size(113, 77)
        Me.button_new_dispatch.TabIndex = 55
        Me.button_new_dispatch.TabStop = False
        Me.button_new_dispatch.Text = "Dispatch"
        Me.button_new_dispatch.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_new_dispatch.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label14.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(196, 81)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 24)
        Me.Label14.TabIndex = 46
        Me.Label14.Text = "Sh+U"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label11.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(86, 81)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(29, 24)
        Me.Label11.TabIndex = 43
        Me.Label11.Text = "F10"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label10.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(316, 1)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 24)
        Me.Label10.TabIndex = 42
        Me.Label10.Text = "ESC"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label9.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(81, 2)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 24)
        Me.Label9.TabIndex = 41
        Me.Label9.Text = "Cl+T"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_new_transaction
        '
        Me.button_new_transaction.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_new_transaction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_new_transaction.FlatAppearance.BorderSize = 0
        Me.button_new_transaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_new_transaction.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_new_transaction.ForeColor = System.Drawing.Color.White
        Me.button_new_transaction.Image = CType(resources.GetObject("button_new_transaction.Image"), System.Drawing.Image)
        Me.button_new_transaction.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_new_transaction.Location = New System.Drawing.Point(2, 2)
        Me.button_new_transaction.Name = "button_new_transaction"
        Me.button_new_transaction.Size = New System.Drawing.Size(113, 77)
        Me.button_new_transaction.TabIndex = 27
        Me.button_new_transaction.TabStop = False
        Me.button_new_transaction.Text = "Transaction"
        Me.button_new_transaction.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_new_transaction.UseVisualStyleBackColor = False
        '
        'button_use_admin
        '
        Me.button_use_admin.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_use_admin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_use_admin.FlatAppearance.BorderSize = 0
        Me.button_use_admin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_use_admin.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_use_admin.ForeColor = System.Drawing.Color.White
        Me.button_use_admin.Image = CType(resources.GetObject("button_use_admin.Image"), System.Drawing.Image)
        Me.button_use_admin.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_use_admin.Location = New System.Drawing.Point(117, 81)
        Me.button_use_admin.Name = "button_use_admin"
        Me.button_use_admin.Size = New System.Drawing.Size(113, 77)
        Me.button_use_admin.TabIndex = 30
        Me.button_use_admin.TabStop = False
        Me.button_use_admin.Text = "Use Admin"
        Me.button_use_admin.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_use_admin.UseVisualStyleBackColor = False
        '
        'button_cancel_order
        '
        Me.button_cancel_order.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_cancel_order.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel_order.Enabled = False
        Me.button_cancel_order.FlatAppearance.BorderSize = 0
        Me.button_cancel_order.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel_order.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel_order.ForeColor = System.Drawing.Color.White
        Me.button_cancel_order.Image = CType(resources.GetObject("button_cancel_order.Image"), System.Drawing.Image)
        Me.button_cancel_order.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_cancel_order.Location = New System.Drawing.Point(232, 2)
        Me.button_cancel_order.Name = "button_cancel_order"
        Me.button_cancel_order.Size = New System.Drawing.Size(113, 77)
        Me.button_cancel_order.TabIndex = 25
        Me.button_cancel_order.TabStop = False
        Me.button_cancel_order.Text = "Cancel Order"
        Me.button_cancel_order.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel_order.UseVisualStyleBackColor = False
        '
        'button_place_order
        '
        Me.button_place_order.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_place_order.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_place_order.Enabled = False
        Me.button_place_order.FlatAppearance.BorderSize = 0
        Me.button_place_order.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_place_order.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_place_order.ForeColor = System.Drawing.Color.White
        Me.button_place_order.Image = CType(resources.GetObject("button_place_order.Image"), System.Drawing.Image)
        Me.button_place_order.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_place_order.Location = New System.Drawing.Point(2, 81)
        Me.button_place_order.Name = "button_place_order"
        Me.button_place_order.Size = New System.Drawing.Size(113, 77)
        Me.button_place_order.TabIndex = 24
        Me.button_place_order.TabStop = False
        Me.button_place_order.Text = "Place Order"
        Me.button_place_order.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_place_order.UseVisualStyleBackColor = False
        '
        'panel_cart_controls
        '
        Me.panel_cart_controls.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.panel_cart_controls.Controls.Add(Me.Label16)
        Me.panel_cart_controls.Controls.Add(Me.Label8)
        Me.panel_cart_controls.Controls.Add(Me.button_set_customer)
        Me.panel_cart_controls.Controls.Add(Me.Label2)
        Me.panel_cart_controls.Controls.Add(Me.Label7)
        Me.panel_cart_controls.Controls.Add(Me.Label6)
        Me.panel_cart_controls.Controls.Add(Me.Label5)
        Me.panel_cart_controls.Controls.Add(Me.button_set_credit)
        Me.panel_cart_controls.Controls.Add(Me.Label13)
        Me.panel_cart_controls.Controls.Add(Me.Label4)
        Me.panel_cart_controls.Controls.Add(Me.Label3)
        Me.panel_cart_controls.Controls.Add(Me.button_set_reference_no)
        Me.panel_cart_controls.Controls.Add(Me.button_price_inquiry)
        Me.panel_cart_controls.Controls.Add(Me.button_change_unit)
        Me.panel_cart_controls.Controls.Add(Me.button_void_item)
        Me.panel_cart_controls.Controls.Add(Me.button_set_price)
        Me.panel_cart_controls.Controls.Add(Me.button_set_quantity)
        Me.panel_cart_controls.Controls.Add(Me.button_browse)
        Me.panel_cart_controls.Location = New System.Drawing.Point(36, 215)
        Me.panel_cart_controls.Name = "panel_cart_controls"
        Me.panel_cart_controls.Size = New System.Drawing.Size(346, 237)
        Me.panel_cart_controls.TabIndex = 21
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label16.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(311, 159)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(34, 24)
        Me.Label16.TabIndex = 58
        Me.Label16.Text = "F9"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label8.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(203, 81)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(27, 24)
        Me.Label8.TabIndex = 45
        Me.Label8.Text = "F5"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_set_customer
        '
        Me.button_set_customer.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_set_customer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_set_customer.Enabled = False
        Me.button_set_customer.FlatAppearance.BorderSize = 0
        Me.button_set_customer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_set_customer.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_set_customer.ForeColor = System.Drawing.Color.White
        Me.button_set_customer.Image = CType(resources.GetObject("button_set_customer.Image"), System.Drawing.Image)
        Me.button_set_customer.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_set_customer.Location = New System.Drawing.Point(232, 159)
        Me.button_set_customer.Name = "button_set_customer"
        Me.button_set_customer.Size = New System.Drawing.Size(113, 77)
        Me.button_set_customer.TabIndex = 57
        Me.button_set_customer.TabStop = False
        Me.button_set_customer.Text = "Customer"
        Me.button_set_customer.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_set_customer.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(201, 158)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 24)
        Me.Label2.TabIndex = 53
        Me.Label2.Text = "F8"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(315, 81)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 24)
        Me.Label7.TabIndex = 44
        Me.Label7.Text = "F6"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label6.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(85, 81)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 24)
        Me.Label6.TabIndex = 43
        Me.Label6.Text = "F4"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label5.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(316, 1)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 24)
        Me.Label5.TabIndex = 42
        Me.Label5.Text = "F3"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_set_credit
        '
        Me.button_set_credit.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_set_credit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_set_credit.Enabled = False
        Me.button_set_credit.FlatAppearance.BorderSize = 0
        Me.button_set_credit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_set_credit.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_set_credit.ForeColor = System.Drawing.Color.White
        Me.button_set_credit.Image = CType(resources.GetObject("button_set_credit.Image"), System.Drawing.Image)
        Me.button_set_credit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_set_credit.Location = New System.Drawing.Point(117, 159)
        Me.button_set_credit.Name = "button_set_credit"
        Me.button_set_credit.Size = New System.Drawing.Size(113, 77)
        Me.button_set_credit.TabIndex = 52
        Me.button_set_credit.TabStop = False
        Me.button_set_credit.Text = "Set Credit"
        Me.button_set_credit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_set_credit.UseVisualStyleBackColor = False
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label13.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(86, 159)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 24)
        Me.Label13.TabIndex = 45
        Me.Label13.Text = "F7"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label4.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(201, 1)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 24)
        Me.Label4.TabIndex = 41
        Me.Label4.Text = "F2"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label3.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(85, 1)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 24)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "F1"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_set_reference_no
        '
        Me.button_set_reference_no.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_set_reference_no.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_set_reference_no.FlatAppearance.BorderSize = 0
        Me.button_set_reference_no.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_set_reference_no.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_set_reference_no.ForeColor = System.Drawing.Color.White
        Me.button_set_reference_no.Image = CType(resources.GetObject("button_set_reference_no.Image"), System.Drawing.Image)
        Me.button_set_reference_no.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_set_reference_no.Location = New System.Drawing.Point(2, 159)
        Me.button_set_reference_no.Name = "button_set_reference_no"
        Me.button_set_reference_no.Size = New System.Drawing.Size(113, 77)
        Me.button_set_reference_no.TabIndex = 29
        Me.button_set_reference_no.TabStop = False
        Me.button_set_reference_no.Text = "Reference No."
        Me.button_set_reference_no.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_set_reference_no.UseVisualStyleBackColor = False
        '
        'button_price_inquiry
        '
        Me.button_price_inquiry.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_price_inquiry.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_price_inquiry.Enabled = False
        Me.button_price_inquiry.FlatAppearance.BorderSize = 0
        Me.button_price_inquiry.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_price_inquiry.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_price_inquiry.ForeColor = System.Drawing.Color.White
        Me.button_price_inquiry.Image = CType(resources.GetObject("button_price_inquiry.Image"), System.Drawing.Image)
        Me.button_price_inquiry.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_price_inquiry.Location = New System.Drawing.Point(117, 1)
        Me.button_price_inquiry.Name = "button_price_inquiry"
        Me.button_price_inquiry.Size = New System.Drawing.Size(113, 77)
        Me.button_price_inquiry.TabIndex = 39
        Me.button_price_inquiry.TabStop = False
        Me.button_price_inquiry.Text = "Price Inquiry"
        Me.button_price_inquiry.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_price_inquiry.UseVisualStyleBackColor = False
        '
        'button_change_unit
        '
        Me.button_change_unit.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_change_unit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_change_unit.Enabled = False
        Me.button_change_unit.FlatAppearance.BorderSize = 0
        Me.button_change_unit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_change_unit.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_change_unit.ForeColor = System.Drawing.Color.White
        Me.button_change_unit.Image = CType(resources.GetObject("button_change_unit.Image"), System.Drawing.Image)
        Me.button_change_unit.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_change_unit.Location = New System.Drawing.Point(117, 80)
        Me.button_change_unit.Name = "button_change_unit"
        Me.button_change_unit.Size = New System.Drawing.Size(113, 77)
        Me.button_change_unit.TabIndex = 31
        Me.button_change_unit.TabStop = False
        Me.button_change_unit.Text = "Change Unit"
        Me.button_change_unit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_change_unit.UseVisualStyleBackColor = False
        '
        'button_void_item
        '
        Me.button_void_item.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_void_item.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_void_item.Enabled = False
        Me.button_void_item.FlatAppearance.BorderSize = 0
        Me.button_void_item.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_void_item.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_void_item.ForeColor = System.Drawing.Color.White
        Me.button_void_item.Image = CType(resources.GetObject("button_void_item.Image"), System.Drawing.Image)
        Me.button_void_item.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_void_item.Location = New System.Drawing.Point(232, 80)
        Me.button_void_item.Name = "button_void_item"
        Me.button_void_item.Size = New System.Drawing.Size(113, 77)
        Me.button_void_item.TabIndex = 23
        Me.button_void_item.TabStop = False
        Me.button_void_item.Text = "Void Item"
        Me.button_void_item.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_void_item.UseVisualStyleBackColor = False
        '
        'button_set_price
        '
        Me.button_set_price.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_set_price.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_set_price.Enabled = False
        Me.button_set_price.FlatAppearance.BorderSize = 0
        Me.button_set_price.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_set_price.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_set_price.ForeColor = System.Drawing.Color.White
        Me.button_set_price.Image = CType(resources.GetObject("button_set_price.Image"), System.Drawing.Image)
        Me.button_set_price.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_set_price.Location = New System.Drawing.Point(232, 1)
        Me.button_set_price.Name = "button_set_price"
        Me.button_set_price.Size = New System.Drawing.Size(113, 77)
        Me.button_set_price.TabIndex = 22
        Me.button_set_price.TabStop = False
        Me.button_set_price.Text = "Edit Price"
        Me.button_set_price.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_set_price.UseVisualStyleBackColor = False
        '
        'button_set_quantity
        '
        Me.button_set_quantity.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_set_quantity.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_set_quantity.Enabled = False
        Me.button_set_quantity.FlatAppearance.BorderSize = 0
        Me.button_set_quantity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_set_quantity.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_set_quantity.ForeColor = System.Drawing.Color.White
        Me.button_set_quantity.Image = CType(resources.GetObject("button_set_quantity.Image"), System.Drawing.Image)
        Me.button_set_quantity.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_set_quantity.Location = New System.Drawing.Point(2, 80)
        Me.button_set_quantity.Name = "button_set_quantity"
        Me.button_set_quantity.Size = New System.Drawing.Size(113, 77)
        Me.button_set_quantity.TabIndex = 21
        Me.button_set_quantity.TabStop = False
        Me.button_set_quantity.Text = "Set Quantity"
        Me.button_set_quantity.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_set_quantity.UseVisualStyleBackColor = False
        '
        'button_browse
        '
        Me.button_browse.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_browse.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_browse.Enabled = False
        Me.button_browse.FlatAppearance.BorderSize = 0
        Me.button_browse.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_browse.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_browse.ForeColor = System.Drawing.Color.White
        Me.button_browse.Image = CType(resources.GetObject("button_browse.Image"), System.Drawing.Image)
        Me.button_browse.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_browse.Location = New System.Drawing.Point(2, 1)
        Me.button_browse.Name = "button_browse"
        Me.button_browse.Size = New System.Drawing.Size(113, 77)
        Me.button_browse.TabIndex = 20
        Me.button_browse.TabStop = False
        Me.button_browse.Text = "Browse"
        Me.button_browse.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_browse.UseVisualStyleBackColor = False
        '
        'panel_item_detail
        '
        Me.panel_item_detail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_item_detail.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.panel_item_detail.Controls.Add(Me.label_product_name)
        Me.panel_item_detail.Controls.Add(Me.label_product_price)
        Me.panel_item_detail.Controls.Add(Me.label_currency)
        Me.panel_item_detail.Controls.Add(Me.label_console)
        Me.panel_item_detail.Location = New System.Drawing.Point(0, 627)
        Me.panel_item_detail.Name = "panel_item_detail"
        Me.panel_item_detail.Size = New System.Drawing.Size(862, 139)
        Me.panel_item_detail.TabIndex = 33
        '
        'panel_header
        '
        Me.panel_header.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_header.BackColor = System.Drawing.Color.Gold
        Me.panel_header.Controls.Add(Me.label_order_mode)
        Me.panel_header.Controls.Add(Me.label_customer_name)
        Me.panel_header.Controls.Add(Me.label_order_type)
        Me.panel_header.Controls.Add(Me.label_reference_no)
        Me.panel_header.Controls.Add(Me.label_cart_count)
        Me.panel_header.Location = New System.Drawing.Point(0, 0)
        Me.panel_header.Name = "panel_header"
        Me.panel_header.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.panel_header.Size = New System.Drawing.Size(863, 59)
        Me.panel_header.TabIndex = 20
        '
        'timer_console
        '
        Me.timer_console.Interval = 500
        '
        'timer_time
        '
        Me.timer_time.Enabled = True
        Me.timer_time.Interval = 500
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(61, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Open Sans Semibold", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(311, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 24)
        Me.Label1.TabIndex = 58
        Me.Label1.Text = "Cl+P"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'button_reprint_dr
        '
        Me.button_reprint_dr.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.button_reprint_dr.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_reprint_dr.FlatAppearance.BorderSize = 0
        Me.button_reprint_dr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_reprint_dr.Font = New System.Drawing.Font("Open Sans", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_reprint_dr.ForeColor = System.Drawing.Color.White
        Me.button_reprint_dr.Image = CType(resources.GetObject("button_reprint_dr.Image"), System.Drawing.Image)
        Me.button_reprint_dr.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.button_reprint_dr.Location = New System.Drawing.Point(232, 81)
        Me.button_reprint_dr.Name = "button_reprint_dr"
        Me.button_reprint_dr.Size = New System.Drawing.Size(113, 77)
        Me.button_reprint_dr.TabIndex = 57
        Me.button_reprint_dr.TabStop = False
        Me.button_reprint_dr.Text = "Reprint DR"
        Me.button_reprint_dr.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_reprint_dr.UseVisualStyleBackColor = False
        '
        'label_order_mode
        '
        Me.label_order_mode.AutoEllipsis = False
        Me.label_order_mode.Font = New System.Drawing.Font("Open Sans Extrabold", 12.0!)
        Me.label_order_mode.ForeColor = System.Drawing.Color.Black
        Me.label_order_mode.Location = New System.Drawing.Point(400, 10)
        Me.label_order_mode.Name = "label_order_mode"
        Me.label_order_mode.Size = New System.Drawing.Size(59, 22)
        Me.label_order_mode.TabIndex = 21
        Me.label_order_mode.TabStop = False
        Me.label_order_mode.Text = "Order"
        Me.label_order_mode.TextAlign = DevaReePharmacy.TextAlignment.Center
        Me.label_order_mode.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_order_mode.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_order_mode.VerticalDirection = False
        '
        'label_customer_name
        '
        Me.label_customer_name.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_customer_name.AutoEllipsis = False
        Me.label_customer_name.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_customer_name.ForeColor = System.Drawing.Color.Black
        Me.label_customer_name.Location = New System.Drawing.Point(773, 30)
        Me.label_customer_name.Name = "label_customer_name"
        Me.label_customer_name.Size = New System.Drawing.Size(83, 19)
        Me.label_customer_name.TabIndex = 20
        Me.label_customer_name.TabStop = False
        Me.label_customer_name.Text = "Customer: -"
        Me.label_customer_name.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_customer_name.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_customer_name.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_customer_name.VerticalDirection = False
        '
        'label_order_type
        '
        Me.label_order_type.AutoEllipsis = False
        Me.label_order_type.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_order_type.ForeColor = System.Drawing.Color.Black
        Me.label_order_type.Location = New System.Drawing.Point(400, 31)
        Me.label_order_type.Name = "label_order_type"
        Me.label_order_type.Size = New System.Drawing.Size(19, 19)
        Me.label_order_type.TabIndex = 19
        Me.label_order_type.TabStop = False
        Me.label_order_type.Text = "--"
        Me.label_order_type.TextAlign = DevaReePharmacy.TextAlignment.Center
        Me.label_order_type.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_order_type.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_order_type.VerticalDirection = False
        '
        'label_reference_no
        '
        Me.label_reference_no.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_reference_no.AutoEllipsis = False
        Me.label_reference_no.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_reference_no.ForeColor = System.Drawing.Color.Black
        Me.label_reference_no.Location = New System.Drawing.Point(759, 10)
        Me.label_reference_no.Name = "label_reference_no"
        Me.label_reference_no.Size = New System.Drawing.Size(98, 19)
        Me.label_reference_no.TabIndex = 18
        Me.label_reference_no.TabStop = False
        Me.label_reference_no.Text = "Reference #: -"
        Me.label_reference_no.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_reference_no.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_reference_no.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_reference_no.VerticalDirection = False
        '
        'label_cart_count
        '
        Me.label_cart_count.AutoEllipsis = False
        Me.label_cart_count.Font = New System.Drawing.Font("Open Sans Light", 10.0!)
        Me.label_cart_count.ForeColor = System.Drawing.Color.Black
        Me.label_cart_count.Location = New System.Drawing.Point(11, 18)
        Me.label_cart_count.Name = "label_cart_count"
        Me.label_cart_count.Size = New System.Drawing.Size(94, 19)
        Me.label_cart_count.TabIndex = 17
        Me.label_cart_count.TabStop = False
        Me.label_cart_count.Text = "Cart (0 items)"
        Me.label_cart_count.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_cart_count.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_cart_count.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_cart_count.VerticalDirection = False
        '
        'label_time
        '
        Me.label_time.AutoSize = False
        Me.label_time.Font = New System.Drawing.Font("Open Sans", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_time.ForeColor = System.Drawing.Color.White
        Me.label_time.Location = New System.Drawing.Point(8, 4)
        Me.label_time.Name = "label_time"
        Me.label_time.Size = New System.Drawing.Size(393, 20)
        Me.label_time.TabIndex = 19
        Me.label_time.TabStop = False
        Me.label_time.Text = "00:00"
        Me.label_time.TextAlign = DevaReePharmacy.TextAlignment.Center
        Me.label_time.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_time.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_time.VerticalDirection = False
        '
        'label_credit
        '
        Me.label_credit.AutoEllipsis = False
        Me.label_credit.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_credit.ForeColor = System.Drawing.Color.White
        Me.label_credit.Location = New System.Drawing.Point(14, 18)
        Me.label_credit.Name = "label_credit"
        Me.label_credit.Size = New System.Drawing.Size(56, 22)
        Me.label_credit.TabIndex = 20
        Me.label_credit.TabStop = False
        Me.label_credit.Text = "Credit"
        Me.label_credit.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_credit.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_credit.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_credit.VerticalDirection = False
        '
        'label_credit_amount
        '
        Me.label_credit_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_credit_amount.AutoAnimate = False
        Me.label_credit_amount.AutoEllipsis = False
        Me.label_credit_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_credit_amount.ForeColor = System.Drawing.Color.White
        Me.label_credit_amount.Location = New System.Drawing.Point(283, -9)
        Me.label_credit_amount.Name = "label_credit_amount"
        Me.label_credit_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_credit_amount.TabIndex = 19
        Me.label_credit_amount.TabStop = False
        Me.label_credit_amount.Text = "0.00"
        Me.label_credit_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_credit_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_credit_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_credit_amount.VerticalDirection = False
        '
        'label_credit_amount_currency
        '
        Me.label_credit_amount_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.label_credit_amount_currency.AutoEllipsis = False
        Me.label_credit_amount_currency.BackColor = System.Drawing.Color.Transparent
        Me.label_credit_amount_currency.Font = New System.Drawing.Font("Open Sans", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_credit_amount_currency.ForeColor = System.Drawing.Color.White
        Me.label_credit_amount_currency.Location = New System.Drawing.Point(251, -4)
        Me.label_credit_amount_currency.Name = "label_credit_amount_currency"
        Me.label_credit_amount_currency.Size = New System.Drawing.Size(44, 51)
        Me.label_credit_amount_currency.TabIndex = 36
        Me.label_credit_amount_currency.TabStop = False
        Me.label_credit_amount_currency.Text = "P"
        Me.label_credit_amount_currency.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_credit_amount_currency.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_credit_amount_currency.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_credit_amount_currency.VerticalDirection = False
        Me.label_credit_amount_currency.Visible = False
        '
        'label_change
        '
        Me.label_change.AutoEllipsis = False
        Me.label_change.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_change.ForeColor = System.Drawing.Color.White
        Me.label_change.Location = New System.Drawing.Point(14, 20)
        Me.label_change.Name = "label_change"
        Me.label_change.Size = New System.Drawing.Size(67, 22)
        Me.label_change.TabIndex = 20
        Me.label_change.TabStop = False
        Me.label_change.Text = "Change"
        Me.label_change.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_change.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_change.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_change.VerticalDirection = False
        '
        'label_change_amount
        '
        Me.label_change_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_change_amount.AutoAnimate = False
        Me.label_change_amount.AutoEllipsis = False
        Me.label_change_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_change_amount.ForeColor = System.Drawing.Color.White
        Me.label_change_amount.Location = New System.Drawing.Point(283, -9)
        Me.label_change_amount.Name = "label_change_amount"
        Me.label_change_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_change_amount.TabIndex = 19
        Me.label_change_amount.TabStop = False
        Me.label_change_amount.Text = "0.00"
        Me.label_change_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_change_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_change_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_change_amount.VerticalDirection = False
        '
        'label_change_amount_currency
        '
        Me.label_change_amount_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.label_change_amount_currency.AutoEllipsis = False
        Me.label_change_amount_currency.BackColor = System.Drawing.Color.Transparent
        Me.label_change_amount_currency.Font = New System.Drawing.Font("Open Sans", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_change_amount_currency.ForeColor = System.Drawing.Color.White
        Me.label_change_amount_currency.Location = New System.Drawing.Point(252, -2)
        Me.label_change_amount_currency.Name = "label_change_amount_currency"
        Me.label_change_amount_currency.Size = New System.Drawing.Size(44, 51)
        Me.label_change_amount_currency.TabIndex = 36
        Me.label_change_amount_currency.TabStop = False
        Me.label_change_amount_currency.Text = "P"
        Me.label_change_amount_currency.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_change_amount_currency.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_change_amount_currency.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_change_amount_currency.VerticalDirection = False
        Me.label_change_amount_currency.Visible = False
        '
        'label_tendered
        '
        Me.label_tendered.AutoEllipsis = False
        Me.label_tendered.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_tendered.ForeColor = System.Drawing.Color.White
        Me.label_tendered.Location = New System.Drawing.Point(14, 17)
        Me.label_tendered.Name = "label_tendered"
        Me.label_tendered.Size = New System.Drawing.Size(83, 22)
        Me.label_tendered.TabIndex = 20
        Me.label_tendered.TabStop = False
        Me.label_tendered.Text = "Tendered"
        Me.label_tendered.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_tendered.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_tendered.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_tendered.VerticalDirection = False
        '
        'label_tendered_amount
        '
        Me.label_tendered_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_tendered_amount.AutoAnimate = False
        Me.label_tendered_amount.AutoEllipsis = False
        Me.label_tendered_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_tendered_amount.ForeColor = System.Drawing.Color.White
        Me.label_tendered_amount.Location = New System.Drawing.Point(283, -9)
        Me.label_tendered_amount.Name = "label_tendered_amount"
        Me.label_tendered_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_tendered_amount.TabIndex = 19
        Me.label_tendered_amount.TabStop = False
        Me.label_tendered_amount.Text = "0.00"
        Me.label_tendered_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_tendered_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_tendered_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_tendered_amount.VerticalDirection = False
        '
        'label_tendered_amount_currency
        '
        Me.label_tendered_amount_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.label_tendered_amount_currency.AutoEllipsis = False
        Me.label_tendered_amount_currency.BackColor = System.Drawing.Color.Transparent
        Me.label_tendered_amount_currency.Font = New System.Drawing.Font("Open Sans", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_tendered_amount_currency.ForeColor = System.Drawing.Color.White
        Me.label_tendered_amount_currency.Location = New System.Drawing.Point(252, -4)
        Me.label_tendered_amount_currency.Name = "label_tendered_amount_currency"
        Me.label_tendered_amount_currency.Size = New System.Drawing.Size(44, 51)
        Me.label_tendered_amount_currency.TabIndex = 36
        Me.label_tendered_amount_currency.TabStop = False
        Me.label_tendered_amount_currency.Text = "P"
        Me.label_tendered_amount_currency.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_tendered_amount_currency.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_tendered_amount_currency.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_tendered_amount_currency.VerticalDirection = False
        Me.label_tendered_amount_currency.Visible = False
        '
        'label_total
        '
        Me.label_total.AutoEllipsis = False
        Me.label_total.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total.ForeColor = System.Drawing.Color.White
        Me.label_total.Location = New System.Drawing.Point(14, 14)
        Me.label_total.Name = "label_total"
        Me.label_total.Size = New System.Drawing.Size(48, 22)
        Me.label_total.TabIndex = 20
        Me.label_total.TabStop = False
        Me.label_total.Text = "Total"
        Me.label_total.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_total.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total.VerticalDirection = False
        '
        'label_total_amount
        '
        Me.label_total_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_total_amount.AutoAnimate = False
        Me.label_total_amount.AutoEllipsis = False
        Me.label_total_amount.Font = New System.Drawing.Font("Open Sans", 36.0!)
        Me.label_total_amount.ForeColor = System.Drawing.Color.White
        Me.label_total_amount.Location = New System.Drawing.Point(283, -9)
        Me.label_total_amount.Name = "label_total_amount"
        Me.label_total_amount.Size = New System.Drawing.Size(122, 65)
        Me.label_total_amount.TabIndex = 19
        Me.label_total_amount.TabStop = False
        Me.label_total_amount.Text = "0.00"
        Me.label_total_amount.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_total_amount.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_total_amount.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_amount.VerticalDirection = False
        '
        'label_total_amount_currency
        '
        Me.label_total_amount_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.label_total_amount_currency.AutoEllipsis = False
        Me.label_total_amount_currency.BackColor = System.Drawing.Color.Transparent
        Me.label_total_amount_currency.Font = New System.Drawing.Font("Open Sans", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_total_amount_currency.ForeColor = System.Drawing.Color.White
        Me.label_total_amount_currency.Location = New System.Drawing.Point(251, -6)
        Me.label_total_amount_currency.Name = "label_total_amount_currency"
        Me.label_total_amount_currency.Size = New System.Drawing.Size(44, 51)
        Me.label_total_amount_currency.TabIndex = 35
        Me.label_total_amount_currency.TabStop = False
        Me.label_total_amount_currency.Text = "P"
        Me.label_total_amount_currency.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_total_amount_currency.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_total_amount_currency.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_total_amount_currency.VerticalDirection = False
        Me.label_total_amount_currency.Visible = False
        '
        'label_product_name
        '
        Me.label_product_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_product_name.AutoSize = False
        Me.label_product_name.BackColor = System.Drawing.Color.Transparent
        Me.label_product_name.Font = New System.Drawing.Font("Open Sans Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_product_name.ForeColor = System.Drawing.Color.White
        Me.label_product_name.Location = New System.Drawing.Point(15, 56)
        Me.label_product_name.Name = "label_product_name"
        Me.label_product_name.Size = New System.Drawing.Size(848, 28)
        Me.label_product_name.TabIndex = 36
        Me.label_product_name.TabStop = False
        Me.label_product_name.Text = "--"
        Me.label_product_name.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_product_name.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_product_name.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_product_name.VerticalDirection = False
        '
        'label_product_price
        '
        Me.label_product_price.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_product_price.AutoSize = False
        Me.label_product_price.BackColor = System.Drawing.Color.Transparent
        Me.label_product_price.Font = New System.Drawing.Font("Open Sans", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_product_price.ForeColor = System.Drawing.Color.White
        Me.label_product_price.Location = New System.Drawing.Point(37, 71)
        Me.label_product_price.Name = "label_product_price"
        Me.label_product_price.Size = New System.Drawing.Size(818, 65)
        Me.label_product_price.TabIndex = 35
        Me.label_product_price.TabStop = False
        Me.label_product_price.Text = "0.00"
        Me.label_product_price.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_product_price.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_product_price.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_product_price.VerticalDirection = False
        '
        'label_currency
        '
        Me.label_currency.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.label_currency.AutoEllipsis = False
        Me.label_currency.BackColor = System.Drawing.Color.Transparent
        Me.label_currency.Font = New System.Drawing.Font("Open Sans", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_currency.ForeColor = System.Drawing.Color.FromArgb(CType(CType(174, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(159, Byte), Integer))
        Me.label_currency.Location = New System.Drawing.Point(10, 74)
        Me.label_currency.Name = "label_currency"
        Me.label_currency.Size = New System.Drawing.Size(44, 51)
        Me.label_currency.TabIndex = 34
        Me.label_currency.TabStop = False
        Me.label_currency.Text = "P"
        Me.label_currency.TextAlign = DevaReePharmacy.TextAlignment.Right
        Me.label_currency.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_currency.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_currency.VerticalDirection = False
        '
        'label_console
        '
        Me.label_console.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_console.AutoAnimate = False
        Me.label_console.AutoSize = False
        Me.label_console.BackColor = System.Drawing.Color.Transparent
        Me.label_console.Font = New System.Drawing.Font("Open Sans Light", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_console.ForeColor = System.Drawing.Color.White
        Me.label_console.Location = New System.Drawing.Point(13, 6)
        Me.label_console.Name = "label_console"
        Me.label_console.Size = New System.Drawing.Size(835, 47)
        Me.label_console.TabIndex = 36
        Me.label_console.TabStop = False
        Me.label_console.Text = "_"
        Me.label_console.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_console.TextRender = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit
        Me.label_console.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_console.VerticalDirection = False
        '
        'form_order
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1268, 765)
        Me.Controls.Add(Me.panel_header)
        Me.Controls.Add(Me.panel_controls)
        Me.Controls.Add(Me.panel_item_detail)
        Me.Controls.Add(Me.list_products)
        Me.KeyPreview = True
        Me.Name = "form_order"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Order"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_amounts.ResumeLayout(False)
        Me.panel_credit.ResumeLayout(False)
        Me.panel_credit.PerformLayout()
        Me.panel_change_amount.ResumeLayout(False)
        Me.panel_change_amount.PerformLayout()
        Me.panel_tendered_amount.ResumeLayout(False)
        Me.panel_tendered_amount.PerformLayout()
        Me.panel_total_amount.ResumeLayout(False)
        Me.panel_total_amount.PerformLayout()
        Me.panel_controls.ResumeLayout(False)
        Me.panel_controls.PerformLayout()
        CType(Me.image_sold, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel_main_controls.ResumeLayout(False)
        Me.panel_cart_controls.ResumeLayout(False)
        Me.panel_item_detail.ResumeLayout(False)
        Me.panel_item_detail.PerformLayout()
        Me.panel_header.ResumeLayout(False)
        Me.panel_header.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents list_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents label_cart_count As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_amounts As System.Windows.Forms.Panel
    Friend WithEvents panel_total_amount As System.Windows.Forms.Panel
    Friend WithEvents label_total As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_total_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_change_amount As System.Windows.Forms.Panel
    Friend WithEvents label_change As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_change_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_tendered_amount As System.Windows.Forms.Panel
    Friend WithEvents label_tendered As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_tendered_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents panel_controls As System.Windows.Forms.Panel
    Friend WithEvents panel_header As System.Windows.Forms.Panel
    Friend WithEvents label_reference_no As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents button_browse As System.Windows.Forms.Button
    Friend WithEvents panel_cart_controls As System.Windows.Forms.Panel
    Friend WithEvents button_set_quantity As System.Windows.Forms.Button
    Friend WithEvents button_set_price As System.Windows.Forms.Button
    Friend WithEvents button_void_item As System.Windows.Forms.Button
    Friend WithEvents button_cancel_order As System.Windows.Forms.Button
    Friend WithEvents button_place_order As System.Windows.Forms.Button
    Friend WithEvents button_new_transaction As System.Windows.Forms.Button
    Friend WithEvents panel_item_detail As System.Windows.Forms.Panel
    Friend WithEvents label_product_price As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_currency As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_product_name As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_console As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents timer_console As System.Windows.Forms.Timer
    Friend WithEvents label_time As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents timer_time As System.Windows.Forms.Timer
    Friend WithEvents button_set_reference_no As System.Windows.Forms.Button
    Friend WithEvents button_change_unit As System.Windows.Forms.Button
    Friend WithEvents button_use_admin As System.Windows.Forms.Button
    Friend WithEvents imagelist_small_icons As System.Windows.Forms.ImageList
    Friend WithEvents panel_main_controls As System.Windows.Forms.Panel
    Friend WithEvents label_order_type As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents button_price_inquiry As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents label_customer_name As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents button_set_credit As System.Windows.Forms.Button
    Friend WithEvents label_order_mode As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents button_set_customer As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents button_new_dispatch As System.Windows.Forms.Button
    Friend WithEvents panel_credit As System.Windows.Forms.Panel
    Friend WithEvents label_credit As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_credit_amount As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents image_sold As System.Windows.Forms.PictureBox
    Friend WithEvents label_credit_amount_currency As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_tendered_amount_currency As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_total_amount_currency As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_change_amount_currency As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents button_reprint_dr As System.Windows.Forms.Button
End Class
