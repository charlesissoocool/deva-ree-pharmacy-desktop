﻿Public Class form_about

    Private Sub form_about_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        label_revision.Text = "Version " & Application.ProductVersion
        label_customer.Text = "Customized for " & SYS_CUSTOMER_NAME
        label_disclaimer.Text = "This system is intended only for " & SYS_CUSTOMER_NAME & ". If you are not the intended company/client/individual you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this system is strictly prohibited."
        link_icons8.LinkColor = module_colors.color_blue
        link_icons8.ActiveLinkColor = module_colors.color_red

    End Sub

    Private Sub link_icons8_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles link_icons8.LinkClicked

        Process.Start("www.icons8.com")

    End Sub
End Class