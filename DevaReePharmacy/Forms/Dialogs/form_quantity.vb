﻿Public Class form_quantity

    Private _checked As Boolean = False
    Private _increment As Double = 1
    Private _has_decimal As Boolean = True
    Private _has_sign As Boolean = False
    Private _has_percentage As Boolean = False

    Public Function OpenQuantityDialog(ByVal parentForm As Form, Optional ByVal title As String = "Quantity", Optional ByVal defaultValue As Double = 0, Optional ByVal increment As Double = 1, Optional ByVal has_decimal As Boolean = True, Optional ByVal has_sign As Boolean = False, Optional ByVal has_percentage As Boolean = False) As String

        Me.Text = title
        _increment = increment
        _has_decimal = has_decimal
        _has_sign = has_sign
        _has_percentage = has_percentage
        text_value.Text = If(has_decimal, Format(defaultValue, "0.00"), Val(defaultValue))
        Me.ShowDialog(parentForm)
        OpenQuantityDialog = If(_checked, text_value.Text, Nothing)

    End Function

    Private Sub form_quantity_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If

    End Sub

    Private Sub AdjustInterface()

        text_value.Focus()

    End Sub

    Private Sub text_value_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_value.GotFocus

        text_value.SelectAll()

    End Sub

    Private Sub text_value_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles text_value.KeyDown

        If IsNumeric(text_value.Text) Then
            If e.KeyCode = Keys.Up Then text_value.Text = Val(text_value.Text) + _increment
            If Val(text_value.Text) <= 0 Then Exit Sub
            If e.KeyCode = Keys.Down Then text_value.Text = Val(text_value.Text) - _increment
        End If

    End Sub

    Private Sub text_value_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_value.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) And Not Trim(text_value.Text) = "" Then
            e.Handled = True
            _checked = True
            Me.Close()
        End If

    End Sub

    Private Sub text_value_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_value.TextChanged

        ValidateNumericTextBox(text_value, _has_decimal, _has_sign, _has_percentage)

    End Sub

    Private Sub vscroll_value_Scroll(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles vscroll_value.Scroll

        If e.OldValue < e.NewValue Then
            If Val(text_value.Text) <= 0 Then Exit Sub
            text_value.Text = Val(text_value.Text) - _increment
        ElseIf e.OldValue > e.NewValue Then
            text_value.Text = Val(text_value.Text) + _increment
        End If

    End Sub
End Class