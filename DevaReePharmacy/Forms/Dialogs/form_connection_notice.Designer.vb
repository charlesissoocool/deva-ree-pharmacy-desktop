﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_connection_notice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_connection_notice))
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.text_server_pass = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.text_server_user = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.text_server_name = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.text_server_host = New System.Windows.Forms.TextBox()
        Me.button_quit = New System.Windows.Forms.Button()
        Me.button_reconnect = New System.Windows.Forms.Button()
        Me.button_connect = New System.Windows.Forms.Button()
        Me.timer_recheck = New System.Windows.Forms.Timer(Me.components)
        Me.prompt_message = New DevaReePharmacy.PromptLabel()
        Me.group_product_details.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_product_details
        '
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.text_server_pass)
        Me.group_product_details.Controls.Add(Me.Label5)
        Me.group_product_details.Controls.Add(Me.text_server_user)
        Me.group_product_details.Controls.Add(Me.Label4)
        Me.group_product_details.Controls.Add(Me.text_server_name)
        Me.group_product_details.Controls.Add(Me.Label3)
        Me.group_product_details.Controls.Add(Me.text_server_host)
        Me.group_product_details.Controls.Add(Me.button_quit)
        Me.group_product_details.Controls.Add(Me.button_reconnect)
        Me.group_product_details.Controls.Add(Me.button_connect)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(12, 106)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(420, 235)
        Me.group_product_details.TabIndex = 8
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Server Information"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label6.Location = New System.Drawing.Point(12, 145)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(66, 17)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Password"
        '
        'text_server_pass
        '
        Me.text_server_pass.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_server_pass.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_server_pass.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_server_pass.ForeColor = System.Drawing.Color.Black
        Me.text_server_pass.Location = New System.Drawing.Point(79, 139)
        Me.text_server_pass.Name = "text_server_pass"
        Me.text_server_pass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.text_server_pass.Size = New System.Drawing.Size(325, 29)
        Me.text_server_pass.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(12, 110)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 17)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "User"
        '
        'text_server_user
        '
        Me.text_server_user.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_server_user.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_server_user.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_server_user.ForeColor = System.Drawing.Color.Black
        Me.text_server_user.Location = New System.Drawing.Point(79, 104)
        Me.text_server_user.Name = "text_server_user"
        Me.text_server_user.Size = New System.Drawing.Size(325, 29)
        Me.text_server_user.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(12, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 17)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Name"
        '
        'text_server_name
        '
        Me.text_server_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_server_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_server_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_server_name.ForeColor = System.Drawing.Color.Black
        Me.text_server_name.Location = New System.Drawing.Point(79, 69)
        Me.text_server_name.Name = "text_server_name"
        Me.text_server_name.Size = New System.Drawing.Size(325, 29)
        Me.text_server_name.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(12, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 17)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Host"
        '
        'text_server_host
        '
        Me.text_server_host.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_server_host.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_server_host.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_server_host.ForeColor = System.Drawing.Color.Black
        Me.text_server_host.Location = New System.Drawing.Point(79, 34)
        Me.text_server_host.Name = "text_server_host"
        Me.text_server_host.Size = New System.Drawing.Size(325, 29)
        Me.text_server_host.TabIndex = 1
        '
        'button_quit
        '
        Me.button_quit.BackColor = System.Drawing.Color.Transparent
        Me.button_quit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_quit.FlatAppearance.BorderSize = 0
        Me.button_quit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_quit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_quit.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_quit.Image = CType(resources.GetObject("button_quit.Image"), System.Drawing.Image)
        Me.button_quit.Location = New System.Drawing.Point(260, 167)
        Me.button_quit.Name = "button_quit"
        Me.button_quit.Size = New System.Drawing.Size(79, 65)
        Me.button_quit.TabIndex = 17
        Me.button_quit.Text = "Quit"
        Me.button_quit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_quit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_quit.UseVisualStyleBackColor = True
        '
        'button_reconnect
        '
        Me.button_reconnect.BackColor = System.Drawing.Color.Transparent
        Me.button_reconnect.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_reconnect.FlatAppearance.BorderSize = 0
        Me.button_reconnect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_reconnect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_reconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_reconnect.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_reconnect.Image = CType(resources.GetObject("button_reconnect.Image"), System.Drawing.Image)
        Me.button_reconnect.Location = New System.Drawing.Point(175, 167)
        Me.button_reconnect.Name = "button_reconnect"
        Me.button_reconnect.Size = New System.Drawing.Size(79, 65)
        Me.button_reconnect.TabIndex = 16
        Me.button_reconnect.Text = "Reconnect"
        Me.button_reconnect.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_reconnect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_reconnect.UseVisualStyleBackColor = True
        '
        'button_connect
        '
        Me.button_connect.BackColor = System.Drawing.Color.Transparent
        Me.button_connect.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_connect.FlatAppearance.BorderSize = 0
        Me.button_connect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_connect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_connect.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_connect.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_connect.Image = CType(resources.GetObject("button_connect.Image"), System.Drawing.Image)
        Me.button_connect.Location = New System.Drawing.Point(86, 167)
        Me.button_connect.Name = "button_connect"
        Me.button_connect.Size = New System.Drawing.Size(83, 65)
        Me.button_connect.TabIndex = 5
        Me.button_connect.Text = "Connect"
        Me.button_connect.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_connect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_connect.UseVisualStyleBackColor = True
        '
        'timer_recheck
        '
        Me.timer_recheck.Interval = 5000
        '
        'prompt_message
        '
        Me.prompt_message.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(196, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.prompt_message.Location = New System.Drawing.Point(12, 12)
        Me.prompt_message.Message = "A problem occurred with the connection to the database server. You can choose to " & _
            "reconnect with the server or specify a new one below."
        Me.prompt_message.Mode = DevaReePharmacy.PromptLabel.PromptLabelMode.Warning
        Me.prompt_message.Name = "prompt_message"
        Me.prompt_message.Size = New System.Drawing.Size(420, 88)
        Me.prompt_message.TabIndex = 0
        '
        'form_connection_notice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(447, 350)
        Me.ControlBox = False
        Me.Controls.Add(Me.group_product_details)
        Me.Controls.Add(Me.prompt_message)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_connection_notice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Connection Problem"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents prompt_message As DevaReePharmacy.PromptLabel
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents text_server_user As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents text_server_name As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents text_server_host As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents text_server_pass As System.Windows.Forms.TextBox
    Friend WithEvents button_connect As System.Windows.Forms.Button
    Friend WithEvents timer_recheck As System.Windows.Forms.Timer
    Friend WithEvents button_reconnect As System.Windows.Forms.Button
    Friend WithEvents button_quit As System.Windows.Forms.Button
End Class
