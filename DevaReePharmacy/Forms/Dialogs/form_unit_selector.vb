﻿Public Class form_unit_selector

    Dim _checked As Boolean = False

    Private Sub AdjustInterface() Handles Me.Load

        radio_pack.Text = SYS_PACK_ALIAS
        radio_pad.Text = SYS_PAD_ALIAS
        Dim max_right As Integer = 0
        max_right = If(max_right < radio_pack.Right, radio_pack.Right, max_right)
        max_right = If(max_right < radio_pad.Right, radio_pad.Right, max_right)
        max_right = If(max_right < radio_unit.Right, radio_unit.Right, max_right)
        Me.Width = max_right + radio_pack.Left + (Me.Width - Me.ClientSize.Width)

    End Sub

    Public Function OpenUnitDialog(ByVal parent_form As Form, Optional ByVal default_unit As ItemUnit = ItemUnit.Pack, Optional ByVal piece_unit As String = "pcs", Optional ByVal enable_pack As Boolean = True, Optional ByVal enable_pad As Boolean = True) As ItemUnit

        radio_unit.Text = piece_unit
        radio_pack.Enabled = enable_pack
        radio_pad.Enabled = enable_pad
        Select Case default_unit
            Case ItemUnit.Pack
                radio_pack.Checked = True
            Case ItemUnit.Pad
                radio_pad.Checked = True
            Case ItemUnit.Unit
                radio_unit.Checked = True
        End Select
        Me.ShowDialog(parent_form)
        If _checked Then
            OpenUnitDialog = If(radio_pack.Checked, ItemUnit.Pack, If(radio_pad.Checked, ItemUnit.Pad, ItemUnit.Unit))
        Else
            OpenUnitDialog = ItemUnit.NoUnit
        End If

    End Function

    Private Sub form_unit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            _checked = True
            Me.Close()
        ElseIf e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            Me.Close()
        End If

    End Sub

    Private Sub radio_pack_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radio_pack.CheckedChanged

    End Sub

    Private Sub radio_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles radio_pack.GotFocus, radio_pad.GotFocus, radio_unit.GotFocus

        sender.Checked = True

    End Sub
End Class