﻿Public Class form_agent_selector

    Private sql As New class_db_user
    Private _checked As Boolean = False
    Private _agent_details As User

    Private Sub form_agent_selector_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then button_cancel.PerformClick()

    End Sub

    Private Sub AdjustInterface() Handles Me.Load
        InvalidateTextBox(text_agent_name)
        LoadAgentList()
    End Sub

    Public Function OpenAgentSelector(ByRef parent_form As Form) As User

        _agent_details = New User
        Me.ShowDialog(parent_form)
        OpenAgentSelector = If(_checked, _agent_details, Nothing)

    End Function

    Private Sub LoadAgentList()

        Dim results = sql.GetAgentList
        Dim agent_list As New AutoCompleteStringCollection
        If Not IsNothing(results) Then
            For Each result In results
                agent_list.Add(result("first_name") & Space(1) & result("last_name"))
            Next
        End If
        text_agent_name.AutoCompleteCustomSource = agent_list

    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        Dim agent_id As Integer = sql.GetAgentIDByFullName(text_agent_name.Text)
        If agent_id > 0 Then
            _agent_details = New User(agent_id)
            _checked = True
            Me.Close()
        Else
            MsgBox("Some fields are invalid.", MsgBoxStyle.Exclamation)
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub

    Private Sub text_agent_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_agent_name.TextChanged

        Dim agent_id = sql.GetAgentIDByFullName(text_agent_name.Text)
        If agent_id > 0 Then
            ValidateTextBox(sender)
        Else
            InvalidateTextBox(sender)
        End If

    End Sub

    Private Sub button_add_agent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_agent.Click

        Dim agent_editor As New form_user_editor
        Dim agent_details = agent_editor.OpenUserEditor(Me, False, False, False, False, True, False)
        If Not IsNothing(agent_details) Then
            If agent_details.Persist Then
                LoadAgentList()
                text_agent_name.Text = agent_details.FullName
            Else
                MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
            End If
        End If
    End Sub
End Class