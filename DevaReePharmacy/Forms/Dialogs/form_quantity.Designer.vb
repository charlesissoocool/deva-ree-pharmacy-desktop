﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_quantity
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.text_value = New System.Windows.Forms.TextBox()
        Me.vscroll_value = New System.Windows.Forms.VScrollBar()
        Me.SuspendLayout()
        '
        'text_value
        '
        Me.text_value.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_value.ForeColor = System.Drawing.Color.Black
        Me.text_value.Location = New System.Drawing.Point(12, 12)
        Me.text_value.MaxLength = 7
        Me.text_value.Name = "text_value"
        Me.text_value.Size = New System.Drawing.Size(102, 29)
        Me.text_value.TabIndex = 4
        '
        'vscroll_value
        '
        Me.vscroll_value.LargeChange = 1
        Me.vscroll_value.Location = New System.Drawing.Point(117, 9)
        Me.vscroll_value.Maximum = 1000000
        Me.vscroll_value.Minimum = -1000000
        Me.vscroll_value.Name = "vscroll_value"
        Me.vscroll_value.Size = New System.Drawing.Size(25, 35)
        Me.vscroll_value.TabIndex = 5
        '
        'form_quantity
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(157, 55)
        Me.Controls.Add(Me.vscroll_value)
        Me.Controls.Add(Me.text_value)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_quantity"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Quantity"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents text_value As System.Windows.Forms.TextBox
    Friend WithEvents vscroll_value As System.Windows.Forms.VScrollBar
End Class
