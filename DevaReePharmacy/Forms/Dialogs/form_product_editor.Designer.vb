﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_product_editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_product_editor))
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.text_product_category = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.label_unit_max_char = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.text_product_code = New System.Windows.Forms.TextBox()
        Me.text_product_description = New System.Windows.Forms.TextBox()
        Me.text_product_brand = New System.Windows.Forms.TextBox()
        Me.text_product_unit = New System.Windows.Forms.TextBox()
        Me.text_product_name = New System.Windows.Forms.TextBox()
        Me.label_packing_size = New System.Windows.Forms.Label()
        Me.label_tip_packing_size = New System.Windows.Forms.Label()
        Me.label_pads_per_pack = New System.Windows.Forms.Label()
        Me.text_pads_per_pack = New System.Windows.Forms.TextBox()
        Me.text_packing_size = New System.Windows.Forms.TextBox()
        Me.group_product_prices = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.text_distribution_price = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.text_wholesale_price = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.text_retail_price = New System.Windows.Forms.TextBox()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_save = New System.Windows.Forms.Button()
        Me.group_packing_information = New System.Windows.Forms.GroupBox()
        Me.label_tip_sold_in_pads = New System.Windows.Forms.Label()
        Me.checkbox_no_pad = New System.Windows.Forms.CheckBox()
        Me.label_tip_sold_in_packs = New System.Windows.Forms.Label()
        Me.checkbox_no_pack = New System.Windows.Forms.CheckBox()
        Me.label_currency_retail = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_currency_wholesale = New DevaReePharmacy.AntiAliasedLabel()
        Me.label_currency_distribution = New DevaReePharmacy.AntiAliasedLabel()
        Me.group_product_details.SuspendLayout()
        Me.group_product_prices.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.group_packing_information.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_product_details
        '
        Me.group_product_details.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_product_details.Controls.Add(Me.Label10)
        Me.group_product_details.Controls.Add(Me.Label9)
        Me.group_product_details.Controls.Add(Me.text_product_category)
        Me.group_product_details.Controls.Add(Me.Label12)
        Me.group_product_details.Controls.Add(Me.label_unit_max_char)
        Me.group_product_details.Controls.Add(Me.Label2)
        Me.group_product_details.Controls.Add(Me.Label8)
        Me.group_product_details.Controls.Add(Me.Label7)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.text_product_code)
        Me.group_product_details.Controls.Add(Me.text_product_description)
        Me.group_product_details.Controls.Add(Me.text_product_brand)
        Me.group_product_details.Controls.Add(Me.text_product_unit)
        Me.group_product_details.Controls.Add(Me.text_product_name)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(10, 2)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(443, 242)
        Me.group_product_details.TabIndex = 1
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Product Information"
        '
        'Label10
        '
        Me.Label10.AutoEllipsis = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(7, 55)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(127, 27)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "(Leave blank to unclassify)"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 38)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(116, 17)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Product Category"
        '
        'text_product_category
        '
        Me.text_product_category.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_product_category.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_product_category.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_product_category.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_category.ForeColor = System.Drawing.Color.Black
        Me.text_product_category.Location = New System.Drawing.Point(141, 38)
        Me.text_product_category.MaxLength = 20
        Me.text_product_category.Name = "text_product_category"
        Me.text_product_category.Size = New System.Drawing.Size(292, 29)
        Me.text_product_category.TabIndex = 1
        '
        'Label12
        '
        Me.Label12.AutoEllipsis = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(8, 172)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(127, 27)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "(Classification of product from other similar ones)"
        '
        'label_unit_max_char
        '
        Me.label_unit_max_char.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_unit_max_char.AutoSize = True
        Me.label_unit_max_char.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_unit_max_char.ForeColor = System.Drawing.Color.Red
        Me.label_unit_max_char.Location = New System.Drawing.Point(299, 133)
        Me.label_unit_max_char.Name = "label_unit_max_char"
        Me.label_unit_max_char.Size = New System.Drawing.Size(39, 13)
        Me.label_unit_max_char.TabIndex = 15
        Me.label_unit_max_char.Text = "Max: 5"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(7, 203)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 17)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Product Code"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, 155)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 17)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Product Description"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(301, 117)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 17)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Unit"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 123)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 17)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Product Brand"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 82)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Product Name"
        '
        'text_product_code
        '
        Me.text_product_code.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_product_code.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_code.ForeColor = System.Drawing.Color.Black
        Me.text_product_code.Location = New System.Drawing.Point(141, 200)
        Me.text_product_code.MaxLength = 50
        Me.text_product_code.Name = "text_product_code"
        Me.text_product_code.Size = New System.Drawing.Size(292, 29)
        Me.text_product_code.TabIndex = 6
        '
        'text_product_description
        '
        Me.text_product_description.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_product_description.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_description.ForeColor = System.Drawing.Color.Black
        Me.text_product_description.Location = New System.Drawing.Point(141, 159)
        Me.text_product_description.MaxLength = 25
        Me.text_product_description.Name = "text_product_description"
        Me.text_product_description.Size = New System.Drawing.Size(292, 29)
        Me.text_product_description.TabIndex = 5
        '
        'text_product_brand
        '
        Me.text_product_brand.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_product_brand.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_product_brand.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_product_brand.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_brand.ForeColor = System.Drawing.Color.Black
        Me.text_product_brand.Location = New System.Drawing.Point(141, 117)
        Me.text_product_brand.MaxLength = 50
        Me.text_product_brand.Name = "text_product_brand"
        Me.text_product_brand.Size = New System.Drawing.Size(154, 29)
        Me.text_product_brand.TabIndex = 3
        '
        'text_product_unit
        '
        Me.text_product_unit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_product_unit.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_product_unit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_product_unit.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_unit.ForeColor = System.Drawing.Color.Black
        Me.text_product_unit.Location = New System.Drawing.Point(340, 117)
        Me.text_product_unit.MaxLength = 5
        Me.text_product_unit.Name = "text_product_unit"
        Me.text_product_unit.Size = New System.Drawing.Size(93, 29)
        Me.text_product_unit.TabIndex = 4
        '
        'text_product_name
        '
        Me.text_product_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_product_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_name.ForeColor = System.Drawing.Color.Black
        Me.text_product_name.Location = New System.Drawing.Point(141, 77)
        Me.text_product_name.MaxLength = 50
        Me.text_product_name.Name = "text_product_name"
        Me.text_product_name.Size = New System.Drawing.Size(292, 29)
        Me.text_product_name.TabIndex = 2
        '
        'label_packing_size
        '
        Me.label_packing_size.AutoSize = True
        Me.label_packing_size.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_packing_size.Location = New System.Drawing.Point(7, 85)
        Me.label_packing_size.Name = "label_packing_size"
        Me.label_packing_size.Size = New System.Drawing.Size(82, 17)
        Me.label_packing_size.TabIndex = 17
        Me.label_packing_size.Text = "Packing Size"
        '
        'label_tip_packing_size
        '
        Me.label_tip_packing_size.AutoSize = True
        Me.label_tip_packing_size.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_tip_packing_size.ForeColor = System.Drawing.Color.Black
        Me.label_tip_packing_size.Location = New System.Drawing.Point(8, 101)
        Me.label_tip_packing_size.Name = "label_tip_packing_size"
        Me.label_tip_packing_size.Size = New System.Drawing.Size(109, 13)
        Me.label_tip_packing_size.TabIndex = 20
        Me.label_tip_packing_size.Text = "(No. of units per pack)"
        '
        'label_pads_per_pack
        '
        Me.label_pads_per_pack.AutoSize = True
        Me.label_pads_per_pack.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_pads_per_pack.Location = New System.Drawing.Point(225, 89)
        Me.label_pads_per_pack.Name = "label_pads_per_pack"
        Me.label_pads_per_pack.Size = New System.Drawing.Size(93, 17)
        Me.label_pads_per_pack.TabIndex = 19
        Me.label_pads_per_pack.Text = "Pads per Pack"
        '
        'text_pads_per_pack
        '
        Me.text_pads_per_pack.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_pads_per_pack.ForeColor = System.Drawing.Color.Black
        Me.text_pads_per_pack.Location = New System.Drawing.Point(324, 83)
        Me.text_pads_per_pack.MaxLength = 50
        Me.text_pads_per_pack.Name = "text_pads_per_pack"
        Me.text_pads_per_pack.Size = New System.Drawing.Size(109, 29)
        Me.text_pads_per_pack.TabIndex = 10
        '
        'text_packing_size
        '
        Me.text_packing_size.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_packing_size.ForeColor = System.Drawing.Color.Black
        Me.text_packing_size.Location = New System.Drawing.Point(125, 85)
        Me.text_packing_size.MaxLength = 50
        Me.text_packing_size.Name = "text_packing_size"
        Me.text_packing_size.Size = New System.Drawing.Size(94, 29)
        Me.text_packing_size.TabIndex = 9
        '
        'group_product_prices
        '
        Me.group_product_prices.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_product_prices.Controls.Add(Me.Label5)
        Me.group_product_prices.Controls.Add(Me.text_distribution_price)
        Me.group_product_prices.Controls.Add(Me.Label4)
        Me.group_product_prices.Controls.Add(Me.text_wholesale_price)
        Me.group_product_prices.Controls.Add(Me.Label3)
        Me.group_product_prices.Controls.Add(Me.text_retail_price)
        Me.group_product_prices.Controls.Add(Me.label_currency_retail)
        Me.group_product_prices.Controls.Add(Me.label_currency_wholesale)
        Me.group_product_prices.Controls.Add(Me.label_currency_distribution)
        Me.group_product_prices.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_prices.ForeColor = System.Drawing.Color.Black
        Me.group_product_prices.Location = New System.Drawing.Point(10, 369)
        Me.group_product_prices.Name = "group_product_prices"
        Me.group_product_prices.Size = New System.Drawing.Size(443, 151)
        Me.group_product_prices.TabIndex = 2
        Me.group_product_prices.TabStop = False
        Me.group_product_prices.Text = "Pack Prices"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(7, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 17)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Distribution Price"
        '
        'text_distribution_price
        '
        Me.text_distribution_price.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_distribution_price.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_distribution_price.ForeColor = System.Drawing.Color.Black
        Me.text_distribution_price.Location = New System.Drawing.Point(141, 109)
        Me.text_distribution_price.MaxLength = 50
        Me.text_distribution_price.Name = "text_distribution_price"
        Me.text_distribution_price.Size = New System.Drawing.Size(292, 29)
        Me.text_distribution_price.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 76)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(103, 17)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Wholesale Price"
        '
        'text_wholesale_price
        '
        Me.text_wholesale_price.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_wholesale_price.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_wholesale_price.ForeColor = System.Drawing.Color.Black
        Me.text_wholesale_price.Location = New System.Drawing.Point(141, 73)
        Me.text_wholesale_price.MaxLength = 50
        Me.text_wholesale_price.Name = "text_wholesale_price"
        Me.text_wholesale_price.Size = New System.Drawing.Size(292, 29)
        Me.text_wholesale_price.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 17)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Retail Price"
        '
        'text_retail_price
        '
        Me.text_retail_price.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_retail_price.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_retail_price.ForeColor = System.Drawing.Color.Black
        Me.text_retail_price.Location = New System.Drawing.Point(141, 38)
        Me.text_retail_price.MaxLength = 50
        Me.text_retail_price.Name = "text_retail_price"
        Me.text_retail_price.Size = New System.Drawing.Size(292, 29)
        Me.text_retail_price.TabIndex = 11
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_save)
        Me.panel_buttons.Location = New System.Drawing.Point(10, 526)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(115, 74)
        Me.panel_buttons.TabIndex = 9
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 15
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_save
        '
        Me.button_save.BackColor = System.Drawing.Color.Transparent
        Me.button_save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_save.FlatAppearance.BorderSize = 0
        Me.button_save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_save.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_save.Image = CType(resources.GetObject("button_save.Image"), System.Drawing.Image)
        Me.button_save.Location = New System.Drawing.Point(1, 2)
        Me.button_save.Name = "button_save"
        Me.button_save.Size = New System.Drawing.Size(53, 65)
        Me.button_save.TabIndex = 14
        Me.button_save.Text = "Save"
        Me.button_save.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_save.UseVisualStyleBackColor = True
        '
        'group_packing_information
        '
        Me.group_packing_information.Controls.Add(Me.label_tip_sold_in_pads)
        Me.group_packing_information.Controls.Add(Me.checkbox_no_pad)
        Me.group_packing_information.Controls.Add(Me.label_tip_sold_in_packs)
        Me.group_packing_information.Controls.Add(Me.checkbox_no_pack)
        Me.group_packing_information.Controls.Add(Me.label_packing_size)
        Me.group_packing_information.Controls.Add(Me.text_packing_size)
        Me.group_packing_information.Controls.Add(Me.label_tip_packing_size)
        Me.group_packing_information.Controls.Add(Me.text_pads_per_pack)
        Me.group_packing_information.Controls.Add(Me.label_pads_per_pack)
        Me.group_packing_information.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_packing_information.ForeColor = System.Drawing.Color.Black
        Me.group_packing_information.Location = New System.Drawing.Point(10, 243)
        Me.group_packing_information.Name = "group_packing_information"
        Me.group_packing_information.Size = New System.Drawing.Size(443, 129)
        Me.group_packing_information.TabIndex = 10
        Me.group_packing_information.TabStop = False
        Me.group_packing_information.Text = "Packing Information"
        '
        'label_tip_sold_in_pads
        '
        Me.label_tip_sold_in_pads.AutoSize = True
        Me.label_tip_sold_in_pads.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_tip_sold_in_pads.ForeColor = System.Drawing.Color.Black
        Me.label_tip_sold_in_pads.Location = New System.Drawing.Point(224, 60)
        Me.label_tip_sold_in_pads.Name = "label_tip_sold_in_pads"
        Me.label_tip_sold_in_pads.Size = New System.Drawing.Size(165, 13)
        Me.label_tip_sold_in_pads.TabIndex = 24
        Me.label_tip_sold_in_pads.Text = "(Check if product isn't sold in pads)"
        '
        'checkbox_no_pad
        '
        Me.checkbox_no_pad.AutoSize = True
        Me.checkbox_no_pad.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.checkbox_no_pad.Location = New System.Drawing.Point(227, 39)
        Me.checkbox_no_pad.Name = "checkbox_no_pad"
        Me.checkbox_no_pad.Size = New System.Drawing.Size(127, 21)
        Me.checkbox_no_pad.TabIndex = 8
        Me.checkbox_no_pad.Text = "Not sold in pads"
        Me.checkbox_no_pad.UseVisualStyleBackColor = True
        '
        'label_tip_sold_in_packs
        '
        Me.label_tip_sold_in_packs.AutoSize = True
        Me.label_tip_sold_in_packs.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_tip_sold_in_packs.ForeColor = System.Drawing.Color.Black
        Me.label_tip_sold_in_packs.Location = New System.Drawing.Point(8, 60)
        Me.label_tip_sold_in_packs.Name = "label_tip_sold_in_packs"
        Me.label_tip_sold_in_packs.Size = New System.Drawing.Size(169, 13)
        Me.label_tip_sold_in_packs.TabIndex = 22
        Me.label_tip_sold_in_packs.Text = "(Check if product isn't sold in packs)"
        '
        'checkbox_no_pack
        '
        Me.checkbox_no_pack.AutoSize = True
        Me.checkbox_no_pack.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.checkbox_no_pack.Location = New System.Drawing.Point(11, 39)
        Me.checkbox_no_pack.Name = "checkbox_no_pack"
        Me.checkbox_no_pack.Size = New System.Drawing.Size(132, 21)
        Me.checkbox_no_pack.TabIndex = 7
        Me.checkbox_no_pack.Text = "Not sold in packs"
        Me.checkbox_no_pack.UseVisualStyleBackColor = True
        '
        'label_currency_retail
        '
        Me.label_currency_retail.AutoEllipsis = False
        Me.label_currency_retail.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold)
        Me.label_currency_retail.ForeColor = System.Drawing.Color.Black
        Me.label_currency_retail.Location = New System.Drawing.Point(120, 36)
        Me.label_currency_retail.Name = "label_currency_retail"
        Me.label_currency_retail.Size = New System.Drawing.Size(26, 30)
        Me.label_currency_retail.TabIndex = 24
        Me.label_currency_retail.Text = "P"
        Me.label_currency_retail.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_currency_retail.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_currency_retail.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_currency_retail.VerticalDirection = False
        '
        'label_currency_wholesale
        '
        Me.label_currency_wholesale.AutoEllipsis = False
        Me.label_currency_wholesale.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold)
        Me.label_currency_wholesale.ForeColor = System.Drawing.Color.Black
        Me.label_currency_wholesale.Location = New System.Drawing.Point(120, 71)
        Me.label_currency_wholesale.Name = "label_currency_wholesale"
        Me.label_currency_wholesale.Size = New System.Drawing.Size(26, 30)
        Me.label_currency_wholesale.TabIndex = 25
        Me.label_currency_wholesale.Text = "P"
        Me.label_currency_wholesale.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_currency_wholesale.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_currency_wholesale.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_currency_wholesale.VerticalDirection = False
        '
        'label_currency_distribution
        '
        Me.label_currency_distribution.AutoEllipsis = False
        Me.label_currency_distribution.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold)
        Me.label_currency_distribution.ForeColor = System.Drawing.Color.Black
        Me.label_currency_distribution.Location = New System.Drawing.Point(120, 107)
        Me.label_currency_distribution.Name = "label_currency_distribution"
        Me.label_currency_distribution.Size = New System.Drawing.Size(26, 30)
        Me.label_currency_distribution.TabIndex = 26
        Me.label_currency_distribution.Text = "P"
        Me.label_currency_distribution.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_currency_distribution.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_currency_distribution.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_currency_distribution.VerticalDirection = False
        '
        'form_product_editor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(460, 598)
        Me.Controls.Add(Me.group_packing_information)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_prices)
        Me.Controls.Add(Me.group_product_details)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_product_editor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Product Details"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.group_product_prices.ResumeLayout(False)
        Me.group_product_prices.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.group_packing_information.ResumeLayout(False)
        Me.group_packing_information.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents text_product_name As System.Windows.Forms.TextBox
    Friend WithEvents group_product_prices As System.Windows.Forms.GroupBox
    Friend WithEvents text_product_unit As System.Windows.Forms.TextBox
    Friend WithEvents text_product_brand As System.Windows.Forms.TextBox
    Friend WithEvents text_product_code As System.Windows.Forms.TextBox
    Friend WithEvents text_product_description As System.Windows.Forms.TextBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_save As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents label_unit_max_char As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents text_retail_price As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents text_wholesale_price As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents text_distribution_price As System.Windows.Forms.TextBox
    Friend WithEvents label_currency_retail As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_currency_wholesale As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_currency_distribution As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_pads_per_pack As System.Windows.Forms.Label
    Friend WithEvents text_pads_per_pack As System.Windows.Forms.TextBox
    Friend WithEvents label_packing_size As System.Windows.Forms.Label
    Friend WithEvents text_packing_size As System.Windows.Forms.TextBox
    Friend WithEvents label_tip_packing_size As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents group_packing_information As System.Windows.Forms.GroupBox
    Friend WithEvents label_tip_sold_in_packs As System.Windows.Forms.Label
    Friend WithEvents checkbox_no_pack As System.Windows.Forms.CheckBox
    Friend WithEvents label_tip_sold_in_pads As System.Windows.Forms.Label
    Friend WithEvents checkbox_no_pad As System.Windows.Forms.CheckBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents text_product_category As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class
