﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_customer_selector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_customer_selector))
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.text_contact = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.text_town = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.text_address = New System.Windows.Forms.TextBox()
        Me.text_customer_name = New System.Windows.Forms.TextBox()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.group_product_details.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_product_details
        '
        Me.group_product_details.Controls.Add(Me.text_contact)
        Me.group_product_details.Controls.Add(Me.Label3)
        Me.group_product_details.Controls.Add(Me.text_town)
        Me.group_product_details.Controls.Add(Me.Label2)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.text_address)
        Me.group_product_details.Controls.Add(Me.text_customer_name)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(8, 3)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(373, 261)
        Me.group_product_details.TabIndex = 2
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Customer Information"
        '
        'text_contact
        '
        Me.text_contact.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_contact.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_contact.ForeColor = System.Drawing.Color.Black
        Me.text_contact.Location = New System.Drawing.Point(122, 218)
        Me.text_contact.Name = "text_contact"
        Me.text_contact.Size = New System.Drawing.Size(235, 29)
        Me.text_contact.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 226)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 17)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Contact"
        '
        'text_town
        '
        Me.text_town.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_town.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_town.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_town.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_town.ForeColor = System.Drawing.Color.Black
        Me.text_town.Location = New System.Drawing.Point(122, 180)
        Me.text_town.Name = "text_town"
        Me.text_town.Size = New System.Drawing.Size(235, 29)
        Me.text_town.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoEllipsis = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 179)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 41)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Town / Municipality"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 17)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Address"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(107, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Customer Name"
        '
        'text_address
        '
        Me.text_address.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_address.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_address.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_address.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_address.ForeColor = System.Drawing.Color.Black
        Me.text_address.Location = New System.Drawing.Point(122, 74)
        Me.text_address.MaxLength = 50
        Me.text_address.Multiline = True
        Me.text_address.Name = "text_address"
        Me.text_address.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.text_address.Size = New System.Drawing.Size(235, 94)
        Me.text_address.TabIndex = 3
        '
        'text_customer_name
        '
        Me.text_customer_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_customer_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_customer_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_customer_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_customer_name.ForeColor = System.Drawing.Color.Black
        Me.text_customer_name.Location = New System.Drawing.Point(122, 34)
        Me.text_customer_name.Name = "text_customer_name"
        Me.text_customer_name.Size = New System.Drawing.Size(235, 29)
        Me.text_customer_name.TabIndex = 1
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_done)
        Me.panel_buttons.Location = New System.Drawing.Point(130, 273)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(115, 74)
        Me.panel_buttons.TabIndex = 17
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 12
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(1, 2)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 11
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'form_customer_selector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(387, 344)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_details)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_customer_selector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Customer"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents text_address As System.Windows.Forms.TextBox
    Friend WithEvents text_customer_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents text_town As System.Windows.Forms.TextBox
    Friend WithEvents text_contact As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
End Class
