﻿Public Class form_customer_selector

    Private _checked As Boolean = False
    Private _customer_detail As Customer
    Private _allow_editing As Boolean = False
    Private sql As New class_db_transaction

    Private Sub form_customer_selector_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then button_cancel.PerformClick()

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        For Each obj In Me.Controls
            If TypeOf obj Is TextBox Then
                If obj Is text_contact Or obj Is text_address Then Continue For
                obj.BackColor = If(Trim(obj.Text) = "", module_colors.color_red, Color.White)
            ElseIf TypeOf obj Is GroupBox Then
                For Each child_obj In obj.Controls
                    If child_obj Is text_contact Or child_obj Is text_address Then Continue For
                    If TypeOf child_obj Is TextBox Then child_obj.BackColor = If(Trim(child_obj.Text) = "", module_colors.color_red, Color.White)
                Next
            End If
        Next

        
        Dim towns = sql.GetTownList()
        Dim town_list As New AutoCompleteStringCollection
        If Not IsNothing(towns) Then
            For Each _town In towns
                town_list.Add(_town("t_town_name"))
            Next
        End If

        text_town.AutoCompleteCustomSource = town_list

    End Sub

    Private Sub LoadCustomerList()

        Dim customers = sql.GetCustomerList()
        Dim customer_list As New AutoCompleteStringCollection
        If Not IsNothing(customers) Then
            For Each _customer In customers
                customer_list.Add(_customer("c_name"))
            Next
        End If
        text_customer_name.AutoCompleteCustomSource = customer_list


    End Sub

    Public Function OpenCustomerSelectorDialog(ByVal parent_form As Form, Optional ByVal allow_editing As Boolean = True) As Customer

        _allow_editing = allow_editing
        If _allow_editing Then LoadCustomerList()
        _customer_detail = New Customer
        _customer_detail.Town = New Town
        Me.ShowDialog(parent_form)
        OpenCustomerSelectorDialog = If(_checked, _customer_detail, Nothing)

    End Function

    Private Sub text_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_customer_name.TextChanged, text_address.TextChanged, text_town.TextChanged, text_contact.TextChanged

        If Trim(sender.Text) = "" Or (sender Is text_town And sql.GetTownIDByName(text_town.Text) = 0) Then
            InvalidateTextBox(sender)
            If sender Is text_town Then _customer_detail.Town = New Town
        Else
            ValidateTextBox(sender)
            If sender Is text_town Then _customer_detail.Town = New Town(sql.GetTownIDByName(text_town.Text))
            If sender Is text_customer_name Then
                Dim customer_id = sql.GetCustomerIDByName(text_customer_name.Text)
                If customer_id > 0 And Not _customer_detail.CustomerID = customer_id Then
                    _customer_detail = New Customer(customer_id)
                    text_address.Text = _customer_detail.Address
                    text_contact.Text = _customer_detail.Contact
                    text_town.Text = _customer_detail.Town.TownName
                End If
            End If
        End If


    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        If Trim(text_customer_name.Text) = "" Or _customer_detail.Town.TownID = 0 Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Critical)
        Else
            _customer_detail.CustomerName = text_customer_name.Text
            _customer_detail.Address = text_address.Text
            _customer_detail.Contact = text_contact.Text
            If _customer_detail.CustomerID = 0 Then
                If Not MsgBox("Do you want to add customer '" & text_customer_name.Text & "' and proceed?", MsgBoxStyle.YesNo) = DialogResult.Yes Then Exit Sub
            ElseIf _customer_detail.CustomerID > 0 And Not _allow_editing Then
                If Not MsgBox(_customer_detail.CustomerName & " is already used. Do you want to proceed and overwrite details for existing detail?", MsgBoxStyle.YesNo) = DialogResult.Yes Then Exit Sub
            End If
            If _customer_detail.Persist Then
                _checked = True
                Me.Close()
            Else
                MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub
End Class