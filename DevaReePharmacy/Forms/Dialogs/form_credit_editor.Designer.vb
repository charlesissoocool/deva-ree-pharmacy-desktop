﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_credit_editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_credit_editor))
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.text_initial_payment = New System.Windows.Forms.TextBox()
        Me.date_due_date = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.text_customer_name = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.text_credit_amount = New System.Windows.Forms.TextBox()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_add_customer = New System.Windows.Forms.Button()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.group_product_details.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_product_details
        '
        Me.group_product_details.Controls.Add(Me.Label2)
        Me.group_product_details.Controls.Add(Me.text_initial_payment)
        Me.group_product_details.Controls.Add(Me.date_due_date)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.text_customer_name)
        Me.group_product_details.Controls.Add(Me.Label3)
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.text_credit_amount)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(9, 0)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(373, 182)
        Me.group_product_details.TabIndex = 3
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Credit Information"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 144)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 17)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Initial Payment"
        '
        'text_initial_payment
        '
        Me.text_initial_payment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_initial_payment.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_initial_payment.ForeColor = System.Drawing.Color.Black
        Me.text_initial_payment.Location = New System.Drawing.Point(122, 139)
        Me.text_initial_payment.Name = "text_initial_payment"
        Me.text_initial_payment.Size = New System.Drawing.Size(236, 29)
        Me.text_initial_payment.TabIndex = 4
        '
        'date_due_date
        '
        Me.date_due_date.CalendarMonthBackground = System.Drawing.Color.White
        Me.date_due_date.CustomFormat = "yyyy / MM / dd"
        Me.date_due_date.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.date_due_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.date_due_date.Location = New System.Drawing.Point(122, 104)
        Me.date_due_date.Name = "date_due_date"
        Me.date_due_date.Size = New System.Drawing.Size(236, 29)
        Me.date_due_date.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 17)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Due Date"
        '
        'text_customer_name
        '
        Me.text_customer_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_customer_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_customer_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_customer_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_customer_name.ForeColor = System.Drawing.Color.Black
        Me.text_customer_name.Location = New System.Drawing.Point(122, 69)
        Me.text_customer_name.Name = "text_customer_name"
        Me.text_customer_name.Size = New System.Drawing.Size(236, 29)
        Me.text_customer_name.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 17)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Bill To"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Credit Amount"
        '
        'text_credit_amount
        '
        Me.text_credit_amount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_credit_amount.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_credit_amount.ForeColor = System.Drawing.Color.Black
        Me.text_credit_amount.Location = New System.Drawing.Point(122, 34)
        Me.text_credit_amount.Name = "text_credit_amount"
        Me.text_credit_amount.Size = New System.Drawing.Size(236, 29)
        Me.text_credit_amount.TabIndex = 1
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_add_customer)
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_done)
        Me.panel_buttons.Location = New System.Drawing.Point(9, 188)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(373, 74)
        Me.panel_buttons.TabIndex = 18
        '
        'button_add_customer
        '
        Me.button_add_customer.BackColor = System.Drawing.Color.Transparent
        Me.button_add_customer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_customer.FlatAppearance.BorderSize = 0
        Me.button_add_customer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_add_customer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_add_customer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_customer.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_customer.Image = CType(resources.GetObject("button_add_customer.Image"), System.Drawing.Image)
        Me.button_add_customer.Location = New System.Drawing.Point(278, 6)
        Me.button_add_customer.Name = "button_add_customer"
        Me.button_add_customer.Size = New System.Drawing.Size(92, 65)
        Me.button_add_customer.TabIndex = 7
        Me.button_add_customer.Text = "New Customer"
        Me.button_add_customer.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_add_customer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_customer.UseVisualStyleBackColor = True
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 6
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(1, 2)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 5
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'form_credit_editor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(392, 262)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_details)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_credit_editor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Credit"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents text_credit_amount As System.Windows.Forms.TextBox
    Friend WithEvents text_customer_name As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date_due_date As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents text_initial_payment As System.Windows.Forms.TextBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
    Friend WithEvents button_add_customer As System.Windows.Forms.Button
End Class
