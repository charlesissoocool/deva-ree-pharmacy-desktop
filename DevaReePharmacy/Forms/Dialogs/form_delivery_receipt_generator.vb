﻿
Public Class form_delivery_receipt_generator

    Private sql As New class_db_transaction
    Private _transaction_detail As Transaction

    Public Sub OpenTransactionReportGeneratorDialog(ByVal parent_form As Form, ByVal transaction_detail As Transaction)

        _transaction_detail = transaction_detail
        GenerateHTML()
        Me.ShowDialog(parent_form)

    End Sub

    Private Sub GenerateHTML()

        pdf_generator.Title = "Delivery Receipt"
        Dim css_list As New Dictionary(Of String, String)
        Dim html As String = ""
        css_list.Add("table.header-list", "width: 100%; border: 1px solid black")
        css_list.Add("table.header-list td", "font-size: 14px; width: 25%")
        css_list.Add("table.header-list td.header-title", "font-size: 14px; font-weight: bold")

        css_list.Add("table.product-list", "width: 100%; border: 1px solid black")
        css_list.Add("table.product-list th", "font-size: 14px; font-style: bold; border: 1px solid black")
        css_list.Add("table.product-list td", "font-size: 14px; border: 1px dashed grey; padding: 5px; ")
        'css_list.Add("table.product-list td", "border-bottom: 1px dashed grey; font-size: 16px")
        css_list.Add("table.product-list tr", "page-break-inside: avoid")

        css_list.Add("table.footer-list", "width: 100%; border: 1px solid black")
        css_list.Add("table.footer-list td", "font-size: 14px;border: 1px solid black")
        css_list.Add("table.footer-list td.fit-width", "width:auto;")
        css_list.Add("span.invoice-no", "font-size: 14px; text-align:right; width: 100%")
        css_list.Add("span.amount-value", "text-align:right; float:right")
        css_list.Add("span.box", "width: 25px; height: 25px; border: 1px solid black; display: inline-block")

        css_list.Add("td.left-border", "border-left: 1px solid black")
        css_list.Add("td.right-border", "border-right: 1px solid black")
        css_list.Add("td.top-border", "border-top: 1px solid black")
        css_list.Add("td.bottom-border", "border-bottom: 1px solid black")
        css_list.Add("td", "padding: 5px")

        

        html &= "<table class='header-list' cellspacing=0>"
        html &= "<tr><td rowspan=3 style='width: 50%' valign='top'>" & If(IsNothing(_transaction_detail.Customer), "Walk-In Guest", _transaction_detail.Customer.CustomerName & " - C" & _transaction_detail.Customer.CustomerID & "<br>" & _transaction_detail.Customer.Contact & "<br>" & _transaction_detail.Customer.Address & "<br>" & _transaction_detail.Customer.Town.TownName) & "</td>"
        html &= "<td class='header-title left-border' style='width: 25%'>D.R. No.</td><td class='header-title' style='width: 25%'>Date</td></tr>"
        html &= "<tr><td class='left-border'>" & _transaction_detail.InvoiceNo.ToString().PadLeft(10, "0") & "</td><td>" & _transaction_detail.TransactionDate.ToString(DTS_HUMAN_WITH_TIME) & "</td></tr>"
        html &= "<tr><td colspan=2 class='left-border top-border'><b>TERMS:</b>&nbsp;&nbsp;&nbsp;" & If(IsNothing(_transaction_detail.Credit), "--", _transaction_detail.Credit.Terms & " (REF: " & _transaction_detail.Credit.CreditID.ToString().PadLeft(10, "0") & ")") & "</td></tr>"

        'html &= "<tr><td class='header-title'>Name of Customer:</td><td>" & If(IsNothing(_transaction_detail.Customer), "Guest", _transaction_detail.Customer.CustomerName) & "</td><td class='header-title'>Terms:</td><td>" & If(IsNothing(_transaction_detail.Credit), "--", _transaction_detail.Credit.Terms & " (REF: " & _transaction_detail.Credit.CreditID.ToString().PadLeft(10, "0") & ")") & "</td></tr>"
        'html &= "<tr><td class='header-title'>Contact No.:</td><td>" & If(IsNothing(_transaction_detail.Customer), "--", _transaction_detail.Customer.Contact) & "</td><td class='header-title'>DR #:</td><td>" & _transaction_detail.InvoiceNo.ToString().PadLeft(10, "0") & "</td></tr>"
        'html &= "<tr><td class='header-title'>Checker:</td><td>--</td><td class='header-title'>Agent:</td><td>" & If(IsNothing(_transaction_detail.Agent), "--", _transaction_detail.Agent.FullName) & "</td></tr>"
        'html &= "<tr><td class='header-title'>Date Transacted:</td><td>" & _transaction_detail.TransactionDate.ToString(DTS_HUMAN_WITH_TIME) & "</td><td class='header-title'>Prepared by:</td><td>" & _transaction_detail.Staff.FullName & "</td></tr>"
        html &= "</table>"

        html &= "<table class='product-list' cellspacing=0>"
        html &= "<thead><tr><th>PCode</th><th>Qty.</th><th>Particular/s</th><th>Lot No.</th><th>Expiry Date</th><th>Unit Price</th><th>Total Amount</th></tr></thead>"
        html &= "<tbody>"
        Dim gross As Double = 0
        Dim discount As Double = 0
        Dim total_price As Double = 0
        For Each cart_item As Transaction_Item In _transaction_detail.TransactionItems
            Dim quantity As Double = cart_item.ItemQuantity
            Dim real_quantity As Double = cart_item.ItemQuantity - cart_item.ReturnQuantity
            Dim unit As String = cart_item.Product.ProductUnit
            Dim real_unit As String = cart_item.Product.ProductUnit
            Dim display_unit As ItemUnit
            Dim divider As Double
            Dim real_divider As Double
            If quantity Mod cart_item.Product.PackingSize = 0 And cart_item.Product.IsSoldInPacks And cart_item.DisplayUnit = ItemUnit.Pack Then
                quantity = quantity / cart_item.Product.PackingSize
                divider = 1
                unit = SYS_PACK_ALIAS
                display_unit = ItemUnit.Pack
            ElseIf quantity Mod (cart_item.Product.PackingSize / cart_item.Product.PadsPerPack) = 0 And cart_item.Product.IsSoldInPads And cart_item.DisplayUnit = ItemUnit.Pad Then
                quantity = quantity / (cart_item.Product.PackingSize / cart_item.Product.PadsPerPack)
                divider = cart_item.Product.PadsPerPack
                unit = SYS_PAD_ALIAS
                display_unit = ItemUnit.Pad
            Else
                divider = cart_item.Product.PackingSize
                display_unit = ItemUnit.Unit
            End If
            If real_quantity Mod cart_item.Product.PackingSize = 0 And cart_item.Product.IsSoldInPacks And cart_item.DisplayUnit = ItemUnit.Pack Then
                real_quantity = real_quantity / cart_item.Product.PackingSize
                real_divider = 1
                real_unit = SYS_PACK_ALIAS
            ElseIf real_quantity Mod (cart_item.Product.PackingSize / cart_item.Product.PadsPerPack) = 0 And cart_item.Product.IsSoldInPads And cart_item.DisplayUnit = ItemUnit.Pad Then
                real_quantity = real_quantity / (cart_item.Product.PackingSize / cart_item.Product.PadsPerPack)
                real_divider = cart_item.Product.PadsPerPack
                real_unit = SYS_PAD_ALIAS
            Else
                real_divider = cart_item.Product.PackingSize
            End If
            total_price = real_quantity * RoundUp(cart_item.SellingPrice / divider, 2)
            html &= "<tr>"
            html &= "<td>" & cart_item.Product.ProductCode & "</td>"
            If real_quantity = quantity Then
                html &= "<td>" & quantity & " " & unit & "</td>"
            Else
                html &= "<td><s>" & quantity & " " & unit & "</s>&nbsp" & real_quantity & " " & real_unit & "</td>"
            End If
            html &= "<td>" & cart_item.Product.ProductCompleteNameWithPacking(display_unit) & "</td>"
            html &= "<td>" & If(IsNothing(cart_item.Stock), "--", cart_item.Stock.LotNumber) & "</td>"
            html &= "<td>" & If(IsNothing(cart_item.Stock), "--", cart_item.Stock.ExpiryDate.ToString(DTS_YYYYMMDD)) & "</td>"
            html &= "<td>" & If(cart_item.SellingPrice < cart_item.OriginalPrice, "<s>" & SYS_CURRENCY & FormatNumber(cart_item.OriginalPrice, 2, , , TriState.True) & "</s>&nbsp;", "") & SYS_CURRENCY & FormatNumber(RoundUp(cart_item.SellingPrice / divider, 2), 2, , , TriState.True) & "</td>"
            html &= "<td>" & SYS_CURRENCY & FormatNumber(total_price, 2, , , TriState.True) & "</td>"
            gross += total_price
            discount += (real_quantity * RoundUp((cart_item.OriginalPrice - cart_item.SellingPrice) / divider, 2))
        Next
        html &= "</tbody>"
        html &= "</table>"

        html &= "<table class='footer-list' cellspacing=0>"
        html &= "<tr><td>Prepared by&nbsp;&nbsp;<b>" & _transaction_detail.Staff.InitialName & "</b></td><td>Checked by&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Received by&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td align='left' rowspan=2>Gross Amount <span class='amount-value'><b>" & SYS_CURRENCY & FormatNumber(gross + discount, 2, , , TriState.True) & "</b></span><br>Net Discount <span class='amount-value'><b>" & SYS_CURRENCY & FormatNumber(discount, 2, , , TriState.True) & "</b></span><br>Net Amount <span class='amount-value'><b>" & SYS_CURRENCY & FormatNumber(gross, 2, , , TriState.True) & "</b></span></td></tr>"
        html &= "<tr><td align='center' colspan=3><span class='box'>&nbsp;</span>&nbsp;Boxes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='box'>&nbsp;</span>&nbsp;Bags</td></tr>"
        html &= "</table>"

        If (IsFontInstalled(SYS_BARCODE_FONT)) Then
            css_list.Add("font.barcode", "font-family: '" & SYS_BARCODE_FONT & "'; font-size: " & SYS_BARCODE_SIZE)
            html &= "<br><font class='barcode'>" & SYS_BARCODE_DELIMITER & _transaction_detail.InvoiceNo.ToString().PadLeft(10, "0") & SYS_BARCODE_DELIMITER & "</font>"
        End If

        pdf_generator.CSS = css_list
        pdf_generator.HTML = html

    End Sub

    Private Sub form_transaction_report_generator_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then
            button_cancel.PerformClick()
            e.Handled = True
        End If

    End Sub

    Private Sub form_transaction_report_generator_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        If e.KeyChar = ChrW(Keys.Escape) Then
            button_cancel.PerformClick()
            e.Handled = True
        ElseIf e.KeyChar = ChrW(Keys.Enter) Then
            button_print.PerformClick()
            e.Handled = True
        End If

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        panel_buttons.Left = (Me.ClientSize.Width / 2) - (panel_buttons.Width / 2)

    End Sub

    Private Sub button_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_print.Click

        GenerateHTML()
        pdf_generator.Print()
        Me.Close()

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub

    Private Sub pdf_generator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pdf_generator.Load

    End Sub

    Private Sub load_preview_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles load_preview.Tick

        load_preview.Enabled = False
        pdf_generator.ShowPreview()

    End Sub
End Class