﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_user_login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.group_security = New System.Windows.Forms.GroupBox()
        Me.button_login = New System.Windows.Forms.Button()
        Me.text_password = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.text_username = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.prompt_login = New DevaReePharmacy.PromptLabel
        Me.group_security.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_security
        '
        Me.group_security.Controls.Add(Me.button_login)
        Me.group_security.Controls.Add(Me.text_password)
        Me.group_security.Controls.Add(Me.Label4)
        Me.group_security.Controls.Add(Me.text_username)
        Me.group_security.Controls.Add(Me.Label3)
        Me.group_security.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_security.ForeColor = System.Drawing.Color.Black
        Me.group_security.Location = New System.Drawing.Point(7, 54)
        Me.group_security.Name = "group_security"
        Me.group_security.Size = New System.Drawing.Size(330, 182)
        Me.group_security.TabIndex = 2
        Me.group_security.TabStop = False
        '
        'button_login
        '
        Me.button_login.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_login.Location = New System.Drawing.Point(116, 132)
        Me.button_login.Name = "button_login"
        Me.button_login.Size = New System.Drawing.Size(89, 40)
        Me.button_login.TabIndex = 7
        Me.button_login.Text = "Login"
        Me.button_login.UseVisualStyleBackColor = True
        '
        'text_password
        '
        Me.text_password.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_password.ForeColor = System.Drawing.Color.Black
        Me.text_password.Location = New System.Drawing.Point(10, 97)
        Me.text_password.Name = "text_password"
        Me.text_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.text_password.Size = New System.Drawing.Size(307, 29)
        Me.text_password.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 73)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 21)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Password"
        '
        'text_username
        '
        Me.text_username.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_username.ForeColor = System.Drawing.Color.Black
        Me.text_username.Location = New System.Drawing.Point(10, 41)
        Me.text_username.Name = "text_username"
        Me.text_username.Size = New System.Drawing.Size(307, 29)
        Me.text_username.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 21)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Username"
        '
        'prompt_login
        '
        Me.prompt_login.BackColor = System.Drawing.Color.FromArgb(CType(CType(80, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.prompt_login.Location = New System.Drawing.Point(8, 12)
        Me.prompt_login.Name = "prompt_login"
        Me.prompt_login.Size = New System.Drawing.Size(330, 41)
        Me.prompt_login.TabIndex = 10
        Me.prompt_login.Visible = False
        '
        'form_user_login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(346, 248)
        Me.Controls.Add(Me.prompt_login)
        Me.Controls.Add(Me.group_security)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_user_login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Login"
        Me.group_security.ResumeLayout(False)
        Me.group_security.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_security As System.Windows.Forms.GroupBox
    Friend WithEvents text_password As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents text_username As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents prompt_login As DevaReePharmacy.PromptLabel
    Friend WithEvents button_login As System.Windows.Forms.Button
End Class
