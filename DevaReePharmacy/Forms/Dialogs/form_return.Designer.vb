﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_return
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_return))
        Me.list_stocks = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.button_return = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.prompt_header = New DevaReePharmacy.PromptLabel()
        Me.SuspendLayout()
        '
        'list_stocks
        '
        Me.list_stocks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_stocks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.list_stocks.Font = New System.Drawing.Font("Segoe UI", 11.25!)
        Me.list_stocks.FullRowSelect = True
        Me.list_stocks.GridLines = True
        Me.list_stocks.Location = New System.Drawing.Point(0, 38)
        Me.list_stocks.Name = "list_stocks"
        Me.list_stocks.Size = New System.Drawing.Size(752, 197)
        Me.list_stocks.TabIndex = 16
        Me.list_stocks.TabStop = False
        Me.list_stocks.UseCompatibleStateImageBehavior = False
        Me.list_stocks.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product Name"
        Me.ColumnHeader1.Width = 131
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Lot ID"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Expiry Date"
        Me.ColumnHeader3.Width = 93
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Quantity Returnable"
        Me.ColumnHeader4.Width = 173
        '
        'button_return
        '
        Me.button_return.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_return.BackColor = System.Drawing.Color.Transparent
        Me.button_return.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_return.FlatAppearance.BorderSize = 0
        Me.button_return.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_return.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_return.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_return.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_return.Image = CType(resources.GetObject("button_return.Image"), System.Drawing.Image)
        Me.button_return.Location = New System.Drawing.Point(687, 247)
        Me.button_return.Name = "button_return"
        Me.button_return.Size = New System.Drawing.Size(53, 65)
        Me.button_return.TabIndex = 11
        Me.button_return.Text = "Return"
        Me.button_return.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_return.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_return.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(12, 247)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 17
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'prompt_header
        '
        Me.prompt_header.BackColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(196, Byte), Integer), CType(CType(15, Byte), Integer))
        Me.prompt_header.Location = New System.Drawing.Point(0, -1)
        Me.prompt_header.Message = "Please refer to LOT ID on product. Returns cannot be reverted."
        Me.prompt_header.Mode = DevaReePharmacy.PromptLabel.PromptLabelMode.Warning
        Me.prompt_header.Name = "prompt_header"
        Me.prompt_header.Size = New System.Drawing.Size(752, 39)
        Me.prompt_header.TabIndex = 18
        '
        'form_return
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(752, 324)
        Me.Controls.Add(Me.prompt_header)
        Me.Controls.Add(Me.button_done)
        Me.Controls.Add(Me.button_return)
        Me.Controls.Add(Me.list_stocks)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_return"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Return Items"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents list_stocks As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_return As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
    Friend WithEvents prompt_header As DevaReePharmacy.PromptLabel
End Class
