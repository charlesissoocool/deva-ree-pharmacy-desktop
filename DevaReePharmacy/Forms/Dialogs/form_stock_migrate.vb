﻿Public Class form_stock_migrate

    Private _stock_detail As Stock
    Private _stock_migrate As Stock_Migrate = Nothing
    Private _checked As Boolean = False

    Private Sub AdjustInterface() Handles Me.Load

        For Each grp In Me.Controls.OfType(Of GroupBox)()
            For Each obj In grp.Controls.OfType(Of TextBox)()
                text_box_TextChanged(obj, Nothing)
            Next
        Next

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        panel_buttons.Left = (Me.ClientSize.Width / 2) - (panel_buttons.Width / 2)

    End Sub

    Public Function OpenMigrateStockDialog(ByVal parent_form As Form, ByVal stock_detail As Stock) As Stock_Migrate

        label_product_name.Text = stock_detail.Product.ProductCompleteName
        label_lot_number.Text = stock_detail.LotNumber
        label_unit.Text = If(stock_detail.Product.IsSoldInPacks, SYS_PACK_ALIAS, stock_detail.Product.ProductUnit)
        _stock_detail = stock_detail
        _stock_migrate = New Stock_Migrate
        _stock_migrate.StockID = stock_detail.StockID
        Me.ShowDialog(parent_form)
        OpenMigrateStockDialog = If(_checked, _stock_migrate, Nothing)

    End Function

    Private Sub text_box_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_stock_quantity.TextChanged, text_transfer_reason.TextChanged

        If sender Is text_stock_quantity Then ValidateNumericTextBox(sender, SYS_DECIMAL_STOCKS)
        If sender.Text = "" Then
            sender.BackColor = module_colors.color_red
            sender.ForeColor = Color.White
        Else
            sender.BackColor = Color.White
            sender.ForeColor = Color.Black
        End If

    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        If Val(text_stock_quantity.Text) <= 0 Or Trim(text_transfer_reason.Text) = "" Then
            MsgBox("Some fields are invalid.")
        Else
            _stock_migrate.MigrateDate = Date.Now()
            Dim stock_quantity As Double = Val(text_stock_quantity.Text)
            If _stock_detail.Product.IsSoldInPacks Then
                stock_quantity *= _stock_detail.Product.PackingSize
            End If
            If stock_quantity > _stock_detail.RemainingQuantity Then
                MsgBox("Could not migrate stocks greater than available stocks.", MsgBoxStyle.Exclamation)
            Else
                _stock_migrate.StockQuantity = stock_quantity
                _stock_migrate.Reason = text_transfer_reason.Text
                _checked = True
                Me.Close()
            End If
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub
End Class