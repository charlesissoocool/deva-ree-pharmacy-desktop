﻿Public Class form_user_editor

    Dim _user_details As User
    Dim _checked As Boolean = False
    Dim sql As New class_db_user

    Private Sub AdjustObjects() Handles Me.Resize

        panel_buttons.Left = (Me.ClientSize.Width / 2) - (panel_buttons.Width / 2)

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        For Each obj In Me.Controls
            If TypeOf obj Is TextBox Then
                InvalidateTextBox(obj)
            ElseIf TypeOf obj Is GroupBox Then
                For Each child_obj In obj.Controls
                    If TypeOf child_obj Is TextBox Then InvalidateTextBox(child_obj)
                Next
            End If
        Next

    End Sub

    Public Function OpenUserEditor(ByVal parent_form As Form, ByVal admin As Boolean, ByVal stocks As Boolean, ByVal products As Boolean, ByVal cashier As Boolean, ByVal agent As Boolean, ByVal finance As Boolean) As User

        _user_details = New User

        text_first_name.Text = _user_details.FirstName
        text_last_name.Text = _user_details.LastName
        text_email.Text = _user_details.Email
        text_username.Text = _user_details.Username
        toggle_administrator.Checked = admin
        toggle_cashier.Checked = cashier
        toggle_products.Checked = products
        toggle_stocks.Checked = stocks
        toggle_agent.Checked = agent
        toggle_finance.Checked = finance
        group_permissions.Enabled = False

        Me.ShowDialog(parent_form)
        OpenUserEditor = If(_checked, _user_details, Nothing)

    End Function

    Public Function OpenUserEditor(ByVal parent_form As Form, Optional ByVal user_details As User = Nothing) As User

        If IsNothing(user_details) Then
            _user_details = New User
        Else
            _user_details = user_details
        End If

        text_first_name.Text = _user_details.FirstName
        text_last_name.Text = _user_details.LastName
        text_email.Text = _user_details.Email
        text_username.Text = _user_details.Username
        toggle_administrator.Checked = _user_details.Permission_Administator
        toggle_cashier.Checked = _user_details.Permission_Cashier
        toggle_products.Checked = _user_details.Permission_Products
        toggle_stocks.Checked = _user_details.Permission_Stocks
        toggle_agent.Checked = _user_details.Permission_Agent
        toggle_finance.Checked = _user_details.Permission_Finance

        Me.ShowDialog(parent_form)
        OpenUserEditor = If(_checked, _user_details, Nothing)

    End Function

    Public Property EnableProfile As Boolean
        Get
            EnableProfile = group_personal_info.Enabled
        End Get
        Set(ByVal value As Boolean)
            group_personal_info.Enabled = value
        End Set

    End Property

    Public Property EnableSecurity As Boolean
        Get
            EnableSecurity = group_security.Enabled
        End Get
        Set(ByVal value As Boolean)
            group_security.Enabled = value
        End Set

    End Property

    Public Property EnablePermissions As Boolean
        Get
            EnablePermissions = group_permissions.Enabled
        End Get
        Set(ByVal value As Boolean)
            group_permissions.Enabled = value
        End Set

    End Property

    Private Sub button_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_save.Click

        Dim sql As New class_db_user

        If Trim(text_first_name.Text) = "" Or Trim(text_last_name.Text) = "" Or Trim(text_email.Text) = "" Or Trim(text_username.Text) = "" Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Critical)
        ElseIf _user_details.UserID = 0 And (Trim(text_password.Text) = "" Or Trim(text_repeat_password.Text) = "") Then
            MsgBox("Please enter password for user.", MsgBoxStyle.Critical)
        ElseIf (_user_details.UserID = 0 And Not text_password.Text = text_repeat_password.Text) Or (Not text_password.Text = "" And Not text_password.Text = text_repeat_password.Text) Then
            MsgBox("Password mismatch.", MsgBoxStyle.Critical)
        ElseIf Not toggle_administrator.Checked And Not toggle_agent.Checked And Not toggle_cashier.Checked And Not toggle_products.Checked And Not toggle_stocks.Checked And Not toggle_finance.Checked Then
            MsgBox("Please enable atleast one permission for user.", MsgBoxStyle.Critical)
        ElseIf sql.IsEmailTaken(Trim(text_email.Text)) And Not _user_details.Email = Trim(text_email.Text) Then
            MsgBox("Email " & text_email.Text & " is already taken.", MsgBoxStyle.Critical)
        ElseIf sql.IsUserNameTaken(Trim(text_username.Text)) And Not _user_details.Username = Trim(text_username.Text) Then
            MsgBox("Username " & text_username.Text & " is already taken.", MsgBoxStyle.Critical)
        Else
            _user_details.FirstName = text_first_name.Text
            _user_details.LastName = text_last_name.Text
            _user_details.Email = text_email.Text
            _user_details.Username = text_username.Text
            If Not text_password.Text = "" Then _user_details.NewPassword = text_password.Text
            _user_details.Permission_Administator = toggle_administrator.Checked
            _user_details.Permission_Agent = toggle_agent.Checked
            _user_details.Permission_Cashier = toggle_cashier.Checked
            _user_details.Permission_Products = toggle_products.Checked
            _user_details.Permission_Stocks = toggle_stocks.Checked
            _user_details.Permission_Finance = toggle_finance.Checked
            _checked = True
            Me.Close()
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub

    Private Sub text_email_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_email.LostFocus

        If sql.IsEmailTaken(Trim(text_email.Text)) And Not Trim(text_email.Text) = _user_details.Email Then
            MsgBox("Email " & text_email.Text & " is already taken", MsgBoxStyle.Critical)
            text_email.BackColor = module_colors.color_red
            text_email.ForeColor = Color.White
        End If

    End Sub

    Private Sub text_username_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_username.LostFocus

        If sql.IsUserNameTaken(Trim(text_username.Text)) And Not Trim(text_username.Text) = _user_details.Username And _user_details.Synced Then
            MsgBox("Username " & text_username.Text & " is already taken.", MsgBoxStyle.Critical)
            text_username.BackColor = module_colors.color_red
            text_username.ForeColor = Color.White
        End If

    End Sub

    Private Sub profile_text_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_first_name.TextChanged, text_last_name.TextChanged, text_email.TextChanged, text_username.TextChanged, text_password.TextChanged, text_repeat_password.TextChanged

        sender.BackColor = If(Trim(sender.Text) = "", module_colors.color_red, Color.White)
        sender.ForeColor = If(Trim(sender.Text) = "", Color.White, Color.Black)
        If sender Is text_repeat_password Then
            sender.BackColor = If(text_repeat_password.Text = text_password.Text And Not Trim(sender.Text) = "", Color.White, module_colors.color_red)
            sender.ForeColor = If(text_repeat_password.Text = text_password.Text And Not Trim(sender.Text) = "", Color.Black, Color.White)
        End If
        

    End Sub

    Private Sub group_permissions_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles group_permissions.Enter

    End Sub

    Private Sub toggle_products_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toggle_products.Load

    End Sub
End Class