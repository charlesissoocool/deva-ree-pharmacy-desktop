﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_manifest_list_generator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_manifest_list_generator))
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_save_pdf = New System.Windows.Forms.Button()
        Me.button_print = New System.Windows.Forms.Button()
        Me.button_preview = New System.Windows.Forms.Button()
        Me.group_options = New System.Windows.Forms.GroupBox()
        Me.date_to_range = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.date_from_range = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pdf_generator = New DevaReePharmacy.PDFGenerator()
        Me.panel_buttons.SuspendLayout()
        Me.group_options.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_buttons
        '
        Me.panel_buttons.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_buttons.Controls.Add(Me.button_save_pdf)
        Me.panel_buttons.Controls.Add(Me.button_print)
        Me.panel_buttons.Controls.Add(Me.button_preview)
        Me.panel_buttons.Location = New System.Drawing.Point(138, 113)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(218, 71)
        Me.panel_buttons.TabIndex = 15
        '
        'button_save_pdf
        '
        Me.button_save_pdf.BackColor = System.Drawing.Color.Transparent
        Me.button_save_pdf.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_save_pdf.Enabled = False
        Me.button_save_pdf.FlatAppearance.BorderSize = 0
        Me.button_save_pdf.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_save_pdf.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_save_pdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_save_pdf.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_save_pdf.Image = CType(resources.GetObject("button_save_pdf.Image"), System.Drawing.Image)
        Me.button_save_pdf.Location = New System.Drawing.Point(68, 3)
        Me.button_save_pdf.Name = "button_save_pdf"
        Me.button_save_pdf.Size = New System.Drawing.Size(86, 65)
        Me.button_save_pdf.TabIndex = 15
        Me.button_save_pdf.Text = "Save PDF"
        Me.button_save_pdf.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_save_pdf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_save_pdf.UseVisualStyleBackColor = True
        '
        'button_print
        '
        Me.button_print.BackColor = System.Drawing.Color.Transparent
        Me.button_print.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_print.Enabled = False
        Me.button_print.FlatAppearance.BorderSize = 0
        Me.button_print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_print.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_print.Image = CType(resources.GetObject("button_print.Image"), System.Drawing.Image)
        Me.button_print.Location = New System.Drawing.Point(145, 3)
        Me.button_print.Name = "button_print"
        Me.button_print.Size = New System.Drawing.Size(63, 65)
        Me.button_print.TabIndex = 13
        Me.button_print.Text = "Print"
        Me.button_print.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_print.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_print.UseVisualStyleBackColor = True
        '
        'button_preview
        '
        Me.button_preview.BackColor = System.Drawing.Color.Transparent
        Me.button_preview.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_preview.Enabled = False
        Me.button_preview.FlatAppearance.BorderSize = 0
        Me.button_preview.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_preview.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_preview.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_preview.Image = CType(resources.GetObject("button_preview.Image"), System.Drawing.Image)
        Me.button_preview.Location = New System.Drawing.Point(5, 4)
        Me.button_preview.Name = "button_preview"
        Me.button_preview.Size = New System.Drawing.Size(63, 65)
        Me.button_preview.TabIndex = 12
        Me.button_preview.Text = "Preview"
        Me.button_preview.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_preview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_preview.UseVisualStyleBackColor = True
        '
        'group_options
        '
        Me.group_options.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_options.Controls.Add(Me.date_to_range)
        Me.group_options.Controls.Add(Me.Label2)
        Me.group_options.Controls.Add(Me.date_from_range)
        Me.group_options.Controls.Add(Me.Label5)
        Me.group_options.Controls.Add(Me.panel_buttons)
        Me.group_options.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_options.ForeColor = System.Drawing.Color.Black
        Me.group_options.Location = New System.Drawing.Point(12, 12)
        Me.group_options.Name = "group_options"
        Me.group_options.Size = New System.Drawing.Size(491, 199)
        Me.group_options.TabIndex = 19
        Me.group_options.TabStop = False
        Me.group_options.Text = "Select Manifest"
        '
        'date_to_range
        '
        Me.date_to_range.CalendarMonthBackground = System.Drawing.Color.White
        Me.date_to_range.CustomFormat = "yyyy / MM / dd"
        Me.date_to_range.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.date_to_range.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.date_to_range.Location = New System.Drawing.Point(186, 78)
        Me.date_to_range.Name = "date_to_range"
        Me.date_to_range.Size = New System.Drawing.Size(228, 29)
        Me.date_to_range.TabIndex = 19
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(74, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 17)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "To Date"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'date_from_range
        '
        Me.date_from_range.CalendarMonthBackground = System.Drawing.Color.White
        Me.date_from_range.CustomFormat = "yyyy / MM / dd"
        Me.date_from_range.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.date_from_range.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.date_from_range.Location = New System.Drawing.Point(186, 43)
        Me.date_from_range.Name = "date_from_range"
        Me.date_from_range.Size = New System.Drawing.Size(228, 29)
        Me.date_from_range.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(74, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 17)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "From Date"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(457, 214)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Preview"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pdf_generator
        '
        Me.pdf_generator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pdf_generator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pdf_generator.CSS = CType(resources.GetObject("pdf_generator.CSS"), System.Collections.Generic.Dictionary(Of String, String))
        Me.pdf_generator.HTML = Nothing
        Me.pdf_generator.Location = New System.Drawing.Point(12, 230)
        Me.pdf_generator.Name = "pdf_generator"
        Me.pdf_generator.Size = New System.Drawing.Size(491, 323)
        Me.pdf_generator.TabIndex = 22
        Me.pdf_generator.Title = "Document"
        '
        'form_manifest_list_generator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(515, 565)
        Me.Controls.Add(Me.pdf_generator)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.group_options)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "form_manifest_list_generator"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Manifest List Generator"
        Me.panel_buttons.ResumeLayout(False)
        Me.group_options.ResumeLayout(False)
        Me.group_options.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_print As System.Windows.Forms.Button
    Friend WithEvents button_preview As System.Windows.Forms.Button
    Friend WithEvents group_options As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pdf_generator As DevaReePharmacy.PDFGenerator
    Friend WithEvents button_save_pdf As System.Windows.Forms.Button
    Friend WithEvents date_to_range As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents date_from_range As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
