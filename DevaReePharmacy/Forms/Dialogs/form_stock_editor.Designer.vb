﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_stock_editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_stock_editor))
        Me.text_product_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.text_stock_quantity = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.text_lot_number = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.date_expiry_date = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.group_product_prices = New System.Windows.Forms.GroupBox()
        Me.combo_unit = New System.Windows.Forms.ComboBox()
        Me.label_tip_supplier_price = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.text_supplier_price = New System.Windows.Forms.TextBox()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.label_currency_price = New DevaReePharmacy.AntiAliasedLabel()
        Me.group_product_prices.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'text_product_name
        '
        Me.text_product_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_product_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_product_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_product_name.ForeColor = System.Drawing.Color.Black
        Me.text_product_name.Location = New System.Drawing.Point(123, 34)
        Me.text_product_name.Name = "text_product_name"
        Me.text_product_name.Size = New System.Drawing.Size(227, 29)
        Me.text_product_name.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(8, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Product Name"
        '
        'text_stock_quantity
        '
        Me.text_stock_quantity.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_stock_quantity.ForeColor = System.Drawing.Color.Black
        Me.text_stock_quantity.Location = New System.Drawing.Point(123, 73)
        Me.text_stock_quantity.Name = "text_stock_quantity"
        Me.text_stock_quantity.Size = New System.Drawing.Size(131, 29)
        Me.text_stock_quantity.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(8, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Stock Quantity"
        '
        'text_lot_number
        '
        Me.text_lot_number.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_lot_number.ForeColor = System.Drawing.Color.Black
        Me.text_lot_number.Location = New System.Drawing.Point(122, 112)
        Me.text_lot_number.Name = "text_lot_number"
        Me.text_lot_number.Size = New System.Drawing.Size(228, 29)
        Me.text_lot_number.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(8, 118)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 17)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Lot / Batch No."
        '
        'date_expiry_date
        '
        Me.date_expiry_date.CalendarMonthBackground = System.Drawing.Color.White
        Me.date_expiry_date.CustomFormat = "yyyy / MM / dd"
        Me.date_expiry_date.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.date_expiry_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.date_expiry_date.Location = New System.Drawing.Point(122, 150)
        Me.date_expiry_date.Name = "date_expiry_date"
        Me.date_expiry_date.Size = New System.Drawing.Size(228, 29)
        Me.date_expiry_date.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label5.Location = New System.Drawing.Point(10, 156)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(78, 17)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Expiry Date"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'group_product_prices
        '
        Me.group_product_prices.Controls.Add(Me.combo_unit)
        Me.group_product_prices.Controls.Add(Me.label_tip_supplier_price)
        Me.group_product_prices.Controls.Add(Me.Label3)
        Me.group_product_prices.Controls.Add(Me.text_supplier_price)
        Me.group_product_prices.Controls.Add(Me.date_expiry_date)
        Me.group_product_prices.Controls.Add(Me.Label1)
        Me.group_product_prices.Controls.Add(Me.text_product_name)
        Me.group_product_prices.Controls.Add(Me.Label5)
        Me.group_product_prices.Controls.Add(Me.Label2)
        Me.group_product_prices.Controls.Add(Me.text_stock_quantity)
        Me.group_product_prices.Controls.Add(Me.text_lot_number)
        Me.group_product_prices.Controls.Add(Me.Label4)
        Me.group_product_prices.Controls.Add(Me.label_currency_price)
        Me.group_product_prices.Font = New System.Drawing.Font("Segoe UI Light", 15.75!)
        Me.group_product_prices.ForeColor = System.Drawing.Color.Black
        Me.group_product_prices.Location = New System.Drawing.Point(7, 5)
        Me.group_product_prices.Name = "group_product_prices"
        Me.group_product_prices.Size = New System.Drawing.Size(360, 227)
        Me.group_product_prices.TabIndex = 15
        Me.group_product_prices.TabStop = False
        Me.group_product_prices.Text = "Stock Information"
        '
        'combo_unit
        '
        Me.combo_unit.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.combo_unit.BackColor = System.Drawing.Color.White
        Me.combo_unit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_unit.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.combo_unit.ForeColor = System.Drawing.Color.Black
        Me.combo_unit.FormattingEnabled = True
        Me.combo_unit.Location = New System.Drawing.Point(260, 73)
        Me.combo_unit.Name = "combo_unit"
        Me.combo_unit.Size = New System.Drawing.Size(90, 29)
        Me.combo_unit.TabIndex = 28
        '
        'label_tip_supplier_price
        '
        Me.label_tip_supplier_price.AutoSize = True
        Me.label_tip_supplier_price.Font = New System.Drawing.Font("Segoe UI Semilight", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_tip_supplier_price.ForeColor = System.Drawing.Color.Black
        Me.label_tip_supplier_price.Location = New System.Drawing.Point(6, 203)
        Me.label_tip_supplier_price.Name = "label_tip_supplier_price"
        Me.label_tip_supplier_price.Size = New System.Drawing.Size(0, 13)
        Me.label_tip_supplier_price.TabIndex = 27
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(7, 187)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 17)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Supplier Price"
        '
        'text_supplier_price
        '
        Me.text_supplier_price.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_supplier_price.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_supplier_price.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_supplier_price.ForeColor = System.Drawing.Color.Black
        Me.text_supplier_price.Location = New System.Drawing.Point(122, 187)
        Me.text_supplier_price.Name = "text_supplier_price"
        Me.text_supplier_price.Size = New System.Drawing.Size(228, 29)
        Me.text_supplier_price.TabIndex = 17
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_done)
        Me.panel_buttons.Location = New System.Drawing.Point(7, 238)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(115, 74)
        Me.panel_buttons.TabIndex = 16
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 12
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(1, 2)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 11
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'label_currency_price
        '
        Me.label_currency_price.AutoEllipsis = False
        Me.label_currency_price.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold)
        Me.label_currency_price.ForeColor = System.Drawing.Color.Black
        Me.label_currency_price.Location = New System.Drawing.Point(101, 185)
        Me.label_currency_price.Name = "label_currency_price"
        Me.label_currency_price.Size = New System.Drawing.Size(26, 30)
        Me.label_currency_price.TabIndex = 25
        Me.label_currency_price.Text = "P"
        Me.label_currency_price.TextAlign = DevaReePharmacy.TextAlignment.Left
        Me.label_currency_price.TextRender = System.Drawing.Text.TextRenderingHint.AntiAlias
        Me.label_currency_price.TextVerticalAlign = DevaReePharmacy.TextVerticalAlignment.Middle
        Me.label_currency_price.VerticalDirection = False
        '
        'form_stock_editor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(374, 316)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_prices)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_stock_editor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock"
        Me.group_product_prices.ResumeLayout(False)
        Me.group_product_prices.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents text_product_name As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents text_stock_quantity As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents text_lot_number As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents date_expiry_date As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents group_product_prices As System.Windows.Forms.GroupBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents text_supplier_price As System.Windows.Forms.TextBox
    Friend WithEvents label_currency_price As DevaReePharmacy.AntiAliasedLabel
    Friend WithEvents label_tip_supplier_price As System.Windows.Forms.Label
    Friend WithEvents combo_unit As System.Windows.Forms.ComboBox
End Class
