﻿Public Class form_user_login_history
    Dim User_Login_HistoryQuerrier As New class_db_login

    Dim userID As Integer = 0
    Dim col_width() As Double = {50, 50}

    Private Sub AdjsutObjects() Handles Me.Resize

        ResizeListViewColumn(list_login_history, col_width)

    End Sub

    Private Sub AdjustInterface() Handles Me.Load
        LoadHistoryList()
    End Sub

    Private Sub LoadHistoryList()

        userID = module_session.current_user_profile.UserID
        Dim UserHistoryLogin = User_Login_HistoryQuerrier.GetUserLoginHistory(userID)
        If Not IsNothing(UserHistoryLogin) Then
            For Each userHistory In UserHistoryLogin
                Dim NewItem As ListViewItem = list_login_history.Items.Add(userHistory("slh_log_time_formatted"))
                NewItem.SubItems.Add(userHistory("slh_log_type"))
            Next
        End If
    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        Me.Close()

    End Sub
End Class