﻿Public Class form_inventory

    Private _product_id As Integer = 0
    Private _col_width As Double() = {25, 25, 10, 5, 5, 10, 10, 10}
    Private _current_search_tag As String = Nothing
    Private _current_offset As Integer
    Private _current_sort_field As String = "product_name"
    Private _current_sort_order As String = "ASC"
    Private sql As New class_db_dashboard
    Private _column_sort As New Dictionary(Of Integer, String)

    Public Function OpenInventory(ByVal parent_form As Form) As Product

        Me.ShowDialog(parent_form)
        If _product_id = 0 Then
            OpenInventory = Nothing
        Else
            OpenInventory = New Product(_product_id)
        End If

    End Function

    Private Sub AdjustObjects() Handles Me.Resize

        ResizeListViewColumn(list_products, _col_width)

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        column_pack_size.Text = SYS_PACK_ALIAS & " Size"
        column_pad_size.Text = SYS_PAD_ALIAS & " Size"
        _column_sort.Add(0, "product_name")
        page_navigator.TotalItems = sql.GetProductStocksCount(Nothing, Nothing)

    End Sub

    Private Sub ReturnSelectedItem()

        If list_products.SelectedItems.Count < 1 Then Exit Sub
        _product_id = Val(list_products.SelectedItems(0).Tag)
        Me.Close()

    End Sub

    Private Sub LoadProducts(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal sort_field As String = "product_name", Optional ByVal sort_order As String = "ASC")

        Dim query = New class_db_dashboard
        Dim products = query.GetProductStocks(search_tag, Nothing, offset, SYS_PAGE_SIZE, sort_field, sort_order)
        _current_search_tag = search_tag
        _current_sort_field = sort_field
        _current_sort_order = sort_order
        _current_offset = offset
        list_products.Items.Clear()
        If Not IsNothing(products) Then
            For Each product In products
                Dim new_item As ListViewItem = list_products.Items.Add(product("product_name"))
                new_item.SubItems.Add(product("product_code"))
                new_item.UseItemStyleForSubItems = False
                new_item.Tag = product("product_id")
                Dim stock_sub_item As New ListViewItem.ListViewSubItem
                Dim stock_quantity = Val(product("stock_quantity"))
                Dim pack_quantity = If(product("packing_size") > 1, Math.Floor(stock_quantity / product("packing_size")), 0)
                Dim pad_quantity = If(product("pads_per_pack") > 1, Math.Floor((stock_quantity - (pack_quantity * product("packing_size"))) / (product("packing_size") / product("pads_per_pack"))), 0)
                Dim unit_quantity = stock_quantity - ((pack_quantity * product("packing_size")) + (pad_quantity * (product("packing_size") / product("pads_per_pack"))))
                stock_sub_item.Text = If(pack_quantity > 0, pack_quantity & " " & SYS_PACK_ALIAS, "")
                If pad_quantity > 0 Then
                    If Not stock_sub_item.Text = "" Then stock_sub_item.Text &= ", "
                    stock_sub_item.Text &= pad_quantity & " " & SYS_PAD_ALIAS
                End If
                If stock_sub_item.Text = "" Or unit_quantity > 0 Then
                    If Not stock_sub_item.Text = "" Then stock_sub_item.Text &= ", "
                    stock_sub_item.Text &= unit_quantity & " " & product("product_unit")
                End If
                If stock_quantity <= 0 Then
                    stock_sub_item.ForeColor = module_colors.color_red
                    stock_sub_item.Font = New Font(list_products.Font, FontStyle.Bold)
                Else
                    stock_sub_item.ForeColor = Color.Black
                End If
                new_item.SubItems.Add(stock_sub_item)
                new_item.SubItems.Add(If(product("packing_size") = 1, "--", product("packing_size") & Space(1) & product("product_unit")))
                new_item.SubItems.Add(If(product("pads_per_pack") = 1, "--", (product("packing_size") / product("pads_per_pack")) & Space(1) & product("product_unit")))
                new_item.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(product("retail_price")), 2, , , TriState.True))
                new_item.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(product("wholesale_price")), 2, , , TriState.True))
                new_item.SubItems.Add(SYS_CURRENCY & FormatNumber(Val(product("distribution_price")), 2, , , TriState.True))

            Next
        End If
    End Sub

    Private Sub list_products_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles list_products.ColumnClick

        If _column_sort.ContainsKey(e.Column) Then
            If _current_sort_field = _column_sort(e.Column) Then
                _current_sort_order = If(_current_sort_order = "ASC", "DESC", "ASC")
            Else
                _current_sort_field = _column_sort(e.Column)
            End If
            page_navigator.TotalItems = sql.GetProductStocksCount(_current_search_tag)
        End If

    End Sub

    Private Sub list_products_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_products.DoubleClick

        ReturnSelectedItem()

    End Sub

    Private Sub button_search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            _current_search_tag = text_search.Text
            page_navigator.TotalItems = sql.GetProductStocksCount(_current_search_tag, Nothing)
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            text_search.Text = ""
            page_navigator.TotalItems = sql.GetProductStocksCount(Nothing, Nothing)
        End If

    End Sub

    Private Sub list_products_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_products.GotFocus

        If list_products.SelectedItems.Count < 1 Then
            If list_products.Items.Count > 0 Then list_products.Items(0).Selected = True
        End If

    End Sub

    Private Sub list_products_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles list_products.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            ReturnSelectedItem()
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            _product_id = 0
            Me.Close()
        End If

    End Sub

    Private Sub text_search_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub page_navigator_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator.PageChanged

        LoadProducts(_current_search_tag, offset, _current_sort_field, _current_sort_order)

    End Sub

    Private Sub list_products_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_products.SelectedIndexChanged

    End Sub

    Private Sub text_search_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_search.TextChanged

    End Sub
End Class