﻿Public Class form_connection_notice

    Private _force_closed As Boolean = False

    Private Sub form_connection_notice_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        e.Cancel = Not module_db_connection.db_connection.Ping() And Not _force_closed

    End Sub

    Private Sub form_connection_notice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        text_server_host.Focus()
        'timer_recheck.Enabled = True
    End Sub

    Private Sub button_connect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_connect.Click

        If Trim(text_server_host.Text) = "" Or Trim(text_server_name.Text) = "" Or Trim(text_server_user.Text = "") Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Critical)
        Else
            Me.Cursor = Cursors.WaitCursor
            button_connect.Enabled = False
            button_reconnect.Enabled = False
            Dim old_host, old_name, old_user, old_pass As String
            old_host = SYS_DB_HOST
            old_name = SYS_DB_NAME
            old_user = SYS_DB_USER
            old_pass = SYS_DB_PASS
            SYS_DB_HOST = text_server_host.Text
            SYS_DB_NAME = text_server_name.Text
            SYS_DB_USER = text_server_user.Text
            SYS_DB_PASS = text_server_pass.Text
            module_db_connection.ConnectToDatabase()
            If Not module_db_connection.Connected Then
                SYS_DB_HOST = old_host
                SYS_DB_NAME = old_name
                SYS_DB_USER = old_user
                SYS_DB_PASS = old_pass
                MsgBox("Could not connect to specified server.")
                button_connect.Enabled = True
                button_reconnect.Enabled = True
            End If
            Me.Cursor = Cursors.Default
        End If

    End Sub

    Private Sub timer_recheck_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_recheck.Tick

        timer_recheck.Enabled = False
        button_connect.Enabled = False
        If module_db_connection.Connected Then
            Me.Close()
        Else
            module_db_connection.ConnectToDatabase()
            If module_db_connection.Connected Then
                Me.Close()
            Else
                timer_recheck.Enabled = True
                button_connect.Enabled = True
            End If
        End If

    End Sub

    Public Sub ForceClose()

        _force_closed = True
        Me.Close()

    End Sub

    Private Sub button_reconnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_reconnect.Click

        Me.Cursor = Cursors.WaitCursor
        button_reconnect.Enabled = False
        button_connect.Enabled = False
        module_db_connection.ConnectToDatabase()
        If Not module_db_connection.Connected Then
            MsgBox("Could not connect to server.")
            button_connect.Enabled = True
            button_reconnect.Enabled = True
        End If
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub button_quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_quit.Click

        Dim ask = MsgBox("Ending the application now will might result to a loss of data. Are you sure you want to quit?", MsgBoxStyle.YesNo)
        If ask = DialogResult.Yes Then End

    End Sub
End Class