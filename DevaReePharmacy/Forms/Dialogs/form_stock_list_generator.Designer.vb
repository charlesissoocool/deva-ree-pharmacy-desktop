﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_stock_list_generator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_stock_list_generator))
        Me.list_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.text_search = New System.Windows.Forms.TextBox()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_save_pdf = New System.Windows.Forms.Button()
        Me.button_invert_selection = New System.Windows.Forms.Button()
        Me.button_print = New System.Windows.Forms.Button()
        Me.button_preview = New System.Windows.Forms.Button()
        Me.button_select_all = New System.Windows.Forms.Button()
        Me.button_search = New System.Windows.Forms.Button()
        Me.group_options = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pdf_generator = New DevaReePharmacy.PDFGenerator()
        Me.panel_buttons.SuspendLayout()
        Me.group_options.SuspendLayout()
        Me.SuspendLayout()
        '
        'list_products
        '
        Me.list_products.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_products.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.list_products.CheckBoxes = True
        Me.list_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3})
        Me.list_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_products.ForeColor = System.Drawing.Color.Black
        Me.list_products.FullRowSelect = True
        Me.list_products.GridLines = True
        Me.list_products.Location = New System.Drawing.Point(14, 69)
        Me.list_products.Name = "list_products"
        Me.list_products.Size = New System.Drawing.Size(480, 302)
        Me.list_products.TabIndex = 13
        Me.list_products.TabStop = False
        Me.list_products.UseCompatibleStateImageBehavior = False
        Me.list_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Product Name"
        Me.ColumnHeader3.Width = 334
        '
        'text_search
        '
        Me.text_search.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_search.ForeColor = System.Drawing.Color.Black
        Me.text_search.Location = New System.Drawing.Point(14, 34)
        Me.text_search.Name = "text_search"
        Me.text_search.Size = New System.Drawing.Size(391, 29)
        Me.text_search.TabIndex = 14
        '
        'panel_buttons
        '
        Me.panel_buttons.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_buttons.Controls.Add(Me.button_save_pdf)
        Me.panel_buttons.Controls.Add(Me.button_invert_selection)
        Me.panel_buttons.Controls.Add(Me.button_print)
        Me.panel_buttons.Controls.Add(Me.button_preview)
        Me.panel_buttons.Controls.Add(Me.button_select_all)
        Me.panel_buttons.Location = New System.Drawing.Point(94, 377)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(367, 71)
        Me.panel_buttons.TabIndex = 15
        '
        'button_save_pdf
        '
        Me.button_save_pdf.BackColor = System.Drawing.Color.Transparent
        Me.button_save_pdf.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_save_pdf.Enabled = False
        Me.button_save_pdf.FlatAppearance.BorderSize = 0
        Me.button_save_pdf.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_save_pdf.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_save_pdf.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_save_pdf.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_save_pdf.Image = CType(resources.GetObject("button_save_pdf.Image"), System.Drawing.Image)
        Me.button_save_pdf.Location = New System.Drawing.Point(225, 3)
        Me.button_save_pdf.Name = "button_save_pdf"
        Me.button_save_pdf.Size = New System.Drawing.Size(86, 65)
        Me.button_save_pdf.TabIndex = 16
        Me.button_save_pdf.Text = "Save PDF"
        Me.button_save_pdf.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_save_pdf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_save_pdf.UseVisualStyleBackColor = True
        '
        'button_invert_selection
        '
        Me.button_invert_selection.BackColor = System.Drawing.Color.Transparent
        Me.button_invert_selection.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_invert_selection.FlatAppearance.BorderSize = 0
        Me.button_invert_selection.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_invert_selection.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_invert_selection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_invert_selection.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_invert_selection.Image = CType(resources.GetObject("button_invert_selection.Image"), System.Drawing.Image)
        Me.button_invert_selection.Location = New System.Drawing.Point(69, 2)
        Me.button_invert_selection.Name = "button_invert_selection"
        Me.button_invert_selection.Size = New System.Drawing.Size(97, 65)
        Me.button_invert_selection.TabIndex = 14
        Me.button_invert_selection.Text = "Invert Selection"
        Me.button_invert_selection.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_invert_selection.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_invert_selection.UseVisualStyleBackColor = True
        '
        'button_print
        '
        Me.button_print.BackColor = System.Drawing.Color.Transparent
        Me.button_print.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_print.Enabled = False
        Me.button_print.FlatAppearance.BorderSize = 0
        Me.button_print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_print.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_print.Image = CType(resources.GetObject("button_print.Image"), System.Drawing.Image)
        Me.button_print.Location = New System.Drawing.Point(301, 2)
        Me.button_print.Name = "button_print"
        Me.button_print.Size = New System.Drawing.Size(63, 65)
        Me.button_print.TabIndex = 13
        Me.button_print.Text = "Print"
        Me.button_print.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_print.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_print.UseVisualStyleBackColor = True
        '
        'button_preview
        '
        Me.button_preview.BackColor = System.Drawing.Color.Transparent
        Me.button_preview.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_preview.Enabled = False
        Me.button_preview.FlatAppearance.BorderSize = 0
        Me.button_preview.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_preview.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_preview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_preview.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_preview.Image = CType(resources.GetObject("button_preview.Image"), System.Drawing.Image)
        Me.button_preview.Location = New System.Drawing.Point(165, 3)
        Me.button_preview.Name = "button_preview"
        Me.button_preview.Size = New System.Drawing.Size(63, 65)
        Me.button_preview.TabIndex = 12
        Me.button_preview.Text = "Preview"
        Me.button_preview.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_preview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_preview.UseVisualStyleBackColor = True
        '
        'button_select_all
        '
        Me.button_select_all.BackColor = System.Drawing.Color.Transparent
        Me.button_select_all.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_select_all.FlatAppearance.BorderSize = 0
        Me.button_select_all.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_select_all.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_select_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_select_all.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_select_all.Image = CType(resources.GetObject("button_select_all.Image"), System.Drawing.Image)
        Me.button_select_all.Location = New System.Drawing.Point(1, 2)
        Me.button_select_all.Name = "button_select_all"
        Me.button_select_all.Size = New System.Drawing.Size(75, 65)
        Me.button_select_all.TabIndex = 11
        Me.button_select_all.Text = "Select All"
        Me.button_select_all.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_select_all.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_select_all.UseVisualStyleBackColor = True
        '
        'button_search
        '
        Me.button_search.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_search.BackColor = System.Drawing.Color.Transparent
        Me.button_search.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_search.FlatAppearance.BorderSize = 0
        Me.button_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_search.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_search.Image = CType(resources.GetObject("button_search.Image"), System.Drawing.Image)
        Me.button_search.Location = New System.Drawing.Point(411, 29)
        Me.button_search.Name = "button_search"
        Me.button_search.Size = New System.Drawing.Size(83, 34)
        Me.button_search.TabIndex = 18
        Me.button_search.Text = "Search"
        Me.button_search.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.button_search.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.button_search.UseVisualStyleBackColor = True
        '
        'group_options
        '
        Me.group_options.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_options.Controls.Add(Me.panel_buttons)
        Me.group_options.Controls.Add(Me.button_search)
        Me.group_options.Controls.Add(Me.list_products)
        Me.group_options.Controls.Add(Me.text_search)
        Me.group_options.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_options.ForeColor = System.Drawing.Color.Black
        Me.group_options.Location = New System.Drawing.Point(9, 5)
        Me.group_options.Name = "group_options"
        Me.group_options.Size = New System.Drawing.Size(511, 467)
        Me.group_options.TabIndex = 19
        Me.group_options.TabStop = False
        Me.group_options.Text = "Select Products"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(868, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Preview"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pdf_generator
        '
        Me.pdf_generator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pdf_generator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pdf_generator.CSS = CType(resources.GetObject("pdf_generator.CSS"), System.Collections.Generic.Dictionary(Of String, String))
        Me.pdf_generator.HTML = Nothing
        Me.pdf_generator.Location = New System.Drawing.Point(526, 20)
        Me.pdf_generator.Name = "pdf_generator"
        Me.pdf_generator.Orientation = DevaReePharmacy.PDF_Orientation.Portrait
        Me.pdf_generator.Size = New System.Drawing.Size(388, 452)
        Me.pdf_generator.TabIndex = 22
        Me.pdf_generator.Title = "Document"
        '
        'form_stock_list_generator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(926, 484)
        Me.Controls.Add(Me.pdf_generator)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.group_options)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "form_stock_list_generator"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock List Generator"
        Me.panel_buttons.ResumeLayout(False)
        Me.group_options.ResumeLayout(False)
        Me.group_options.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents list_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents text_search As System.Windows.Forms.TextBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_select_all As System.Windows.Forms.Button
    Friend WithEvents button_print As System.Windows.Forms.Button
    Friend WithEvents button_preview As System.Windows.Forms.Button
    Friend WithEvents button_search As System.Windows.Forms.Button
    Friend WithEvents group_options As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pdf_generator As DevaReePharmacy.PDFGenerator
    Friend WithEvents button_invert_selection As System.Windows.Forms.Button
    Friend WithEvents button_save_pdf As System.Windows.Forms.Button
End Class
