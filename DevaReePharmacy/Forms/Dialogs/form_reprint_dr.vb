﻿Public Class form_reprint_dr

    Private sql_transaction As New class_db_transaction

    Private Sub form_reprint_dr_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then
            button_cancel.PerformClick()
            e.Handled = True
        End If

    End Sub

    Private Sub form_reprint_dr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        If e.KeyChar = ChrW(Keys.Escape) Then
            button_cancel.PerformClick()
            e.Handled = True
        End If

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        LoadCustomerList()

    End Sub

    Private Sub LoadCustomerList()

        Dim customer_list = sql_transaction.GetCustomerList()
        If Not IsNothing(customer_list) Then
            Dim source As New AutoCompleteStringCollection
            For Each customer_detail In customer_list
                source.Add(customer_detail("c_name"))
            Next
            text_customer_name.AutoCompleteCustomSource = source
        Else
            text_customer_name.AutoCompleteCustomSource = Nothing
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub

    Private Sub button_lookup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_lookup.Click

        If text_customer_name.Text = "" Or text_dr_no.Text = "" Or Not IsNumeric(text_dr_no.Text) Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Critical)
        Else
            Dim transaction_id = sql_transaction.GetTransactionIDByCriteria(sql_transaction.GetCustomerIDByName(text_customer_name.Text), date_transacted.Value, Val(text_dr_no.Text))
            If transaction_id > 0 Then
                Dim dr_print_form As New form_delivery_receipt_generator
                dr_print_form.OpenTransactionReportGeneratorDialog(Me, New Transaction(transaction_id))
            Else
                MsgBox("Could not find any Delivery Receipt matching the criteria.", MsgBoxStyle.Exclamation)
            End If
        End If

    End Sub
End Class