﻿Public Class form_product_editor

    Dim sql As New class_db_products
    Dim _product_details As Product
    Dim _checked As Boolean = False

    Public WriteOnly Property EnableDetails() As Boolean
        Set(ByVal value As Boolean)
            group_product_details.Enabled = value
        End Set
    End Property

    Public WriteOnly Property EnablePrices() As Boolean
        Set(ByVal value As Boolean)
            group_product_prices.Enabled = value
        End Set
    End Property

    Private Sub AdjustInterface() Handles Me.Load

        Dim autocomplete As New AutoCompleteStringCollection
        Dim brands = sql.GetBrandList()
        If Not IsNothing(brands) Then
            For Each brand In brands
                autocomplete.Add(brand("pb_brand_name"))
            Next
            text_product_brand.AutoCompleteCustomSource = autocomplete
        End If
        autocomplete = New AutoCompleteStringCollection
        Dim units = sql.GetUnitList
        If Not IsNothing(units) Then
            For Each unit In units
                autocomplete.Add(unit("pu_product_unit"))
            Next
            text_product_unit.AutoCompleteCustomSource = autocomplete
        End If
        autocomplete = New AutoCompleteStringCollection
        Dim categories = sql.GetProductCategoryList()
        If Not IsNothing(categories) Then
            For Each category In categories
                autocomplete.Add(category("pc_category_name"))
            Next
            text_product_category.AutoCompleteCustomSource = autocomplete
        End If

        For Each grp In Me.Controls.OfType(Of GroupBox)()
            For Each obj In grp.Controls.OfType(Of TextBox)()
                If obj Is text_product_category Then Continue For
                text_box_TextChanged(obj, Nothing)
            Next
        Next

        label_unit_max_char.Text = "Max: " & text_product_unit.MaxLength
        label_unit_max_char.ForeColor = module_colors.color_red
        label_packing_size.Text = SYS_PACK_ALIAS & " Size"
        label_pads_per_pack.Text = SYS_PAD_ALIAS & " per " & SYS_PACK_ALIAS
        checkbox_no_pack.Text = "Not sold in " & SYS_PACK_ALIAS
        checkbox_no_pad.Text = "Not sold in " & SYS_PAD_ALIAS
        label_tip_sold_in_packs.Text = "(Check if product isn't sold in " & SYS_PACK_ALIAS & ")"
        label_tip_sold_in_packs.Text = "(Check if product isn't sold in " & SYS_PAD_ALIAS & ")"
        label_tip_packing_size.Text = "(No. of units per " & SYS_PACK_ALIAS & ")"
        label_currency_retail.Text = SYS_CURRENCY
        label_currency_wholesale.Text = SYS_CURRENCY
        label_currency_distribution.Text = SYS_CURRENCY

    End Sub

    Public Function OpenProductEditor(ByVal parent_form As Form, Optional ByVal product_details As Product = Nothing) As Product

        _product_details = New Product

        If IsNothing(product_details) Then
            _product_details = New Product
        Else
            _product_details = product_details
            group_packing_information.Enabled = Not sql.IsProductInFlow(_product_details.ProductID)
        End If

        text_product_category.Text = If(IsNothing(_product_details.ProductCategory), "", _product_details.ProductCategory.CategoryName)
        text_product_name.Text = _product_details.ProductName
        text_product_description.Text = _product_details.ProductDescription
        text_product_brand.Text = _product_details.ProductBrand
        text_product_unit.Text = _product_details.ProductUnit
        text_product_code.Text = _product_details.ProductCode
        text_retail_price.Text = FormatNumber(_product_details.PackPrice.Retail, 2, , , TriState.True)
        text_wholesale_price.Text = FormatNumber(_product_details.PackPrice.Wholesale, 2, , , TriState.True)
        text_distribution_price.Text = FormatNumber(_product_details.PackPrice.Distribution, 2, , , TriState.True)
        text_packing_size.Text = _product_details.PackingSize
        text_pads_per_pack.Text = _product_details.PadsPerPack
        checkbox_no_pack.Checked = Not _product_details.IsSoldInPacks
        checkbox_no_pad.Checked = Not _product_details.IsSoldInPads
        checkbox_no_pack_CheckedChanged(Nothing, Nothing)

        Me.ShowDialog(parent_form)
        OpenProductEditor = If(_checked, _product_details, Nothing)

    End Function

    Private Sub text_box_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_product_name.TextChanged, text_product_brand.TextChanged, text_product_unit.TextChanged, text_product_description.TextChanged, text_product_code.TextChanged, text_packing_size.TextChanged, text_pads_per_pack.TextChanged, text_retail_price.TextChanged, text_wholesale_price.TextChanged, text_distribution_price.TextChanged

        sender.BackColor = If(Trim(sender.text) = "", module_colors.color_red, Color.White)
        sender.Forecolor = If(Trim(sender.text) = "", Color.White, Color.Black)
        If (sender Is text_retail_price Or sender Is text_wholesale_price Or sender Is text_distribution_price) Then
            sender.BackColor = If(Val(sender.Text) <= 0, module_colors.color_red, Color.White)
            sender.Forecolor = If(Val(sender.Text) <= 0, Color.White, Color.Black)
        ElseIf sender Is text_product_unit And (Trim(sender.Text) = SYS_PACK_ALIAS Or Trim(sender.Text) = SYS_PAD_ALIAS) Then
            sender.BackColor = module_colors.color_red
            sender.ForeColor = Color.White
        End If

    End Sub

    Private Sub button_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_save.Click

        If Trim(text_product_name.Text) = "" Or Trim(text_product_brand.Text) = "" Or Trim(text_product_unit.Text) = "" Or Trim(text_product_code.Text) = "" Or Trim(text_product_description.Text) = "" Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Exclamation)
        ElseIf Val(text_retail_price.Text) <= 0 Or Val(text_wholesale_price.Text) <= 0 Or Val(text_distribution_price.Text) <= 0 Or Val(text_packing_size.Text) <= 0 Or Val(text_packing_size.Text) <= 0 Then
            MsgBox("Some prices are invalid.", MsgBoxStyle.Exclamation)
        ElseIf Val(text_packing_size.Text) Mod Val(text_pads_per_pack.Text) <> 0 Then
            MsgBox("Invalid packing information.", MsgBoxStyle.Exclamation)
        ElseIf Trim(text_product_unit.Text) = SYS_PACK_ALIAS Or Trim(text_product_unit.Text) = SYS_PAD_ALIAS Then
            MsgBox("Unit '" & SYS_PACK_ALIAS & "' or '" & SYS_PAD_ALIAS & "' is restricted for use in products.", MsgBoxStyle.Exclamation)
        ElseIf (_product_details.ProductID = 0 And sql.IsProductCodeTaken(Trim(text_product_code.Text))) Or (_product_details.ProductID > 0 And Not _product_details.ProductCode = Trim(text_product_code.Text) And sql.IsProductCodeTaken(Trim(text_product_code.Text))) Then
            MsgBox("Product code is already taken.", MsgBoxStyle.Exclamation)
        Else
            Dim category_id As Integer = sql.GetCategoryIDByName(Trim(text_product_category.Text))
            Dim category_detail As New Product_Category
            If Trim(text_product_category.Text) = "" Then
                category_detail = Nothing
            ElseIf category_id = 0 Then
                If Not MsgBox("Category '" & Trim(text_product_category.Text) & "' does not exist. Create and proceed?", MsgBoxStyle.YesNo) = DialogResult.Yes Then Exit Sub
                category_detail.CategoryName = text_product_category.Text
            Else
                category_detail = New Product_Category(category_id)
            End If

            _product_details.ProductCategory = category_detail
            _product_details.ProductName = text_product_name.Text
            _product_details.ProductBrand = text_product_brand.Text
            _product_details.ProductCode = text_product_code.Text
            _product_details.ProductDescription = text_product_description.Text
            _product_details.ProductUnit = text_product_unit.Text
            _product_details.PackPrice.Retail = Val(ClearNumberFormat(text_retail_price.Text))
            _product_details.PackPrice.Wholesale = Val(ClearNumberFormat(text_wholesale_price.Text))
            _product_details.PackPrice.Distribution = Val(ClearNumberFormat(text_distribution_price.Text))
            _product_details.PackingSize = Val(ClearNumberFormat(text_packing_size.Text))
            _product_details.PadsPerPack = Val(ClearNumberFormat(text_pads_per_pack.Text))
            _checked = True
            Me.Close()
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.Close()

    End Sub

    Private Sub checkbox_no_pack_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkbox_no_pack.CheckedChanged

        text_packing_size.Enabled = Not checkbox_no_pack.Checked
        text_pads_per_pack.Enabled = text_packing_size.Enabled
        If checkbox_no_pack.Checked Then
            text_packing_size.Text = "1"
            text_pads_per_pack.Text = "1"
            group_product_prices.Text = "Unit Prices"
            checkbox_no_pad.Enabled = False
            checkbox_no_pad.Checked = True
        Else
            checkbox_no_pad.Enabled = True
            checkbox_no_pad.Checked = False
            group_product_prices.Text = SYS_PACK_ALIAS & " Prices"
        End If

    End Sub

    Private Sub labels_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles checkbox_no_pack.SizeChanged, label_pads_per_pack.SizeChanged, label_packing_size.SizeChanged, label_tip_packing_size.SizeChanged, label_tip_sold_in_pads.SizeChanged

        If label_tip_packing_size.Right > label_packing_size.Right Then
            text_packing_size.Left = label_tip_packing_size.Left + label_tip_packing_size.Width + 2
        Else
            text_packing_size.Left = label_packing_size.Left + label_packing_size.Width + 2
        End If
        label_pads_per_pack.Left = text_packing_size.Right + 2
        label_tip_sold_in_pads.Left = label_pads_per_pack.Left
        checkbox_no_pad.Left = label_pads_per_pack.Left
        text_pads_per_pack.Left = label_pads_per_pack.Right + 2
        If label_tip_sold_in_pads.Right > text_pads_per_pack.Right Then
            group_packing_information.Width = label_tip_sold_in_pads.Right + 11
        Else
            group_packing_information.Width = text_pads_per_pack.Right + 11
        End If
        Me.Width = group_packing_information.Right + group_packing_information.Left
        group_packing_information.Width = group_product_details.Width

    End Sub

    Private Sub label_tip_sold_in_packs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_tip_sold_in_packs.Click

    End Sub

    Private Sub checkbox_no_pad_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkbox_no_pad.CheckedChanged

        If checkbox_no_pad.Checked Then
            text_pads_per_pack.Text = "1"
            text_pads_per_pack.Enabled = False
        Else
            text_pads_per_pack.Enabled = True
        End If

    End Sub
End Class