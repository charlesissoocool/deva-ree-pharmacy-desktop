﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_manifest_selector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_manifest_selector))
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.text_manifest_code = New System.Windows.Forms.TextBox()
        Me.text_supplier_name = New System.Windows.Forms.TextBox()
        Me.panel_general_controls = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_ok = New System.Windows.Forms.Button()
        Me.group_product_details.SuspendLayout()
        Me.panel_general_controls.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_product_details
        '
        Me.group_product_details.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.text_manifest_code)
        Me.group_product_details.Controls.Add(Me.text_supplier_name)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(7, 2)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(385, 116)
        Me.group_product_details.TabIndex = 2
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Manifest Information"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 17)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Manifest Code"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Supplier Name"
        '
        'text_manifest_code
        '
        Me.text_manifest_code.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_manifest_code.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_manifest_code.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_manifest_code.Enabled = False
        Me.text_manifest_code.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_manifest_code.ForeColor = System.Drawing.Color.Black
        Me.text_manifest_code.Location = New System.Drawing.Point(109, 74)
        Me.text_manifest_code.MaxLength = 50
        Me.text_manifest_code.Name = "text_manifest_code"
        Me.text_manifest_code.Size = New System.Drawing.Size(261, 29)
        Me.text_manifest_code.TabIndex = 2
        '
        'text_supplier_name
        '
        Me.text_supplier_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_supplier_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_supplier_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_supplier_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_supplier_name.ForeColor = System.Drawing.Color.Black
        Me.text_supplier_name.Location = New System.Drawing.Point(109, 34)
        Me.text_supplier_name.Name = "text_supplier_name"
        Me.text_supplier_name.Size = New System.Drawing.Size(261, 29)
        Me.text_supplier_name.TabIndex = 1
        '
        'panel_general_controls
        '
        Me.panel_general_controls.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.panel_general_controls.Controls.Add(Me.button_cancel)
        Me.panel_general_controls.Controls.Add(Me.button_ok)
        Me.panel_general_controls.Location = New System.Drawing.Point(8, 122)
        Me.panel_general_controls.Name = "panel_general_controls"
        Me.panel_general_controls.Size = New System.Drawing.Size(115, 74)
        Me.panel_general_controls.TabIndex = 18
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(59, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 4
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_ok
        '
        Me.button_ok.BackColor = System.Drawing.Color.Transparent
        Me.button_ok.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_ok.Enabled = False
        Me.button_ok.FlatAppearance.BorderSize = 0
        Me.button_ok.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_ok.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_ok.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_ok.Image = CType(resources.GetObject("button_ok.Image"), System.Drawing.Image)
        Me.button_ok.Location = New System.Drawing.Point(1, 2)
        Me.button_ok.Name = "button_ok"
        Me.button_ok.Size = New System.Drawing.Size(53, 65)
        Me.button_ok.TabIndex = 3
        Me.button_ok.Text = "Ok"
        Me.button_ok.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_ok.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_ok.UseVisualStyleBackColor = True
        '
        'form_manifest_selector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(404, 196)
        Me.Controls.Add(Me.panel_general_controls)
        Me.Controls.Add(Me.group_product_details)
        Me.ForeColor = System.Drawing.Color.Black
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "form_manifest_selector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Select Manifest"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.panel_general_controls.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents text_manifest_code As System.Windows.Forms.TextBox
    Friend WithEvents text_supplier_name As System.Windows.Forms.TextBox
    Friend WithEvents panel_general_controls As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_ok As System.Windows.Forms.Button
End Class
