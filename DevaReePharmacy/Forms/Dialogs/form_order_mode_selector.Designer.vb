﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_order_mode_selector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.radio_retail = New System.Windows.Forms.RadioButton()
        Me.radio_wholesale = New System.Windows.Forms.RadioButton()
        Me.radio_distribution = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'radio_retail
        '
        Me.radio_retail.Font = New System.Drawing.Font("Open Sans", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radio_retail.ForeColor = System.Drawing.Color.Black
        Me.radio_retail.Location = New System.Drawing.Point(7, 4)
        Me.radio_retail.Name = "radio_retail"
        Me.radio_retail.Size = New System.Drawing.Size(87, 40)
        Me.radio_retail.TabIndex = 1
        Me.radio_retail.Text = "Retail"
        Me.radio_retail.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.radio_retail.UseVisualStyleBackColor = True
        '
        'radio_wholesale
        '
        Me.radio_wholesale.Font = New System.Drawing.Font("Open Sans", 12.0!)
        Me.radio_wholesale.ForeColor = System.Drawing.Color.Black
        Me.radio_wholesale.Location = New System.Drawing.Point(7, 39)
        Me.radio_wholesale.Name = "radio_wholesale"
        Me.radio_wholesale.Size = New System.Drawing.Size(125, 40)
        Me.radio_wholesale.TabIndex = 2
        Me.radio_wholesale.Text = "Wholesale"
        Me.radio_wholesale.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.radio_wholesale.UseVisualStyleBackColor = True
        '
        'radio_distribution
        '
        Me.radio_distribution.Font = New System.Drawing.Font("Open Sans", 12.0!)
        Me.radio_distribution.ForeColor = System.Drawing.Color.Black
        Me.radio_distribution.Location = New System.Drawing.Point(7, 75)
        Me.radio_distribution.Name = "radio_distribution"
        Me.radio_distribution.Size = New System.Drawing.Size(136, 40)
        Me.radio_distribution.TabIndex = 3
        Me.radio_distribution.Text = "Distribution"
        Me.radio_distribution.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.radio_distribution.UseVisualStyleBackColor = True
        '
        'form_order_mode_selector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(140, 120)
        Me.Controls.Add(Me.radio_distribution)
        Me.Controls.Add(Me.radio_wholesale)
        Me.Controls.Add(Me.radio_retail)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_order_mode_selector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Order Mode"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents radio_retail As System.Windows.Forms.RadioButton
    Friend WithEvents radio_wholesale As System.Windows.Forms.RadioButton
    Friend WithEvents radio_distribution As System.Windows.Forms.RadioButton
End Class
