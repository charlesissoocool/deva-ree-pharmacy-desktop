﻿
Public Class form_stock_list_generator

    Private sql As New class_db_products

    Private Sub AdjustInterface() Handles Me.Load

        LoadProductNamesList()

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        panel_buttons.Left = (group_options.Width / 2) - (panel_buttons.Width / 2)
        ResizeListViewColumn(list_products, {100})

    End Sub

    Private Sub LoadProductNamesList(Optional ByVal search_tag As String = "")

        RemoveUnchecked()

        Dim names = sql.GetProductNames(search_tag)
        If Not IsNothing(names) Then
            For Each product_detail In names
                If Not IsProductOnList(product_detail("product_name")) Then
                    Dim list_row As ListViewItem = list_products.Items.Add(product_detail("product_name"))
                    list_row.Tag = product_detail("product_id")
                End If
            Next
        End If

    End Sub

    Private Function IsProductOnList(ByVal product_name As String) As Boolean

        For Each list_row As ListViewItem In list_products.Items
            If list_row.Text = product_name Then
                IsProductOnList = True
                Exit Function
            End If
        Next

        IsProductOnList = False

    End Function

    Private Sub RemoveUnchecked()

        For Each list_row As ListViewItem In list_products.Items
            If Not list_row.Checked Then list_row.Remove()
        Next

    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) And Not Trim(text_search.Text) = "" Then
            e.Handled = True
            button_search.PerformClick()
        End If

    End Sub

    Private Sub text_search_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_search.TextChanged

    End Sub

    Private Sub button_select_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_select_all.Click

        For Each list_row As ListViewItem In list_products.Items
            If Not list_row.Checked Then list_row.Checked = True
        Next

    End Sub

    Private Sub button_search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_search.Click

        LoadProductNamesList(Trim(text_search.Text))

    End Sub

    Private Sub GenerateHTML()

        list_products.Sorting = SortOrder.Ascending
        list_products.ListViewItemSorter = New ListViewComparerString(0)
        list_products.Sort()

        Dim css_list As New Dictionary(Of String, String)
        css_list.Add("table.product-list", "width: 100%")
        css_list.Add("table.product-list th", "font-size: 15px; font-style: bold")
        css_list.Add("table.product-list td", "border-bottom: 1px dashed grey; font-size: 16px")
        css_list.Add("table.product-list tr", "page-break-inside: avoid")
        'css_list.Add("thead, tfoot", "display: table-row-group")

        pdf_generator.Title = "Stock List"
        pdf_generator.CSS = css_list
        Dim html As String = ""
        html &= "<table class='product-list'><thead><tr><th align='left'>Product Name</th><th align='center'>Product Price</th></tr></thead><tbody>"
        RemoveUnchecked()

        Dim listProd_id As New List(Of Integer)
        For Each list_row As ListViewItem In list_products.CheckedItems
            Dim prod_id = list_row.Tag
            listProd_id.Add(prod_id)
        Next

        Dim stock_products As New class_db_stocks
        Dim stocks = stock_products.GetInventoryForProducts(listProd_id)
        For Each stock In stocks
            Dim product_detail As New Product(stock("product_id"))
            Dim stock_quantity As String
            If product_detail.IsSoldInPacks Then
                Dim rem_pack As Double = Math.Floor(stock("stock_quantity") / product_detail.PackingSize)
                Dim rem_stock As Double = stock("stock_quantity") - (rem_pack * product_detail.PackingSize)
                stock_quantity = FormatNumber(rem_pack, 0, , , TriState.True) & " " & SYS_PACK_ALIAS & If(rem_stock > 0, " and " & FormatNumber(rem_stock, If(SYS_DECIMAL_STOCKS, 2, 0), , , TriState.True) & " " & product_detail.ProductUnit, "")
            Else
                stock_quantity = FormatNumber(stock("stock_quantity"), If(SYS_DECIMAL_STOCKS, 2, 0), , , TriState.True) & " " & product_detail.ProductUnit
            End If
            html &= "<tr><td align='left' valign='top'>" & stock("product_name") & "</td><td align='center'>" & stock_quantity & "</td></tr>"
        Next

        html &= "</tbody></table>"
        pdf_generator.HTML = html
        pdf_generator.ShowPreview()

        list_products.Sorting = SortOrder.None
        list_products.ListViewItemSorter = Nothing

    End Sub

    Private Sub button_preview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_preview.Click

        If list_products.CheckedItems.Count < 1 Then
            button_preview.Enabled = False
            MsgBox("Must check at least one product to include in price list.", MsgBoxStyle.Exclamation)
        Else
            GenerateHTML()
        End If

    End Sub

    Private Sub button_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_print.Click

        If list_products.CheckedItems.Count < 1 Then
            button_preview.Enabled = False
            MsgBox("Must check at least one product to include in price list.", MsgBoxStyle.Exclamation)
        Else
            GenerateHTML()
            pdf_generator.Print()
        End If

    End Sub

    Private Sub button_invert_selection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_invert_selection.Click

        For Each list_item As ListViewItem In list_products.Items
            list_item.Checked = Not list_item.Checked
        Next

    End Sub

    Private Sub list_products_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles list_products.ItemChecked

        Try
            button_preview.Enabled = list_products.CheckedItems.Count > 0
            button_save_pdf.Enabled = button_preview.Enabled
            button_print.Enabled = button_preview.Enabled
        Catch ex As Exception
        End Try

    End Sub

    Private Sub list_products_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_products.SelectedIndexChanged

    End Sub

    Private Sub button_save_pdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_save_pdf.Click

        If list_products.CheckedItems.Count < 1 Then
            button_preview.Enabled = False
            MsgBox("Must check at least one product to include in price list.", MsgBoxStyle.Exclamation)
        Else
            GenerateHTML()
            pdf_generator.SavePrinterFriendly()
        End If

    End Sub
End Class