﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_user_login_history
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_user_login_history))
        Me.list_login_history = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.button_done = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'list_login_history
        '
        Me.list_login_history.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_login_history.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.list_login_history.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_login_history.ForeColor = System.Drawing.Color.Black
        Me.list_login_history.FullRowSelect = True
        Me.list_login_history.GridLines = True
        Me.list_login_history.Location = New System.Drawing.Point(0, 0)
        Me.list_login_history.Name = "list_login_history"
        Me.list_login_history.Size = New System.Drawing.Size(525, 303)
        Me.list_login_history.TabIndex = 7
        Me.list_login_history.UseCompatibleStateImageBehavior = False
        Me.list_login_history.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Date and Time"
        Me.ColumnHeader1.Width = 250
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Type"
        Me.ColumnHeader2.Width = 100
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(8, 308)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 12
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'form_user_login_history
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(525, 379)
        Me.Controls.Add(Me.button_done)
        Me.Controls.Add(Me.list_login_history)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_user_login_history"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login History"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents list_login_history As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_done As System.Windows.Forms.Button
End Class
