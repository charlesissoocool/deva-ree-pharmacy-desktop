﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_stock_migrate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_stock_migrate))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.group_product_prices = New System.Windows.Forms.GroupBox()
        Me.text_transfer_reason = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.text_stock_quantity = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.label_lot_number = New System.Windows.Forms.Label()
        Me.label_product_name = New System.Windows.Forms.Label()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.label_unit = New System.Windows.Forms.Label()
        Me.group_product_prices.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(8, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Product Name"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(7, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 17)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Lot / Batch No."
        '
        'group_product_prices
        '
        Me.group_product_prices.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_product_prices.Controls.Add(Me.label_unit)
        Me.group_product_prices.Controls.Add(Me.text_transfer_reason)
        Me.group_product_prices.Controls.Add(Me.Label3)
        Me.group_product_prices.Controls.Add(Me.text_stock_quantity)
        Me.group_product_prices.Controls.Add(Me.Label2)
        Me.group_product_prices.Controls.Add(Me.label_lot_number)
        Me.group_product_prices.Controls.Add(Me.label_product_name)
        Me.group_product_prices.Controls.Add(Me.Label1)
        Me.group_product_prices.Controls.Add(Me.Label4)
        Me.group_product_prices.Font = New System.Drawing.Font("Segoe UI Light", 15.75!)
        Me.group_product_prices.ForeColor = System.Drawing.Color.Black
        Me.group_product_prices.Location = New System.Drawing.Point(7, 5)
        Me.group_product_prices.Name = "group_product_prices"
        Me.group_product_prices.Size = New System.Drawing.Size(368, 227)
        Me.group_product_prices.TabIndex = 15
        Me.group_product_prices.TabStop = False
        Me.group_product_prices.Text = "Stock Information"
        '
        'text_transfer_reason
        '
        Me.text_transfer_reason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_transfer_reason.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_transfer_reason.ForeColor = System.Drawing.Color.Black
        Me.text_transfer_reason.Location = New System.Drawing.Point(130, 120)
        Me.text_transfer_reason.Multiline = True
        Me.text_transfer_reason.Name = "text_transfer_reason"
        Me.text_transfer_reason.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.text_transfer_reason.Size = New System.Drawing.Size(228, 98)
        Me.text_transfer_reason.TabIndex = 35
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(8, 120)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 17)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Migrate Reason"
        '
        'text_stock_quantity
        '
        Me.text_stock_quantity.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_stock_quantity.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_stock_quantity.ForeColor = System.Drawing.Color.Black
        Me.text_stock_quantity.Location = New System.Drawing.Point(130, 84)
        Me.text_stock_quantity.Name = "text_stock_quantity"
        Me.text_stock_quantity.Size = New System.Drawing.Size(151, 29)
        Me.text_stock_quantity.TabIndex = 32
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(8, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 17)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Quantity"
        '
        'label_lot_number
        '
        Me.label_lot_number.AutoSize = True
        Me.label_lot_number.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.label_lot_number.Location = New System.Drawing.Point(126, 58)
        Me.label_lot_number.Name = "label_lot_number"
        Me.label_lot_number.Size = New System.Drawing.Size(22, 21)
        Me.label_lot_number.TabIndex = 30
        Me.label_lot_number.Text = "--"
        '
        'label_product_name
        '
        Me.label_product_name.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.label_product_name.Location = New System.Drawing.Point(126, 34)
        Me.label_product_name.Name = "label_product_name"
        Me.label_product_name.Size = New System.Drawing.Size(232, 21)
        Me.label_product_name.TabIndex = 29
        Me.label_product_name.Text = "--"
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_done)
        Me.panel_buttons.Location = New System.Drawing.Point(130, 238)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(115, 74)
        Me.panel_buttons.TabIndex = 16
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 12
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(1, 2)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 11
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'label_unit
        '
        Me.label_unit.AutoSize = True
        Me.label_unit.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.label_unit.Location = New System.Drawing.Point(287, 90)
        Me.label_unit.Name = "label_unit"
        Me.label_unit.Size = New System.Drawing.Size(44, 17)
        Me.label_unit.TabIndex = 36
        Me.label_unit.Text = "boxes"
        '
        'form_stock_migrate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(383, 311)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_prices)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_stock_migrate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock Migrate"
        Me.group_product_prices.ResumeLayout(False)
        Me.group_product_prices.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents group_product_prices As System.Windows.Forms.GroupBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
    Friend WithEvents label_product_name As System.Windows.Forms.Label
    Friend WithEvents label_lot_number As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents text_stock_quantity As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents text_transfer_reason As System.Windows.Forms.TextBox
    Friend WithEvents label_unit As System.Windows.Forms.Label
End Class
