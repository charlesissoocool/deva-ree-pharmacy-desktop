﻿Public Class form_user_login

    Dim _user_id As Integer = 0

    Public Function ShowLoginDialog(ByVal parent_form As Form, Optional ByVal message As String = "Please enter your login credentials") As User

        _user_id = 0
        text_username.Text = ""
        text_password.Text = ""
        prompt_login.Message = message
        prompt_login.Mode = PromptLabel.PromptLabelMode.Info
        Me.ShowDialog(parent_form)
        If _user_id = 0 Then
            ShowLoginDialog = Nothing
        Else
            ShowLoginDialog = New User(_user_id)
        End If

    End Function

    Private Sub button_login_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_login.Click

        If Trim(text_username.Text) = "" Then
            text_username.Focus()
            prompt_login.Message = "Please enter username."
            prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
            Exit Sub
        End If

        If Trim(text_password.Text) = "" Then
            text_password.Focus()
            prompt_login.Message = "Please enter password."
            prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
            Exit Sub
        End If

        Dim sql_login As New class_db_user
        Dim user_id = sql_login.GetUserIDByCredential(text_username.Text, text_password.Text)
        If user_id = 0 Then
            prompt_login.Message = "Invalid username or password."
            prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
        Else
            _user_id = user_id
            Me.Close()
        End If

    End Sub

    Private Sub text_username_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_username.GotFocus

        text_username.SelectAll()

    End Sub

    Private Sub text_username_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_username.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            text_password.Focus()
        End If

    End Sub

    Private Sub text_username_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_username.TextChanged

    End Sub

    Private Sub text_password_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_password.GotFocus

        text_password.SelectAll()

    End Sub

    Private Sub text_password_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_password.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            button_login.PerformClick()
        End If

    End Sub

    Private Sub text_password_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_password.TextChanged

    End Sub
End Class