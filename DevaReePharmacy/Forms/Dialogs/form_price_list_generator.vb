﻿
Public Class form_price_list_generator

    Private _sql_print As New class_db_print
    Private _sql_products As New class_db_products

    Private Sub AdjustInterface() Handles Me.Load

        combo_price.SelectedIndex = 0
        LoadProductNamesList()

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        panel_buttons.Left = (group_options.Width / 2) - (panel_buttons.Width / 2)
        ResizeListViewColumn(list_products, {50, 50})

    End Sub

    Private Sub LoadProductNamesList(Optional ByVal search_tag As String = "")

        RemoveUnchecked()

        Dim names = _sql_products.GetProductNames(search_tag)
        If Not IsNothing(names) Then
            For Each product_detail In names
                If Not IsProductOnList(product_detail("product_name")) Then
                    Dim list_row As ListViewItem = list_products.Items.Add(product_detail("product_name"))
                    list_row.SubItems.Add(product_detail("category_name"))
                    list_row.Tag = product_detail("product_id")
                End If
            Next
        End If

    End Sub

    Private Function IsProductOnList(ByVal product_name As String) As Boolean

        For Each list_row As ListViewItem In list_products.Items
            If list_row.Text = product_name Then
                IsProductOnList = True
                Exit Function
            End If
        Next

        IsProductOnList = False

    End Function

    Private Sub RemoveUnchecked()

        For Each list_row As ListViewItem In list_products.Items
            If Not list_row.Checked Then list_row.Remove()
        Next

    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) And Not Trim(text_search.Text) = "" Then
            e.Handled = True
            button_search.PerformClick()
        End If

    End Sub

    Private Sub button_select_all_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_select_all.Click

        For Each list_row As ListViewItem In list_products.Items
            If Not list_row.Checked Then list_row.Checked = True
        Next

    End Sub

    Private Sub button_search_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_search.Click

        LoadProductNamesList(Trim(text_search.Text))

    End Sub

    Private Sub GenerateHTML()

        Dim css_list As New Dictionary(Of String, String)
        css_list.Add("table.product-list", "width: 100%; border-bottom: 1px solid black")
        css_list.Add("table.product-list th", "font-size: 14px; font-style: bold")
        css_list.Add("table.product-list td", "font-size: 14px")
        css_list.Add("table.product-list td.content, table.product-list th.content-title", "width: 16.67%; text-align: center; border: 1px dashed grey; -ms-word-break: break-all; word-break: break-all; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; -ms-hyphens: auto; hyphens: auto")
        css_list.Add("table.product-list td.divider-right, table.product-list th.divider-right", "border-right: 1px solid black")
        css_list.Add("table.product-list td.divider-left, table.product-list th.divider-left", "border-left: 1px solid black")
        css_list.Add("table.product-list th.category-title", "background-color: grey; color: white")
        css_list.Add("tr", "page-break-inside: avoid !important")
        'css_list.Add("thead, tfoot", "display: table-row-group")

        pdf_generator.Title = "Product List"
        pdf_generator.CSS = css_list
        Dim html As String = ""

        Dim filter_ids As New List(Of Integer)
        For Each list_row As ListViewItem In list_products.CheckedItems
            filter_ids.Add(Val(list_row.Tag))
        Next

        Dim product_ids = _sql_print.GetProductIDListGroupedByCategory(filter_ids)
        Dim last_category As String = ""
        Dim loop_count As Integer = 0
        For Each product_id In product_ids
            Dim product_detail As New Product(product_id("product_id"))
            Dim category_name As String = If(IsNothing(product_detail.ProductCategory), "Unclassified", product_detail.ProductCategory.CategoryName)
            If Not category_name = last_category Then
                If loop_count Mod 2 = 1 Then html &= "<td colspan=3 class='content divider-right'>&nbsp;</td></tr></tbody></table>"
                html &= "<table class='product-list' cellspacing=0 cellpadding=5><thead><tr><th colspan=6 align='center' class='category-title'>" & category_name & "</th></tr><tr><th class='content-title divider-left'>Product Name</th><th class='content-title'>Packing</th><th class='content-title divider-right'>Price</th><th class='content-title'>Product Name</th><th class='content-title'>Packing</th><th class='content-title divider-right'>Price</th></thead><tbody>"
                last_category = category_name
                loop_count = 0
            End If
            loop_count += 1
            Dim display_unit As ItemUnit = If(product_detail.IsSoldInPacks, ItemUnit.Pack, If(product_detail.IsSoldInPads, ItemUnit.Pad, ItemUnit.Unit))
            Dim text_unit As String = If(display_unit = ItemUnit.Pack, SYS_PACK_ALIAS, If(display_unit = ItemUnit.Pad, SYS_PAD_ALIAS, product_detail.ProductUnit))
            Dim unit_price As Double
            Select Case display_unit
                Case ItemUnit.Pack
                    unit_price = If(combo_price.SelectedItem = "Retail", product_detail.PackPrice.Retail, If(combo_price.SelectedItem = "Wholesale", product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
                Case ItemUnit.Pad
                    unit_price = If(combo_price.SelectedItem = "Retail", product_detail.PadPrice.Retail, If(combo_price.SelectedItem = "Wholesale", product_detail.PadPrice.Wholesale, product_detail.PadPrice.Distribution))
                Case ItemUnit.Unit
                    unit_price = If(combo_price.SelectedItem = "Retail", product_detail.UnitPrice.Retail, If(combo_price.SelectedItem = "Wholesale", product_detail.UnitPrice.Wholesale, product_detail.UnitPrice.Distribution))
            End Select
            If loop_count Mod 2 = 1 Then
                html &= "<tr><td class='content divider-left'>" & product_detail.ProductCompleteNameWithPacking(display_unit) & "</td><td class='content'>per " & text_unit & "</td><td class='content divider-right'>" & SYS_CURRENCY & FormatNumber(unit_price, 2, , , TriState.True) & "</td>"
            Else
                html &= "<td class='content'>" & product_detail.ProductCompleteNameWithPacking(display_unit) & "</td><td class='content'>per " & text_unit & "</td><td class='content divider-right'>" & SYS_CURRENCY & FormatNumber(unit_price, 2, , , TriState.True) & "</td></tr>"
            End If
        Next
        If loop_count Mod 2 = 1 Then html &= "<td colspan=3 class='content divider-right'>&nbsp;</td></tr>"
        html &= "</tbody></table>"

        'For Each list_row As ListViewItem In list_products.CheckedItems
        '    Dim product_detail As New Product(Val(list_row.Tag))
        '    Dim pack_price As Double = If(combo_price.SelectedItem = "Retail", product_detail.PackPrice.Retail, If(combo_price.SelectedItem = "Wholesale", product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
        '    Dim pad_price As Double = If(combo_price.SelectedItem = "Retail", product_detail.PadPrice.Retail, If(combo_price.SelectedItem = "Wholesale", product_detail.PadPrice.Wholesale, product_detail.PadPrice.Distribution))
        '    Dim unit_price As Double = If(combo_price.SelectedItem = "Retail", product_detail.UnitPrice.Retail, If(combo_price.SelectedItem = "Wholesale", product_detail.UnitPrice.Wholesale, product_detail.UnitPrice.Distribution))
        '    Dim pricing As String
        '    If product_detail.PackingSize = 0 Or product_detail.PadsPerPack = 0 Then
        '        pricing = "--"
        '    Else
        '        pricing = SYS_CURRENCY & FormatNumber(pack_price, 2, , , TriState.True) & " <i>per " & If(product_detail.PackingSize > 1, SYS_PACK_ALIAS, product_detail.ProductUnit) & "</i>"
        '        If checkbox_include_unit_prices.Checked Then
        '            If product_detail.PadsPerPack > 1 Then pricing &= "<br>" & SYS_CURRENCY & FormatNumber(pad_price, 2, , , TriState.True) & " <i>per " & SYS_PAD_ALIAS & "</i>"
        '            If product_detail.PackingSize > 1 Then pricing &= "<br>" & SYS_CURRENCY & FormatNumber(unit_price, 2, , , TriState.True) & " <i>per " & product_detail.ProductUnit & "</i>"
        '        End If
        '    End If
        '    html &= "<tr><td align='left' valign='top'>" & product_detail.ProductCompleteName & "</td><td align='center'>" & pricing & "</td></tr>"
        'Next
        'html &= "</tbody></table>"
        pdf_generator.HTML = html
        pdf_generator.Footer = "*Prices may change without prior notice."
        pdf_generator.ShowPreview()

        list_products.Sorting = SortOrder.None
        list_products.ListViewItemSorter = Nothing

    End Sub

    Private Sub button_preview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_preview.Click

        If list_products.CheckedItems.Count < 1 Then
            button_preview.Enabled = False
            MsgBox("Must check at least one product to include in price list.", MsgBoxStyle.Exclamation)
        Else
            GenerateHTML()
        End If

    End Sub

    Private Sub button_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_print.Click

        If list_products.CheckedItems.Count < 1 Then
            button_preview.Enabled = False
            MsgBox("Must check at least one product to include in price list.", MsgBoxStyle.Exclamation)
        Else
            GenerateHTML()
            pdf_generator.Print()
        End If

    End Sub

    Private Sub button_invert_selection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_invert_selection.Click

        For Each list_item As ListViewItem In list_products.Items
            list_item.Checked = Not list_item.Checked
        Next

    End Sub

    Private Sub list_products_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles list_products.ItemChecked

        Try
            button_preview.Enabled = list_products.CheckedItems.Count > 0
            button_save_pdf.Enabled = button_preview.Enabled
            button_print.Enabled = button_preview.Enabled
        Catch ex As Exception
        End Try

    End Sub

    Private Sub list_products_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_products.SelectedIndexChanged

    End Sub

    Private Sub button_save_pdf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_save_pdf.Click

        If list_products.CheckedItems.Count < 1 Then
            button_preview.Enabled = False
            MsgBox("Must check at least one product to include in price list.", MsgBoxStyle.Exclamation)
        Else
            GenerateHTML()
            pdf_generator.SavePrinterFriendly()
        End If

    End Sub
End Class