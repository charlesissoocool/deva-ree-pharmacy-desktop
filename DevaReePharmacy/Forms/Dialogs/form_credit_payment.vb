﻿Public Class form_credit_payment

    Private sql As New class_db_transaction
    Private _checked As Boolean = False
    Private _credit_detail As Credit

    Private Sub form_agent_selector_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then button_done.PerformClick()

    End Sub

    Public Sub OpenCreditPayment(ByVal parent_form As Form)

        Me.ShowDialog(parent_form)

    End Sub

    Private Sub AdjustInterface() Handles Me.Load
        GetTransactionList()
        label_total_credit.ForeColor = module_colors.color_yellow
        label_total_payed.ForeColor = module_colors.color_green
        label_remaining_balance.ForeColor = module_colors.color_pink
    End Sub

    Private Sub GetTransactionList()

        Dim results = sql.GetTransactionListWithCredits()
        combo_credit_number.Items.Clear()
        If Not IsNothing(results) Then
            For Each result In results
                combo_credit_number.Items.Add(result("td_credit_id").ToString().PadLeft(10, "0"))
            Next
        Else
            MsgBox("There are no credits registered.", MsgBoxStyle.Exclamation)
            button_done.PerformClick()
        End If

    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        Me.Close()

        'If text_pay_amount.Text = "" Or Val(text_pay_amount.Text) <= 0 Then
        '    MsgBox("Must enter valid amount.", MsgBoxStyle.Exclamation)
        'ElseIf Val(combo_credit_number.SelectedItem) < 1 Then
        '    MsgBox("Must select credit number.", MsgBoxStyle.Exclamation)
        'Else
        '    Dim credit_id As Integer = Val(combo_credit_number.SelectedItem)
        '    Dim credit_details As New Credit(credit_id)
        '    Dim pay_amount As Double = Val(text_pay_amount.Text)
        '    If credit_details.CreditAmount - credit_details.TotalPaymentAmount < pay_amount Then
        '        MsgBox("Amount exceeds remaining balance.", MsgBoxStyle.Exclamation)
        '    Else
        '        _payment_details.PaymentAmount = Val(text_pay_amount.Text)
        '        _payment_details.PaymentDate = Date.Now()
        '        _payment_details.Staff = module_session.current_user_profile
        '        _payment_details.CreditID = credit_id
        '        _checked = True
        '        Me.Close()
        '    End If
        'End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Me.Close()

    End Sub

    Private Sub AdjustInterface(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub combo_transaction_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles combo_credit_number.SelectedIndexChanged

        list_payments.Items.Clear()
        If combo_credit_number.SelectedIndex >= 0 Then
            _credit_detail = New Credit(Val(combo_credit_number.SelectedItem))
            label_due_date.Text = _credit_detail.DueDate.ToString(DTS_HUMAN)
            label_customer_name.Text = _credit_detail.Customer.CustomerName
            label_total_credit.Text = FormatNumber(_credit_detail.CreditAmount, 2, , , TriState.True)
            label_total_payed.Text = FormatNumber(_credit_detail.TotalPaymentAmount, 2, , , TriState.True)
            label_remaining_balance.Text = FormatNumber(_credit_detail.CreditAmount - _credit_detail.TotalPaymentAmount, 2, , , TriState.True)
            text_pay_amount.Enabled = _credit_detail.CreditAmount > _credit_detail.TotalPaymentAmount
            If Not IsNothing(_credit_detail.CreditPayments) Then
                For Each payment_detail As Credit_Payment In _credit_detail.CreditPayments
                    Dim new_item As ListViewItem = list_payments.Items.Add(payment_detail.PaymentDate.ToString(DTS_YYYYMMDDHHmmSS))
                    new_item.SubItems.Add(SYS_CURRENCY & FormatNumber(payment_detail.PaymentAmount, 2, , , TriState.True))
                Next
            End If
        Else
            label_due_date.Text = "--"
            label_customer_name.Text = "--"
            label_total_credit.Text = "0.00"
            label_total_payed.Text = "0.00"
            label_remaining_balance.Text = "0.00"
            text_pay_amount.Enabled = True
        End If
        text_pay_amount.Text = ""
        button_add_payment.Enabled = text_pay_amount.Enabled

    End Sub

    Private Sub text_pay_amount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_pay_amount.TextChanged

        ValidateNumericTextBox(sender)

    End Sub

    Private Sub button_add_payment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_payment.Click

        If IsNothing(_credit_detail) Then
            button_add_payment.Enabled = False
        Else
            Dim payment_detail As New Credit_Payment
            payment_detail.PaymentAmount = Val(text_pay_amount.Text)
            payment_detail.Staff = module_session.current_user_profile
            payment_detail.PaymentDate = Date.Now
            If payment_detail.PaymentAmount <= 0 Then
                MsgBox("Invalid amount.", MsgBoxStyle.Exclamation)
            ElseIf payment_detail.PaymentAmount > (_credit_detail.CreditAmount - _credit_detail.TotalPaymentAmount) Then
                MsgBox("Amount greater than remaining balance.", MsgBoxStyle.Exclamation)
            ElseIf MsgBox("Are you sure you want to add " & SYS_CURRENCY & FormatNumber(payment_detail.PaymentAmount, 2, , , TriState.True) & " as payment?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                _credit_detail.CreditPayments.Add(payment_detail)
                If _credit_detail.Persist Then
                    combo_transaction_SelectedIndexChanged(Nothing, Nothing)
                Else
                    MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                End If
            End If
        End If


    End Sub
End Class