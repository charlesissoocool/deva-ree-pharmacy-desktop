﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_about
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_about))
        Me.label_staff_name = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.label_customer = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.label_revision = New System.Windows.Forms.Label()
        Me.label_copyright = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.label_disclaimer = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.image_icons8 = New System.Windows.Forms.PictureBox()
        Me.link_icons8 = New System.Windows.Forms.LinkLabel()
        CType(Me.image_icons8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'label_staff_name
        '
        Me.label_staff_name.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.label_staff_name.Location = New System.Drawing.Point(81, 37)
        Me.label_staff_name.Name = "label_staff_name"
        Me.label_staff_name.Size = New System.Drawing.Size(143, 17)
        Me.label_staff_name.TabIndex = 12
        Me.label_staff_name.Text = "Charles Migrino"
        Me.label_staff_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 17)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "Developers"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.Label1.Location = New System.Drawing.Point(264, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 17)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Aljun Bajao"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label_customer
        '
        Me.label_customer.AutoSize = True
        Me.label_customer.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_customer.Location = New System.Drawing.Point(12, 105)
        Me.label_customer.Name = "label_customer"
        Me.label_customer.Size = New System.Drawing.Size(136, 17)
        Me.label_customer.TabIndex = 15
        Me.label_customer.Text = "Customized for Demo"
        Me.label_customer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(81, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 17)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "UX Designer, Back-end"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(264, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 17)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Back-end"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label_revision
        '
        Me.label_revision.AutoSize = True
        Me.label_revision.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_revision.Location = New System.Drawing.Point(12, 88)
        Me.label_revision.Name = "label_revision"
        Me.label_revision.Size = New System.Drawing.Size(83, 17)
        Me.label_revision.TabIndex = 20
        Me.label_revision.Text = "Version 1.0.0"
        Me.label_revision.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label_copyright
        '
        Me.label_copyright.AutoSize = True
        Me.label_copyright.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_copyright.Location = New System.Drawing.Point(12, 122)
        Me.label_copyright.Name = "label_copyright"
        Me.label_copyright.Size = New System.Drawing.Size(235, 17)
        Me.label_copyright.TabIndex = 21
        Me.label_copyright.Text = "© Copyright 2015. All Rights Reserved."
        Me.label_copyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 154)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 17)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Disclaimer"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'label_disclaimer
        '
        Me.label_disclaimer.AutoEllipsis = True
        Me.label_disclaimer.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_disclaimer.Location = New System.Drawing.Point(12, 171)
        Me.label_disclaimer.Name = "label_disclaimer"
        Me.label_disclaimer.Size = New System.Drawing.Size(421, 76)
        Me.label_disclaimer.TabIndex = 23
        Me.label_disclaimer.Text = resources.GetString("label_disclaimer.Text")
        Me.label_disclaimer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.AutoEllipsis = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.Label6.Location = New System.Drawing.Point(69, 266)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(364, 58)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Most or all of the icons used in this application are owned by Icons8 and are dis" & _
            "tributed under a Creative Commons Attribution-NoDerivs 3.0 Unported license." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'image_icons8
        '
        Me.image_icons8.Image = CType(resources.GetObject("image_icons8.Image"), System.Drawing.Image)
        Me.image_icons8.Location = New System.Drawing.Point(15, 281)
        Me.image_icons8.Name = "image_icons8"
        Me.image_icons8.Size = New System.Drawing.Size(48, 48)
        Me.image_icons8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.image_icons8.TabIndex = 25
        Me.image_icons8.TabStop = False
        '
        'link_icons8
        '
        Me.link_icons8.AutoSize = True
        Me.link_icons8.Font = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.link_icons8.Location = New System.Drawing.Point(199, 325)
        Me.link_icons8.Name = "link_icons8"
        Me.link_icons8.Size = New System.Drawing.Size(73, 17)
        Me.link_icons8.TabIndex = 26
        Me.link_icons8.TabStop = True
        Me.link_icons8.Text = "icons8.com"
        '
        'form_about
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(445, 368)
        Me.Controls.Add(Me.link_icons8)
        Me.Controls.Add(Me.image_icons8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.label_disclaimer)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.label_copyright)
        Me.Controls.Add(Me.label_revision)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.label_customer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.label_staff_name)
        Me.Controls.Add(Me.Label3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_about"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "About"
        CType(Me.image_icons8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents label_staff_name As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents label_customer As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents label_revision As System.Windows.Forms.Label
    Friend WithEvents label_copyright As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents label_disclaimer As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents image_icons8 As System.Windows.Forms.PictureBox
    Friend WithEvents link_icons8 As System.Windows.Forms.LinkLabel
End Class
