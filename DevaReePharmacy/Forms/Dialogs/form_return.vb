﻿Public Class form_return

    Dim col_width As Double() = {25, 25, 25, 25}
    Private _item_details As Transaction_Item

    Private Sub form_return_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If e.KeyCode = Keys.Escape Then
            Me.Close()
            e.Handled = True
        End If

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        ResizeListViewColumn(list_stocks, col_width)

    End Sub

    Public Sub OpenReturnForm(ByRef parent_form As Form, ByVal transaction_item_id As Integer)

        _item_details = New Transaction_Item(transaction_item_id)
        LoadReturnList()
        Me.ShowDialog(parent_form)

    End Sub

    Private Sub LoadReturnList()

        Dim sql As New class_db_transaction
        Dim items = sql.GetTransactionItems(_item_details.TransactionID)
        list_stocks.Items.Clear()
        If Not IsNothing(items) Then
            For Each item In items
                If item("product_id") <> _item_details.Product.ProductID Or (Val(item("item_quantity")) - Val(item("return_quantity"))) <= 0 Then Continue For
                Dim stock_detail As New Stock(Val(item("stock_id")))
                Dim new_item As ListViewItem = list_stocks.Items.Add(_item_details.Product.ProductCompleteName)
                new_item.SubItems.Add(If(stock_detail.Synced, stock_detail.LotNumber, "--"))
                new_item.SubItems.Add(If(stock_detail.Synced, stock_detail.ExpiryDate.ToString(DTS_YYYYMMDD), "--"))
                new_item.SubItems.Add(Val(item("item_quantity")) - Val(item("return_quantity")))
                new_item.Tag = item("item_id")
            Next
        End If

    End Sub

    Private Sub list_products_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub button_return_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_return.Click

        If list_stocks.SelectedItems.Count < 1 Then
            MsgBox("Please select stock to return.", MsgBoxStyle.Exclamation)
        Else
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            Dim item_detail = New Transaction_Item(Val(sel_row.Tag))
            Dim quantity_form As New form_quantity
            Dim return_quantity = quantity_form.OpenQuantityDialog(Me, "Return Quantity")
            If return_quantity > (item_detail.ItemQuantity - item_detail.ReturnQuantity) Then
                MsgBox("Value exceeds returnable quantity.", MsgBoxStyle.Exclamation)
            ElseIf return_quantity > 0 Then
                Dim ask = MsgBox("Are you sure you want return " & return_quantity & " " & _item_details.Product.ProductUnit & If(return_quantity > 1, "s", "") & " to stock " & sel_row.SubItems(1).Text & "?", MsgBoxStyle.YesNo)
                If ask = DialogResult.Yes Then
                    If item_detail.AddReturn(return_quantity) Then
                        MsgBox(return_quantity & " " & _item_details.Product.ProductUnit & If(return_quantity > 1, "s", "") & " returned.", MsgBoxStyle.Information)
                        LoadReturnList()
                    Else
                        MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        Me.Close()

    End Sub
End Class