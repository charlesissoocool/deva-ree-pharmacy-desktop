﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_inventory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.list_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.column_pack_size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.column_pad_size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.text_search = New System.Windows.Forms.TextBox()
        Me.page_navigator = New DevaReePharmacy.PageNavigator()
        Me.SuspendLayout()
        '
        'list_products
        '
        Me.list_products.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader8, Me.ColumnHeader4, Me.column_pack_size, Me.column_pad_size, Me.ColumnHeader3, Me.ColumnHeader5, Me.ColumnHeader2})
        Me.list_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_products.FullRowSelect = True
        Me.list_products.GridLines = True
        Me.list_products.HideSelection = False
        Me.list_products.Location = New System.Drawing.Point(-1, 65)
        Me.list_products.MultiSelect = False
        Me.list_products.Name = "list_products"
        Me.list_products.Size = New System.Drawing.Size(677, 329)
        Me.list_products.TabIndex = 2
        Me.list_products.UseCompatibleStateImageBehavior = False
        Me.list_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product Name"
        Me.ColumnHeader1.Width = 95
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Code"
        Me.ColumnHeader8.Width = 88
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Stock Quantity"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader4.Width = 71
        '
        'column_pack_size
        '
        Me.column_pack_size.Text = "Pack Size"
        '
        'column_pad_size
        '
        Me.column_pad_size.Text = "Pad Size"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Retail Price"
        Me.ColumnHeader3.Width = 88
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Wholesale Price"
        Me.ColumnHeader5.Width = 124
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Distribution Price"
        '
        'text_search
        '
        Me.text_search.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_search.BackColor = System.Drawing.Color.White
        Me.text_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_search.ForeColor = System.Drawing.Color.Black
        Me.text_search.Location = New System.Drawing.Point(0, 36)
        Me.text_search.Name = "text_search"
        Me.text_search.Size = New System.Drawing.Size(677, 29)
        Me.text_search.TabIndex = 1
        '
        'page_navigator
        '
        Me.page_navigator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.page_navigator.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator.ListView = Nothing
        Me.page_navigator.Location = New System.Drawing.Point(0, 0)
        Me.page_navigator.Loopable = True
        Me.page_navigator.Name = "page_navigator"
        Me.page_navigator.PageSize = 2
        Me.page_navigator.Size = New System.Drawing.Size(675, 36)
        Me.page_navigator.TabIndex = 15
        Me.page_navigator.TabStop = False
        Me.page_navigator.TotalItems = 0
        '
        'form_inventory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(676, 394)
        Me.Controls.Add(Me.page_navigator)
        Me.Controls.Add(Me.text_search)
        Me.Controls.Add(Me.list_products)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MinimizeBox = False
        Me.Name = "form_inventory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Inventory"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents list_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents column_pack_size As System.Windows.Forms.ColumnHeader
    Friend WithEvents column_pad_size As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents text_search As System.Windows.Forms.TextBox
    Friend WithEvents page_navigator As DevaReePharmacy.PageNavigator
End Class
