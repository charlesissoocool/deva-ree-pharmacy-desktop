﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_unit_selector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_unit_selector))
        Me.radio_pack = New System.Windows.Forms.RadioButton()
        Me.radio_pad = New System.Windows.Forms.RadioButton()
        Me.radio_unit = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'radio_pack
        '
        Me.radio_pack.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.radio_pack.ForeColor = System.Drawing.Color.Black
        Me.radio_pack.Image = CType(resources.GetObject("radio_pack.Image"), System.Drawing.Image)
        Me.radio_pack.Location = New System.Drawing.Point(12, 12)
        Me.radio_pack.Name = "radio_pack"
        Me.radio_pack.Size = New System.Drawing.Size(87, 40)
        Me.radio_pack.TabIndex = 1
        Me.radio_pack.Text = "Box"
        Me.radio_pack.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.radio_pack.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.radio_pack.UseVisualStyleBackColor = True
        '
        'radio_pad
        '
        Me.radio_pad.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.radio_pad.ForeColor = System.Drawing.Color.Black
        Me.radio_pad.Image = CType(resources.GetObject("radio_pad.Image"), System.Drawing.Image)
        Me.radio_pad.Location = New System.Drawing.Point(12, 58)
        Me.radio_pad.Name = "radio_pad"
        Me.radio_pad.Size = New System.Drawing.Size(87, 40)
        Me.radio_pad.TabIndex = 2
        Me.radio_pad.Text = "Pad"
        Me.radio_pad.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.radio_pad.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.radio_pad.UseVisualStyleBackColor = True
        '
        'radio_unit
        '
        Me.radio_unit.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.radio_unit.ForeColor = System.Drawing.Color.Black
        Me.radio_unit.Image = CType(resources.GetObject("radio_unit.Image"), System.Drawing.Image)
        Me.radio_unit.Location = New System.Drawing.Point(12, 101)
        Me.radio_unit.Name = "radio_unit"
        Me.radio_unit.Size = New System.Drawing.Size(87, 40)
        Me.radio_unit.TabIndex = 3
        Me.radio_unit.Text = "Unit"
        Me.radio_unit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.radio_unit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.radio_unit.UseVisualStyleBackColor = True
        '
        'form_unit_selector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(109, 151)
        Me.Controls.Add(Me.radio_unit)
        Me.Controls.Add(Me.radio_pad)
        Me.Controls.Add(Me.radio_pack)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_unit_selector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Unit"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents radio_pack As System.Windows.Forms.RadioButton
    Friend WithEvents radio_pad As System.Windows.Forms.RadioButton
    Friend WithEvents radio_unit As System.Windows.Forms.RadioButton
End Class
