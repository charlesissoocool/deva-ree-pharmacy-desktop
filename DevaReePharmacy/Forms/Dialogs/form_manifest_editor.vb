﻿Public Class form_manifest_editor

    Private col_width() As Double = {25, 25, 15, 20, 15}
    Private _checked As Boolean = False
    Private _view_only As Boolean
    Private _manifest_details As Manifest
    Private _has_changes = False
    Private sql_stocks As New class_db_stocks
    Private _parent_form As form_stock_management

    Public Function OpenManifestEditor(ByRef parent_form As Form, Optional ByVal manifest_details As Manifest = Nothing, Optional ByVal view_only As Boolean = True) As Manifest

        If IsNothing(manifest_details) Then
            label_staff_name.Text = module_session.current_user_profile.FirstName & " " & module_session.current_user_profile.LastName
            _manifest_details = New Manifest
            label_entry_date.Text = "--"
            label_updated_date.Text = "--"
            _view_only = False
        Else
            _view_only = view_only
            button_add_stock.Enabled = Not view_only
            text_manifest_code.Enabled = button_add_stock.Enabled
            button_cancel.Visible = button_add_stock.Enabled
            button_done.Enabled = view_only
            _manifest_details = manifest_details
            text_manifest_code.Text = manifest_details.ManifestCode
            label_staff_name.Text = _manifest_details.Staff.FirstName & " " & _manifest_details.Staff.LastName
            text_supplier_name.Text = _manifest_details.Supplier.SupplierName
            text_supplier_name.Enabled = button_add_stock.Enabled
            list_stocks.Items.Clear()
            For Each stock_item In _manifest_details.Stocks
                Dim new_item As ListViewItem = list_stocks.Items.Add(stock_item.Product.ProductCompleteName)
                new_item.UseItemStyleForSubItems = False
                If stock_item.Product.IsSoldInPacks Then
                    new_item.SubItems.Add(FormatNumber(stock_item.StockQuantity, 0, , , TriState.True) & " " & stock_item.Product.ProductUnit & " (" & (Math.Round(stock_item.StockQuantity / stock_item.Product.PackingSize, 0)) & " " & SYS_PACK_ALIAS & ")")
                Else
                    new_item.SubItems.Add(FormatNumber(stock_item.StockQuantity, 0, , , TriState.True) & " " & stock_item.Product.ProductUnit)
                End If
                new_item.SubItems.Add(stock_item.LotNumber)
                Dim expiry_date_sub As New ListViewItem.ListViewSubItem
                expiry_date_sub.Text = stock_item.ExpiryDate.ToString(DTS_YYYYMMDD)
                expiry_date_sub.ForeColor = If(stock_item.IsExpired, module_colors.color_red, Color.Black)
                expiry_date_sub.Font = If(stock_item.IsExpired, New Font(list_stocks.Font, FontStyle.Bold), list_stocks.Font)
                new_item.SubItems.Add(expiry_date_sub)
                new_item.SubItems.Add(FormatNumber(stock_item.SupplierPrice, , , , TriState.True) & " / " & If(stock_item.Product.IsSoldInPacks, SYS_PACK_ALIAS, stock_item.Product.ProductUnit))
                new_item.ImageKey = If(sql_stocks.IsStockInFlow(stock_item.StockID), "used", "new")
                new_item.Tag = stock_item.StockID
            Next
            label_entry_date.Text = _manifest_details.EntryDate.ToString(DTS_HUMAN_WITH_TIME)
            label_updated_date.Text = If(_manifest_details.UpdatedDate = Date.MinValue, "Never", _manifest_details.UpdatedDate.ToString(DTS_HUMAN_WITH_TIME))
        End If
        'button_edit_stocks.Enabled = False
        'button_delete_stocks.Enabled = False

        _parent_form = parent_form
        Me.ShowDialog(parent_form)
        OpenManifestEditor = If(_checked, _manifest_details, Nothing)

    End Function

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        If list_stocks.Items.Count > 0 Or _has_changes Then
            Dim ask = MsgBox("Are you sure you want to cancel your changes?", MsgBoxStyle.YesNo)
            If ask = DialogResult.Yes Then Me.Close()
        Else
            Me.Close()
        End If

    End Sub

    Private Sub button_add_stock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_stock.Click

        Dim stock_editor As New form_stock_editor
        Dim stock_detail = stock_editor.OpenStockEditor(Me)

        If Not IsNothing(stock_detail) Then
            Dim new_row = list_stocks.Items.Add(stock_detail.Product.ProductCompleteName)
            If stock_detail.Product.IsSoldInPacks Then
                new_row.SubItems.Add(FormatNumber(stock_detail.StockQuantity, 0, , , TriState.True) & " " & stock_detail.Product.ProductUnit & " (" & (Math.Round(stock_detail.StockQuantity / stock_detail.Product.PackingSize, 0)) & " " & SYS_PACK_ALIAS & ")")
            Else
                new_row.SubItems.Add(FormatNumber(stock_detail.StockQuantity, 0, , , TriState.True) & " " & stock_detail.Product.ProductUnit)
            End If
            new_row.SubItems.Add(stock_detail.LotNumber)
            new_row.SubItems.Add(stock_detail.ExpiryDate.ToString(DTS_YYYYMMDD))
            new_row.SubItems.Add(FormatNumber(stock_detail.SupplierPrice, , , , TriState.True) & " / " & If(stock_detail.Product.IsSoldInPacks, SYS_PACK_ALIAS, stock_detail.Product.ProductUnit))
            new_row.ImageKey = "new"
            _has_changes = True
        End If

        button_done.Enabled = list_stocks.Items.Count > 0

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        Dim supplier_details = sql_stocks.GetSuppliers()
        Dim suppliers As New AutoCompleteStringCollection
        For Each supplier_detail In supplier_details
            suppliers.Add(supplier_detail("s_supplier_name"))
        Next
        text_supplier_name.AutoCompleteCustomSource = suppliers

        For Each obj In Me.Controls
            If TypeOf obj Is TextBox Then
                obj.BackColor = If(Trim(obj.Text) = "", module_colors.color_red, Color.White)
            ElseIf TypeOf obj Is GroupBox Then
                For Each child_obj In obj.Controls
                    If TypeOf child_obj Is TextBox Then child_obj.BackColor = If(Trim(child_obj.Text) = "", module_colors.color_red, Color.White)
                Next
            End If
        Next

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        ResizeListViewColumn(list_stocks, col_width)

    End Sub

    Private Sub list_stocks_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_stocks.DoubleClick

        If list_stocks.SelectedItems.Count > 0 And button_edit_stocks.Enabled Then button_edit_stocks.PerformClick()

    End Sub

    Private Sub list_stocks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_stocks.SelectedIndexChanged

        If Not _view_only Then
            button_edit_stocks.Enabled = list_stocks.SelectedItems.Count > 0
            button_delete_stocks.Enabled = button_edit_stocks.Enabled
            button_migrate.Enabled = button_edit_stocks.Enabled
            button_manifest_transfer.Enabled = button_edit_stocks.Enabled And _manifest_details.ManifestID > 0
        End If

    End Sub

    Private Sub button_edit_stocks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_edit_stocks.Click

        If list_stocks.SelectedItems.Count < 1 Then
            button_edit_stocks.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            Dim stock_detail As New Stock
            Dim sql_products As New class_db_products
            If Val(sel_row.Tag) > 0 Then
                stock_detail = New Stock(Val(sel_row.Tag))
            Else
                stock_detail.Product = New Product(sql_products.GetProductIDByName(sel_row.Text))
                stock_detail.StockQuantity = Val(ClearNumberFormat(sel_row.SubItems(1).Text.Split("(").First.Replace(stock_detail.Product.ProductUnit, "").Replace(SYS_PACK_ALIAS, "")))
                stock_detail.LotNumber = sel_row.SubItems(2).Text
                stock_detail.ExpiryDate = Date.Parse(sel_row.SubItems(3).Text)
                stock_detail.SupplierPrice = Val(sel_row.SubItems(4).Text)
            End If

            Dim stock_editor As New form_stock_editor
            stock_detail = stock_editor.OpenStockEditor(Me, stock_detail)
            If Not IsNothing(stock_detail) Then
                sel_row.Text = stock_detail.Product.ProductCompleteName
                If stock_detail.Product.IsSoldInPacks Then
                    sel_row.SubItems(1).Text = FormatNumber(stock_detail.StockQuantity, 0, , , TriState.True) & " " & stock_detail.Product.ProductUnit & " (" & (Math.Round(stock_detail.StockQuantity / stock_detail.Product.PackingSize, 0)) & " " & SYS_PACK_ALIAS & ")"
                Else
                    sel_row.SubItems(1).Text = FormatNumber(stock_detail.StockQuantity, 0, , , TriState.True) & " " & stock_detail.Product.ProductUnit
                End If
                sel_row.SubItems(2).Text = stock_detail.LotNumber
                sel_row.SubItems(3).Text = stock_detail.ExpiryDate.ToString(DTS_YYYYMMDD)
                sel_row.SubItems(4).Text = FormatNumber(stock_detail.SupplierPrice, , , , TriState.True) & " / " & If(stock_detail.Product.IsSoldInPacks, SYS_PACK_ALIAS, stock_detail.Product.ProductUnit)
                _has_changes = True
            End If
            button_done.Enabled = list_stocks.Items.Count > 0
        End If
    End Sub

    Private Sub button_delete_stocks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_delete_stocks.Click

        If list_stocks.SelectedItems.Count < 1 Then
            button_delete_stocks.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            If sel_row.ImageKey = "used" Then
                MsgBox("Cannot delete stock. Stock already in use.", MsgBoxStyle.Exclamation)
            Else
                If (MsgBox("Are you sure you want to remove selected stock from manifest?", MsgBoxStyle.YesNo) = DialogResult.Yes) Then
                    sel_row.Remove()
                    _has_changes = True
                End If
                button_done.Enabled = list_stocks.Items.Count > 0
            End If
        End If

    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        If _view_only Then
            _checked = False
            Me.Close()
        ElseIf Trim(text_supplier_name.Text) = "" Or Trim(text_manifest_code.Text) = "" Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Exclamation)
        Else
            text_manifest_code.Text = Trim(text_manifest_code.Text)
            If list_stocks.Items.Count < 1 Then
                MsgBox("Must add atleast one stock to manifest.", MsgBoxStyle.Exclamation)
            ElseIf (_manifest_details.ManifestID = 0 And sql_stocks.IsManifestCodeTaken(text_manifest_code.Text)) Or (_manifest_details.ManifestID > 0 And Not text_manifest_code.Text = _manifest_details.ManifestCode And sql_stocks.IsManifestCodeTaken(text_manifest_code.Text)) Then
                MsgBox("Manifest code is already taken.", MsgBoxStyle.Exclamation)
            Else
                _manifest_details.RemoveAllStock()
                Dim sql_products As New class_db_products
                For Each list_row As ListViewItem In list_stocks.Items
                    Dim stock_item As New Stock
                    If Val(list_row.Tag) > 0 Then
                        stock_item.StockID = Val(list_row.Tag)
                    Else
                        stock_item.StockID = 0
                    End If
                    stock_item.Product = New Product(sql_products.GetProductIDByName(list_row.Text))
                    stock_item.StockQuantity = Val(ClearNumberFormat(list_row.SubItems(1).Text.Split("(").First.Replace(stock_item.Product.ProductUnit, "").Replace(SYS_PACK_ALIAS, "")))
                    stock_item.LotNumber = list_row.SubItems(2).Text
                    stock_item.ExpiryDate = Date.Parse(list_row.SubItems(3).Text)
                    stock_item.SupplierPrice = Val(list_row.SubItems(4).Text)
                    _manifest_details.AddStock(stock_item)
                Next
                If IsNothing(_manifest_details.Supplier) Then _manifest_details.Supplier = New Supplier()
                _manifest_details.Supplier.SupplierName = text_supplier_name.Text
                _manifest_details.ManifestCode = text_manifest_code.Text
                _manifest_details.EntryDate = Date.Now()
                _manifest_details.Staff = New User(module_session.current_user_profile.UserID)
                _checked = True
                Me.Close()
            End If
        End If

    End Sub

    Private Sub text_manifest_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_manifest_code.TextChanged, text_supplier_name.TextChanged

        sender.BackColor = If(Trim(sender.Text) = "", module_colors.color_red, Color.White)
        sender.ForeColor = If(Trim(sender.Text) = "", Color.White, Color.Black)
        button_done.Enabled = Not Trim(sender.text) = ""

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub button_transfer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_manifest_transfer.Click

        If list_stocks.SelectedItems.Count > 0 Then
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            Dim stock_details As New Stock(Val(sel_row.Tag))
            Dim manifest_selector As New form_manifest_selector
            Dim manifest_id = manifest_selector.OpenManifestSelectorDialog(Me)
            If manifest_id > 0 Then
                Dim manifest_details As New Manifest(manifest_id)
                Dim ask = MsgBox("Are you sure you want to transfer selected stock to manifest #" & manifest_details.ManifestCode & " by " & manifest_details.Supplier.SupplierName & "?", MsgBoxStyle.YesNo)
                If ask = DialogResult.Yes Then
                    stock_details.ManifestID = manifest_id
                    If stock_details.Persist() Then
                        sel_row.Remove()
                        _parent_form.RefreshPage()
                        MsgBox("Stock item has been transferred.", MsgBoxStyle.Information)
                    Else
                        MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                    End If
                End If
            End If
        Else
            MsgBox("Must select a stock to transfer.", MsgBoxStyle.Exclamation)
        End If

    End Sub

    Private Sub button_migrate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_migrate.Click

        If list_stocks.SelectedItems.Count < 1 Then
            button_migrate.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_stocks.SelectedItems(0)
            Dim stock_detail As New Stock(Val(sel_row.Tag))
            Dim stock_migrate_form As New form_stock_migrate
            Dim migrate_detail = stock_migrate_form.OpenMigrateStockDialog(Me, stock_detail)
            If Not IsNothing(migrate_detail) Then
                If MsgBox("Are you sure you want to continue migrating stocks?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                    migrate_detail.Staff = module_session.current_user_profile
                    stock_detail.StockQuantity -= migrate_detail.StockQuantity
                    If migrate_detail.Persist And stock_detail.Persist Then
                        If stock_detail.Product.IsSoldInPacks Then
                            sel_row.SubItems(1).Text = FormatNumber(stock_detail.StockQuantity, 0, , , TriState.True) & " " & stock_detail.Product.ProductUnit & " (" & (Math.Round(stock_detail.StockQuantity / stock_detail.Product.PackingSize, 0)) & " " & SYS_PACK_ALIAS & ")"
                        Else
                            sel_row.SubItems(1).Text = FormatNumber(stock_detail.StockQuantity, 0, , , TriState.True) & " " & stock_detail.Product.ProductUnit
                        End If
                        MsgBox("Migrate stock successful.", MsgBoxStyle.Information)
                    Else
                        MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                    End If
                End If
            End If
        End If
    End Sub
End Class