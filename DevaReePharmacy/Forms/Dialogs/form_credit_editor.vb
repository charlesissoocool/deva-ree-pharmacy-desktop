﻿Public Class form_credit_editor

    Private sql As New class_db_transaction
    Private _checked As Boolean = False
    Private _credit_detail As Credit

    Private Sub form_credit_editor_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        If e.KeyChar = ChrW(Keys.Escape) Then
            button_cancel.PerformClick()
            e.Handled = True
        End If

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        text_box_TextChanged(text_credit_amount, Nothing)
        text_box_TextChanged(text_customer_name, Nothing)
        LoadCustomerList()

    End Sub

    Private Sub LoadCustomerList()

        Dim customer_list As New AutoCompleteStringCollection
        Dim results = sql.GetCustomerList()
        If Not IsNothing(results) Then
            For Each result In results
                customer_list.Add(result("c_name"))
            Next
        End If
        text_customer_name.AutoCompleteCustomSource = customer_list

    End Sub

    Public Function OpenCreditEditor(ByRef parent_form As Form, ByVal default_credit_amount As Double, ByVal default_customer As Customer, Optional ByVal changeable_amount As Boolean = False) As Credit

        _credit_detail = New Credit
        text_credit_amount.Text = default_credit_amount
        text_customer_name.Text = If(IsNothing(default_customer), "", default_customer.CustomerName)
        text_credit_amount.Enabled = changeable_amount
        Me.ShowDialog(parent_form)
        OpenCreditEditor = If(_checked, _credit_detail, Nothing)

    End Function

    Public Function OpenCreditEditor(ByRef parent_form As Form, Optional ByVal credit_details As Credit = Nothing) As Credit

        If IsNothing(credit_details) Then
            _credit_detail = New Credit
            text_initial_payment.Enabled = True
        Else
            _credit_detail = credit_details
            text_credit_amount.Text = _credit_detail.CreditAmount
            text_customer_name.Text = _credit_detail.Customer.CustomerName
            date_due_date.Value = _credit_detail.DueDate
            text_initial_payment.Text = _credit_detail.TotalPaymentAmount
            text_initial_payment.Enabled = False
        End If
        Me.ShowDialog(parent_form)
        OpenCreditEditor = If(_checked, _credit_detail, Nothing)

    End Function

    Private Sub button_add_customer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_customer.Click

        Dim customer_editor As New form_customer_selector
        Dim customer_detail = customer_editor.OpenCustomerSelectorDialog(Me)
        If Not IsNothing(customer_detail) Then
            If customer_detail.Persist Then
                LoadCustomerList()
                text_customer_name.Text = customer_detail.CustomerName
            Else
                MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub text_box_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_credit_amount.TextChanged, text_customer_name.TextChanged, text_initial_payment.TextChanged

        If sender Is text_credit_amount Or sender Is text_initial_payment Then
            ValidateNumericTextBox(sender)
            If sender Is text_credit_amount And Val(text_credit_amount.Text) <= 0 Then InvalidateTextBox(sender) Else ValidateTextBox(sender)
            If sender Is text_initial_payment And Not text_initial_payment.Text = "" And Not IsNumeric(text_initial_payment.Text) Then InvalidateTextBox(sender) Else ValidateTextBox(sender)
        ElseIf sender Is text_customer_name Then
            If sql.GetCustomerIDByName(text_customer_name.Text) > 0 Then
                ValidateTextBox(sender)
            Else
                InvalidateTextBox(sender)
            End If
        End If

    End Sub

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        If text_credit_amount.BackColor = module_colors.color_red Or text_customer_name.BackColor = module_colors.color_red Or text_initial_payment.BackColor = module_colors.color_red Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Exclamation)
        ElseIf Val(text_initial_payment.Text) > Val(text_credit_amount.Text) Then
            MsgBox("Initial payment cannot be greater than credit amount.", MsgBoxStyle.Exclamation)
        Else
            _credit_detail.CreditAmount = Val(text_credit_amount.Text)
            If _credit_detail.TotalPaymentAmount > _credit_detail.CreditAmount Then
                MsgBox("Credit amount cannot be lesser than the total amount payed.")
                Exit Sub
            End If
            _credit_detail.Customer = New Customer(sql.GetCustomerIDByName(text_customer_name.Text))
            _credit_detail.DueDate = date_due_date.Value
            If text_initial_payment.Enabled And Val(text_initial_payment.Text) > 0 Then
                Dim _initial_payment As New Credit_Payment
                _initial_payment.PaymentAmount = Val(text_initial_payment.Text)
                _initial_payment.Staff = module_session.current_user_profile
                _credit_detail.AddPaymentItem(_initial_payment)
            End If
            _checked = True
            Me.Close()
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        Me.Close()

    End Sub
End Class