﻿Public Class form_stock_editor

    Private _stock_detail As Stock = Nothing
    Private _checked As Boolean = False
    Private sql_products As New class_db_products
    Private sql As New class_db_stocks
    Private _unit As ItemUnit = ItemUnit.NoUnit

    Private Sub AdjustInterface() Handles Me.Load

        Dim products = sql_products.GetProductListAndDetails(Nothing, 0, 0)
        Dim product_list As New AutoCompleteStringCollection
        For Each product In products
            product_list.Add(product("product_name"))
        Next
        text_product_name.AutoCompleteCustomSource = product_list

        For Each obj In Me.Controls
            If TypeOf obj Is TextBox Then
                obj.BackColor = If(Trim(obj.Text) = "", module_colors.color_red, Color.White)
            ElseIf TypeOf obj Is GroupBox Then
                For Each child_obj In obj.Controls
                    If TypeOf child_obj Is TextBox Then child_obj.BackColor = If(Trim(child_obj.Text) = "", module_colors.color_red, Color.White)
                Next
            End If
        Next

        label_currency_price.Text = SYS_CURRENCY

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize


    End Sub

    Public Function OpenStockEditor(ByVal parent_form As Form, Optional ByVal stock_detail As Stock = Nothing) As Stock

        If Not IsNothing(stock_detail) Then
            _stock_detail = stock_detail
            text_product_name.Text = _stock_detail.Product.ProductCompleteName
            If _stock_detail.StockQuantity Mod _stock_detail.Product.PackingSize = 0 And _stock_detail.Product.IsSoldInPacks Then
                text_stock_quantity.Text = _stock_detail.StockQuantity / _stock_detail.Product.PackingSize
                combo_unit.SelectedItem = SYS_PACK_ALIAS
            ElseIf _stock_detail.StockQuantity Mod (_stock_detail.Product.PackingSize / _stock_detail.Product.PadsPerPack) = 0 And _stock_detail.Product.IsSoldInPads Then
                text_stock_quantity.Text = _stock_detail.StockQuantity / _stock_detail.Product.PackingSize
                combo_unit.SelectedItem = SYS_PAD_ALIAS
            Else
                text_stock_quantity.Text = _stock_detail.StockQuantity
                combo_unit.SelectedItem = _stock_detail.Product.ProductUnit
            End If
            combo_unit_SelectedIndexChanged(Nothing, Nothing)
            text_supplier_price.Text = FormatNumber(_stock_detail.SupplierPrice, 2, , , TriState.True)
            text_lot_number.Text = _stock_detail.LotNumber
            date_expiry_date.Value = _stock_detail.ExpiryDate
            text_product_name.Enabled = Not sql.IsStockInFlow(stock_detail.StockID)
        Else
            _stock_detail = New Stock()
        End If

        Me.ShowDialog(parent_form)
        OpenStockEditor = If(_checked, _stock_detail, Nothing)

    End Function

    Private Sub button_done_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_done.Click

        If text_product_name.BackColor = module_colors.color_red Or text_stock_quantity.BackColor = module_colors.color_red Or text_lot_number.BackColor = module_colors.color_red Or combo_unit.Items.Count < 1 Then
            MsgBox("Some fields are invalid.", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If Val(text_supplier_price.Text) <= 0 Then
            MsgBox("Invalid supplier price.", MsgBoxStyle.Critical)
            Exit Sub
        End If

        Dim sql_stocks As New class_db_stocks
        If Not _stock_detail.LotNumber = text_lot_number.Text And sql_stocks.DoesLotNumberExists(text_lot_number.Text) Then
            MsgBox("Lot number " & text_lot_number.Text & " is already in use.")
            Exit Sub
        End If

        If _stock_detail.Product.PackingSize = 0 Or _stock_detail.Product.PadsPerPack = 0 Then
            MsgBox("Restricted adding stock to product. Product has invalid packing information.", MsgBoxStyle.Critical)
        Else
            Dim stock_quantity As Double = Val(ClearNumberFormat(text_stock_quantity.Text))
            Dim multiplier As Integer = If(_unit = ItemUnit.Pack, _stock_detail.Product.PackingSize, If(_unit = ItemUnit.Pad, _stock_detail.Product.PackingSize / _stock_detail.Product.PadsPerPack, 1))
            If _stock_detail.StockID > 0 And (stock_quantity * multiplier) < (_stock_detail.StockQuantity - _stock_detail.RemainingQuantity) Then
                MsgBox("Cannot change quantity lower than sold items.", MsgBoxStyle.Critical)
                Exit Sub
            End If
            _stock_detail.StockQuantity = stock_quantity * multiplier
            _stock_detail.SupplierPrice = Val(ClearNumberFormat(text_supplier_price.Text))
            _stock_detail.ExpiryDate = date_expiry_date.Value
            _stock_detail.LotNumber = text_lot_number.Text
            _checked = True
            Me.Close()
        End If


    End Sub

    Private Sub date_expiry_date_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles date_expiry_date.ValueChanged

        _stock_detail.ExpiryDate = date_expiry_date.Value

    End Sub

    Private Sub text_product_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_product_name.TextChanged

        Dim product_detail As New Product(sql_products.GetProductIDByName(text_product_name.Text))
        combo_unit.Items.Clear()
        If IsNothing(product_detail) Then
            _unit = ItemUnit.NoUnit
            _stock_detail.Product = Nothing
            label_tip_supplier_price.Text = ""
            sender.BackColor = module_colors.color_red
            sender.ForeColor = Color.White
        Else
            _stock_detail.Product = product_detail
            If product_detail.IsSoldInPacks Then combo_unit.Items.Add(SYS_PACK_ALIAS)
            If product_detail.IsSoldInPads Then combo_unit.Items.Add(SYS_PAD_ALIAS)
            combo_unit.Items.Add(product_detail.ProductUnit)
            'label_tip_supplier_price.Text = "(Price per " & If(product_detail.IsSoldInPacks, SYS_PACK_ALIAS, product_detail.ProductUnit) & ")"
            sender.backcolor = Color.White
            sender.ForeColor = Color.Black
        End If

    End Sub

    Private Sub text_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_lot_number.TextChanged, text_stock_quantity.TextChanged, text_supplier_price.TextChanged

        If sender Is text_stock_quantity Then ValidateNumericTextBox(sender, SYS_DECIMAL_STOCKS)
        If sender Is text_supplier_price Then ValidateNumericTextBox(sender)

        If Trim(sender.Text) = "" Then
            sender.BackColor = module_colors.color_red
            sender.ForeColor = Color.White
        Else
            sender.BackColor = Color.White
            sender.ForeColor = Color.Black
        End If


    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        _stock_detail = Nothing
        Me.Close()

    End Sub

    Private Sub combo_unit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles combo_unit.SelectedIndexChanged

        _unit = If(combo_unit.SelectedItem = SYS_PACK_ALIAS, ItemUnit.Pack, If(combo_unit.SelectedItem = SYS_PAD_ALIAS, ItemUnit.Pad, ItemUnit.Unit))
        label_tip_supplier_price.Text = "(Price per " & combo_unit.SelectedItem & ")"

    End Sub
End Class