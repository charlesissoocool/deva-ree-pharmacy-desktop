﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_user_editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_user_editor))
        Me.group_personal_info = New System.Windows.Forms.GroupBox()
        Me.text_last_name = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.text_first_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.text_username = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.text_password = New System.Windows.Forms.TextBox()
        Me.group_permissions = New System.Windows.Forms.GroupBox()
        Me.toggle_agent = New DevaReePharmacy.SwitchToggle()
        Me.toggle_cashier = New DevaReePharmacy.SwitchToggle()
        Me.toggle_products = New DevaReePharmacy.SwitchToggle()
        Me.toggle_stocks = New DevaReePharmacy.SwitchToggle()
        Me.toggle_administrator = New DevaReePharmacy.SwitchToggle()
        Me.group_security = New System.Windows.Forms.GroupBox()
        Me.text_repeat_password = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_save = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.text_email = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.toggle_finance = New DevaReePharmacy.SwitchToggle()
        Me.group_personal_info.SuspendLayout()
        Me.group_permissions.SuspendLayout()
        Me.group_security.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_personal_info
        '
        Me.group_personal_info.Controls.Add(Me.text_last_name)
        Me.group_personal_info.Controls.Add(Me.Label2)
        Me.group_personal_info.Controls.Add(Me.text_first_name)
        Me.group_personal_info.Controls.Add(Me.Label1)
        Me.group_personal_info.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_personal_info.ForeColor = System.Drawing.Color.Black
        Me.group_personal_info.Location = New System.Drawing.Point(12, 2)
        Me.group_personal_info.Name = "group_personal_info"
        Me.group_personal_info.Size = New System.Drawing.Size(392, 109)
        Me.group_personal_info.TabIndex = 0
        Me.group_personal_info.TabStop = False
        Me.group_personal_info.Text = "Personal Information"
        '
        'text_last_name
        '
        Me.text_last_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_last_name.ForeColor = System.Drawing.Color.Black
        Me.text_last_name.Location = New System.Drawing.Point(85, 69)
        Me.text_last_name.Name = "text_last_name"
        Me.text_last_name.Size = New System.Drawing.Size(290, 29)
        Me.text_last_name.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(11, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 17)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Last Name"
        '
        'text_first_name
        '
        Me.text_first_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_first_name.ForeColor = System.Drawing.Color.Black
        Me.text_first_name.Location = New System.Drawing.Point(85, 34)
        Me.text_first_name.Name = "text_first_name"
        Me.text_first_name.Size = New System.Drawing.Size(290, 29)
        Me.text_first_name.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 17)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "First Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Username"
        '
        'text_username
        '
        Me.text_username.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_username.ForeColor = System.Drawing.Color.Black
        Me.text_username.Location = New System.Drawing.Point(85, 35)
        Me.text_username.Name = "text_username"
        Me.text_username.Size = New System.Drawing.Size(290, 29)
        Me.text_username.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 17)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Password"
        '
        'text_password
        '
        Me.text_password.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_password.ForeColor = System.Drawing.Color.Black
        Me.text_password.Location = New System.Drawing.Point(85, 70)
        Me.text_password.Name = "text_password"
        Me.text_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.text_password.Size = New System.Drawing.Size(290, 29)
        Me.text_password.TabIndex = 4
        '
        'group_permissions
        '
        Me.group_permissions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_permissions.Controls.Add(Me.toggle_finance)
        Me.group_permissions.Controls.Add(Me.toggle_agent)
        Me.group_permissions.Controls.Add(Me.toggle_cashier)
        Me.group_permissions.Controls.Add(Me.toggle_products)
        Me.group_permissions.Controls.Add(Me.toggle_stocks)
        Me.group_permissions.Controls.Add(Me.toggle_administrator)
        Me.group_permissions.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_permissions.ForeColor = System.Drawing.Color.Black
        Me.group_permissions.Location = New System.Drawing.Point(410, 2)
        Me.group_permissions.Name = "group_permissions"
        Me.group_permissions.Size = New System.Drawing.Size(192, 339)
        Me.group_permissions.TabIndex = 2
        Me.group_permissions.TabStop = False
        Me.group_permissions.Text = "Permissions"
        '
        'toggle_agent
        '
        Me.toggle_agent.BackColor = System.Drawing.Color.Transparent
        Me.toggle_agent.Checked = True
        Me.toggle_agent.DisplayStatus = True
        Me.toggle_agent.Location = New System.Drawing.Point(17, 213)
        Me.toggle_agent.Margin = New System.Windows.Forms.Padding(68, 196, 68, 196)
        Me.toggle_agent.Name = "toggle_agent"
        Me.toggle_agent.Size = New System.Drawing.Size(166, 51)
        Me.toggle_agent.TabIndex = 10
        Me.toggle_agent.Text = "Agent"
        '
        'toggle_cashier
        '
        Me.toggle_cashier.BackColor = System.Drawing.Color.Transparent
        Me.toggle_cashier.Checked = True
        Me.toggle_cashier.DisplayStatus = True
        Me.toggle_cashier.Location = New System.Drawing.Point(17, 165)
        Me.toggle_cashier.Margin = New System.Windows.Forms.Padding(37, 85, 37, 85)
        Me.toggle_cashier.Name = "toggle_cashier"
        Me.toggle_cashier.Size = New System.Drawing.Size(166, 51)
        Me.toggle_cashier.TabIndex = 9
        Me.toggle_cashier.Text = "Cashier"
        '
        'toggle_products
        '
        Me.toggle_products.BackColor = System.Drawing.Color.Transparent
        Me.toggle_products.Checked = True
        Me.toggle_products.DisplayStatus = True
        Me.toggle_products.Location = New System.Drawing.Point(17, 122)
        Me.toggle_products.Margin = New System.Windows.Forms.Padding(20, 37, 20, 37)
        Me.toggle_products.Name = "toggle_products"
        Me.toggle_products.Size = New System.Drawing.Size(166, 51)
        Me.toggle_products.TabIndex = 8
        Me.toggle_products.Text = "Products"
        '
        'toggle_stocks
        '
        Me.toggle_stocks.BackColor = System.Drawing.Color.Transparent
        Me.toggle_stocks.Checked = True
        Me.toggle_stocks.DisplayStatus = True
        Me.toggle_stocks.Location = New System.Drawing.Point(17, 76)
        Me.toggle_stocks.Margin = New System.Windows.Forms.Padding(11, 16, 11, 16)
        Me.toggle_stocks.Name = "toggle_stocks"
        Me.toggle_stocks.Size = New System.Drawing.Size(166, 51)
        Me.toggle_stocks.TabIndex = 7
        Me.toggle_stocks.Text = "Stocks"
        '
        'toggle_administrator
        '
        Me.toggle_administrator.BackColor = System.Drawing.Color.Transparent
        Me.toggle_administrator.Checked = True
        Me.toggle_administrator.DisplayStatus = True
        Me.toggle_administrator.Location = New System.Drawing.Point(17, 34)
        Me.toggle_administrator.Margin = New System.Windows.Forms.Padding(6, 7, 6, 7)
        Me.toggle_administrator.Name = "toggle_administrator"
        Me.toggle_administrator.Size = New System.Drawing.Size(177, 46)
        Me.toggle_administrator.TabIndex = 6
        Me.toggle_administrator.Text = "Administrator"
        '
        'group_security
        '
        Me.group_security.Controls.Add(Me.text_repeat_password)
        Me.group_security.Controls.Add(Me.Label9)
        Me.group_security.Controls.Add(Me.text_password)
        Me.group_security.Controls.Add(Me.Label4)
        Me.group_security.Controls.Add(Me.text_username)
        Me.group_security.Controls.Add(Me.Label3)
        Me.group_security.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_security.ForeColor = System.Drawing.Color.Black
        Me.group_security.Location = New System.Drawing.Point(12, 191)
        Me.group_security.Name = "group_security"
        Me.group_security.Size = New System.Drawing.Size(392, 150)
        Me.group_security.TabIndex = 1
        Me.group_security.TabStop = False
        Me.group_security.Text = "Account Security"
        '
        'text_repeat_password
        '
        Me.text_repeat_password.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_repeat_password.ForeColor = System.Drawing.Color.Black
        Me.text_repeat_password.Location = New System.Drawing.Point(85, 105)
        Me.text_repeat_password.Name = "text_repeat_password"
        Me.text_repeat_password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.text_repeat_password.Size = New System.Drawing.Size(290, 29)
        Me.text_repeat_password.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(7, 109)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 17)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Repeat"
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_save)
        Me.panel_buttons.Location = New System.Drawing.Point(208, 347)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(115, 74)
        Me.panel_buttons.TabIndex = 8
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 12
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_save
        '
        Me.button_save.BackColor = System.Drawing.Color.Transparent
        Me.button_save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_save.FlatAppearance.BorderSize = 0
        Me.button_save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_save.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_save.Image = CType(resources.GetObject("button_save.Image"), System.Drawing.Image)
        Me.button_save.Location = New System.Drawing.Point(1, 2)
        Me.button_save.Name = "button_save"
        Me.button_save.Size = New System.Drawing.Size(53, 65)
        Me.button_save.TabIndex = 11
        Me.button_save.Text = "Save"
        Me.button_save.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_save.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.text_email)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(12, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(392, 77)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Contact Information"
        '
        'text_email
        '
        Me.text_email.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_email.ForeColor = System.Drawing.Color.Black
        Me.text_email.Location = New System.Drawing.Point(85, 34)
        Me.text_email.Name = "text_email"
        Me.text_email.Size = New System.Drawing.Size(290, 29)
        Me.text_email.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(12, 38)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 17)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Email"
        '
        'toggle_finance
        '
        Me.toggle_finance.BackColor = System.Drawing.Color.Transparent
        Me.toggle_finance.Checked = True
        Me.toggle_finance.DisplayStatus = True
        Me.toggle_finance.Location = New System.Drawing.Point(17, 259)
        Me.toggle_finance.Margin = New System.Windows.Forms.Padding(125, 452, 125, 452)
        Me.toggle_finance.Name = "toggle_finance"
        Me.toggle_finance.Size = New System.Drawing.Size(166, 51)
        Me.toggle_finance.TabIndex = 11
        Me.toggle_finance.Text = "Finance"
        '
        'form_user_editor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(614, 428)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_permissions)
        Me.Controls.Add(Me.group_security)
        Me.Controls.Add(Me.group_personal_info)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_user_editor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Profile"
        Me.group_personal_info.ResumeLayout(False)
        Me.group_personal_info.PerformLayout()
        Me.group_permissions.ResumeLayout(False)
        Me.group_security.ResumeLayout(False)
        Me.group_security.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_personal_info As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents text_last_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents text_first_name As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents text_username As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents text_password As System.Windows.Forms.TextBox
    Friend WithEvents group_permissions As System.Windows.Forms.GroupBox
    Friend WithEvents group_security As System.Windows.Forms.GroupBox
    Friend WithEvents text_repeat_password As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_save As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents text_email As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents toggle_administrator As DevaReePharmacy.SwitchToggle
    Friend WithEvents toggle_stocks As DevaReePharmacy.SwitchToggle
    Friend WithEvents toggle_products As DevaReePharmacy.SwitchToggle
    Friend WithEvents toggle_cashier As DevaReePharmacy.SwitchToggle
    Friend WithEvents toggle_agent As DevaReePharmacy.SwitchToggle
    Friend WithEvents toggle_finance As DevaReePharmacy.SwitchToggle
End Class
