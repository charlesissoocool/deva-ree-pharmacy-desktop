﻿Public Class form_manifest_selector

    Private sql As New class_db_stocks
    Private _supplier_id As Integer = 0
    Private _manifest_id As Integer = 0
    Private _checked As Boolean = False

    Private Sub AdjustInterface() Handles Me.Load

        Dim supplier_list As New AutoCompleteStringCollection
        Dim suppliers = sql.GetSuppliers
        For Each supplier_detail In suppliers
            supplier_list.Add(supplier_detail("s_supplier_name"))
        Next
        text_supplier_name.AutoCompleteCustomSource = supplier_list
        text_supplier_name.BackColor = module_colors.color_red
        text_supplier_name.ForeColor = Color.White
        text_manifest_code.BackColor = module_colors.color_red
        text_manifest_code.BackColor = Color.White

    End Sub

    Public Function OpenManifestSelectorDialog(ByVal parent_form As Form) As Integer

        Me.ShowDialog(parent_form)
        OpenManifestSelectorDialog = If(_checked, _manifest_id, 0)

    End Function

    Private Sub text_supplier_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_supplier_name.TextChanged

        Dim supplier_id = sql.GetSupplierID(text_supplier_name.Text)
        If supplier_id > 0 Then
            _supplier_id = supplier_id
            Dim manifest_list As New AutoCompleteStringCollection
            Dim manifests = sql.GetManifestDetailsUnderSupplier(supplier_id)
            For Each manifest_detail In manifests
                manifest_list.Add(manifest_detail("md_manifest_code"))
            Next
            text_manifest_code.AutoCompleteCustomSource = manifest_list
            text_manifest_code.Enabled = True
            text_supplier_name.BackColor = Color.White
            text_supplier_name.ForeColor = Color.Black
        Else
            text_supplier_name.BackColor = module_colors.color_red
            text_supplier_name.ForeColor = Color.White
            _supplier_id = 0
            _manifest_id = 0
            text_manifest_code.Enabled = False
        End If
        text_manifest_code.Text = ""

    End Sub

    Private Sub text_manifest_code_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_manifest_code.TextChanged

        If _supplier_id > 0 Then
            Dim manifest_id = sql.GetManifestIDByCodeAndSupplier(_supplier_id, text_manifest_code.Text)
            If manifest_id > 0 Then
                _manifest_id = manifest_id
                text_manifest_code.BackColor = Color.White
                text_manifest_code.ForeColor = Color.Black
                button_ok.Enabled = True
            Else
                text_manifest_code.BackColor = module_colors.color_red
                text_manifest_code.ForeColor = Color.White
                button_ok.Enabled = False
            End If
        Else
            text_manifest_code.Enabled = False
        End If

    End Sub

    Private Sub button_ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_ok.Click

        If _manifest_id = 0 Then
            MsgBox("No manifest matches the details you provided. Please enter manifest details correctly.", MsgBoxStyle.Exclamation)
        Else
            _checked = True
            Me.Close()
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        _manifest_id = 0
        Me.Close()

    End Sub
End Class