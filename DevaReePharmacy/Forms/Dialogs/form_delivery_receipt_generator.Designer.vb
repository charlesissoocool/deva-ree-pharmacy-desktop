﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_delivery_receipt_generator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_delivery_receipt_generator))
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_print = New System.Windows.Forms.Button()
        Me.load_preview = New System.Windows.Forms.Timer(Me.components)
        Me.pdf_generator = New DevaReePharmacy.PDFGenerator()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_buttons
        '
        Me.panel_buttons.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_print)
        Me.panel_buttons.Location = New System.Drawing.Point(197, 425)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(140, 71)
        Me.panel_buttons.TabIndex = 23
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(72, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 14
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_print
        '
        Me.button_print.BackColor = System.Drawing.Color.Transparent
        Me.button_print.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_print.FlatAppearance.BorderSize = 0
        Me.button_print.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_print.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_print.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_print.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_print.Image = CType(resources.GetObject("button_print.Image"), System.Drawing.Image)
        Me.button_print.Location = New System.Drawing.Point(3, 3)
        Me.button_print.Name = "button_print"
        Me.button_print.Size = New System.Drawing.Size(63, 65)
        Me.button_print.TabIndex = 13
        Me.button_print.Text = "Print"
        Me.button_print.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_print.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_print.UseVisualStyleBackColor = True
        '
        'load_preview
        '
        Me.load_preview.Enabled = True
        Me.load_preview.Interval = 500
        '
        'pdf_generator
        '
        Me.pdf_generator.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pdf_generator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pdf_generator.CSS = CType(resources.GetObject("pdf_generator.CSS"), System.Collections.Generic.Dictionary(Of String, String))
        Me.pdf_generator.Footer = ""
        Me.pdf_generator.HTML = Nothing
        Me.pdf_generator.Location = New System.Drawing.Point(10, 12)
        Me.pdf_generator.Name = "pdf_generator"
        Me.pdf_generator.Orientation = DevaReePharmacy.PDF_Orientation.Portrait
        Me.pdf_generator.Size = New System.Drawing.Size(458, 407)
        Me.pdf_generator.TabIndex = 22
        Me.pdf_generator.TabStop = False
        Me.pdf_generator.Title = "Document"
        '
        'form_delivery_receipt_generator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(480, 503)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.pdf_generator)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_delivery_receipt_generator"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Delivery Receipt"
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pdf_generator As DevaReePharmacy.PDFGenerator
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_print As System.Windows.Forms.Button
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents load_preview As System.Windows.Forms.Timer
End Class
