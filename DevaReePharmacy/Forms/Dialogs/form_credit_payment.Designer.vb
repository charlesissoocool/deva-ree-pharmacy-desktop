﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_credit_payment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_credit_payment))
        Me.Label6 = New System.Windows.Forms.Label()
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.button_add_payment = New System.Windows.Forms.Button()
        Me.list_payments = New System.Windows.Forms.ListView()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.label_customer_name = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.label_due_date = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.label_remaining_balance = New System.Windows.Forms.Label()
        Me.label_total_payed = New System.Windows.Forms.Label()
        Me.label_total_credit = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.combo_credit_number = New System.Windows.Forms.ComboBox()
        Me.text_pay_amount = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_done = New System.Windows.Forms.Button()
        Me.group_product_details.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 42)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 17)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Credit No."
        '
        'group_product_details
        '
        Me.group_product_details.Controls.Add(Me.button_add_payment)
        Me.group_product_details.Controls.Add(Me.list_payments)
        Me.group_product_details.Controls.Add(Me.label_customer_name)
        Me.group_product_details.Controls.Add(Me.Label7)
        Me.group_product_details.Controls.Add(Me.label_due_date)
        Me.group_product_details.Controls.Add(Me.Label10)
        Me.group_product_details.Controls.Add(Me.label_remaining_balance)
        Me.group_product_details.Controls.Add(Me.label_total_payed)
        Me.group_product_details.Controls.Add(Me.label_total_credit)
        Me.group_product_details.Controls.Add(Me.Label4)
        Me.group_product_details.Controls.Add(Me.Label3)
        Me.group_product_details.Controls.Add(Me.Label2)
        Me.group_product_details.Controls.Add(Me.combo_credit_number)
        Me.group_product_details.Controls.Add(Me.text_pay_amount)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.ShapeContainer1)
        Me.group_product_details.Controls.Add(Me.Label5)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(8, -2)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(739, 249)
        Me.group_product_details.TabIndex = 15
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Payment Details"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(133, 173)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 21)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "-"
        '
        'button_add_payment
        '
        Me.button_add_payment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_add_payment.BackColor = System.Drawing.Color.Transparent
        Me.button_add_payment.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_payment.FlatAppearance.BorderSize = 0
        Me.button_add_payment.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_add_payment.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_add_payment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_payment.Font = New System.Drawing.Font("Open Sans", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_payment.Image = CType(resources.GetObject("button_add_payment.Image"), System.Drawing.Image)
        Me.button_add_payment.Location = New System.Drawing.Point(656, 31)
        Me.button_add_payment.Name = "button_add_payment"
        Me.button_add_payment.Size = New System.Drawing.Size(70, 34)
        Me.button_add_payment.TabIndex = 29
        Me.button_add_payment.TabStop = False
        Me.button_add_payment.Text = "Add"
        Me.button_add_payment.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.button_add_payment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.button_add_payment.UseVisualStyleBackColor = True
        '
        'list_payments
        '
        Me.list_payments.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader1})
        Me.list_payments.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_payments.Location = New System.Drawing.Point(408, 66)
        Me.list_payments.Name = "list_payments"
        Me.list_payments.Size = New System.Drawing.Size(318, 169)
        Me.list_payments.TabIndex = 27
        Me.list_payments.UseCompatibleStateImageBehavior = False
        Me.list_payments.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Date"
        Me.ColumnHeader2.Width = 167
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Amount"
        Me.ColumnHeader1.Width = 147
        '
        'label_customer_name
        '
        Me.label_customer_name.AutoSize = True
        Me.label_customer_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_customer_name.Location = New System.Drawing.Point(144, 108)
        Me.label_customer_name.Name = "label_customer_name"
        Me.label_customer_name.Size = New System.Drawing.Size(22, 21)
        Me.label_customer_name.TabIndex = 26
        Me.label_customer_name.Text = "--"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(17, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 17)
        Me.Label7.TabIndex = 25
        Me.Label7.Text = "Customer"
        '
        'label_due_date
        '
        Me.label_due_date.AutoSize = True
        Me.label_due_date.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_due_date.Location = New System.Drawing.Point(144, 77)
        Me.label_due_date.Name = "label_due_date"
        Me.label_due_date.Size = New System.Drawing.Size(22, 21)
        Me.label_due_date.TabIndex = 24
        Me.label_due_date.Text = "--"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(17, 80)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(64, 17)
        Me.Label10.TabIndex = 23
        Me.Label10.Text = "Due Date"
        '
        'label_remaining_balance
        '
        Me.label_remaining_balance.AutoSize = True
        Me.label_remaining_balance.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_remaining_balance.Location = New System.Drawing.Point(144, 213)
        Me.label_remaining_balance.Name = "label_remaining_balance"
        Me.label_remaining_balance.Size = New System.Drawing.Size(50, 25)
        Me.label_remaining_balance.TabIndex = 22
        Me.label_remaining_balance.Text = "0.00"
        '
        'label_total_payed
        '
        Me.label_total_payed.AutoSize = True
        Me.label_total_payed.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold)
        Me.label_total_payed.Location = New System.Drawing.Point(144, 172)
        Me.label_total_payed.Name = "label_total_payed"
        Me.label_total_payed.Size = New System.Drawing.Size(50, 25)
        Me.label_total_payed.TabIndex = 21
        Me.label_total_payed.Text = "0.00"
        Me.label_total_payed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'label_total_credit
        '
        Me.label_total_credit.AutoSize = True
        Me.label_total_credit.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold)
        Me.label_total_credit.Location = New System.Drawing.Point(144, 140)
        Me.label_total_credit.Name = "label_total_credit"
        Me.label_total_credit.Size = New System.Drawing.Size(50, 25)
        Me.label_total_credit.TabIndex = 20
        Me.label_total_credit.Text = "0.00"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 218)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(122, 17)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Remaining Balance"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 175)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 17)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Settled Balance"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(16, 143)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 17)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Total Credit"
        '
        'combo_credit_number
        '
        Me.combo_credit_number.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_credit_number.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.combo_credit_number.FormattingEnabled = True
        Me.combo_credit_number.Location = New System.Drawing.Point(147, 36)
        Me.combo_credit_number.Name = "combo_credit_number"
        Me.combo_credit_number.Size = New System.Drawing.Size(238, 29)
        Me.combo_credit_number.TabIndex = 1
        '
        'text_pay_amount
        '
        Me.text_pay_amount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_pay_amount.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_pay_amount.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_pay_amount.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_pay_amount.ForeColor = System.Drawing.Color.Black
        Me.text_pay_amount.Location = New System.Drawing.Point(471, 31)
        Me.text_pay_amount.Name = "text_pay_amount"
        Me.text_pay_amount.Size = New System.Drawing.Size(179, 29)
        Me.text_pay_amount.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(407, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 17)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Amount"
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(3, 31)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape2, Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(733, 215)
        Me.ShapeContainer1.TabIndex = 28
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape2
        '
        Me.LineShape2.BorderColor = System.Drawing.Color.LightGray
        Me.LineShape2.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dash
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 242
        Me.LineShape2.X2 = 144
        Me.LineShape2.Y1 = 173
        Me.LineShape2.Y2 = 173
        '
        'LineShape1
        '
        Me.LineShape1.BorderColor = System.Drawing.Color.LightGray
        Me.LineShape1.BorderStyle = System.Drawing.Drawing2D.DashStyle.Dash
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 392
        Me.LineShape1.X2 = 392
        Me.LineShape1.Y1 = 2
        Me.LineShape1.Y2 = 203
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_done)
        Me.panel_buttons.Location = New System.Drawing.Point(8, 253)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(62, 74)
        Me.panel_buttons.TabIndex = 18
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(4, 3)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 3
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'form_credit_payment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(754, 326)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_details)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "form_credit_payment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Credit Payment"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_done As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents text_pay_amount As System.Windows.Forms.TextBox
    Friend WithEvents combo_credit_number As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents label_remaining_balance As System.Windows.Forms.Label
    Friend WithEvents label_total_payed As System.Windows.Forms.Label
    Friend WithEvents label_total_credit As System.Windows.Forms.Label
    Friend WithEvents label_due_date As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents label_customer_name As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents list_payments As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents button_add_payment As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
End Class
