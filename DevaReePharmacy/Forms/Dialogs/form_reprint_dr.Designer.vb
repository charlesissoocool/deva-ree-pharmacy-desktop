﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_reprint_dr
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_reprint_dr))
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.date_transacted = New System.Windows.Forms.DateTimePicker()
        Me.text_customer_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.text_dr_no = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_lookup = New System.Windows.Forms.Button()
        Me.group_product_details.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'group_product_details
        '
        Me.group_product_details.Controls.Add(Me.date_transacted)
        Me.group_product_details.Controls.Add(Me.text_customer_name)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.text_dr_no)
        Me.group_product_details.Controls.Add(Me.Label3)
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(9, 0)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(373, 145)
        Me.group_product_details.TabIndex = 3
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Delivery Receipt Information"
        '
        'date_transacted
        '
        Me.date_transacted.CalendarMonthBackground = System.Drawing.Color.White
        Me.date_transacted.CustomFormat = "yyyy / MM / dd"
        Me.date_transacted.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.date_transacted.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.date_transacted.Location = New System.Drawing.Point(122, 69)
        Me.date_transacted.Name = "date_transacted"
        Me.date_transacted.Size = New System.Drawing.Size(238, 29)
        Me.date_transacted.TabIndex = 2
        '
        'text_customer_name
        '
        Me.text_customer_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_customer_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_customer_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_customer_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_customer_name.ForeColor = System.Drawing.Color.Black
        Me.text_customer_name.Location = New System.Drawing.Point(122, 34)
        Me.text_customer_name.Name = "text_customer_name"
        Me.text_customer_name.Size = New System.Drawing.Size(238, 29)
        Me.text_customer_name.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 110)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 17)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "D.R. No."
        '
        'text_dr_no
        '
        Me.text_dr_no.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_dr_no.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_dr_no.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_dr_no.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_dr_no.ForeColor = System.Drawing.Color.Black
        Me.text_dr_no.Location = New System.Drawing.Point(122, 104)
        Me.text_dr_no.Name = "text_dr_no"
        Me.text_dr_no.Size = New System.Drawing.Size(238, 29)
        Me.text_dr_no.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(105, 17)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Date Transacted"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 17)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Customer"
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_lookup)
        Me.panel_buttons.Location = New System.Drawing.Point(9, 151)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(373, 74)
        Me.panel_buttons.TabIndex = 18
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(59, 2)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 5
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_lookup
        '
        Me.button_lookup.BackColor = System.Drawing.Color.Transparent
        Me.button_lookup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_lookup.FlatAppearance.BorderSize = 0
        Me.button_lookup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_lookup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_lookup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_lookup.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_lookup.Image = CType(resources.GetObject("button_lookup.Image"), System.Drawing.Image)
        Me.button_lookup.Location = New System.Drawing.Point(1, 2)
        Me.button_lookup.Name = "button_lookup"
        Me.button_lookup.Size = New System.Drawing.Size(58, 65)
        Me.button_lookup.TabIndex = 4
        Me.button_lookup.Text = "Lookup"
        Me.button_lookup.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_lookup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_lookup.UseVisualStyleBackColor = True
        '
        'form_reprint_dr
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(392, 220)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_details)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_reprint_dr"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reprint D.R."
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents text_dr_no As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_lookup As System.Windows.Forms.Button
    Friend WithEvents text_customer_name As System.Windows.Forms.TextBox
    Friend WithEvents date_transacted As System.Windows.Forms.DateTimePicker
End Class
