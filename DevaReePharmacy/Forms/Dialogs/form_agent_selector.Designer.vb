﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_agent_selector
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_agent_selector))
        Me.Label6 = New System.Windows.Forms.Label()
        Me.text_agent_name = New System.Windows.Forms.TextBox()
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.panel_buttons = New System.Windows.Forms.Panel()
        Me.button_add_agent = New System.Windows.Forms.Button()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.group_product_details.SuspendLayout()
        Me.panel_buttons.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(15, 40)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 17)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Agent Name"
        '
        'text_agent_name
        '
        Me.text_agent_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_agent_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_agent_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_agent_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_agent_name.ForeColor = System.Drawing.Color.Black
        Me.text_agent_name.Location = New System.Drawing.Point(115, 35)
        Me.text_agent_name.Name = "text_agent_name"
        Me.text_agent_name.Size = New System.Drawing.Size(243, 29)
        Me.text_agent_name.TabIndex = 11
        '
        'group_product_details
        '
        Me.group_product_details.Controls.Add(Me.Label6)
        Me.group_product_details.Controls.Add(Me.text_agent_name)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(8, -2)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(373, 80)
        Me.group_product_details.TabIndex = 15
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Agent Information"
        '
        'panel_buttons
        '
        Me.panel_buttons.Controls.Add(Me.button_add_agent)
        Me.panel_buttons.Controls.Add(Me.button_cancel)
        Me.panel_buttons.Controls.Add(Me.button_done)
        Me.panel_buttons.Location = New System.Drawing.Point(8, 84)
        Me.panel_buttons.Name = "panel_buttons"
        Me.panel_buttons.Size = New System.Drawing.Size(373, 74)
        Me.panel_buttons.TabIndex = 18
        '
        'button_add_agent
        '
        Me.button_add_agent.BackColor = System.Drawing.Color.Transparent
        Me.button_add_agent.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_agent.FlatAppearance.BorderSize = 0
        Me.button_add_agent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_add_agent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_add_agent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_agent.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_agent.Image = CType(resources.GetObject("button_add_agent.Image"), System.Drawing.Image)
        Me.button_add_agent.Location = New System.Drawing.Point(293, 4)
        Me.button_add_agent.Name = "button_add_agent"
        Me.button_add_agent.Size = New System.Drawing.Size(77, 65)
        Me.button_add_agent.TabIndex = 13
        Me.button_add_agent.Text = "Add Agent"
        Me.button_add_agent.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_add_agent.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_agent.UseVisualStyleBackColor = True
        '
        'button_cancel
        '
        Me.button_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(63, 4)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 12
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(4, 3)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 11
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'form_agent_selector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(391, 156)
        Me.Controls.Add(Me.panel_buttons)
        Me.Controls.Add(Me.group_product_details)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.KeyPreview = True
        Me.Name = "form_agent_selector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Select Agent"
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.panel_buttons.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents text_agent_name As System.Windows.Forms.TextBox
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents panel_buttons As System.Windows.Forms.Panel
    Friend WithEvents button_add_agent As System.Windows.Forms.Button
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
End Class
