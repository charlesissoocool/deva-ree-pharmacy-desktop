﻿Public Class form_order_mode_selector

    Dim _checked As Boolean = False

    Public Function OpenOrderModeDialog(ByVal parent_form As Form, Optional ByVal default_mode As OrderMode = OrderMode.Retail) As OrderMode

        If default_mode = OrderMode.Retail Or default_mode = OrderMode.NoMode Then
            radio_retail.Checked = True
        ElseIf default_mode = OrderMode.Wholesale Then
            radio_wholesale.Checked = True
        Else
            radio_distribution.Checked = True
        End If

        Me.ShowDialog(parent_form)
        If _checked Then
            If radio_retail.Checked Then
                OpenOrderModeDialog = OrderMode.Retail
            ElseIf radio_wholesale.Checked Then
                OpenOrderModeDialog = OrderMode.Wholesale
            ElseIf radio_distribution.Checked Then
                OpenOrderModeDialog = OrderMode.Distribution
            Else
                OpenOrderModeDialog = OrderMode.NoMode
            End If
        Else
            OpenOrderModeDialog = OrderMode.NoMode
        End If

    End Function

    Private Sub form_unit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            _checked = True
            Me.Close()
        ElseIf e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            Me.Close()
        End If

    End Sub

End Class