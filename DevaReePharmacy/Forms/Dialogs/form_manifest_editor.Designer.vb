﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_manifest_editor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_manifest_editor))
        Me.list_stocks = New System.Windows.Forms.ListView()
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.imagelist_small_icons = New System.Windows.Forms.ImageList(Me.components)
        Me.panel_general_controls = New System.Windows.Forms.Panel()
        Me.button_cancel = New System.Windows.Forms.Button()
        Me.button_done = New System.Windows.Forms.Button()
        Me.panel_stock_controls = New System.Windows.Forms.Panel()
        Me.button_migrate = New System.Windows.Forms.Button()
        Me.button_manifest_transfer = New System.Windows.Forms.Button()
        Me.button_delete_stocks = New System.Windows.Forms.Button()
        Me.button_edit_stocks = New System.Windows.Forms.Button()
        Me.button_add_stock = New System.Windows.Forms.Button()
        Me.group_product_details = New System.Windows.Forms.GroupBox()
        Me.label_updated_date = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.label_entry_date = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.text_supplier_name = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.text_manifest_code = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.label_staff_name = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.panel_general_controls.SuspendLayout()
        Me.panel_stock_controls.SuspendLayout()
        Me.group_product_details.SuspendLayout()
        Me.SuspendLayout()
        '
        'list_stocks
        '
        Me.list_stocks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_stocks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.list_stocks.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_stocks.FullRowSelect = True
        Me.list_stocks.GridLines = True
        Me.list_stocks.HideSelection = False
        Me.list_stocks.Location = New System.Drawing.Point(0, 138)
        Me.list_stocks.Name = "list_stocks"
        Me.list_stocks.Size = New System.Drawing.Size(840, 253)
        Me.list_stocks.SmallImageList = Me.imagelist_small_icons
        Me.list_stocks.TabIndex = 3
        Me.list_stocks.TabStop = False
        Me.list_stocks.UseCompatibleStateImageBehavior = False
        Me.list_stocks.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Product Name"
        Me.ColumnHeader10.Width = 146
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Stock Quantity"
        Me.ColumnHeader11.Width = 158
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Lot / Batch No."
        Me.ColumnHeader1.Width = 134
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Expiry Date"
        Me.ColumnHeader2.Width = 111
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Supplier Price"
        Me.ColumnHeader3.Width = 142
        '
        'imagelist_small_icons
        '
        Me.imagelist_small_icons.ImageStream = CType(resources.GetObject("imagelist_small_icons.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imagelist_small_icons.TransparentColor = System.Drawing.Color.Transparent
        Me.imagelist_small_icons.Images.SetKeyName(0, "new")
        Me.imagelist_small_icons.Images.SetKeyName(1, "used")
        '
        'panel_general_controls
        '
        Me.panel_general_controls.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.panel_general_controls.Controls.Add(Me.button_cancel)
        Me.panel_general_controls.Controls.Add(Me.button_done)
        Me.panel_general_controls.Location = New System.Drawing.Point(12, 398)
        Me.panel_general_controls.Name = "panel_general_controls"
        Me.panel_general_controls.Size = New System.Drawing.Size(115, 74)
        Me.panel_general_controls.TabIndex = 17
        '
        'button_cancel
        '
        Me.button_cancel.BackColor = System.Drawing.Color.Transparent
        Me.button_cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_cancel.FlatAppearance.BorderSize = 0
        Me.button_cancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_cancel.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_cancel.Image = CType(resources.GetObject("button_cancel.Image"), System.Drawing.Image)
        Me.button_cancel.Location = New System.Drawing.Point(60, 3)
        Me.button_cancel.Name = "button_cancel"
        Me.button_cancel.Size = New System.Drawing.Size(53, 65)
        Me.button_cancel.TabIndex = 8
        Me.button_cancel.Text = "Cancel"
        Me.button_cancel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_cancel.UseVisualStyleBackColor = True
        '
        'button_done
        '
        Me.button_done.BackColor = System.Drawing.Color.Transparent
        Me.button_done.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_done.Enabled = False
        Me.button_done.FlatAppearance.BorderSize = 0
        Me.button_done.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_done.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_done.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_done.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_done.Image = CType(resources.GetObject("button_done.Image"), System.Drawing.Image)
        Me.button_done.Location = New System.Drawing.Point(1, 2)
        Me.button_done.Name = "button_done"
        Me.button_done.Size = New System.Drawing.Size(53, 65)
        Me.button_done.TabIndex = 7
        Me.button_done.Text = "Done"
        Me.button_done.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_done.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_done.UseVisualStyleBackColor = True
        '
        'panel_stock_controls
        '
        Me.panel_stock_controls.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_stock_controls.Controls.Add(Me.button_migrate)
        Me.panel_stock_controls.Controls.Add(Me.button_manifest_transfer)
        Me.panel_stock_controls.Controls.Add(Me.button_delete_stocks)
        Me.panel_stock_controls.Controls.Add(Me.button_edit_stocks)
        Me.panel_stock_controls.Controls.Add(Me.button_add_stock)
        Me.panel_stock_controls.Location = New System.Drawing.Point(485, 397)
        Me.panel_stock_controls.Name = "panel_stock_controls"
        Me.panel_stock_controls.Size = New System.Drawing.Size(347, 74)
        Me.panel_stock_controls.TabIndex = 18
        '
        'button_migrate
        '
        Me.button_migrate.BackColor = System.Drawing.Color.Transparent
        Me.button_migrate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_migrate.Enabled = False
        Me.button_migrate.FlatAppearance.BorderSize = 0
        Me.button_migrate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_migrate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_migrate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_migrate.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_migrate.Image = CType(resources.GetObject("button_migrate.Image"), System.Drawing.Image)
        Me.button_migrate.Location = New System.Drawing.Point(178, 3)
        Me.button_migrate.Name = "button_migrate"
        Me.button_migrate.Size = New System.Drawing.Size(59, 65)
        Me.button_migrate.TabIndex = 8
        Me.button_migrate.Text = "Migrate"
        Me.button_migrate.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_migrate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_migrate.UseVisualStyleBackColor = True
        '
        'button_manifest_transfer
        '
        Me.button_manifest_transfer.BackColor = System.Drawing.Color.Transparent
        Me.button_manifest_transfer.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_manifest_transfer.Enabled = False
        Me.button_manifest_transfer.FlatAppearance.BorderSize = 0
        Me.button_manifest_transfer.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_manifest_transfer.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_manifest_transfer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_manifest_transfer.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_manifest_transfer.Image = CType(resources.GetObject("button_manifest_transfer.Image"), System.Drawing.Image)
        Me.button_manifest_transfer.Location = New System.Drawing.Point(233, 3)
        Me.button_manifest_transfer.Name = "button_manifest_transfer"
        Me.button_manifest_transfer.Size = New System.Drawing.Size(108, 65)
        Me.button_manifest_transfer.TabIndex = 7
        Me.button_manifest_transfer.Text = "Manifest Transfer"
        Me.button_manifest_transfer.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_manifest_transfer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_manifest_transfer.UseVisualStyleBackColor = True
        '
        'button_delete_stocks
        '
        Me.button_delete_stocks.BackColor = System.Drawing.Color.Transparent
        Me.button_delete_stocks.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_delete_stocks.Enabled = False
        Me.button_delete_stocks.FlatAppearance.BorderSize = 0
        Me.button_delete_stocks.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_delete_stocks.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_delete_stocks.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_delete_stocks.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_delete_stocks.Image = CType(resources.GetObject("button_delete_stocks.Image"), System.Drawing.Image)
        Me.button_delete_stocks.Location = New System.Drawing.Point(119, 2)
        Me.button_delete_stocks.Name = "button_delete_stocks"
        Me.button_delete_stocks.Size = New System.Drawing.Size(53, 65)
        Me.button_delete_stocks.TabIndex = 6
        Me.button_delete_stocks.Text = "Delete"
        Me.button_delete_stocks.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_delete_stocks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_delete_stocks.UseVisualStyleBackColor = True
        '
        'button_edit_stocks
        '
        Me.button_edit_stocks.BackColor = System.Drawing.Color.Transparent
        Me.button_edit_stocks.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_edit_stocks.Enabled = False
        Me.button_edit_stocks.FlatAppearance.BorderSize = 0
        Me.button_edit_stocks.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_edit_stocks.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_edit_stocks.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_edit_stocks.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_edit_stocks.Image = CType(resources.GetObject("button_edit_stocks.Image"), System.Drawing.Image)
        Me.button_edit_stocks.Location = New System.Drawing.Point(60, 3)
        Me.button_edit_stocks.Name = "button_edit_stocks"
        Me.button_edit_stocks.Size = New System.Drawing.Size(53, 65)
        Me.button_edit_stocks.TabIndex = 5
        Me.button_edit_stocks.Text = "Edit"
        Me.button_edit_stocks.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_edit_stocks.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_edit_stocks.UseVisualStyleBackColor = True
        '
        'button_add_stock
        '
        Me.button_add_stock.BackColor = System.Drawing.Color.Transparent
        Me.button_add_stock.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_stock.FlatAppearance.BorderSize = 0
        Me.button_add_stock.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.button_add_stock.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.button_add_stock.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_stock.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_stock.Image = CType(resources.GetObject("button_add_stock.Image"), System.Drawing.Image)
        Me.button_add_stock.Location = New System.Drawing.Point(1, 2)
        Me.button_add_stock.Name = "button_add_stock"
        Me.button_add_stock.Size = New System.Drawing.Size(53, 65)
        Me.button_add_stock.TabIndex = 4
        Me.button_add_stock.Text = "Add"
        Me.button_add_stock.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_add_stock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_stock.UseVisualStyleBackColor = True
        '
        'group_product_details
        '
        Me.group_product_details.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.group_product_details.Controls.Add(Me.label_updated_date)
        Me.group_product_details.Controls.Add(Me.Label7)
        Me.group_product_details.Controls.Add(Me.label_entry_date)
        Me.group_product_details.Controls.Add(Me.Label4)
        Me.group_product_details.Controls.Add(Me.text_supplier_name)
        Me.group_product_details.Controls.Add(Me.Label2)
        Me.group_product_details.Controls.Add(Me.text_manifest_code)
        Me.group_product_details.Controls.Add(Me.Label1)
        Me.group_product_details.Controls.Add(Me.label_staff_name)
        Me.group_product_details.Controls.Add(Me.Label3)
        Me.group_product_details.Font = New System.Drawing.Font("Segoe UI Light", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_product_details.ForeColor = System.Drawing.Color.Black
        Me.group_product_details.Location = New System.Drawing.Point(9, -1)
        Me.group_product_details.Name = "group_product_details"
        Me.group_product_details.Size = New System.Drawing.Size(823, 133)
        Me.group_product_details.TabIndex = 19
        Me.group_product_details.TabStop = False
        Me.group_product_details.Text = "Manifest Details"
        '
        'label_updated_date
        '
        Me.label_updated_date.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_updated_date.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.label_updated_date.Location = New System.Drawing.Point(502, 93)
        Me.label_updated_date.Name = "label_updated_date"
        Me.label_updated_date.Size = New System.Drawing.Size(310, 21)
        Me.label_updated_date.TabIndex = 16
        Me.label_updated_date.Text = "--"
        Me.label_updated_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label7.Location = New System.Drawing.Point(673, 76)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 17)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Last Updated"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'label_entry_date
        '
        Me.label_entry_date.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_entry_date.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.label_entry_date.Location = New System.Drawing.Point(498, 51)
        Me.label_entry_date.Name = "label_entry_date"
        Me.label_entry_date.Size = New System.Drawing.Size(314, 21)
        Me.label_entry_date.TabIndex = 14
        Me.label_entry_date.Text = "--"
        Me.label_entry_date.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label4.Location = New System.Drawing.Point(728, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 17)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Added on"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'text_supplier_name
        '
        Me.text_supplier_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_supplier_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_supplier_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_supplier_name.ForeColor = System.Drawing.Color.Black
        Me.text_supplier_name.Location = New System.Drawing.Point(116, 61)
        Me.text_supplier_name.MaxLength = 25
        Me.text_supplier_name.Name = "text_supplier_name"
        Me.text_supplier_name.Size = New System.Drawing.Size(348, 29)
        Me.text_supplier_name.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label2.Location = New System.Drawing.Point(12, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 17)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Supplier"
        '
        'text_manifest_code
        '
        Me.text_manifest_code.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_manifest_code.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_manifest_code.ForeColor = System.Drawing.Color.Black
        Me.text_manifest_code.Location = New System.Drawing.Point(116, 96)
        Me.text_manifest_code.MaxLength = 25
        Me.text_manifest_code.Name = "text_manifest_code"
        Me.text_manifest_code.Size = New System.Drawing.Size(348, 29)
        Me.text_manifest_code.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label1.Location = New System.Drawing.Point(12, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 17)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Manifest Code"
        '
        'label_staff_name
        '
        Me.label_staff_name.AutoSize = True
        Me.label_staff_name.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.label_staff_name.Location = New System.Drawing.Point(112, 37)
        Me.label_staff_name.Name = "label_staff_name"
        Me.label_staff_name.Size = New System.Drawing.Size(22, 21)
        Me.label_staff_name.TabIndex = 10
        Me.label_staff_name.Text = "--"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label3.Location = New System.Drawing.Point(12, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 17)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Staff Uploader"
        '
        'form_manifest_editor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(839, 477)
        Me.Controls.Add(Me.group_product_details)
        Me.Controls.Add(Me.panel_stock_controls)
        Me.Controls.Add(Me.panel_general_controls)
        Me.Controls.Add(Me.list_stocks)
        Me.Name = "form_manifest_editor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Manifest"
        Me.panel_general_controls.ResumeLayout(False)
        Me.panel_stock_controls.ResumeLayout(False)
        Me.group_product_details.ResumeLayout(False)
        Me.group_product_details.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents list_stocks As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents panel_general_controls As System.Windows.Forms.Panel
    Friend WithEvents button_cancel As System.Windows.Forms.Button
    Friend WithEvents button_done As System.Windows.Forms.Button
    Friend WithEvents panel_stock_controls As System.Windows.Forms.Panel
    Friend WithEvents button_delete_stocks As System.Windows.Forms.Button
    Friend WithEvents button_edit_stocks As System.Windows.Forms.Button
    Friend WithEvents button_add_stock As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents group_product_details As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents label_staff_name As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents text_manifest_code As System.Windows.Forms.TextBox
    Friend WithEvents text_supplier_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents label_updated_date As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents label_entry_date As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents button_manifest_transfer As System.Windows.Forms.Button
    Friend WithEvents button_migrate As System.Windows.Forms.Button
    Friend WithEvents imagelist_small_icons As System.Windows.Forms.ImageList
End Class
