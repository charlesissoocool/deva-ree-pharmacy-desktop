﻿Public Class form_dashboard
    Dim querierProd As New class_db_dashboard

    Dim startCritical As Integer = 0
    Dim startExpire As Integer = 0
    Dim startDue As Integer = 0

    Dim critical_prod = querierProd.GetCriticalProducts(startCritical, 4)
    Dim expire_prod = querierProd.GetExpiringProducts(startExpire, 4)
    Dim due_credit = querierProd.GetCreditDues(startDue, 4)
    Dim critical_prodCount = querierProd.GetCriticalProductsCount()
    Dim expire_prodCount = querierProd.GetExpiringProductCount()
    Dim due_creditCount = querierProd.GetCreditDuesCount()

    Dim _clear_for_close As Boolean = False
    Dim sql As New class_db_dashboard

    Private Sub form_dashboard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If _clear_for_close Then
            e.Cancel = False
        Else
            e.Cancel = True
            Logout()
        End If

    End Sub

    'added 07/17/2015 by Aljun shortcut in each functions
    Private Sub form_dashboard_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Select Case e.KeyCode
            Case Keys.F1
                Dim transaction_form As New form_order
                transaction_form.ShowDialog(Me)
                AdjustInterface()
                AdjustObjects()
            Case Keys.F2

            Case Keys.F3
                For Each open_form As Form In Application.OpenForms
                    If open_form.GetType Is form_product_management.GetType Then
                        If open_form.WindowState = FormWindowState.Minimized Then open_form.WindowState = FormWindowState.Normal
                        open_form.Focus()
                        Exit Sub
                    End If
                Next

                Dim product_form As New form_product_management
                product_form.Show(Me)
                AdjustInterface()

            Case Keys.F4
                Dim stock_form As New form_stock_management
                stock_form.Show(Me)
                'list_critical_products.Items.Clear()
                'list_expiring_products.Items.Clear()
                AdjustInterface()
            Case Keys.F5
                For Each open_form As Form In Application.OpenForms
                    If open_form.GetType Is form_staff_management.GetType Then
                        If open_form.WindowState = FormWindowState.Minimized Then open_form.WindowState = FormWindowState.Normal
                        open_form.Focus()
                        Exit Sub
                    End If
                Next

                Dim staff_form As New form_staff_management
                staff_form.Show()
                AdjustInterface()

        End Select
    End Sub

    Private Sub ReloadChart()

        Dim labels(23) As String
        Dim loss As Dictionary(Of String, Object)() = sql.GetHourlyLossForToday
        Dim profit As Dictionary(Of String, Object)() = sql.GetHourlyProfitForToday
        Dim gross As Dictionary(Of String, Object)() = sql.GetHourlyGrossForToday
        Dim loss_values(23) As Double
        Dim profit_values(23) As Double
        Dim gross_values(23) As Double

        If Not IsNothing(loss) Then
            For Each loss_value In loss
                If Not IsNumeric(loss_value("hour")) Or (Val(loss_value("hour")) < 0 And Val(loss_value("hour")) > 23) Then Continue For
                loss_values(loss_value("hour")) = Math.Abs(loss_value("loss"))
            Next
        End If

        If Not IsNothing(profit) Then
            For Each profit_value In profit
                If Not IsNumeric(profit_value("hour")) Or (Val(profit_value("hour")) < 0 And Val(profit_value("hour")) > 23) Then Continue For
                profit_values(profit_value("hour")) = profit_value("profit")
            Next
        End If

        If Not IsNothing(gross) Then
            For Each gross_value In gross
                If Not IsNumeric(gross_value("hour")) Or (Val(gross_value("hour")) < 0 And Val(gross_value("hour")) > 23) Then Continue For
                gross_values(gross_value("hour")) = gross_value("gross")
            Next
        End If

        For i = 0 To 23
            labels(i) = i & "H"
        Next

        chart_gpl_report.SetTitle("Gross Vs. Profit Vs. Loss Chart")
        chart_gpl_report.RemoveAllChartData()
        chart_gpl_report.SetLabels(labels)
        chart_gpl_report.SetValuePattern(SYS_CURRENCY & "%value%")
        chart_gpl_report.AddChartData(loss_values, ChartLineColors.Red, "Loss")
        chart_gpl_report.AddChartData(profit_values, ChartLineColors.Green, "Profit")
        chart_gpl_report.AddChartData(gross_values, ChartLineColors.Blue, "Gross")

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        list_critical_products.Items.Clear()
        list_expiring_products.Items.Clear()
        list_credit_dues.Items.Clear()

        panel_welcome_message.BackColor = module_colors.color_gray
        button_transaction.FlatAppearance.MouseOverBackColor = module_colors.color_light_blue
        button_product_management.FlatAppearance.MouseOverBackColor = module_colors.color_light_blue
        button_staff_management.FlatAppearance.MouseOverBackColor = module_colors.color_light_blue
        button_stock_management.FlatAppearance.MouseOverBackColor = module_colors.color_light_blue
        button_customer_management.FlatAppearance.MouseOverBackColor = module_colors.color_light_blue
        button_sales_management.FlatAppearance.MouseOverBackColor = module_colors.color_light_blue
        group_critical_products.Visible = True
        group_expiring_products.Visible = True
        group_credit_dues.Visible = True
        prompt_critical_products.Message = "Low Quantity Stocks"
        prompt_critical_products.Mode = PromptLabel.PromptLabelMode.Warning
        prompt_expiring_products.Message = "Expiring Stocks"
        prompt_expiring_products.Mode = PromptLabel.PromptLabelMode.Critical
        prompt_credit_dues.Message = "Credits Due"
        prompt_credit_dues.Mode = PromptLabel.PromptLabelMode.Warning
        numbers_total_transactions.BackColor = module_colors.color_light_blue
        numbers_total_gross.BackColor = module_colors.color_green
        numbers_total_cash.BackColor = module_colors.color_yellow
        numbers_mine_total_transactions.BackColor = module_colors.color_blue
        numbers_mine_total_gross.BackColor = module_colors.color_button_green
        numbers_mine_total_cash.BackColor = module_colors.color_pink
        numbers_total_gross.HasDecimal = True
        numbers_total_cash.HasDecimal = True
        numbers_total_gross.ShowCurrency = True
        numbers_total_cash.ShowCurrency = True

        Dim user_id As Integer = module_session.current_user_profile.UserID

        button_transaction.Enabled = False

        If module_session.current_user_profile.Permission_Cashier = True Then
            button_transaction.Enabled = True
        End If

        LoadDashboardData()
        LoadDashboardNumbers()
        ReloadChart()

        Me.KeyPreview = True


    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        panel_welcome_message.Width = Me.ClientSize.Width
        group_critical_products.Top = panel_welcome_message.Height
        group_critical_products.Left = 0
        group_critical_products.Width = Me.ClientSize.Width * 0.333
        group_expiring_products.Top = group_critical_products.Top
        group_expiring_products.Left = group_critical_products.Right
        group_expiring_products.Width = group_critical_products.Width
        group_credit_dues.Top = group_critical_products.Top
        group_credit_dues.Left = group_expiring_products.Right
        group_credit_dues.Width = group_critical_products.Width
        panel_numbers.Top = group_critical_products.Top + group_critical_products.Height
        panel_numbers.Left = 0
        list_critical_products.Width = group_critical_products.Width
        list_expiring_products.Width = group_expiring_products.Width
        list_credit_dues.Width = group_credit_dues.Width
        prompt_critical_products.Width = group_critical_products.Width
        prompt_expiring_products.Width = group_expiring_products.Width
        prompt_credit_dues.Width = group_credit_dues.Width
        link_load_more_critical.Left = (group_critical_products.Width / 2) - (link_load_more_critical.Width / 2)
        link_load_more_expiring.Left = (group_expiring_products.Width / 2) - (link_load_more_expiring.Width / 2)
        link_load_more_dues.Left = (group_credit_dues.Width / 2) - (link_load_more_dues.Width / 2)
        chart_gpl_report.Top = panel_numbers.Bottom
        chart_gpl_report.Left = 0
        chart_gpl_report.Width = Me.ClientSize.Width
        chart_gpl_report.Height = Me.ClientSize.Height - chart_gpl_report.Top

    End Sub

    Private Sub LoadDashboardNumbers()

        numbers_total_transactions.Visible = module_session.current_user_profile.Permission_Administator
        numbers_total_gross.Visible = numbers_total_transactions.Visible
        numbers_total_cash.Visible = numbers_total_transactions.Visible

        If module_session.current_user_profile.Permission_Administator Then
            numbers_total_transactions.Value = sql.GetTotalTransactions()
            numbers_total_gross.Value = FormatNumber(sql.GetTotalGrossDaily(), 2, , , TriState.True)
            numbers_total_cash.Value = sql.GetTotalCash()
        End If

        If module_session.current_user_profile.Permission_Cashier Then
            numbers_mine_total_transactions.Value = sql.GetTotalTransactionsByStaff(module_session.current_user_profile.UserID)
            numbers_mine_total_gross.Value = FormatNumber(sql.GetTotalGrossDaily(module_session.current_user_profile.UserID), 2, , , TriState.True)
            numbers_mine_total_cash.Value = sql.GetTotalCash(module_session.current_user_profile.UserID)
        Else
            numbers_mine_total_transactions.Value = 0
            numbers_mine_total_gross.Value = 0
            numbers_mine_total_cash.Value = 0
        End If

    End Sub

    Private Sub LoadDashboardData()
        startCritical = 0
        startExpire = 0
        startDue = 0

        critical_prod = querierProd.GetCriticalProducts(startCritical, 4)
        expire_prod = querierProd.GetExpiringProducts(startExpire, 4)
        due_credit = querierProd.GetCreditDues(startDue, 4)
        critical_prodCount = querierProd.GetCriticalProductsCount()
        expire_prodCount = querierProd.GetExpiringProductCount()
        due_creditCount = querierProd.GetCreditDuesCount()


        'Jun diri ag pag initiate sa mga welcome message
        label_welcome_message.Text = "Welcome back " & module_session.current_user_profile.FirstName & Space(1) & module_session.current_user_profile.LastName & "!"
        'button_transaction.Left = label_welcome_message.Left + label_welcome_message.Width + 10
        'button_management.Left = button_transaction.Left + button_transaction.Width + 10
        label_last_login.Text = "Last Login: " & module_session.current_user_profile.LastLogin

        If critical_prodCount <= 4 Then
            link_load_more_critical.Hide()
        Else
            link_load_more_critical.Show()
        End If

        If expire_prodCount <= 4 Then
            link_load_more_expiring.Hide()
        Else
            link_load_more_expiring.Show()
        End If

        If due_creditCount <= 4 Then
            link_load_more_dues.Hide()
        Else
            link_load_more_dues.Show()
        End If

        button_product_management.Enabled = module_session.current_user_profile.Permission_Products
        button_stock_management.Enabled = module_session.current_user_profile.Permission_Stocks
        button_staff_management.Enabled = module_session.current_user_profile.Permission_Administator
        button_transaction.Enabled = module_session.current_user_profile.Permission_Cashier
        button_customer_management.Enabled = module_session.current_user_profile.Permission_Finance
        button_sales_management.Enabled = module_session.current_user_profile.Permission_Finance

        If Not IsNothing(critical_prod) Then link_load_more_critical_LinkClicked(Nothing, Nothing)
        If Not IsNothing(expire_prod) Then link_load_more_expiring_LinkClicked(Nothing, Nothing)
        If Not IsNothing(due_credit) Then link_load_more_dues_LinkClicked(Nothing, Nothing)

    End Sub

    Private Sub Logout()

        Dim user_id As Integer
        Dim querier = New class_db_login
        user_id = module_session.current_user_profile.UserID
        Select Case MessageBox.Show("Are you sure you want to logout?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            Case Windows.Forms.DialogResult.Yes
                'querier.AddUserLog("Out", user_id)
                module_session.ClearSessionVariables()
                Dim form_login = New form_login
                form_login.Show()
                _clear_for_close = True
                Me.Close()
        End Select

    End Sub

    Private Sub link_logout_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles link_logout.LinkClicked

        Logout()

    End Sub

    Private Sub link_load_more_critical_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles link_load_more_critical.LinkClicked
        critical_prod = querierProd.GetCriticalProducts(startCritical, 4)
        If Not IsNothing(critical_prod) Then
            For Each prod In critical_prod
                Dim NewItem As ListViewItem = list_critical_products.Items.Add(prod("product_name"))
                NewItem.SubItems.Add(Val(prod("stock_quantity")))
                If Val(prod("stock_quantity")) <= 0 Then
                    NewItem.BackColor = module_colors.color_red
                    NewItem.ForeColor = Color.White
                Else
                    NewItem.BackColor = Color.White
                    NewItem.ForeColor = Color.Black
                End If
            Next
            list_critical_products.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)
            list_critical_products.Items(list_critical_products.Items.Count - 1).EnsureVisible()
        End If

        startCritical = startCritical + 4
        If list_critical_products.Items.Count = critical_prodCount Then
            link_load_more_critical.Hide()
        End If
    End Sub

    Private Sub link_load_more_expiring_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles link_load_more_expiring.LinkClicked
        expire_prod = querierProd.GetExpiringProducts(startExpire, 4)

        If Not IsNothing(expire_prod) Then
            For Each exp_prod In expire_prod
                Dim NewItem As ListViewItem = list_expiring_products.Items.Add(exp_prod("product_name"))
                NewItem.SubItems.Add(exp_prod("stock_quantity"))
                NewItem.SubItems.Add(exp_prod("lot_number"))
                NewItem.SubItems.Add(exp_prod("expiry_date_formatted"))
            Next
            list_expiring_products.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)
            list_expiring_products.Items(list_expiring_products.Items.Count - 1).EnsureVisible()
        End If
        startExpire = startExpire + 4

        If list_expiring_products.Items.Count = expire_prodCount Then
            link_load_more_expiring.Hide()
        End If
    End Sub

    Private Sub link_load_more_dues_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles link_load_more_dues.LinkClicked

        due_credit = querierProd.GetCreditDues(startDue, 4)

        If Not IsNothing(due_credit) Then
            For Each credit In due_credit
                Dim NewItem As ListViewItem = list_credit_dues.Items.Add(credit("customer_name"))
                NewItem.SubItems.Add(SYS_CURRENCY & FormatNumber(credit("total_balance"), 2, , , TriState.True))
                Dim due_sub As New ListViewItem.ListViewSubItem
                If credit("due_date_days") > 0 Then
                    due_sub.Text = credit("due_date_days") & " day/s"
                ElseIf credit("due_date_days") = 0 Then
                    due_sub.Text = "Today"
                Else
                    due_sub.Text = Math.Abs(credit("due_date_days")) & " day/s ago"
                    'due_sub.Font = New Font(list_credit_dues.Font, FontStyle.Bold)
                    NewItem.BackColor = module_colors.color_red
                    NewItem.ForeColor = Color.White
                End If
                NewItem.SubItems.Add(due_sub)
            Next
            list_credit_dues.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)
            list_credit_dues.Items(list_credit_dues.Items.Count - 1).EnsureVisible()
        End If
        startDue = startDue + 4

        If list_credit_dues.Items.Count = due_creditCount Then
            link_load_more_dues.Hide()
        End If

    End Sub

    Private Sub button_transaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_transaction.Click

        Dim transaction_form As New form_order
        transaction_form.ShowDialog(Me)
        AdjustInterface()
        AdjustObjects()

    End Sub

    Private Sub label_dashboard_header_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_welcome_message.TextChanged, label_last_login.TextChanged

        button_transaction.Left = If(label_welcome_message.Width >= label_last_login.Width, label_welcome_message.Left + label_welcome_message.Width + 10, label_last_login.Left + label_welcome_message.Width + 10)
        button_staff_management.Left = button_transaction.Right + 10
        button_product_management.Left = button_staff_management.Right + 10
        button_stock_management.Left = button_product_management.Right + 10
        button_customer_management.Left = button_stock_management.Right + 10
        button_sales_management.Left = button_customer_management.Right + 10

    End Sub

    Private Sub button_distribution_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Dim distribution_form As New form_distribution
        'distribution_form.ShowDialog(Me)
        AdjustInterface()
        AdjustObjects()

    End Sub

    Private Sub button_staff_management_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_staff_management.Click

        For Each open_form As Form In Application.OpenForms
            If open_form.GetType Is form_staff_management.GetType Then
                If open_form.WindowState = FormWindowState.Minimized Then open_form.WindowState = FormWindowState.Normal
                open_form.Focus()
                Exit Sub
            End If
        Next

        Dim staff_form As New form_staff_management
        staff_form.Show()
        AdjustInterface()

    End Sub

    Private Sub button_product_management_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_product_management.Click

        For Each open_form As Form In Application.OpenForms
            If open_form.GetType Is form_product_management.GetType Then
                If open_form.WindowState = FormWindowState.Minimized Then open_form.WindowState = FormWindowState.Normal
                open_form.Focus()
                Exit Sub
            End If
        Next

        Dim product_form As New form_product_management
        product_form.Show(Me)
        AdjustInterface()

    End Sub

    Private Sub button_stock_management_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_stock_management.Click

        For Each open_form As Form In Application.OpenForms
            If open_form.GetType Is form_stock_management.GetType Then
                If open_form.WindowState = FormWindowState.Minimized Then open_form.WindowState = FormWindowState.Normal
                open_form.Focus()
                Exit Sub
            End If
        Next

        Dim stock_form As New form_stock_management
        stock_form.Show(Me)
        'list_critical_products.Items.Clear()
        'list_expiring_products.Items.Clear()
        AdjustInterface()

    End Sub

    Private Sub label_last_login_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_last_login.Click
        Dim form_user_login_history As New form_user_login_history
        form_user_login_history.ShowDialog(Me)
    End Sub

    Private Sub panel_numbers_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles panel_numbers.Layout

        If panel_numbers.Controls(panel_numbers.Controls.Count - 1).Right > panel_numbers.ClientSize.Width Then
            panel_numbers.Height = numbers_total_transactions.Height + 18
        Else
            panel_numbers.Height = numbers_total_transactions.Height
        End If
        chart_gpl_report.Top = panel_numbers.Bottom
        chart_gpl_report.Height = Me.ClientSize.Height - chart_gpl_report.Top

    End Sub

    Private Sub label_welcome_message_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_welcome_message.Click

    End Sub

    Private Sub button_customer_management_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_customer_management.Click

        Dim form_customer_report As New form_customer_report
        form_customer_report.ShowDialog(Me)

    End Sub

    Private Sub button_sales_management_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_sales_management.Click

        Dim form_sales_history As New form_sales_history
        form_sales_history.ShowDialog(Me)

    End Sub
End Class