﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_dashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_dashboard))
        Me.panel_welcome_message = New System.Windows.Forms.Panel()
        Me.button_staff_management = New System.Windows.Forms.Button()
        Me.button_stock_management = New System.Windows.Forms.Button()
        Me.button_product_management = New System.Windows.Forms.Button()
        Me.button_transaction = New System.Windows.Forms.Button()
        Me.link_logout = New System.Windows.Forms.LinkLabel()
        Me.label_last_login = New System.Windows.Forms.Label()
        Me.label_welcome_message = New System.Windows.Forms.Label()
        Me.group_critical_products = New System.Windows.Forms.GroupBox()
        Me.link_load_more_critical = New System.Windows.Forms.LinkLabel()
        Me.list_critical_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.prompt_critical_products = New DevaReePharmacy.PromptLabel()
        Me.group_expiring_products = New System.Windows.Forms.GroupBox()
        Me.link_load_more_expiring = New System.Windows.Forms.LinkLabel()
        Me.list_expiring_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.prompt_expiring_products = New DevaReePharmacy.PromptLabel()
        Me.group_credit_dues = New System.Windows.Forms.GroupBox()
        Me.link_load_more_dues = New System.Windows.Forms.LinkLabel()
        Me.list_credit_dues = New System.Windows.Forms.ListView()
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.prompt_credit_dues = New DevaReePharmacy.PromptLabel()
        Me.numbers_total_gross = New DevaReePharmacy.RunningLabel()
        Me.numbers_total_transactions = New DevaReePharmacy.RunningLabel()
        Me.panel_numbers = New System.Windows.Forms.FlowLayoutPanel()
        Me.numbers_total_cash = New DevaReePharmacy.RunningLabel()
        Me.numbers_mine_total_transactions = New DevaReePharmacy.RunningLabel()
        Me.numbers_mine_total_gross = New DevaReePharmacy.RunningLabel()
        Me.numbers_mine_total_cash = New DevaReePharmacy.RunningLabel()
        Me.chart_gpl_report = New DevaReePharmacy.Chart()
        Me.button_customer_management = New System.Windows.Forms.Button()
        Me.button_sales_management = New System.Windows.Forms.Button()
        Me.panel_welcome_message.SuspendLayout()
        Me.group_critical_products.SuspendLayout()
        Me.group_expiring_products.SuspendLayout()
        Me.group_credit_dues.SuspendLayout()
        Me.panel_numbers.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_welcome_message
        '
        Me.panel_welcome_message.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_welcome_message.BackColor = System.Drawing.Color.Silver
        Me.panel_welcome_message.Controls.Add(Me.button_sales_management)
        Me.panel_welcome_message.Controls.Add(Me.button_customer_management)
        Me.panel_welcome_message.Controls.Add(Me.button_staff_management)
        Me.panel_welcome_message.Controls.Add(Me.button_stock_management)
        Me.panel_welcome_message.Controls.Add(Me.button_product_management)
        Me.panel_welcome_message.Controls.Add(Me.button_transaction)
        Me.panel_welcome_message.Controls.Add(Me.link_logout)
        Me.panel_welcome_message.Controls.Add(Me.label_last_login)
        Me.panel_welcome_message.Controls.Add(Me.label_welcome_message)
        Me.panel_welcome_message.Location = New System.Drawing.Point(0, 0)
        Me.panel_welcome_message.Name = "panel_welcome_message"
        Me.panel_welcome_message.Size = New System.Drawing.Size(1130, 75)
        Me.panel_welcome_message.TabIndex = 0
        '
        'button_staff_management
        '
        Me.button_staff_management.BackColor = System.Drawing.Color.Transparent
        Me.button_staff_management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_staff_management.FlatAppearance.BorderSize = 0
        Me.button_staff_management.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_staff_management.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.button_staff_management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_staff_management.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_staff_management.ForeColor = System.Drawing.Color.White
        Me.button_staff_management.Image = CType(resources.GetObject("button_staff_management.Image"), System.Drawing.Image)
        Me.button_staff_management.Location = New System.Drawing.Point(718, 0)
        Me.button_staff_management.Name = "button_staff_management"
        Me.button_staff_management.Size = New System.Drawing.Size(97, 75)
        Me.button_staff_management.TabIndex = 13
        Me.button_staff_management.Text = "Staff"
        Me.button_staff_management.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_staff_management.UseVisualStyleBackColor = False
        '
        'button_stock_management
        '
        Me.button_stock_management.BackColor = System.Drawing.Color.Transparent
        Me.button_stock_management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_stock_management.FlatAppearance.BorderSize = 0
        Me.button_stock_management.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_stock_management.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.button_stock_management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_stock_management.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_stock_management.ForeColor = System.Drawing.Color.White
        Me.button_stock_management.Image = CType(resources.GetObject("button_stock_management.Image"), System.Drawing.Image)
        Me.button_stock_management.Location = New System.Drawing.Point(615, 0)
        Me.button_stock_management.Name = "button_stock_management"
        Me.button_stock_management.Size = New System.Drawing.Size(97, 75)
        Me.button_stock_management.TabIndex = 12
        Me.button_stock_management.Text = "Stocks"
        Me.button_stock_management.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_stock_management.UseVisualStyleBackColor = False
        '
        'button_product_management
        '
        Me.button_product_management.BackColor = System.Drawing.Color.Transparent
        Me.button_product_management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_product_management.FlatAppearance.BorderSize = 0
        Me.button_product_management.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_product_management.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.button_product_management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_product_management.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_product_management.ForeColor = System.Drawing.Color.White
        Me.button_product_management.Image = CType(resources.GetObject("button_product_management.Image"), System.Drawing.Image)
        Me.button_product_management.Location = New System.Drawing.Point(512, 0)
        Me.button_product_management.Name = "button_product_management"
        Me.button_product_management.Size = New System.Drawing.Size(97, 75)
        Me.button_product_management.TabIndex = 11
        Me.button_product_management.Text = "Products"
        Me.button_product_management.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_product_management.UseVisualStyleBackColor = False
        '
        'button_transaction
        '
        Me.button_transaction.BackColor = System.Drawing.Color.Transparent
        Me.button_transaction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_transaction.FlatAppearance.BorderSize = 0
        Me.button_transaction.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_transaction.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.button_transaction.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_transaction.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_transaction.ForeColor = System.Drawing.Color.White
        Me.button_transaction.Image = CType(resources.GetObject("button_transaction.Image"), System.Drawing.Image)
        Me.button_transaction.Location = New System.Drawing.Point(413, 0)
        Me.button_transaction.Name = "button_transaction"
        Me.button_transaction.Size = New System.Drawing.Size(97, 75)
        Me.button_transaction.TabIndex = 8
        Me.button_transaction.Text = "Transaction"
        Me.button_transaction.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_transaction.UseVisualStyleBackColor = False
        '
        'link_logout
        '
        Me.link_logout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.link_logout.AutoSize = True
        Me.link_logout.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.link_logout.ForeColor = System.Drawing.Color.Black
        Me.link_logout.LinkColor = System.Drawing.Color.White
        Me.link_logout.Location = New System.Drawing.Point(1062, 9)
        Me.link_logout.Name = "link_logout"
        Me.link_logout.Size = New System.Drawing.Size(56, 20)
        Me.link_logout.TabIndex = 3
        Me.link_logout.TabStop = True
        Me.link_logout.Text = "Logout"
        Me.link_logout.VisitedLinkColor = System.Drawing.Color.White
        '
        'label_last_login
        '
        Me.label_last_login.AutoSize = True
        Me.label_last_login.Cursor = System.Windows.Forms.Cursors.Hand
        Me.label_last_login.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_last_login.ForeColor = System.Drawing.Color.White
        Me.label_last_login.Location = New System.Drawing.Point(12, 42)
        Me.label_last_login.Name = "label_last_login"
        Me.label_last_login.Size = New System.Drawing.Size(94, 17)
        Me.label_last_login.TabIndex = 2
        Me.label_last_login.Text = "Last Login: ----"
        '
        'label_welcome_message
        '
        Me.label_welcome_message.AutoSize = True
        Me.label_welcome_message.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_welcome_message.ForeColor = System.Drawing.Color.White
        Me.label_welcome_message.Location = New System.Drawing.Point(10, 17)
        Me.label_welcome_message.Name = "label_welcome_message"
        Me.label_welcome_message.Size = New System.Drawing.Size(387, 25)
        Me.label_welcome_message.TabIndex = 1
        Me.label_welcome_message.Text = "Welcome back <first_name> <last_name>!"
        '
        'group_critical_products
        '
        Me.group_critical_products.Controls.Add(Me.link_load_more_critical)
        Me.group_critical_products.Controls.Add(Me.list_critical_products)
        Me.group_critical_products.Controls.Add(Me.prompt_critical_products)
        Me.group_critical_products.Location = New System.Drawing.Point(15, 166)
        Me.group_critical_products.Name = "group_critical_products"
        Me.group_critical_products.Size = New System.Drawing.Size(348, 218)
        Me.group_critical_products.TabIndex = 5
        Me.group_critical_products.TabStop = False
        Me.group_critical_products.Visible = False
        '
        'link_load_more_critical
        '
        Me.link_load_more_critical.AutoSize = True
        Me.link_load_more_critical.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.link_load_more_critical.ForeColor = System.Drawing.Color.Black
        Me.link_load_more_critical.LinkColor = System.Drawing.Color.Black
        Me.link_load_more_critical.Location = New System.Drawing.Point(6, 197)
        Me.link_load_more_critical.Name = "link_load_more_critical"
        Me.link_load_more_critical.Size = New System.Drawing.Size(64, 15)
        Me.link_load_more_critical.TabIndex = 7
        Me.link_load_more_critical.TabStop = True
        Me.link_load_more_critical.Text = "Load More"
        Me.link_load_more_critical.VisitedLinkColor = System.Drawing.Color.White
        '
        'list_critical_products
        '
        Me.list_critical_products.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_critical_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.list_critical_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_critical_products.FullRowSelect = True
        Me.list_critical_products.Location = New System.Drawing.Point(0, 39)
        Me.list_critical_products.Name = "list_critical_products"
        Me.list_critical_products.Size = New System.Drawing.Size(348, 152)
        Me.list_critical_products.TabIndex = 6
        Me.list_critical_products.UseCompatibleStateImageBehavior = False
        Me.list_critical_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product"
        Me.ColumnHeader1.Width = 250
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Stock Remaining"
        Me.ColumnHeader2.Width = 100
        '
        'prompt_critical_products
        '
        Me.prompt_critical_products.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prompt_critical_products.BackColor = System.Drawing.Color.FromArgb(CType(CType(110, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.prompt_critical_products.Location = New System.Drawing.Point(0, 0)
        Me.prompt_critical_products.Message = "--"
        Me.prompt_critical_products.Mode = DevaReePharmacy.PromptLabel.PromptLabelMode.Info
        Me.prompt_critical_products.Name = "prompt_critical_products"
        Me.prompt_critical_products.Size = New System.Drawing.Size(347, 39)
        Me.prompt_critical_products.TabIndex = 5
        '
        'group_expiring_products
        '
        Me.group_expiring_products.Controls.Add(Me.link_load_more_expiring)
        Me.group_expiring_products.Controls.Add(Me.list_expiring_products)
        Me.group_expiring_products.Controls.Add(Me.prompt_expiring_products)
        Me.group_expiring_products.Location = New System.Drawing.Point(619, 166)
        Me.group_expiring_products.Name = "group_expiring_products"
        Me.group_expiring_products.Size = New System.Drawing.Size(394, 218)
        Me.group_expiring_products.TabIndex = 6
        Me.group_expiring_products.TabStop = False
        Me.group_expiring_products.Visible = False
        '
        'link_load_more_expiring
        '
        Me.link_load_more_expiring.AutoSize = True
        Me.link_load_more_expiring.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.link_load_more_expiring.ForeColor = System.Drawing.Color.Black
        Me.link_load_more_expiring.LinkColor = System.Drawing.Color.Black
        Me.link_load_more_expiring.Location = New System.Drawing.Point(6, 197)
        Me.link_load_more_expiring.Name = "link_load_more_expiring"
        Me.link_load_more_expiring.Size = New System.Drawing.Size(64, 15)
        Me.link_load_more_expiring.TabIndex = 8
        Me.link_load_more_expiring.TabStop = True
        Me.link_load_more_expiring.Text = "Load More"
        Me.link_load_more_expiring.VisitedLinkColor = System.Drawing.Color.White
        '
        'list_expiring_products
        '
        Me.list_expiring_products.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_expiring_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6})
        Me.list_expiring_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_expiring_products.FullRowSelect = True
        Me.list_expiring_products.Location = New System.Drawing.Point(0, 39)
        Me.list_expiring_products.Name = "list_expiring_products"
        Me.list_expiring_products.Size = New System.Drawing.Size(394, 152)
        Me.list_expiring_products.TabIndex = 6
        Me.list_expiring_products.UseCompatibleStateImageBehavior = False
        Me.list_expiring_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Product"
        Me.ColumnHeader3.Width = 250
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Stock Quantity"
        Me.ColumnHeader4.Width = 100
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Lot Number"
        Me.ColumnHeader5.Width = 100
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Expiry Date"
        Me.ColumnHeader6.Width = 150
        '
        'prompt_expiring_products
        '
        Me.prompt_expiring_products.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prompt_expiring_products.BackColor = System.Drawing.Color.FromArgb(CType(CType(110, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.prompt_expiring_products.Location = New System.Drawing.Point(0, 0)
        Me.prompt_expiring_products.Message = "--"
        Me.prompt_expiring_products.Mode = DevaReePharmacy.PromptLabel.PromptLabelMode.Info
        Me.prompt_expiring_products.Name = "prompt_expiring_products"
        Me.prompt_expiring_products.Size = New System.Drawing.Size(393, 39)
        Me.prompt_expiring_products.TabIndex = 5
        '
        'group_credit_dues
        '
        Me.group_credit_dues.Controls.Add(Me.link_load_more_dues)
        Me.group_credit_dues.Controls.Add(Me.list_credit_dues)
        Me.group_credit_dues.Controls.Add(Me.prompt_credit_dues)
        Me.group_credit_dues.Location = New System.Drawing.Point(401, 214)
        Me.group_credit_dues.Name = "group_credit_dues"
        Me.group_credit_dues.Size = New System.Drawing.Size(148, 218)
        Me.group_credit_dues.TabIndex = 13
        Me.group_credit_dues.TabStop = False
        Me.group_credit_dues.Visible = False
        '
        'link_load_more_dues
        '
        Me.link_load_more_dues.AutoSize = True
        Me.link_load_more_dues.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.link_load_more_dues.ForeColor = System.Drawing.Color.Black
        Me.link_load_more_dues.LinkColor = System.Drawing.Color.Black
        Me.link_load_more_dues.Location = New System.Drawing.Point(6, 197)
        Me.link_load_more_dues.Name = "link_load_more_dues"
        Me.link_load_more_dues.Size = New System.Drawing.Size(64, 15)
        Me.link_load_more_dues.TabIndex = 8
        Me.link_load_more_dues.TabStop = True
        Me.link_load_more_dues.Text = "Load More"
        Me.link_load_more_dues.VisitedLinkColor = System.Drawing.Color.White
        '
        'list_credit_dues
        '
        Me.list_credit_dues.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_credit_dues.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13})
        Me.list_credit_dues.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_credit_dues.FullRowSelect = True
        Me.list_credit_dues.Location = New System.Drawing.Point(0, 39)
        Me.list_credit_dues.Name = "list_credit_dues"
        Me.list_credit_dues.Size = New System.Drawing.Size(148, 152)
        Me.list_credit_dues.TabIndex = 6
        Me.list_credit_dues.UseCompatibleStateImageBehavior = False
        Me.list_credit_dues.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Customer Name"
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "Balance"
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Due Date"
        '
        'prompt_credit_dues
        '
        Me.prompt_credit_dues.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prompt_credit_dues.BackColor = System.Drawing.Color.FromArgb(CType(CType(110, Byte), Integer), CType(CType(190, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.prompt_credit_dues.Location = New System.Drawing.Point(0, 0)
        Me.prompt_credit_dues.Message = "--"
        Me.prompt_credit_dues.Mode = DevaReePharmacy.PromptLabel.PromptLabelMode.Info
        Me.prompt_credit_dues.Name = "prompt_credit_dues"
        Me.prompt_credit_dues.Size = New System.Drawing.Size(147, 39)
        Me.prompt_credit_dues.TabIndex = 5
        '
        'numbers_total_gross
        '
        Me.numbers_total_gross.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.numbers_total_gross.HasDecimal = False
        Me.numbers_total_gross.Location = New System.Drawing.Point(279, 0)
        Me.numbers_total_gross.Margin = New System.Windows.Forms.Padding(0)
        Me.numbers_total_gross.Name = "numbers_total_gross"
        Me.numbers_total_gross.ShowCurrency = False
        Me.numbers_total_gross.Size = New System.Drawing.Size(226, 74)
        Me.numbers_total_gross.TabIndex = 1
        Me.numbers_total_gross.Title = "Total Gross For Today (All)"
        Me.numbers_total_gross.Value = "0"
        '
        'numbers_total_transactions
        '
        Me.numbers_total_transactions.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.numbers_total_transactions.HasDecimal = False
        Me.numbers_total_transactions.Location = New System.Drawing.Point(0, 0)
        Me.numbers_total_transactions.Margin = New System.Windows.Forms.Padding(0)
        Me.numbers_total_transactions.Name = "numbers_total_transactions"
        Me.numbers_total_transactions.ShowCurrency = False
        Me.numbers_total_transactions.Size = New System.Drawing.Size(279, 74)
        Me.numbers_total_transactions.TabIndex = 0
        Me.numbers_total_transactions.Title = "Total Transactions For Today (All)"
        Me.numbers_total_transactions.Value = "0"
        '
        'panel_numbers
        '
        Me.panel_numbers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_numbers.AutoScroll = True
        Me.panel_numbers.Controls.Add(Me.numbers_total_transactions)
        Me.panel_numbers.Controls.Add(Me.numbers_total_gross)
        Me.panel_numbers.Controls.Add(Me.numbers_total_cash)
        Me.panel_numbers.Controls.Add(Me.numbers_mine_total_transactions)
        Me.panel_numbers.Controls.Add(Me.numbers_mine_total_gross)
        Me.panel_numbers.Controls.Add(Me.numbers_mine_total_cash)
        Me.panel_numbers.Location = New System.Drawing.Point(0, 75)
        Me.panel_numbers.Name = "panel_numbers"
        Me.panel_numbers.Size = New System.Drawing.Size(1130, 74)
        Me.panel_numbers.TabIndex = 2
        Me.panel_numbers.WrapContents = False
        '
        'numbers_total_cash
        '
        Me.numbers_total_cash.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.numbers_total_cash.HasDecimal = False
        Me.numbers_total_cash.Location = New System.Drawing.Point(505, 0)
        Me.numbers_total_cash.Margin = New System.Windows.Forms.Padding(0)
        Me.numbers_total_cash.Name = "numbers_total_cash"
        Me.numbers_total_cash.ShowCurrency = False
        Me.numbers_total_cash.Size = New System.Drawing.Size(180, 74)
        Me.numbers_total_cash.TabIndex = 2
        Me.numbers_total_cash.Title = "Daily Total Cash (All)"
        Me.numbers_total_cash.Value = "0"
        '
        'numbers_mine_total_transactions
        '
        Me.numbers_mine_total_transactions.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.numbers_mine_total_transactions.HasDecimal = False
        Me.numbers_mine_total_transactions.Location = New System.Drawing.Point(685, 0)
        Me.numbers_mine_total_transactions.Margin = New System.Windows.Forms.Padding(0)
        Me.numbers_mine_total_transactions.Name = "numbers_mine_total_transactions"
        Me.numbers_mine_total_transactions.ShowCurrency = False
        Me.numbers_mine_total_transactions.Size = New System.Drawing.Size(284, 74)
        Me.numbers_mine_total_transactions.TabIndex = 3
        Me.numbers_mine_total_transactions.Title = "Total Transactions For Today (Me)"
        Me.numbers_mine_total_transactions.Value = "0"
        '
        'numbers_mine_total_gross
        '
        Me.numbers_mine_total_gross.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.numbers_mine_total_gross.HasDecimal = False
        Me.numbers_mine_total_gross.Location = New System.Drawing.Point(969, 0)
        Me.numbers_mine_total_gross.Margin = New System.Windows.Forms.Padding(0)
        Me.numbers_mine_total_gross.Name = "numbers_mine_total_gross"
        Me.numbers_mine_total_gross.ShowCurrency = False
        Me.numbers_mine_total_gross.Size = New System.Drawing.Size(231, 74)
        Me.numbers_mine_total_gross.TabIndex = 4
        Me.numbers_mine_total_gross.Title = "Total Gross For Today (Me)"
        Me.numbers_mine_total_gross.Value = "0"
        '
        'numbers_mine_total_cash
        '
        Me.numbers_mine_total_cash.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.numbers_mine_total_cash.HasDecimal = False
        Me.numbers_mine_total_cash.Location = New System.Drawing.Point(1200, 0)
        Me.numbers_mine_total_cash.Margin = New System.Windows.Forms.Padding(0)
        Me.numbers_mine_total_cash.Name = "numbers_mine_total_cash"
        Me.numbers_mine_total_cash.ShowCurrency = False
        Me.numbers_mine_total_cash.Size = New System.Drawing.Size(185, 74)
        Me.numbers_mine_total_cash.TabIndex = 5
        Me.numbers_mine_total_cash.Title = "Daily Total Cash (Me)"
        Me.numbers_mine_total_cash.Value = "0"
        '
        'chart_gpl_report
        '
        Me.chart_gpl_report.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.chart_gpl_report.Location = New System.Drawing.Point(457, 349)
        Me.chart_gpl_report.Name = "chart_gpl_report"
        Me.chart_gpl_report.Size = New System.Drawing.Size(470, 239)
        Me.chart_gpl_report.TabIndex = 14
        '
        'button_customer_management
        '
        Me.button_customer_management.BackColor = System.Drawing.Color.Transparent
        Me.button_customer_management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_customer_management.FlatAppearance.BorderSize = 0
        Me.button_customer_management.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_customer_management.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.button_customer_management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_customer_management.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_customer_management.ForeColor = System.Drawing.Color.White
        Me.button_customer_management.Image = CType(resources.GetObject("button_customer_management.Image"), System.Drawing.Image)
        Me.button_customer_management.Location = New System.Drawing.Point(812, 0)
        Me.button_customer_management.Name = "button_customer_management"
        Me.button_customer_management.Size = New System.Drawing.Size(97, 75)
        Me.button_customer_management.TabIndex = 14
        Me.button_customer_management.Text = "Customers"
        Me.button_customer_management.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_customer_management.UseVisualStyleBackColor = False
        '
        'button_sales_management
        '
        Me.button_sales_management.BackColor = System.Drawing.Color.Transparent
        Me.button_sales_management.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_sales_management.FlatAppearance.BorderSize = 0
        Me.button_sales_management.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_sales_management.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.button_sales_management.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_sales_management.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_sales_management.ForeColor = System.Drawing.Color.White
        Me.button_sales_management.Image = CType(resources.GetObject("button_sales_management.Image"), System.Drawing.Image)
        Me.button_sales_management.Location = New System.Drawing.Point(915, 0)
        Me.button_sales_management.Name = "button_sales_management"
        Me.button_sales_management.Size = New System.Drawing.Size(97, 75)
        Me.button_sales_management.TabIndex = 15
        Me.button_sales_management.Text = "Sales"
        Me.button_sales_management.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_sales_management.UseVisualStyleBackColor = False
        '
        'form_dashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1130, 646)
        Me.Controls.Add(Me.chart_gpl_report)
        Me.Controls.Add(Me.panel_numbers)
        Me.Controls.Add(Me.group_credit_dues)
        Me.Controls.Add(Me.group_expiring_products)
        Me.Controls.Add(Me.group_critical_products)
        Me.Controls.Add(Me.panel_welcome_message)
        Me.Name = "form_dashboard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dashboard"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_welcome_message.ResumeLayout(False)
        Me.panel_welcome_message.PerformLayout()
        Me.group_critical_products.ResumeLayout(False)
        Me.group_critical_products.PerformLayout()
        Me.group_expiring_products.ResumeLayout(False)
        Me.group_expiring_products.PerformLayout()
        Me.group_credit_dues.ResumeLayout(False)
        Me.group_credit_dues.PerformLayout()
        Me.panel_numbers.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panel_welcome_message As System.Windows.Forms.Panel
    Friend WithEvents label_welcome_message As System.Windows.Forms.Label
    Friend WithEvents label_last_login As System.Windows.Forms.Label
    Friend WithEvents group_critical_products As System.Windows.Forms.GroupBox
    Friend WithEvents link_logout As System.Windows.Forms.LinkLabel
    Friend WithEvents list_critical_products As System.Windows.Forms.ListView
    Friend WithEvents prompt_critical_products As DevaReePharmacy.PromptLabel
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents group_expiring_products As System.Windows.Forms.GroupBox
    Friend WithEvents list_expiring_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents prompt_expiring_products As DevaReePharmacy.PromptLabel
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents link_load_more_critical As System.Windows.Forms.LinkLabel
    Friend WithEvents link_load_more_expiring As System.Windows.Forms.LinkLabel
    Friend WithEvents button_transaction As System.Windows.Forms.Button
    Friend WithEvents button_product_management As System.Windows.Forms.Button
    Friend WithEvents button_stock_management As System.Windows.Forms.Button
    Friend WithEvents button_staff_management As System.Windows.Forms.Button
    Friend WithEvents group_credit_dues As System.Windows.Forms.GroupBox
    Friend WithEvents link_load_more_dues As System.Windows.Forms.LinkLabel
    Friend WithEvents list_credit_dues As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents prompt_credit_dues As DevaReePharmacy.PromptLabel
    Friend WithEvents numbers_total_gross As DevaReePharmacy.RunningLabel
    Friend WithEvents numbers_total_transactions As DevaReePharmacy.RunningLabel
    Friend WithEvents panel_numbers As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents numbers_total_cash As DevaReePharmacy.RunningLabel
    Friend WithEvents numbers_mine_total_transactions As DevaReePharmacy.RunningLabel
    Friend WithEvents numbers_mine_total_gross As DevaReePharmacy.RunningLabel
    Friend WithEvents numbers_mine_total_cash As DevaReePharmacy.RunningLabel
    Friend WithEvents chart_gpl_report As DevaReePharmacy.Chart
    Friend WithEvents button_sales_management As System.Windows.Forms.Button
    Friend WithEvents button_customer_management As System.Windows.Forms.Button
End Class
