﻿Public Class form_order

    Private _console As String
    Private _col_width() As Double = {45, 10, 10, 17.5, 17.5}

    Private _transaction_detail As Transaction
    Private _dispatch_detail As Dispatch
    Private _last_order_detail As Object

    Private _admin_override As Boolean = False

    Private _cart_items As Dictionary(Of ListViewItem, Product)
    Private _price_inquiry As Boolean = False
    Private _ui_mode As UIMode
    Private _order_mode As OrderMode = Nothing
    Private _order_type As OrderType = Nothing

    Private sql As New class_db_transaction

    Public Enum UIMode
        NoOrder = 0
        OnTransactionOrder = 1
        OnDispatchOrder = 2
        CanceledOrder = 3
        FinishedOrder = 4
    End Enum

    Private Sub SetNewOrder(ByVal order_type As OrderType)

        If list_products.Items.Count > 0 And (_ui_mode = UIMode.OnTransactionOrder Or _ui_mode = UIMode.OnDispatchOrder) Then
            If Not MsgBox("You are in a middle of an order. Do you want to cancel this order and start a new one?", MsgBoxStyle.YesNo) = DialogResult.Yes Then Exit Sub
        End If

        Dim order_selector As New form_order_mode_selector
        Dim order_mode = order_selector.OpenOrderModeDialog(Me, If(Not _order_mode = OrderMode.NoMode, _order_mode, GetLastOrderMode()))

        If Not order_mode = OrderMode.NoMode Then
            _transaction_detail = Nothing
            _dispatch_detail = Nothing
            _order_type = order_type
            _order_mode = order_mode
            list_products.Items.Clear()
            _cart_items = New Dictionary(Of ListViewItem, Product)
            _admin_override = False
            If order_type = OrderType.Transaction Then
                _transaction_detail = New Transaction
                _transaction_detail.TransactionType = order_mode
                _transaction_detail.InvoiceNo = sql.GetRecommendedInvoiceNo
                SetUIMode(UIMode.OnTransactionOrder)
            ElseIf order_type = OrderType.Dispatch Then
                _dispatch_detail = New Dispatch
                _dispatch_detail.DispatchType = order_mode
                _dispatch_detail.DispatchNo = sql.GetRecommendedDispatchNo
                SetUIMode(UIMode.OnDispatchOrder)
            End If
        End If

    End Sub

    Private Sub SetUIMode(ByVal ui_mode As UIMode)

        button_browse.Enabled = ui_mode = UIMode.OnTransactionOrder Or ui_mode = UIMode.OnDispatchOrder
        button_price_inquiry.Enabled = button_browse.Enabled
        button_cancel_order.Enabled = button_browse.Enabled
        button_set_quantity.Enabled = False
        button_set_price.Enabled = False
        button_void_item.Enabled = False
        button_change_unit.Enabled = False
        button_place_order.Enabled = list_products.Items.Count > 0
        button_set_credit.Enabled = ui_mode = UIMode.OnTransactionOrder
        button_set_customer.Enabled = button_set_credit.Enabled
        button_new_transaction.Enabled = True
        button_new_dispatch.Enabled = True
        button_set_reference_no.Enabled = button_browse.Enabled
        button_use_admin.Enabled = button_browse.Enabled
        button_use_admin.BackColor = If(_admin_override Or module_session.current_user_profile.Permission_Administator, module_colors.color_green, module_colors.color_dark_gray)
        If ui_mode = UIMode.OnTransactionOrder Or ui_mode = UIMode.OnDispatchOrder Then
            label_tendered_amount.Text = "0.00"
            label_credit_amount.Text = "0.00"
            label_total_amount.Text = "0.00"
            label_change_amount.Text = "0.00"
        End If
        label_console.Text = "_"
        label_currency.Text = SYS_CURRENCY
        label_product_price.Text = "0.00"
        If ui_mode = UIMode.OnTransactionOrder Then
            label_order_type.Text = If(_transaction_detail.TransactionType = OrderMode.Retail, "Retail", If(_transaction_detail.TransactionType = OrderMode.Wholesale, "Wholesale", "Distribution"))
            label_reference_no.Text = "Invoice No.: " & _transaction_detail.InvoiceNo.ToString().PadLeft(10, "0")
            label_customer_name.Text = "Customer: Guest"
            label_order_mode.Text = "Transaction"
            button_set_reference_no.Text = "Invoice No."
            label_product_name.Text = "Browse, scan or enter product code."
            button_new_transaction.BackColor = module_colors.color_green
            button_new_dispatch.BackColor = module_colors.color_dark_gray
        ElseIf ui_mode = UIMode.OnDispatchOrder Then
            label_order_type.Text = If(_dispatch_detail.DispatchType = OrderMode.Retail, "Retail", If(_dispatch_detail.DispatchType = OrderMode.Wholesale, "Wholesale", "Distribution"))
            label_reference_no.Text = "Dispatch No.: " & _dispatch_detail.DispatchNo.ToString().PadLeft(10, "0")
            label_customer_name.Text = "Customer: NA"
            label_order_mode.Text = "Dispatch"
            button_set_reference_no.Text = "Dispatch No."
            label_product_name.Text = "Browse, scan or enter product code."
            button_new_transaction.BackColor = module_colors.color_dark_gray
            button_new_dispatch.BackColor = module_colors.color_green
        Else
            If Not ui_mode = UIMode.FinishedOrder Then
                label_order_type.Text = If(ui_mode = UIMode.CanceledOrder, "Cancelled Order", "--")
                label_reference_no.Text = "Reference No: --"
                label_customer_name.Text = "Customer: --"
                label_order_mode.Text = "--"
                button_set_reference_no.Text = "Reference No."
            End If
            label_product_name.Text = "Press 'Transaction' or 'Dispatch' to start an order."
            button_new_transaction.BackColor = module_colors.color_dark_gray
            button_new_dispatch.BackColor = module_colors.color_dark_gray
        End If
        image_sold.Visible = ui_mode = UIMode.FinishedOrder
        panel_item_detail.BackColor = module_colors.color_dark_gray
        label_cart_count.Text = "Cart (0 items)"
        _ui_mode = ui_mode

    End Sub

    Private Sub LoadTransaction(ByVal transaction_detail As Transaction)

        list_products.Items.Clear()
        If Not IsNothing(transaction_detail) Then
            For Each item As Transaction_Item In transaction_detail.TransactionItems
                Dim list_row As ListViewItem = list_products.Items.Add(item.Product.ProductCompleteName)
                Dim quantity As Double = item.ItemQuantity - item.ReturnQuantity
                list_row.SubItems.Add(quantity)
                Dim unit As String = If(quantity = item.Product.PackingSize, SYS_PACK_ALIAS, If(quantity = (item.Product.PackingSize / item.Product.PadsPerPack), "pads", item.Product.ProductUnit))
                list_row.SubItems.Add(unit)
                list_row.SubItems.Add(FormatNumber(item.SellingPrice, 2, , , TriState.True))
                list_row.SubItems.Add(FormatNumber(item.SellingPrice * (item.ItemQuantity - item.ReturnQuantity), 2, , , TriState.True))
                list_row.Tag = item.ItemID
            Next
            label_reference_no.Text = "REF #" & transaction_detail.InvoiceNo
            _transaction_detail = transaction_detail
        End If


    End Sub

    Private Sub form_transaction_redesign_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If _ui_mode = UIMode.OnTransactionOrder Or _ui_mode = UIMode.OnDispatchOrder Then
            If e.KeyValue >= 48 And e.KeyValue <= 57 Then
                e.Handled = True
                Dim number_input As Integer = e.KeyValue - 48
                AppendCharacterToConsole(number_input.ToString)
            ElseIf e.KeyCode = Keys.Back Then
                e.Handled = True
                AppendCharacterToConsole(e.KeyData.ToString)
            ElseIf e.KeyCode = Keys.Return Or e.KeyCode = Keys.Enter Then
                'Trigger Input
                If Trim(_console) = "" Then Exit Sub
                e.Handled = True

                Dim sql_products As New class_db_products
                Dim product_id = sql_products.GetProductIDByCode(_console)
                If product_id > 0 Then
                    Dim product_detail As New Product(product_id)
                    If _price_inquiry Then
                        ShowPriceCheck(product_detail)
                    Else
                        AddItemToCart(product_detail)
                    End If
                    label_product_name.ForeColor = Color.White
                Else
                    label_product_name.Text = "Item " & _console & " does not exist in inventory."
                    label_product_price.Text = "0.00"
                    label_product_name.ForeColor = module_colors.color_pink
                End If
                _console = ""
                label_console.Text = ""
            End If
        End If

        'added 07/17/2015 by Aljun shortcut to close the form
        Select Case e.KeyCode
            Case Keys.F1
                button_browse.PerformClick()
            Case Keys.F2
                button_price_inquiry.PerformClick()
            Case Keys.F3
                button_set_price.PerformClick()
            Case Keys.F4
                button_set_quantity.PerformClick()
            Case Keys.F5
                button_change_unit.PerformClick()
            Case Keys.F6
                button_void_item.PerformClick()
            Case Keys.F7
                button_set_reference_no.PerformClick()
            Case Keys.F8
                button_set_credit.PerformClick()
            Case Keys.F9
                button_set_customer.PerformClick()
            Case Keys.F10
                button_place_order.PerformClick()
            Case Keys.T
                If e.Control Then button_new_transaction.PerformClick()
            Case Keys.D
                If e.Control Then button_new_dispatch.PerformClick()
            Case Keys.S
                If e.Shift Then button_use_admin.PerformClick()
            Case Keys.P
                If e.Control Then button_reprint_dr.PerformClick()
            Case Keys.Escape
                button_cancel_order.PerformClick()
            Case Keys.F12
                Me.Close()
        End Select

    End Sub

    Private Sub AppendCharacterToConsole(ByVal chr As Char)

        If chr = "B" Then
            If Len(_console) > 0 Then _console = _console.Substring(0, Len(_console) - 1)
        Else
            _console &= chr
        End If
        label_console.Text = _console

    End Sub

    Private Sub ChangeOrderMode(ByVal _new_order_mode As OrderMode)

        If list_products.Items.Count > 0 Then
            Dim ask = MsgBox("You are currently in a middle of an order. The system will try to convert prices to its appropriate values. Are you sure you want to continue?", MsgBoxStyle.YesNo)
            If Not ask = DialogResult.Yes Then Exit Sub
        End If

        _order_mode = _new_order_mode

        If list_products.Items.Count > 0 Then
            For Each list_row As ListViewItem In list_products.Items
                Dim product_detail = New Product(Val(list_row.Tag))
                If Not product_detail.Synced Then Continue For
                Dim old_price As Double
                Dim new_price As Double
                If _new_order_mode = OrderMode.Retail Then
                    old_price = product_detail.PackPrice.Wholesale
                    new_price = product_detail.PackPrice.Retail
                Else
                    old_price = product_detail.PackPrice.Retail
                    new_price = product_detail.PackPrice.Wholesale
                End If
                If Val(list_row.SubItems(3).Text) = old_price Then
                    list_row.SubItems(3).Text = FormatNumber(new_price, 2, , , TriState.True)
                    list_row.SubItems(4).Text = FormatNumber(Val(list_row.SubItems(1).Text) * new_price, 2, , , TriState.True)
                End If
            Next
            RecalculateTotalAmount()
        End If

    End Sub

    Private Sub AdjustInterface() Handles Me.Load

        For Each obj_button In Me.Controls.OfType(Of Button)()
            AddHandler obj_button.GotFocus, AddressOf form_transaction_redesign_LostFocus
        Next

        label_total_amount.ForeColor = module_colors.color_light_blue
        label_credit_amount.ForeColor = module_colors.color_pink
        label_tendered_amount.ForeColor = module_colors.color_light_green
        label_change_amount.ForeColor = module_colors.color_yellow
        label_total_amount_currency.ForeColor = Color.White
        label_credit_amount_currency.ForeColor = Color.White
        label_tendered_amount_currency.ForeColor = Color.White
        label_change_amount_currency.ForeColor = Color.White
        label_total_amount_currency.Text = SYS_CURRENCY
        label_credit_amount_currency.Text = SYS_CURRENCY
        label_tendered_amount_currency.Text = SYS_CURRENCY
        label_change_amount_currency.Text = SYS_CURRENCY
        panel_total_amount.BackColor = Color.Transparent
        panel_credit.BackColor = Color.Transparent
        panel_tendered_amount.BackColor = Color.Transparent
        panel_change_amount.BackColor = Color.Transparent
        panel_controls.BackColor = module_colors.color_cool_gray
        For Each obj As Button In panel_cart_controls.Controls.OfType(Of Button)()
            obj.BackColor = module_colors.color_dark_gray
        Next
        panel_header.BackColor = module_colors.color_yellow
        timer_console.Enabled = True
        Me.MinimumSize = New Size(panel_amounts.Right + panel_controls.Width + 10, panel_cart_controls.Bottom + label_console.Height + 20 + panel_item_detail.Height)
        SetUIMode(UIMode.NoOrder)

        list_products.Focus()
        'Me.KeyPreview = True

        'For development only

    End Sub

    Private Sub form_transaction_redesign_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LostFocus

        Me.Focus()

    End Sub

    Private Sub RecalculateTotalAmount()

        Dim total_amount As Double
        Dim unit_price As Double
        Dim quantity As Double
        Dim rounded_price As Double
        Dim product As Product
        For Each list_row As ListViewItem In list_products.Items
            product = _cart_items(list_row)
            If list_row.SubItems(2).Text = SYS_PACK_ALIAS Then
                unit_price = If(_order_mode = OrderMode.Retail, product.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product.PackPrice.Wholesale, product.PackPrice.Distribution))
            ElseIf list_row.SubItems(2).Text = SYS_PAD_ALIAS Then
                unit_price = If(_order_mode = OrderMode.Retail, product.PadPrice.Retail, If(_order_mode = OrderMode.Wholesale, product.PadPrice.Wholesale, product.PadPrice.Distribution))
            Else
                unit_price = If(_order_mode = OrderMode.Retail, product.UnitPrice.Retail, If(_order_mode = OrderMode.Wholesale, product.UnitPrice.Wholesale, product.UnitPrice.Distribution))
            End If
            quantity = Val(list_row.SubItems(1).Text)
            rounded_price = Math.Round(unit_price, 2)
            list_row.SubItems(4).Text = FormatNumber(quantity * rounded_price, 2, , , TriState.True)
            total_amount += quantity * rounded_price
        Next
        label_total_amount.Text = FormatNumber(total_amount, 2, , , TriState.True)

    End Sub

    Private Function GetRecalculatedTotalAmount() As Double

        Dim total_amount As Double
        Dim unit_price As Double
        Dim rounded_price As Double
        Dim quantity As Double
        Dim product As Product
        For Each list_row As ListViewItem In list_products.Items
            product = _cart_items(list_row)
            If list_row.SubItems(2).Text = SYS_PACK_ALIAS Then
                unit_price = If(_order_mode = OrderMode.Retail, product.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product.PackPrice.Wholesale, product.PackPrice.Distribution))
            ElseIf list_row.SubItems(2).Text = SYS_PAD_ALIAS Then
                unit_price = If(_order_mode = OrderMode.Retail, product.PadPrice.Retail, If(_order_mode = OrderMode.Wholesale, product.PadPrice.Wholesale, product.PadPrice.Distribution))
            Else
                unit_price = If(_order_mode = OrderMode.Retail, product.UnitPrice.Retail, If(_order_mode = OrderMode.Wholesale, product.UnitPrice.Wholesale, product.UnitPrice.Distribution))
            End If
            quantity = Val(list_row.SubItems(1).Text)
            rounded_price = Math.Round(unit_price, 2)
            total_amount += quantity * rounded_price
        Next
        GetRecalculatedTotalAmount = Math.Round(total_amount, 2)

    End Function

    Private Sub AdjustObjects() Handles Me.Resize, label_order_mode.TextChanged, label_order_type.TextChanged

        panel_main_controls.Left = (panel_controls.Width / 2) - (panel_main_controls.Width / 2)
        panel_cart_controls.Left = (panel_controls.Width / 2) - (panel_cart_controls.Width / 2)
        panel_amounts.Left = (panel_controls.Width / 2) - (panel_amounts.Width / 2)
        label_order_mode.Left = (panel_header.Width / 2) - (label_order_mode.Width / 2)
        label_order_type.Left = (panel_header.Width / 2) - (label_order_type.Width / 2)
        image_sold.Top = panel_cart_controls.Bottom + (((panel_amounts.Top - panel_cart_controls.Bottom) / 2) - (image_sold.Height / 2))
        image_sold.Left = (panel_controls.Width / 2) - (image_sold.Width / 2)
        ResizeListViewColumn(list_products, _col_width)

    End Sub

    Private Sub timer_console_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_console.Tick

        If label_console.Text = "_" Then
            label_console.Text = ""
        ElseIf Trim(label_console.Text) = "" Then
            label_console.Text = "_"
        Else
            timer_console.Enabled = False
        End If

    End Sub

    Private Sub label_console_TextChanged() Handles label_console.TextChanged

        timer_console.Enabled = Trim(label_console.Text) = "" Or Trim(label_console.Text) = "_"

    End Sub

    Private Sub timer_time_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer_time.Tick

        If label_time.Text.Contains(":") Then
            label_time.Text = Date.Now.ToString("hh mm tt")
        Else
            label_time.Text = Date.Now.ToString("hh:mm tt")
        End If

    End Sub

    Private Sub button_new_transaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_new_transaction.Click

        SetNewOrder(OrderType.Transaction)

    End Sub

    Private Sub CartItemsChanged()

        label_cart_count.Text = "Cart (" & list_products.Items.Count & " Item" & If(list_products.Items.Count > 1, "s", "") & ")"

    End Sub

    Private Sub ShowPriceCheck(ByVal product_detail As Product)

        If product_detail.PackingSize = 0 Or product_detail.PadsPerPack = 0 Then
            MsgBox("Product has invalid packing information.", MsgBoxStyle.Critical)
        Else
            Dim unit_price As Double = If(_order_mode = OrderMode.Retail, product_detail.UnitPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.UnitPrice.Wholesale, product_detail.UnitPrice.Distribution))
            Dim pad_price As Double = If(_order_mode = OrderMode.Retail, product_detail.PadPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PadPrice.Wholesale, product_detail.PadPrice.Distribution))
            Dim pack_price As Double = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
            label_product_name.Text = product_detail.ProductCompleteName
            label_product_price.Text = If(product_detail.IsSoldInPacks, FormatNumber(pack_price, 2, , , TriState.True) & " / " & FormatNumber(pad_price, 2, , , TriState.True) & " / " & FormatNumber(unit_price, 2, , , TriState.True), FormatNumber(unit_price, 2, , , TriState.True))
        End If
        button_price_inquiry.BackColor = module_colors.color_dark_gray
        _price_inquiry = False

    End Sub

    Private Sub AddItemToCart(ByVal product_detail As Product, Optional ByVal sell_price As Double = -1, Optional ByVal quantity As Double = 1, Optional ByVal unit As ItemUnit = ItemUnit.Pack)

        If product_detail.PackingSize = 0 Or product_detail.PadsPerPack = 0 Then
            MsgBox("Restricted adding product to cart. Product has invalid packing information.", MsgBoxStyle.Critical)
        Else
            Dim unit_price As Double
            Dim text_unit As String
            Dim display_unit As ItemUnit
            If unit = ItemUnit.Pack And product_detail.IsSoldInPacks Then
                unit_price = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
                text_unit = SYS_PACK_ALIAS
                display_unit = ItemUnit.Pack
            ElseIf unit = ItemUnit.Pad And product_detail.IsSoldInPads Then
                unit_price = If(_order_mode = OrderMode.Retail, product_detail.PadPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PadPrice.Wholesale, product_detail.PadPrice.Distribution))
                text_unit = SYS_PAD_ALIAS
                display_unit = ItemUnit.Pad
            Else
                unit_price = If(_order_mode = OrderMode.Retail, product_detail.UnitPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.UnitPrice.Wholesale, product_detail.UnitPrice.Distribution))
                text_unit = product_detail.ProductUnit
                display_unit = ItemUnit.Unit
            End If
            Dim pack_price As Double = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
            Dim new_item As ListViewItem = list_products.Items.Add(product_detail.ProductCompleteNameWithPacking(display_unit))
            new_item.SubItems.Add(quantity)
            new_item.SubItems.Add(text_unit)
            new_item.SubItems.Add(FormatNumber(pack_price, 2, , , TriState.True) & If(Not text_unit = SYS_PACK_ALIAS, " (" & FormatNumber(unit_price, 2, , , TriState.True) & " per " & text_unit & ")", ""))
            new_item.SubItems.Add(FormatNumber(quantity * unit_price, 2, , , TriState.True))
            new_item.Tag = product_detail.ProductID
            new_item.ImageKey = If(text_unit = SYS_PACK_ALIAS, "box", If(text_unit = SYS_PAD_ALIAS, "pad", "pcs"))
            new_item.Selected = True
            label_product_name.Text = product_detail.ProductCompleteName & " x " & quantity & " " & text_unit
            label_product_name.ForeColor = Color.White
            label_product_price.Text = FormatNumber(unit_price, 2, , , TriState.True)
            button_place_order.Enabled = list_products.Items.Count > 0
            button_set_credit.Enabled = button_place_order.Enabled
            _cart_items.Add(new_item, product_detail)
            CartItemsChanged()
            RecalculateTotalAmount()
        End If

    End Sub

    Private Sub button_browse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_browse.Click

        If _price_inquiry Then button_price_inquiry.PerformClick()
        Dim inventory_form As New form_inventory
        Dim product_detail = inventory_form.OpenInventory(Me)
        If Not IsNothing(product_detail) Then
            AddItemToCart(product_detail)
        End If

    End Sub

    Private Sub button_set_quantity_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_set_quantity.Click

        If list_products.SelectedItems.Count < 1 Then
            MsgBox("No item selected.", MsgBoxStyle.Exclamation)
        Else
            Dim sel_row As ListViewItem = list_products.SelectedItems(0)
            Dim quantity_form As New form_quantity
            Dim temp = quantity_form.OpenQuantityDialog(Me, "Quantity", Val(sel_row.SubItems(1).Text), , SYS_DECIMAL_STOCKS)
            If Not IsNothing(temp) Then
                Dim new_quantity As Double = Val(temp)
                If new_quantity >= 0 Then
                    sel_row.SubItems(1).Text = new_quantity
                    RecalculateTotalAmount()
                End If
            End If
        End If

    End Sub

    Private Sub list_products_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles list_products.ItemSelectionChanged

        button_set_quantity.Enabled = list_products.SelectedItems.Count > 0 And (_ui_mode = UIMode.OnTransactionOrder Or _ui_mode = UIMode.OnDispatchOrder)
        button_set_price.Enabled = button_set_quantity.Enabled
        button_change_unit.Enabled = button_set_quantity.Enabled
        button_void_item.Enabled = button_set_quantity.Enabled And (_admin_override Or module_session.current_user_profile.Permission_Administator)

    End Sub

    Private Sub button_set_price_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_set_price.Click

        If list_products.SelectedItems.Count < 1 Then
            MsgBox("No item selected.", MsgBoxStyle.Exclamation)
        Else
            Dim sel_row As ListViewItem = list_products.SelectedItems(0)
            Dim product_detail = _cart_items(sel_row)
            Dim quantity_form As New form_quantity
            Dim text_unit As String = sel_row.SubItems(2).Text
            Dim current_price As Double
            Dim unit As ItemUnit = If(text_unit = SYS_PACK_ALIAS, ItemUnit.Pack, If(text_unit = SYS_PAD_ALIAS, ItemUnit.Pad, ItemUnit.Unit))
            If unit = ItemUnit.Pack Then
                current_price = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
            ElseIf unit = ItemUnit.Pad Then
                current_price = If(_order_mode = OrderMode.Retail, product_detail.PadPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PadPrice.Wholesale, product_detail.PadPrice.Distribution))
            Else
                current_price = If(_order_mode = OrderMode.Retail, product_detail.UnitPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.UnitPrice.Wholesale, product_detail.UnitPrice.Distribution))
            End If
            Dim temp = quantity_form.OpenQuantityDialog(Me, "Set " & text_unit & " Price", current_price, , True, True, True)
            If Not IsNothing(temp) Then
                Dim increase As Double
                Dim new_price As Double
                If temp.Contains("%") Then
                    increase = current_price * (Val(temp.Replace("+", "").Replace("%", "")) / 100)
                Else
                    increase = Val(temp.Replace("+", "").Replace("%", ""))
                End If
                If temp.Contains("+") Or temp.Contains("-") Then
                    new_price = current_price + increase
                Else
                    new_price = increase
                End If
                If new_price >= 0 Then
                    Dim pack_price As Double = new_price
                    If unit = ItemUnit.Pad Then
                        pack_price *= product_detail.PadsPerPack
                    ElseIf unit = ItemUnit.Unit Then
                        pack_price *= product_detail.PackingSize
                    End If
                    If _order_mode = OrderMode.Retail Then
                        product_detail.PackPrice.Retail = pack_price
                    ElseIf _order_mode = OrderMode.Wholesale Then
                        product_detail.PackPrice.Wholesale = pack_price
                    Else
                        product_detail.PackPrice.Distribution = pack_price
                    End If
                    _cart_items(sel_row) = product_detail
                    sel_row.SubItems(3).Text = FormatNumber(pack_price, 2, , , TriState.True) & If(Not unit = ItemUnit.Pack, " (" & FormatNumber(new_price, 2, , , TriState.True) & " per " & text_unit & ")", "")
                    sel_row.BackColor = module_colors.color_yellow
                    'sel_row.SubItems(4).Text = FormatNumber(new_quantity * ClearNumberFormat(sel_row.SubItems(3).Text), 2, , , TriState.True)
                    RecalculateTotalAmount()
                End If
            End If
        End If
    End Sub

    Private Sub button_use_admin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_use_admin.Click

        If Not module_session.current_user_profile.Permission_Administator Then
            If _admin_override Then
                _admin_override = False
                button_use_admin.BackColor = module_colors.color_dark_gray
            Else
                Dim login_form As New form_user_login
                Dim override_user = login_form.ShowLoginDialog(Me)
                If Not IsNothing(override_user) Then
                    If override_user.Permission_Administator Then
                        _admin_override = True
                        button_use_admin.BackColor = module_colors.color_green
                    Else
                        MsgBox("Permission insufficient.", MsgBoxStyle.Exclamation)
                    End If
                End If
            End If
            list_products_ItemSelectionChanged(Nothing, Nothing)
        End If

    End Sub

    Private Sub button_remove_item_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_void_item.Click

        If list_products.SelectedItems.Count < 1 Then
            MsgBox("No item selected.", MsgBoxStyle.Exclamation)
        Else
            Dim sel_row As ListViewItem = list_products.SelectedItems(0)
            _cart_items.Remove(sel_row)
            list_products.Items.Remove(sel_row)
            button_place_order.Enabled = list_products.Items.Count > 0
            button_set_credit.Enabled = button_place_order.Enabled
            CartItemsChanged()
            RecalculateTotalAmount()
        End If

    End Sub

    Private Sub SetReferenceNo() Handles button_set_reference_no.Click

        Dim quantity_form As New form_quantity
        Dim ref_title As String = ""
        Dim ref_no As Integer

        If _order_type = OrderType.Transaction Then
            ref_title = "Invoice No."
            ref_no = sql.GetRecommendedInvoiceNo
        ElseIf _order_type = OrderType.Dispatch Then
            ref_title = "Dispatch No."
            ref_no = sql.GetRecommendedDispatchNo
        End If
        Dim ref As Double = Val(quantity_form.OpenQuantityDialog(Me, ref_title, ref_no, 1, False))
        If ref > 0 Then
            If _order_type = OrderType.Transaction Then
                If sql.IsInvoiceTaken(ref) Then
                    MsgBox("Invoice No. already used. Please choose another one.", MsgBoxStyle.Exclamation)
                Else
                    _transaction_detail.InvoiceNo = ref
                End If
            ElseIf _order_type = OrderType.Dispatch Then
                If sql.IsDispatchNoTaken(ref) Then
                    MsgBox("Dispatch No. already used. Please choose another one.", MsgBoxStyle.Exclamation)
                Else
                    _dispatch_detail.DispatchNo = ref
                End If
            End If
            label_reference_no.Text = ref_title & ": " & ref.ToString().PadLeft(10, "0")
        ElseIf ref = 0 Then
            MsgBox("Invalid number.", MsgBoxStyle.Critical)
        End If

    End Sub

    Private Sub CancelOrder() Handles button_cancel_order.Click

        Dim ask = MsgBox("Are you sure you want to cancel the current transaction?", MsgBoxStyle.YesNo)
        If ask = DialogResult.Yes Then
            _order_type = OrderType.NoType
            _order_mode = OrderMode.NoMode
            list_products.Items.Clear()
            _cart_items = New Dictionary(Of ListViewItem, Product)
            _admin_override = False
            SetUIMode(UIMode.CanceledOrder)
        End If

    End Sub

    Private Sub PlaceTransactionOrder(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If _transaction_detail.InvoiceNo < 1 Then
            button_set_reference_no.PerformClick()
        ElseIf sql.IsInvoiceTaken(_transaction_detail.InvoiceNo) Then
            MsgBox("Invoice No. already used. Please choose another one.", MsgBoxStyle.Exclamation)
            button_set_reference_no.PerformClick()
        Else
            Dim form_customer_selector As New form_customer_selector
            Dim form_agent_selector As New form_agent_selector
            Dim agent_detail = form_agent_selector.OpenAgentSelector(Me)
            If Not IsNothing(agent_detail) Then
                _transaction_detail.Agent = agent_detail
            Else
                _transaction_detail.Agent = Nothing
            End If
            Dim quantity_form As New form_quantity
            Dim total_amount = GetRecalculatedTotalAmount()
            Dim tendered_amount As Double

            Dim temp = Val(quantity_form.OpenQuantityDialog(Me, "Tendered Amount", total_amount, 0.25))
            If IsNothing(temp) Then Exit Sub
            tendered_amount = Val(temp)

            Dim credit_amount As Double = If(IsNothing(_transaction_detail.Credit), 0, _transaction_detail.Credit.CreditAmount)
            Dim change_amount As Double
            If (tendered_amount + credit_amount) < total_amount Then
                MsgBox("Insufficient funds.", MsgBoxStyle.Exclamation)
            Else
                change_amount = (tendered_amount + credit_amount) - total_amount
                'Try
                Dim has_insufficiency As Boolean = False
                _transaction_detail.RemoveAllItems()
                label_product_name.Text = "Checking out..."
                label_currency.Text = ""
                label_product_price.Text = ""
                Dim multiplier As Double
                Dim stock_req As New Dictionary(Of Integer, Double)
                For Each list_row As ListViewItem In list_products.Items
                    Dim product_detail = _cart_items(list_row)
                    Dim product_id As Integer = product_detail.ProductID
                    If Not stock_req.ContainsKey(product_id) Then stock_req.Add(product_id, 0)
                    multiplier = If(list_row.SubItems(2).Text = SYS_PACK_ALIAS, product_detail.PackingSize, If(list_row.SubItems(2).Text = SYS_PAD_ALIAS, product_detail.PackingSize / product_detail.PadsPerPack, 1))
                    stock_req(product_id) += (Val(list_row.SubItems(1).Text) * multiplier)
                Next

                For Each req As KeyValuePair(Of Integer, Double) In stock_req
                    Dim stock_ids = sql.GetSuitableStocksForProduct(req.Key, req.Value)
                    Dim index As Integer = 0
                    For Each list_row As ListViewItem In list_products.Items
                        If Not Val(list_row.Tag) = req.Key Then Continue For
                        list_row.ImageKey = If(list_row.SubItems(2).Text = SYS_PACK_ALIAS, "box", If(list_row.SubItems(2).Text = SYS_PAD_ALIAS, "pad", "unit"))
                        Dim product_detail = _cart_items(list_row)
                        Dim current_price, original_price As Double
                        Dim unit As ItemUnit = If(list_row.SubItems(2).Text = SYS_PACK_ALIAS, ItemUnit.Pack, If(list_row.SubItems(2).Text = SYS_PAD_ALIAS, ItemUnit.Pad, ItemUnit.Unit))
                        current_price = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
                        product_detail.ProductID = product_detail.ProductID
                        original_price = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
                        multiplier = If(unit = ItemUnit.Pack, product_detail.PackingSize, If(unit = ItemUnit.Pad, product_detail.PackingSize / product_detail.PadsPerPack, 1))
                        Dim total_items As Double = Val(list_row.SubItems(1).Text) * multiplier
                        While total_items > 0
                            Dim transaction_item As New Transaction_Item
                            transaction_item.SellingPrice = current_price
                            transaction_item.OriginalPrice = original_price
                            transaction_item.Product = product_detail
                            transaction_item.DisplayUnit = unit
                            If IsNothing(stock_ids) Then
                                transaction_item.ItemQuantity = total_items
                                transaction_item.Stock = Nothing
                                has_insufficiency = True
                                list_row.ImageKey = "invalid"
                            ElseIf index > (stock_ids.Count - 1) Then
                                transaction_item.ItemQuantity = total_items
                                transaction_item.Stock = Nothing
                                has_insufficiency = True
                                list_row.ImageKey = "invalid"
                            ElseIf stock_ids(index)("quantity_available") > total_items Then
                                transaction_item.ItemQuantity = total_items
                                transaction_item.Stock = New Stock(stock_ids(index)("stock_id"))
                                stock_ids(index)("quantity_available") -= total_items
                            Else
                                transaction_item.ItemQuantity = stock_ids(index)("quantity_available")
                                transaction_item.Stock = New Stock(stock_ids(index)("stock_id"))
                                stock_ids(index)("quantity_available") = 0
                                index += 1
                            End If
                            total_items -= transaction_item.ItemQuantity
                            _transaction_detail.AddItem(transaction_item)
                        End While
                    Next
                Next
                _transaction_detail.Staff = module_session.current_user_profile
                _transaction_detail.TransactionDate = Date.Now
                If has_insufficiency Then
                    If Not MsgBox("Some items in cart have insufficient stocks in inventory. Do you wish to proceed with the transaction?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                        SetUIMode(UIMode.OnTransactionOrder)
                        RecalculateTotalAmount()
                        Exit Sub
                    End If
                End If
                If Not _transaction_detail.Persist() Then Throw New Exception("Transaction persist failed.")
                label_tendered_amount.Text = FormatNumber(tendered_amount, 2, , , TriState.True)
                label_change_amount.Text = FormatNumber(change_amount, 2, , , TriState.True)
                image_sold.Visible = True
                _last_order_detail = _transaction_detail
                label_customer_name.Text = If(IsNothing(_transaction_detail.Customer), "Sold to: Guest", "Sold to: " & _transaction_detail.Customer.CustomerName)
                SetUIMode(UIMode.FinishedOrder)
                MsgBox("Transaction complete.", MsgBoxStyle.Information)
                Dim transaction_report As New form_delivery_receipt_generator
                transaction_report.OpenTransactionReportGeneratorDialog(Me, _transaction_detail)
                'Catch ex As Exception
                '    MsgBox(SYS_ERROR_MESSAGE & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                'End Try
            End If
        End If
    End Sub

    Private Sub PlaceDispatchOrder(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim form_quantity As New form_quantity
        If _dispatch_detail.DispatchNo < 1 Then
            button_set_reference_no.PerformClick()
        ElseIf sql.IsDispatchNoTaken(_dispatch_detail.DispatchNo) Then
            MsgBox("Dispatch No. already used.", MsgBoxStyle.Exclamation)
        Else
            Dim form_agent_selector As New form_agent_selector
            Dim agent_detail = form_agent_selector.OpenAgentSelector(Me)
            If Not IsNothing(agent_detail) Then
                _dispatch_detail.Agent = agent_detail
                Try
                    Dim has_insufficiency As Boolean = False
                    _transaction_detail.RemoveAllItems()
                    label_product_name.Text = "Preparing for dispatch..."
                    label_currency.Text = ""
                    label_product_price.Text = ""
                    Dim multiplier As Double
                    Dim stock_req As New Dictionary(Of Integer, Double)
                    For Each list_row As ListViewItem In list_products.Items
                        Dim product_detail = _cart_items(list_row)
                        Dim product_id As Integer = product_detail.ProductID
                        If Not stock_req.ContainsKey(product_id) Then stock_req.Add(product_id, 0)
                        multiplier = If(list_row.SubItems(2).Text = SYS_PACK_ALIAS, product_detail.PackingSize, If(list_row.SubItems(2).Text = SYS_PAD_ALIAS, product_detail.PackingSize / product_detail.PadsPerPack, 1))
                        stock_req(product_id) += (Val(list_row.SubItems(1).Text) * multiplier)
                    Next

                    For Each req As KeyValuePair(Of Integer, Double) In stock_req
                        Dim stock_ids = sql.GetSuitableStocksForProduct(req.Key, req.Value)
                        Dim index As Integer = 0
                        For Each list_row As ListViewItem In list_products.Items
                            If Not Val(list_row.Tag) = req.Key Then Continue For
                            list_row.ImageKey = If(list_row.SubItems(2).Text = SYS_PACK_ALIAS, "box", If(list_row.SubItems(2).Text = SYS_PAD_ALIAS, "pad", "unit"))
                            Dim product_detail = _cart_items(list_row)
                            Dim current_price, original_price As Double
                            Dim unit As ItemUnit = If(list_row.SubItems(2).Text = SYS_PACK_ALIAS, ItemUnit.Pack, If(list_row.SubItems(2).Text = SYS_PAD_ALIAS, ItemUnit.Pad, ItemUnit.Unit))
                            current_price = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
                            product_detail.ProductID = product_detail.ProductID
                            original_price = If(_order_mode = OrderMode.Retail, product_detail.PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, product_detail.PackPrice.Wholesale, product_detail.PackPrice.Distribution))
                            multiplier = If(unit = ItemUnit.Pack, product_detail.PackingSize, If(unit = ItemUnit.Pad, product_detail.PackingSize / product_detail.PadsPerPack, 1))
                            Dim total_items As Double = Val(list_row.SubItems(1).Text) * multiplier
                            While total_items > 0
                                Dim dispatch_item As New Dispatch_Item
                                dispatch_item.DispatchPrice = current_price
                                dispatch_item.DisplayUnit = unit
                                If IsNothing(stock_ids) Then
                                    has_insufficiency = True
                                    list_row.ImageKey = "invalid"
                                    Exit While
                                ElseIf index > (stock_ids.Count - 1) Then
                                    has_insufficiency = True
                                    list_row.ImageKey = "invalid"
                                    Exit While
                                ElseIf stock_ids(index)("quantity_available") > total_items Then
                                    dispatch_item.ItemQuantity = total_items
                                    dispatch_item.Stock = New Stock(stock_ids(index)("stock_id"))
                                    stock_ids(index)("quantity_available") -= total_items
                                Else
                                    dispatch_item.ItemQuantity = stock_ids(index)("quantity_available")
                                    dispatch_item.Stock = New Stock(stock_ids(index)("stock_id"))
                                    stock_ids(index)("quantity_available") = 0
                                    index += 1
                                End If
                                total_items -= dispatch_item.ItemQuantity
                                _dispatch_detail.AddDispatchItem(dispatch_item)
                            End While
                        Next
                    Next
                    _dispatch_detail.Staff = module_session.current_user_profile
                    _dispatch_detail.DispatchDate = Date.Now()
                    If has_insufficiency Then
                        MsgBox("Some items in cart have insufficient stocks in inventory. To proceed, please load stocks for marked items. Dispatch aborted.", MsgBoxStyle.Exclamation)
                        SetUIMode(UIMode.OnDispatchOrder)
                        RecalculateTotalAmount()
                        Exit Sub
                    End If

                    If Not _dispatch_detail.Persist() Then Throw New Exception("Dispatch persist failed.")
                    image_sold.Visible = True
                    label_customer_name.Text = "Dispatched with " & agent_detail.FullName
                    _last_order_detail = _dispatch_detail
                    SetUIMode(UIMode.FinishedOrder)
                    MsgBox("Dispatch complete.", MsgBoxStyle.Information)
                Catch ex As Exception
                    MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                End Try
            Else
                MsgBox("Must select an agent for dispatch.", MsgBoxStyle.Exclamation)
            End If
        End If
    End Sub

    Private Sub button_load_transaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim form_stock_history As New form_sales_history
        form_stock_history.ShowDialog(Me)
        'SetUIMode(TransactionUIMode.LoadedTransaction)

    End Sub

    Private Sub button_change_unit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_change_unit.Click

        If list_products.SelectedItems.Count < 1 Then
            MsgBox("Must select a product.", MsgBoxStyle.Exclamation)
        Else
            Dim sel_row As ListViewItem = list_products.SelectedItems(0)
            Dim current_selection As ItemUnit = If(sel_row.SubItems(2).Text = SYS_PACK_ALIAS, ItemUnit.Pack, If(sel_row.SubItems(2).Text = SYS_PAD_ALIAS, ItemUnit.Pad, ItemUnit.Unit))
            Dim form_unit As New form_unit_selector
            Dim product_detail As New Product(Val(sel_row.Tag))
            Dim new_selection = form_unit.OpenUnitDialog(Me, current_selection, _cart_items(sel_row).ProductUnit, product_detail.IsSoldInPacks, product_detail.IsSoldInPads)
            If Not new_selection = ItemUnit.NoUnit Then
                Dim pack_price As Double
                Dim unit_price As Double
                Dim old_quantity As Double = Val(sel_row.SubItems(1).Text)
                Dim old_display_unit As ItemUnit = If(sel_row.SubItems(2).Text = SYS_PACK_ALIAS, ItemUnit.Pack, If(sel_row.SubItems(2).Text = SYS_PAD_ALIAS, ItemUnit.Pad, ItemUnit.Unit))
                Select Case new_selection
                    Case ItemUnit.Pack
                        pack_price = If(_order_mode = OrderMode.Retail, _cart_items(sel_row).PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, _cart_items(sel_row).PackPrice.Wholesale, _cart_items(sel_row).PackPrice.Distribution))
                        sel_row.SubItems(2).Text = SYS_PACK_ALIAS
                        sel_row.SubItems(3).Text = FormatNumber(pack_price, 2, , , TriState.True)
                        sel_row.ImageKey = "box"
                    Case ItemUnit.Pad
                        pack_price = If(_order_mode = OrderMode.Retail, _cart_items(sel_row).PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, _cart_items(sel_row).PackPrice.Wholesale, _cart_items(sel_row).PackPrice.Distribution))
                        unit_price = If(_order_mode = OrderMode.Retail, _cart_items(sel_row).PadPrice.Retail, If(_order_mode = OrderMode.Wholesale, _cart_items(sel_row).PadPrice.Wholesale, _cart_items(sel_row).PadPrice.Distribution))
                        sel_row.SubItems(2).Text = SYS_PAD_ALIAS
                        sel_row.SubItems(3).Text = FormatNumber(pack_price, 2, , , TriState.True) & " (" & FormatNumber(unit_price, 2, , , TriState.True) & " per pad)"
                        sel_row.ImageKey = "pad"
                    Case ItemUnit.Unit
                        pack_price = If(_order_mode = OrderMode.Retail, _cart_items(sel_row).PackPrice.Retail, If(_order_mode = OrderMode.Wholesale, _cart_items(sel_row).PackPrice.Wholesale, _cart_items(sel_row).PackPrice.Distribution))
                        unit_price = If(_order_mode = OrderMode.Retail, _cart_items(sel_row).UnitPrice.Retail, If(_order_mode = OrderMode.Wholesale, _cart_items(sel_row).UnitPrice.Wholesale, _cart_items(sel_row).UnitPrice.Distribution))
                        sel_row.SubItems(2).Text = _cart_items(sel_row).ProductUnit
                        sel_row.SubItems(3).Text = FormatNumber(pack_price, 2, , , TriState.True) & " (" & FormatNumber(unit_price, 2, , , TriState.True) & " per " & _cart_items(sel_row).ProductUnit & ")"
                        sel_row.ImageKey = "pcs"
                End Select
                If Not new_selection = old_display_unit Then
                    Dim multiplier As Double
                    multiplier = If(new_selection = ItemUnit.Pack, 1, If(new_selection = ItemUnit.Pad, _cart_items(sel_row).PackingSize / _cart_items(sel_row).PadsPerPack, _cart_items(sel_row).PackingSize)) / If(old_display_unit = ItemUnit.Pack, 1, If(old_display_unit = ItemUnit.Pad, _cart_items(sel_row).PackingSize / _cart_items(sel_row).PadsPerPack, _cart_items(sel_row).PackingSize))
                    sel_row.SubItems(1).Text = Math.Round(old_quantity * multiplier, If(SYS_DECIMAL_STOCKS, 2, 0))
                End If
                sel_row.Text = _cart_items(sel_row).ProductCompleteNameWithPacking(new_selection)
                RecalculateTotalAmount()
            End If
        End If

    End Sub

    Private Sub ResetFocus() Handles button_new_transaction.GotFocus, button_new_dispatch.GotFocus, button_cancel_order.GotFocus, button_place_order.GotFocus, button_set_reference_no.GotFocus, button_use_admin.GotFocus, button_browse.GotFocus, button_price_inquiry.GotFocus, button_set_price.GotFocus, button_set_quantity.GotFocus, button_void_item.GotFocus, button_change_unit.GotFocus, button_set_credit.GotFocus, button_set_customer.GotFocus

        list_products.Focus()

    End Sub

    Private Sub button_price_check_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_price_inquiry.Click

        If _price_inquiry Then
            _price_inquiry = False
            button_price_inquiry.BackColor = module_colors.color_dark_gray
            label_product_name.Text = "Browse, scan or enter product barcode."
            label_product_price.Text = "0.00"
        Else
            _price_inquiry = True
            button_price_inquiry.BackColor = module_colors.color_green
            label_product_name.Text = "Scan or enter product code for Price Inquiry."
            label_product_price.Text = "0.00"
        End If

    End Sub

    Private Sub button_customer_report_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim form_customer_report As New form_customer_report
        form_customer_report.ShowDialog(Me)

    End Sub

    Private Sub button_sales_history_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim form_sales_history As New form_sales_history
        form_sales_history.ShowDialog(Me)

    End Sub

    Private Sub button_new_dispatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_new_dispatch.Click

        SetNewOrder(OrderType.Dispatch)

    End Sub

    Private Sub button_place_order_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_place_order.Click

        If _order_type = OrderType.Transaction Then
            PlaceTransactionOrder(Nothing, Nothing)
        Else
            PlaceDispatchOrder(Nothing, Nothing)
        End If

    End Sub

    Public Function GetLastOrderMode() As OrderMode

        If IsNothing(_last_order_detail) Then
            GetLastOrderMode = Nothing
        Else
            If TypeOf _last_order_detail Is Transaction Then
                Dim clone_detail As Transaction = CType(_last_order_detail, Transaction)
                GetLastOrderMode = clone_detail.TransactionType
            ElseIf TypeOf _last_order_detail Is Dispatch Then
                Dim clone_detail As Dispatch = CType(_last_order_detail, Dispatch)
                GetLastOrderMode = clone_detail.DispatchType
            Else
                GetLastOrderMode = Nothing
            End If
        End If

    End Function

    Public Function GetLastOrderType() As OrderType

        If IsNothing(_last_order_detail) Then
            GetLastOrderType = Nothing
        Else
            If TypeOf _last_order_detail Is Transaction Then
                GetLastOrderType = OrderType.Transaction
            ElseIf TypeOf _last_order_detail Is Dispatch Then
                GetLastOrderType = OrderType.Dispatch
            Else
                GetLastOrderType = Nothing
            End If
        End If

    End Function

    Private Sub ChangeCustomer() Handles button_set_customer.Click

        Dim customer_selector As New form_customer_selector
        Dim customer_detail = customer_selector.OpenCustomerSelectorDialog(Me, True)
        If Not IsNothing(customer_detail) Then
            If _order_type = OrderType.Transaction Then
                _transaction_detail.Customer = customer_detail
                label_customer_name.Text = "Customer: " & customer_detail.CustomerName
            Else
                button_set_customer.Enabled = False
                MsgBox("Cannot change customer for this mode.", MsgBoxStyle.Exclamation)
            End If
        End If

    End Sub

    Private Sub SetCredit() Handles button_set_credit.Click

        If Not IsNothing(_transaction_detail) Then
            Dim credit_selector As New form_credit_editor
            Dim credit_detail = credit_selector.OpenCreditEditor(Me, GetRecalculatedTotalAmount(), _transaction_detail.Customer, True)
            If Not IsNothing(credit_detail) Then
                _transaction_detail.Credit = credit_detail
                label_credit_amount.Text = FormatNumber(credit_detail.CreditAmount, 2, , , TriState.True)
            End If
        End If

    End Sub

    Private Sub label_amount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_total_amount.TextChanged, label_credit_amount.TextChanged, label_tendered_amount.TextChanged, label_change_amount.TextChanged

        If sender Is label_total_amount Then
            label_total_amount_currency.Left = label_total_amount.Left - 26
        ElseIf sender Is label_credit_amount Then
            label_credit_amount_currency.Left = label_credit_amount.Left - 26
        ElseIf sender Is label_tendered_amount Then
            label_tendered_amount_currency.Left = label_tendered_amount.Left - 26
        ElseIf sender Is label_change_amount Then
            label_change_amount_currency.Left = label_change_amount.Left - 26
        End If

    End Sub

    Private Sub button_reprint_dr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_reprint_dr.Click

        Dim form_reprint_dr As New form_reprint_dr
        form_reprint_dr.ShowDialog(Me)

    End Sub

    Private Sub label_console_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_console.TextChanged

    End Sub

    Private Sub AdjustObjects(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize, label_order_type.TextChanged, label_order_mode.TextChanged

    End Sub
End Class