﻿Public Class form_sales_history

    Private _current_offset As Integer = 0
    Private sql As New class_db_transaction
    Private _transaction_col_width() As Double = {10, 20, 10, 10, 10, 10, 10, 20}
    Private _transaction_column_sort As New Dictionary(Of Integer, String)
    Private _transaction_current_sort_field As String = "td_transaction_date"
    Private _transaction_current_sort_order As String = "DESC"

    Private Sub AdjustInterface() Handles Me.Load

        _transaction_column_sort.Add(0, "td_invoice_no")
        _transaction_column_sort.Add(1, "td_transaction_type")
        _transaction_column_sort.Add(3, "c_name")
        _transaction_column_sort.Add(4, "sl_username")
        _transaction_column_sort.Add(5, "asl_username")
        _transaction_column_sort.Add(6, "td_transaction_verified")
        _transaction_column_sort.Add(7, "td_transaction_date")
        label_total_amount.ForeColor = module_colors.color_light_green
        label_credit_amount.ForeColor = module_colors.color_pink

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        panel_controls.BackColor = module_colors.color_gray
        list_transactions.Height = (Me.ClientSize.Height / 2) - list_transactions.Top
        panel_header.Top = list_transactions.Bottom
        list_products.Width = Me.ClientSize.Width / 2
        list_products.Top = panel_header.Bottom
        list_products.Height = Me.ClientSize.Height - panel_header.Bottom
        list_returns.Left = list_products.Right
        list_returns.Width = list_products.Width
        list_returns.Top = list_products.Top
        list_returns.Height = Me.ClientSize.Height - panel_total_amount.Height - list_returns.Top
        panel_total_amount.Left = list_returns.Left
        panel_total_amount.Width = (list_returns.Width / 2)
        panel_total_amount.Top = list_returns.Bottom
        panel_credit_amount.Width = panel_total_amount.Width
        panel_credit_amount.Left = panel_total_amount.Right
        panel_credit_amount.Top = panel_total_amount.Top
        label_total_amount.Left = panel_total_amount.Width - label_total_amount.Width
        label_credit_amount.Left = panel_credit_amount.Width - label_credit_amount.Width
        label_invoice_no.Left = (panel_header.Width / 2) - (label_invoice_no.Width / 2)
        ResizeListViewColumn(list_transactions, _transaction_col_width)

    End Sub

    Private Sub LoadTransactionList(Optional ByVal staff_list As List(Of Integer) = Nothing, Optional ByVal offset As Integer = 0, Optional ByVal count As Integer = -1)

        list_transactions.Items.Clear()
        Dim results = sql.GetTransactionList(staff_list, offset, count, _transaction_current_sort_field, _transaction_current_sort_order)
        _current_offset = offset
        If Not IsNothing(results) Then
            For Each result In results
                Dim list_item As ListViewItem = list_transactions.Items.Add(result("td_invoice_no").ToString().PadLeft(10, "0"))
                list_item.SubItems.Add(result("td_transaction_type"))
                list_item.SubItems.Add(If(result("debtor_name") = "--", "--", "To: " & result("debtor_name") & vbCrLf & "; Amount: " & SYS_CURRENCY & FormatNumber(result("credit_amount"), 2, , , TriState.True)))
                list_item.SubItems.Add(result("c_name"))
                list_item.SubItems.Add(result("sp_staff_name"))
                Dim status_subitem As New ListViewItem.ListViewSubItem
                If result("td_transaction_verified") Then
                    status_subitem.Text = "Verified"
                    list_item.ImageKey = "valid"
                Else
                    status_subitem.Text = "Unverified"
                    list_item.ImageKey = "invalid"

                End If
                list_item.SubItems.Add(result("asl_username"))
                list_item.SubItems.Add(status_subitem)
                list_item.SubItems.Add(Date.Parse(result("td_transaction_date")).ToString(DTS_HUMAN_WITH_TIME))
                list_item.Tag = result("td_transaction_id")
            Next
        End If

    End Sub

    Private Sub page_navigator_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator.PageChanged

        LoadTransactionList(, offset, page_count)

    End Sub

    Private Sub form_sales_history_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        page_navigator.ListView = list_transactions
        page_navigator.TotalItems = sql.GetTransactionCount()

    End Sub

    Private Sub list_transactions_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles list_transactions.ColumnClick

        If _transaction_column_sort.ContainsKey(e.Column) Then
            If _transaction_current_sort_field = _transaction_column_sort(e.Column) Then
                _transaction_current_sort_order = If(_transaction_current_sort_order = "ASC", "DESC", "ASC")
            Else
                _transaction_current_sort_field = _transaction_column_sort(e.Column)
            End If
            page_navigator.TotalItems = sql.GetTransactionCount
        End If

    End Sub

    Private Sub list_transactions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_transactions.SelectedIndexChanged

        If list_transactions.SelectedItems.Count > 0 Then
            Dim sel_row As ListViewItem = list_transactions.SelectedItems(0)
            Dim transaction_detail As New Transaction(Val(sel_row.Tag))
            Dim total_amount As Double = 0
            button_verify_transaction.Enabled = Not transaction_detail.Verified
            button_reprint_dr.Enabled = Not button_verify_transaction.Enabled
            button_change_customer.Enabled = button_reprint_dr.Enabled
            button_change_agent.Enabled = button_reprint_dr.Enabled
            button_change_credit.Enabled = True
            button_void_transaction.Enabled = module_session.current_user_profile.Permission_Administator
            list_products.Items.Clear()
            list_returns.Items.Clear()
            For Each item In transaction_detail.TransactionItems
                Dim new_item As ListViewItem = list_products.Items.Add(item.Product.ProductCompleteNameWithPacking(item.DisplayUnit))
                new_item.UseItemStyleForSubItems = True
                Dim unit As String
                Dim unit_price As Double
                Dim temp_price As String
                Dim true_quantity As Double
                If item.ReturnQuantity > 0 Then
                    new_item.BackColor = module_colors.color_yellow
                Else
                    new_item.BackColor = Color.White
                End If
                new_item.SubItems.Add(If(IsNothing(item.Stock), "--", item.Stock.LotNumber))
                Dim item_quantity As Double = item.ItemQuantity - item.ReturnQuantity
                If item_quantity Mod item.Product.PackingSize = 0 And item.Product.IsSoldInPacks And item.DisplayUnit = ItemUnit.Pack Then
                    unit = SYS_PACK_ALIAS
                    true_quantity = item_quantity / item.Product.PackingSize
                ElseIf item_quantity Mod (item.Product.PackingSize / item.Product.PadsPerPack) = 0 And item.Product.IsSoldInPads And item.DisplayUnit = ItemUnit.Pad Then
                    unit = SYS_PAD_ALIAS
                    true_quantity = item_quantity / (item.Product.PackingSize / item.Product.PadsPerPack)
                Else
                    unit = item.Product.ProductUnit
                    true_quantity = item_quantity
                End If
                new_item.SubItems.Add(FormatNumber(true_quantity, If(SYS_DECIMAL_STOCKS, 2, 0), , , TriState.True))
                new_item.SubItems.Add(unit)
                temp_price = FormatNumber(item.SellingPrice, 2, , , TriState.True)
                If Not unit = SYS_PACK_ALIAS Then
                    If unit = SYS_PAD_ALIAS Then
                        unit_price = RoundUp(item.SellingPrice / item.Product.PadsPerPack, 2)
                        temp_price &= " (" & FormatNumber(unit_price, 2, , , TriState.True)
                    Else
                        unit_price = RoundUp(item.SellingPrice / item.Product.PackingSize, 2)
                        temp_price &= " (" & FormatNumber(unit_price, 2, , , TriState.True)
                    End If
                    temp_price &= " per " & unit & ")"
                Else
                    unit_price = item.SellingPrice
                End If
                total_amount += (unit_price * true_quantity)
                new_item.SubItems.Add(temp_price)
                new_item.SubItems.Add(FormatNumber(unit_price * true_quantity, 2, , , TriState.True))
                new_item.Tag = item.ItemID
                For Each return_entry In item.ReturnEntries
                    Dim return_item As ListViewItem = list_returns.Items.Add(item.Product.ProductCompleteName)
                    If return_entry.ReturnQuantity Mod item.Product.PackingSize = 0 And item.Product.IsSoldInPacks And item.DisplayUnit = ItemUnit.Pack Then
                        unit = SYS_PACK_ALIAS
                        unit_price = item.SellingPrice
                        true_quantity = return_entry.ReturnQuantity / item.Product.PackingSize
                    ElseIf return_entry.ReturnQuantity Mod (item.Product.PackingSize / item.Product.PadsPerPack) = 0 And item.Product.IsSoldInPads And item.DisplayUnit = ItemUnit.Pad Then
                        unit = SYS_PAD_ALIAS
                        unit_price = item.SellingPrice / item.Product.PadsPerPack
                        true_quantity = return_entry.ReturnQuantity / (item.Product.PackingSize / item.Product.PadsPerPack)
                    Else
                        unit = item.Product.ProductUnit
                        unit_price = item.SellingPrice / item.Product.PackingSize
                        true_quantity = return_entry.ReturnQuantity
                    End If
                    return_item.SubItems.Add(true_quantity)
                    return_item.SubItems.Add(unit)
                    return_item.SubItems.Add(FormatNumber(true_quantity * unit_price, 2, , , TriState.True))
                Next
            Next
            label_invoice_no.Text = If(Not transaction_detail.Verified, "(Unverified) ", "") & "D.R. No.: " & transaction_detail.InvoiceNo.ToString().PadLeft(10, "0")
            label_cart_count.Text = "Cart (" & list_products.Items.Count & " items)"
            label_return_count.Text = "Returned (" & list_returns.Items.Count & " items)"
            label_total_amount.Text = SYS_CURRENCY & FormatNumber(total_amount, 2, , , TriState.True)
            label_credit_amount.Text = SYS_CURRENCY & FormatNumber(If(IsNothing(transaction_detail.Credit), 0, transaction_detail.Credit.CreditAmount - transaction_detail.Credit.TotalPaymentAmount), 2, , , TriState.True)
        Else
            button_reprint_dr.Enabled = False
            button_verify_transaction.Enabled = False
            button_change_customer.Enabled = False
            button_change_agent.Enabled = False
            button_change_credit.Enabled = False
            button_void_transaction.Enabled = False
            list_products.Items.Clear()
            list_returns.Items.Clear()
            label_cart_count.Text = "Cart"
            label_return_count.Text = "Returned"
            label_total_amount.Text = SYS_CURRENCY & "0.00"
            label_credit_amount.Text = SYS_CURRENCY & "0.00"
            label_invoice_no.Text = "--"
        End If

    End Sub

    Private Sub button_reprint_dr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_reprint_dr.Click

        If list_transactions.SelectedItems.Count < 1 Then
            button_reprint_dr.Enabled = False
        Else
            Dim form_dr_printer As New form_delivery_receipt_generator
            form_dr_printer.OpenTransactionReportGeneratorDialog(Me, New Transaction(Val(list_transactions.SelectedItems(0).Tag)))
        End If

    End Sub

    Private Sub button_verify_transaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_verify_transaction.Click

        If list_transactions.SelectedItems.Count < 1 Then
            button_verify_transaction.Enabled = False
        Else
            If MsgBox("Are you sure you want to validate this transaction?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                Dim sql As New class_db_transaction
                If sql.ValidateTransaction(Val(list_transactions.SelectedItems(0).Tag)) Then
                    MsgBox("Transaction verified.", MsgBoxStyle.Information)
                    page_navigator.TotalItems = sql.GetTransactionCount()
                Else
                    MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                End If
            End If
        End If

    End Sub

    Private Sub button_change_customer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_change_customer.Click

        If list_transactions.SelectedItems.Count < 1 Then
            button_change_customer.Enabled = False
        Else
            Dim sel_row = list_transactions.SelectedItems(0)
            Dim customer_selector As New form_customer_selector
            Dim customer_detail = customer_selector.OpenCustomerSelectorDialog(Me)
            If Not IsNothing(customer_detail) Then
                If MsgBox("Are you sure you want to change this transaction's customer to " & customer_detail.CustomerName & "?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                    Dim transaction_detail As New Transaction(Val(sel_row.Tag))
                    transaction_detail.Customer = customer_detail
                    If transaction_detail.Persist Then
                        MsgBox("Update successful.", MsgBoxStyle.Information)
                        page_navigator.TotalItems = sql.GetTransactionCount
                    Else
                        MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub button_change_agent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_change_agent.Click

        If list_transactions.SelectedItems.Count < 1 Then
            button_change_agent.Enabled = False
        Else
            Dim sel_row = list_transactions.SelectedItems(0)
            Dim agent_selector As New form_agent_selector
            Dim agent_detail = agent_selector.OpenAgentSelector(Me)
            If Not IsNothing(agent_detail) Then
                If MsgBox("Are you sure you want to change this transaction's agent to " & agent_detail.FullName & "?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                    Dim transaction_detail As New Transaction(Val(sel_row.Tag))
                    transaction_detail.Agent = agent_detail
                    If transaction_detail.Persist Then
                        MsgBox("Update successful.", MsgBoxStyle.Information)
                        page_navigator.TotalItems = sql.GetTransactionCount
                    Else
                        MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub list_products_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles list_products.ItemSelectionChanged

        button_return.Enabled = list_products.SelectedItems.Count > 0

    End Sub

    Private Sub button_return_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_return.Click

        If list_products.SelectedItems.Count = 0 Then
            button_return.Enabled = False
        Else
            Dim sel_row As ListViewItem = list_products.SelectedItems(0)
            Dim item_detail As New Transaction_Item(sel_row.Tag)
            If item_detail.ItemQuantity - item_detail.ReturnQuantity = 0 Then
                MsgBox("Please select another item.", MsgBoxStyle.Exclamation)
            Else
                Dim form_quantity As New form_quantity
                Dim quantity = form_quantity.OpenQuantityDialog(Me, "Return " & sel_row.SubItems(3).Text, , , SYS_DECIMAL_STOCKS)
                If Val(quantity) <= 0 Then
                    MsgBox("Invalid quantity.", MsgBoxStyle.Exclamation)
                Else
                    Dim multiplier As Double = If(sel_row.SubItems(3).Text = SYS_PACK_ALIAS, item_detail.Product.PackingSize, If(sel_row.SubItems(3).Text = SYS_PAD_ALIAS, item_detail.Product.PackingSize / item_detail.Product.PadsPerPack, 1))
                    If Not IsNothing(quantity) Then
                        Dim true_quantity As Double = Val(quantity)
                        If (true_quantity * multiplier) > (item_detail.ItemQuantity - item_detail.ReturnQuantity) Then
                            MsgBox("Quantity exceeds remaining count for item.", MsgBoxStyle.Exclamation)
                        Else
                            Dim return_item As New Transaction_Item_Return
                            return_item.ReturnQuantity = true_quantity * multiplier
                            return_item.Staff = module_session.current_user_profile
                            item_detail.AddReturnItem(return_item)
                            If MsgBox("Are you sure you want to return " & true_quantity & " " & sel_row.SubItems(3).Text & "?", MsgBoxStyle.YesNo) = vbYes Then
                                If item_detail.Persist Then
                                    list_transactions_SelectedIndexChanged(Nothing, Nothing)
                                    MsgBox("Return successful.", MsgBoxStyle.Information)
                                Else
                                    MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    Private Sub label_invoice_no_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles label_invoice_no.Load

    End Sub

    Private Sub label_invoice_no_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles label_invoice_no.TextChanged

        label_invoice_no.Left = (panel_header.Width / 2) - (label_invoice_no.Width / 2)

    End Sub

    Private Sub button_change_credit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_change_credit.Click

        If list_transactions.SelectedItems.Count < 1 Then
            button_change_credit.Enabled = False
        Else
            Dim transaction_detail As New Transaction(Val(list_transactions.SelectedItems(0).Tag))
            If Not IsNothing(transaction_detail) Then
                Dim credit_form As New form_credit_editor
                Dim credit_details As Credit
                If IsNothing(transaction_detail.Credit) Then
                    credit_details = credit_form.OpenCreditEditor(Me, transaction_detail.TotalAmount, transaction_detail.Customer, True)
                Else
                    credit_details = credit_form.OpenCreditEditor(Me, transaction_detail.Credit)
                End If
                If Not IsNothing(credit_details) Then
                    transaction_detail.Credit = credit_details
                    If transaction_detail.Persist Then
                        page_navigator.TotalItems = sql.GetTransactionCount
                    Else
                        MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                    End If
                End If
            End If
            End If

    End Sub

    Private Sub label_total_amount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub button_void_transaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_void_transaction.Click

        If list_transactions.SelectedItems.Count < 1 Then
            button_void_transaction.Enabled = False
        Else
            Dim transaction_id = Val(list_transactions.SelectedItems(0).Tag)
            If MsgBox("NOTE: This action cannot be undone." & vbCrLf & vbCrLf & "Are you sure you want to void this transaction? ", MsgBoxStyle.YesNo) = DialogResult.Yes Then
                If sql.VoidTransaction(transaction_id) Then
                    page_navigator.TotalItems = sql.GetTransactionCount
                Else
                    MsgBox(SYS_ERROR_MESSAGE, MsgBoxStyle.Critical)
                End If
            End If
        End If

    End Sub
End Class