﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_product_management
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_product_management))
        Me.panel_header = New System.Windows.Forms.Panel()
        Me.button_print_price_list = New System.Windows.Forms.Button()
        Me.button_delete_product = New System.Windows.Forms.Button()
        Me.button_edit_product = New System.Windows.Forms.Button()
        Me.button_add_product = New System.Windows.Forms.Button()
        Me.text_search = New System.Windows.Forms.TextBox()
        Me.list_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.page_navigator = New DevaReePharmacy.PageNavigator()
        Me.button_resort = New System.Windows.Forms.Button()
        Me.panel_header.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_header
        '
        Me.panel_header.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_header.BackColor = System.Drawing.Color.Silver
        Me.panel_header.Controls.Add(Me.button_print_price_list)
        Me.panel_header.Controls.Add(Me.button_delete_product)
        Me.panel_header.Controls.Add(Me.button_edit_product)
        Me.panel_header.Controls.Add(Me.button_add_product)
        Me.panel_header.Controls.Add(Me.button_resort)
        Me.panel_header.Location = New System.Drawing.Point(0, 0)
        Me.panel_header.Name = "panel_header"
        Me.panel_header.Size = New System.Drawing.Size(738, 63)
        Me.panel_header.TabIndex = 10
        '
        'button_print_price_list
        '
        Me.button_print_price_list.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_print_price_list.BackColor = System.Drawing.Color.Transparent
        Me.button_print_price_list.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_print_price_list.FlatAppearance.BorderSize = 0
        Me.button_print_price_list.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_print_price_list.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_print_price_list.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_print_price_list.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_print_price_list.ForeColor = System.Drawing.Color.White
        Me.button_print_price_list.Image = CType(resources.GetObject("button_print_price_list.Image"), System.Drawing.Image)
        Me.button_print_price_list.Location = New System.Drawing.Point(641, 2)
        Me.button_print_price_list.Name = "button_print_price_list"
        Me.button_print_price_list.Size = New System.Drawing.Size(96, 60)
        Me.button_print_price_list.TabIndex = 13
        Me.button_print_price_list.TabStop = False
        Me.button_print_price_list.Text = "Print Price List"
        Me.button_print_price_list.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_print_price_list.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_print_price_list.UseVisualStyleBackColor = False
        '
        'button_delete_product
        '
        Me.button_delete_product.BackColor = System.Drawing.Color.Transparent
        Me.button_delete_product.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_delete_product.Enabled = False
        Me.button_delete_product.FlatAppearance.BorderSize = 0
        Me.button_delete_product.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_delete_product.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_delete_product.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_delete_product.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_delete_product.ForeColor = System.Drawing.Color.White
        Me.button_delete_product.Image = CType(resources.GetObject("button_delete_product.Image"), System.Drawing.Image)
        Me.button_delete_product.Location = New System.Drawing.Point(178, 2)
        Me.button_delete_product.Name = "button_delete_product"
        Me.button_delete_product.Size = New System.Drawing.Size(96, 60)
        Me.button_delete_product.TabIndex = 12
        Me.button_delete_product.TabStop = False
        Me.button_delete_product.Text = "Delete Product"
        Me.button_delete_product.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_delete_product.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_delete_product.UseVisualStyleBackColor = False
        '
        'button_edit_product
        '
        Me.button_edit_product.BackColor = System.Drawing.Color.Transparent
        Me.button_edit_product.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_edit_product.Enabled = False
        Me.button_edit_product.FlatAppearance.BorderSize = 0
        Me.button_edit_product.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_edit_product.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_edit_product.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_edit_product.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_edit_product.ForeColor = System.Drawing.Color.White
        Me.button_edit_product.Image = CType(resources.GetObject("button_edit_product.Image"), System.Drawing.Image)
        Me.button_edit_product.Location = New System.Drawing.Point(89, 2)
        Me.button_edit_product.Name = "button_edit_product"
        Me.button_edit_product.Size = New System.Drawing.Size(83, 60)
        Me.button_edit_product.TabIndex = 11
        Me.button_edit_product.TabStop = False
        Me.button_edit_product.Text = "Edit Product"
        Me.button_edit_product.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_edit_product.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_edit_product.UseVisualStyleBackColor = False
        '
        'button_add_product
        '
        Me.button_add_product.BackColor = System.Drawing.Color.Transparent
        Me.button_add_product.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_add_product.FlatAppearance.BorderSize = 0
        Me.button_add_product.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_add_product.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_add_product.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_add_product.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_add_product.ForeColor = System.Drawing.Color.White
        Me.button_add_product.Image = CType(resources.GetObject("button_add_product.Image"), System.Drawing.Image)
        Me.button_add_product.Location = New System.Drawing.Point(0, 2)
        Me.button_add_product.Name = "button_add_product"
        Me.button_add_product.Size = New System.Drawing.Size(83, 60)
        Me.button_add_product.TabIndex = 10
        Me.button_add_product.TabStop = False
        Me.button_add_product.Text = "Add Product"
        Me.button_add_product.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.button_add_product.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_add_product.UseVisualStyleBackColor = False
        '
        'text_search
        '
        Me.text_search.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_search.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_search.ForeColor = System.Drawing.Color.Black
        Me.text_search.Location = New System.Drawing.Point(0, 63)
        Me.text_search.Name = "text_search"
        Me.text_search.Size = New System.Drawing.Size(738, 29)
        Me.text_search.TabIndex = 1
        '
        'list_products
        '
        Me.list_products.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_products.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.list_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9})
        Me.list_products.Font = New System.Drawing.Font("Open Sans", 10.2!)
        Me.list_products.ForeColor = System.Drawing.Color.Black
        Me.list_products.FullRowSelect = True
        Me.list_products.GridLines = True
        Me.list_products.Location = New System.Drawing.Point(0, 131)
        Me.list_products.Name = "list_products"
        Me.list_products.Size = New System.Drawing.Size(738, 211)
        Me.list_products.TabIndex = 12
        Me.list_products.TabStop = False
        Me.list_products.UseCompatibleStateImageBehavior = False
        Me.list_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Product Name"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product Code"
        Me.ColumnHeader1.Width = 86
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Unit"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Retail Price"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Wholesale Price"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Distribution Price"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Packing Size"
        Me.ColumnHeader7.Width = 118
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Pads / Pack"
        Me.ColumnHeader8.Width = 128
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Available Quantity"
        Me.ColumnHeader9.Width = 99
        '
        'page_navigator
        '
        Me.page_navigator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.page_navigator.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(68, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.page_navigator.ListView = Nothing
        Me.page_navigator.Location = New System.Drawing.Point(0, 92)
        Me.page_navigator.Loopable = True
        Me.page_navigator.Name = "page_navigator"
        Me.page_navigator.PageSize = 2
        Me.page_navigator.Size = New System.Drawing.Size(737, 37)
        Me.page_navigator.TabIndex = 11
        Me.page_navigator.TotalItems = 0
        '
        'button_resort
        '
        Me.button_resort.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.button_resort.BackColor = System.Drawing.Color.Transparent
        Me.button_resort.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_resort.FlatAppearance.BorderSize = 0
        Me.button_resort.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.button_resort.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Gray
        Me.button_resort.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_resort.Font = New System.Drawing.Font("Segoe UI Semilight", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_resort.ForeColor = System.Drawing.Color.White
        Me.button_resort.Image = CType(resources.GetObject("button_resort.Image"), System.Drawing.Image)
        Me.button_resort.Location = New System.Drawing.Point(530, 2)
        Me.button_resort.Name = "button_resort"
        Me.button_resort.Size = New System.Drawing.Size(121, 60)
        Me.button_resort.TabIndex = 17
        Me.button_resort.TabStop = False
        Me.button_resort.Text = "Re-sort Categories"
        Me.button_resort.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.button_resort.UseVisualStyleBackColor = False
        '
        'form_product_management
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(738, 341)
        Me.Controls.Add(Me.list_products)
        Me.Controls.Add(Me.page_navigator)
        Me.Controls.Add(Me.text_search)
        Me.Controls.Add(Me.panel_header)
        Me.Name = "form_product_management"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Product Management"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_header.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panel_header As System.Windows.Forms.Panel
    Friend WithEvents text_search As System.Windows.Forms.TextBox
    Friend WithEvents page_navigator As DevaReePharmacy.PageNavigator
    Friend WithEvents button_add_product As System.Windows.Forms.Button
    Friend WithEvents button_edit_product As System.Windows.Forms.Button
    Friend WithEvents list_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_delete_product As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents button_print_price_list As System.Windows.Forms.Button
    Friend WithEvents button_resort As System.Windows.Forms.Button
End Class
