﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_distribution
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.panel_distribution_menu = New System.Windows.Forms.Panel()
        Me.button_quantity = New System.Windows.Forms.Button()
        Me.button_place_distribution = New System.Windows.Forms.Button()
        Me.button_remove_item = New System.Windows.Forms.Button()
        Me.button_inventory = New System.Windows.Forms.Button()
        Me.link_logout = New System.Windows.Forms.LinkLabel()
        Me.label_agent = New System.Windows.Forms.Label()
        Me.text_agent_name = New System.Windows.Forms.TextBox()
        Me.list_products = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.label_code = New System.Windows.Forms.Label()
        Me.text_code = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.panel_cashier_info = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.label_cashier_name = New System.Windows.Forms.Label()
        Me.Timer_distribution = New System.Windows.Forms.Timer(Me.components)
        Me.panel_remittance_menu = New System.Windows.Forms.Panel()
        Me.label_distribution_id = New System.Windows.Forms.Label()
        Me.button_close_distribution = New System.Windows.Forms.Button()
        Me.button_remit = New System.Windows.Forms.Button()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.panel_distribution_menu.SuspendLayout()
        Me.panel_cashier_info.SuspendLayout()
        Me.panel_remittance_menu.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_distribution_menu
        '
        Me.panel_distribution_menu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_distribution_menu.BackColor = System.Drawing.Color.Silver
        Me.panel_distribution_menu.Controls.Add(Me.button_quantity)
        Me.panel_distribution_menu.Controls.Add(Me.button_place_distribution)
        Me.panel_distribution_menu.Controls.Add(Me.button_remove_item)
        Me.panel_distribution_menu.Controls.Add(Me.button_inventory)
        Me.panel_distribution_menu.Controls.Add(Me.link_logout)
        Me.panel_distribution_menu.Enabled = False
        Me.panel_distribution_menu.Location = New System.Drawing.Point(0, 0)
        Me.panel_distribution_menu.Name = "panel_distribution_menu"
        Me.panel_distribution_menu.Size = New System.Drawing.Size(891, 63)
        Me.panel_distribution_menu.TabIndex = 9
        '
        'button_quantity
        '
        Me.button_quantity.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.button_quantity.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_quantity.FlatAppearance.BorderSize = 0
        Me.button_quantity.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_quantity.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_quantity.ForeColor = System.Drawing.Color.White
        Me.button_quantity.Location = New System.Drawing.Point(119, 0)
        Me.button_quantity.Name = "button_quantity"
        Me.button_quantity.Size = New System.Drawing.Size(113, 63)
        Me.button_quantity.TabIndex = 18
        Me.button_quantity.TabStop = False
        Me.button_quantity.Text = "Quantity" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.button_quantity.UseVisualStyleBackColor = False
        '
        'button_place_distribution
        '
        Me.button_place_distribution.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.button_place_distribution.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_place_distribution.FlatAppearance.BorderSize = 0
        Me.button_place_distribution.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_place_distribution.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_place_distribution.ForeColor = System.Drawing.Color.White
        Me.button_place_distribution.Location = New System.Drawing.Point(357, 0)
        Me.button_place_distribution.Name = "button_place_distribution"
        Me.button_place_distribution.Size = New System.Drawing.Size(122, 63)
        Me.button_place_distribution.TabIndex = 17
        Me.button_place_distribution.TabStop = False
        Me.button_place_distribution.Text = "Place Distribution"
        Me.button_place_distribution.UseVisualStyleBackColor = False
        '
        'button_remove_item
        '
        Me.button_remove_item.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.button_remove_item.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_remove_item.FlatAppearance.BorderSize = 0
        Me.button_remove_item.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_remove_item.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_remove_item.ForeColor = System.Drawing.Color.White
        Me.button_remove_item.Location = New System.Drawing.Point(238, 0)
        Me.button_remove_item.Name = "button_remove_item"
        Me.button_remove_item.Size = New System.Drawing.Size(113, 63)
        Me.button_remove_item.TabIndex = 16
        Me.button_remove_item.TabStop = False
        Me.button_remove_item.Text = "Remove" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.button_remove_item.UseVisualStyleBackColor = False
        '
        'button_inventory
        '
        Me.button_inventory.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.button_inventory.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_inventory.FlatAppearance.BorderSize = 0
        Me.button_inventory.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_inventory.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_inventory.ForeColor = System.Drawing.Color.White
        Me.button_inventory.Location = New System.Drawing.Point(0, 0)
        Me.button_inventory.Name = "button_inventory"
        Me.button_inventory.Size = New System.Drawing.Size(113, 63)
        Me.button_inventory.TabIndex = 8
        Me.button_inventory.TabStop = False
        Me.button_inventory.Text = "Inventory" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.button_inventory.UseVisualStyleBackColor = False
        '
        'link_logout
        '
        Me.link_logout.AutoSize = True
        Me.link_logout.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.link_logout.ForeColor = System.Drawing.Color.Black
        Me.link_logout.LinkColor = System.Drawing.Color.White
        Me.link_logout.Location = New System.Drawing.Point(768, 9)
        Me.link_logout.Name = "link_logout"
        Me.link_logout.Size = New System.Drawing.Size(0, 20)
        Me.link_logout.TabIndex = 3
        Me.link_logout.VisitedLinkColor = System.Drawing.Color.White
        '
        'label_agent
        '
        Me.label_agent.AutoSize = True
        Me.label_agent.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_agent.ForeColor = System.Drawing.Color.Black
        Me.label_agent.Location = New System.Drawing.Point(12, 70)
        Me.label_agent.Name = "label_agent"
        Me.label_agent.Size = New System.Drawing.Size(42, 17)
        Me.label_agent.TabIndex = 12
        Me.label_agent.Text = "Agent"
        '
        'text_agent_name
        '
        Me.text_agent_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_agent_name.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.text_agent_name.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.text_agent_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_agent_name.ForeColor = System.Drawing.Color.Black
        Me.text_agent_name.Location = New System.Drawing.Point(11, 92)
        Me.text_agent_name.Name = "text_agent_name"
        Me.text_agent_name.Size = New System.Drawing.Size(510, 29)
        Me.text_agent_name.TabIndex = 11
        '
        'list_products
        '
        Me.list_products.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.list_products.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.list_products.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.list_products.FullRowSelect = True
        Me.list_products.HideSelection = False
        Me.list_products.Location = New System.Drawing.Point(12, 150)
        Me.list_products.Name = "list_products"
        Me.list_products.Size = New System.Drawing.Size(865, 232)
        Me.list_products.TabIndex = 13
        Me.list_products.TabStop = False
        Me.list_products.UseCompatibleStateImageBehavior = False
        Me.list_products.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Product"
        Me.ColumnHeader1.Width = 95
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Quantity"
        Me.ColumnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader2.Width = 132
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Unit"
        Me.ColumnHeader3.Width = 78
        '
        'label_code
        '
        Me.label_code.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.label_code.AutoSize = True
        Me.label_code.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_code.ForeColor = System.Drawing.Color.Black
        Me.label_code.Location = New System.Drawing.Point(8, 385)
        Me.label_code.Name = "label_code"
        Me.label_code.Size = New System.Drawing.Size(221, 17)
        Me.label_code.TabIndex = 15
        Me.label_code.Text = "Enter product code or scan barcode"
        '
        'text_code
        '
        Me.text_code.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.text_code.Enabled = False
        Me.text_code.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.text_code.ForeColor = System.Drawing.Color.Black
        Me.text_code.Location = New System.Drawing.Point(12, 409)
        Me.text_code.Name = "text_code"
        Me.text_code.Size = New System.Drawing.Size(865, 29)
        Me.text_code.TabIndex = 14
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 128)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 17)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Cart"
        '
        'panel_cashier_info
        '
        Me.panel_cashier_info.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_cashier_info.BackColor = System.Drawing.Color.Yellow
        Me.panel_cashier_info.Controls.Add(Me.Label5)
        Me.panel_cashier_info.Controls.Add(Me.Label3)
        Me.panel_cashier_info.Controls.Add(Me.Label2)
        Me.panel_cashier_info.Controls.Add(Me.label_cashier_name)
        Me.panel_cashier_info.Location = New System.Drawing.Point(527, 69)
        Me.panel_cashier_info.Name = "panel_cashier_info"
        Me.panel_cashier_info.Size = New System.Drawing.Size(350, 72)
        Me.panel_cashier_info.TabIndex = 17
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(12, 34)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 21)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Date and Time:"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(12, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(111, 21)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Cashier Name:"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(129, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(158, 21)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "00/00/0000 00:00:00"
        '
        'label_cashier_name
        '
        Me.label_cashier_name.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.label_cashier_name.AutoSize = True
        Me.label_cashier_name.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_cashier_name.ForeColor = System.Drawing.Color.Black
        Me.label_cashier_name.Location = New System.Drawing.Point(129, 13)
        Me.label_cashier_name.Name = "label_cashier_name"
        Me.label_cashier_name.Size = New System.Drawing.Size(22, 21)
        Me.label_cashier_name.TabIndex = 11
        Me.label_cashier_name.Text = "--"
        '
        'Timer_distribution
        '
        '
        'panel_remittance_menu
        '
        Me.panel_remittance_menu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panel_remittance_menu.BackColor = System.Drawing.Color.Silver
        Me.panel_remittance_menu.Controls.Add(Me.label_distribution_id)
        Me.panel_remittance_menu.Controls.Add(Me.button_close_distribution)
        Me.panel_remittance_menu.Controls.Add(Me.button_remit)
        Me.panel_remittance_menu.Controls.Add(Me.LinkLabel1)
        Me.panel_remittance_menu.Location = New System.Drawing.Point(0, 181)
        Me.panel_remittance_menu.Name = "panel_remittance_menu"
        Me.panel_remittance_menu.Size = New System.Drawing.Size(891, 63)
        Me.panel_remittance_menu.TabIndex = 18
        Me.panel_remittance_menu.Visible = False
        '
        'label_distribution_id
        '
        Me.label_distribution_id.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.label_distribution_id.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label_distribution_id.ForeColor = System.Drawing.Color.White
        Me.label_distribution_id.Location = New System.Drawing.Point(500, 12)
        Me.label_distribution_id.Name = "label_distribution_id"
        Me.label_distribution_id.Size = New System.Drawing.Size(377, 17)
        Me.label_distribution_id.TabIndex = 8
        Me.label_distribution_id.Text = "Distribution Ref #: 0"
        Me.label_distribution_id.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'button_close_distribution
        '
        Me.button_close_distribution.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.button_close_distribution.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_close_distribution.FlatAppearance.BorderSize = 0
        Me.button_close_distribution.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_close_distribution.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_close_distribution.ForeColor = System.Drawing.Color.White
        Me.button_close_distribution.Location = New System.Drawing.Point(120, 0)
        Me.button_close_distribution.Name = "button_close_distribution"
        Me.button_close_distribution.Size = New System.Drawing.Size(149, 63)
        Me.button_close_distribution.TabIndex = 7
        Me.button_close_distribution.TabStop = False
        Me.button_close_distribution.Text = "Close Distribution"
        Me.button_close_distribution.UseVisualStyleBackColor = False
        '
        'button_remit
        '
        Me.button_remit.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.button_remit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.button_remit.FlatAppearance.BorderSize = 0
        Me.button_remit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.button_remit.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.button_remit.ForeColor = System.Drawing.Color.White
        Me.button_remit.Location = New System.Drawing.Point(0, 0)
        Me.button_remit.Name = "button_remit"
        Me.button_remit.Size = New System.Drawing.Size(113, 63)
        Me.button_remit.TabIndex = 6
        Me.button_remit.TabStop = False
        Me.button_remit.Text = "Remit"
        Me.button_remit.UseVisualStyleBackColor = False
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.ForeColor = System.Drawing.Color.Black
        Me.LinkLabel1.LinkColor = System.Drawing.Color.White
        Me.LinkLabel1.Location = New System.Drawing.Point(768, 9)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(0, 20)
        Me.LinkLabel1.TabIndex = 3
        Me.LinkLabel1.VisitedLinkColor = System.Drawing.Color.White
        '
        'form_distribution
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(889, 449)
        Me.Controls.Add(Me.panel_remittance_menu)
        Me.Controls.Add(Me.panel_cashier_info)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.label_code)
        Me.Controls.Add(Me.text_code)
        Me.Controls.Add(Me.list_products)
        Me.Controls.Add(Me.label_agent)
        Me.Controls.Add(Me.text_agent_name)
        Me.Controls.Add(Me.panel_distribution_menu)
        Me.Name = "form_distribution"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Distribution"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.panel_distribution_menu.ResumeLayout(False)
        Me.panel_distribution_menu.PerformLayout()
        Me.panel_cashier_info.ResumeLayout(False)
        Me.panel_cashier_info.PerformLayout()
        Me.panel_remittance_menu.ResumeLayout(False)
        Me.panel_remittance_menu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panel_distribution_menu As System.Windows.Forms.Panel
    Friend WithEvents link_logout As System.Windows.Forms.LinkLabel
    Friend WithEvents label_agent As System.Windows.Forms.Label
    Friend WithEvents text_agent_name As System.Windows.Forms.TextBox
    Friend WithEvents list_products As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents label_code As System.Windows.Forms.Label
    Friend WithEvents text_code As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents button_inventory As System.Windows.Forms.Button
    Friend WithEvents panel_cashier_info As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents label_cashier_name As System.Windows.Forms.Label
    Friend WithEvents button_place_distribution As System.Windows.Forms.Button
    Friend WithEvents button_remove_item As System.Windows.Forms.Button
    Friend WithEvents button_quantity As System.Windows.Forms.Button
    Friend WithEvents Timer_distribution As System.Windows.Forms.Timer
    Friend WithEvents panel_remittance_menu As System.Windows.Forms.Panel
    Friend WithEvents button_remit As System.Windows.Forms.Button
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents button_close_distribution As System.Windows.Forms.Button
    Friend WithEvents label_distribution_id As System.Windows.Forms.Label
End Class
