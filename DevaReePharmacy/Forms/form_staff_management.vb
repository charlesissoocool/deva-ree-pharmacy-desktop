﻿Public Class form_staff_management

    Private sql As New class_db_user
    Private _col_width() As Double = {25, 25, 25, 25}
    Private _current_search_tag As String = Nothing
    Private _current_offset As Integer

    Private Sub form_staff_management_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        'added 07/17/2015 by Aljun shortcut to close the form
        If e.KeyCode = Keys.F12 Then
            Me.Close()
        End If
    End Sub

    Private Sub AdjustInteface() Handles Me.Load

        panel_controls.BackColor = module_colors.color_gray
        ' page_navigator.BackColor = module_colors.color_gray
        button_add_user.FlatAppearance.MouseOverBackColor = module_colors.color_black
        button_edit_user.FlatAppearance.MouseOverBackColor = module_colors.color_black
        button_activate_user.FlatAppearance.MouseOverBackColor = module_colors.color_black
        button_deactivate_user.FlatAppearance.MouseOverBackColor = module_colors.color_black
        page_navigator.TotalItems = sql.GetUserListCount()
        page_navigator.ListView = list_user
        LoadUserList()

        Me.KeyPreview = True

    End Sub

    Private Sub AdjustObjects() Handles Me.Resize

        ResizeListViewColumn(list_user, _col_width)

    End Sub

    Private Sub LoadUserList(Optional ByVal search_tag As String = Nothing, Optional ByVal offset As Integer = 0)

        list_user.Items.Clear()
        Dim user_list = sql.GetUserList(search_tag, offset)
        _current_search_tag = search_tag
        _current_offset = offset
        If Not IsNothing(user_list) Then
            For Each user In user_list
                Dim new_item As ListViewItem = list_user.Items.Add(user("username"))
                If user("is_active") Then
                    new_item.SubItems.Add(user("first_name") & Space(1) & user("last_name"))
                    new_item.SubItems.Add(sql.GetImplodedUserPermissions(Val(user("staff_id"))))
                    new_item.SubItems.Add("Active", module_colors.color_green, Color.White, list_user.Font)
                Else
                    new_item.SubItems.Add("--")
                    new_item.SubItems.Add("--")
                    new_item.SubItems.Add("Inactive", module_colors.color_red, Color.White, list_user.Font)
                    new_item.BackColor = module_colors.color_red
                    new_item.ForeColor = Color.White
                End If
                new_item.Tag = user("staff_id")
            Next
        End If

    End Sub

    Private Sub button_new_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim user_editor As New form_user_editor
        user_editor.OpenUserEditor(Me)
        LoadUserList(_current_search_tag, _current_offset)

    End Sub

    Private Sub text_search_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_search.KeyPress

        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            _current_search_tag = text_search.Text
            page_navigator.TotalItems = sql.GetUserListCount(_current_search_tag)
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            e.Handled = True
            _current_search_tag = Nothing
            page_navigator.TotalItems = sql.GetUserListCount()
        End If

    End Sub

    Private Sub page_navigator_PageChanged(ByVal current_page As Integer, ByVal offset As Integer, ByVal page_count As Integer) Handles page_navigator.PageChanged

        LoadUserList(_current_search_tag, offset)

    End Sub

    Private Sub button_add_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_add_user.Click

        Dim user_editor As New form_user_editor
        Dim user_details = user_editor.OpenUserEditor(Me)
        If Not IsNothing(user_details) Then
            If user_details.Persist() Then
                page_navigator.TotalItems = sql.GetUserListCount
            Else
                MsgBox("Failed to add user.", MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub button_edit_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_edit_user.Click

        If list_user.SelectedItems.Count < 1 Then
            sender.Enabled = False
            Exit Sub
        End If

        Dim sel_row = list_user.SelectedItems(0)
        Dim user_editor As New form_user_editor
        Dim user_details As New User(Val(sel_row.Tag))
        user_details = user_editor.OpenUserEditor(Me, user_details)
        If Not IsNothing(user_details) Then
            If Not user_details.Persist() Then
                MsgBox("Failed to update user profile.", MsgBoxStyle.Critical)
            Else
                LoadUserList()
            End If
        End If

    End Sub

    Private Sub button_deactivate_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_deactivate_user.Click

        If list_user.SelectedItems.Count < 1 Then
            sender.Enabled = False
            Exit Sub
        End If

        Dim sel_row = list_user.SelectedItems(0)
        Dim user_id As Integer = Val(sel_row.Tag)
        Dim ask = MessageBox.Show("Are you sure you want to deactive selected user?", "Deactivate User", MessageBoxButtons.YesNo)
        If Not ask = DialogResult.Yes Then Exit Sub
        Dim db_user As New class_db_user
        If Not db_user.DeactivateUser(user_id) Then MessageBox.Show("An error occurred while trying to deactivate user.")
        LoadUserList(_current_search_tag, _current_offset)

    End Sub

    Private Sub button_activate_user_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_activate_user.Click

        If list_user.SelectedItems.Count < 1 Then
            sender.Enabled = False
            Exit Sub
        End If

        Dim sel_row = list_user.SelectedItems(0)
        MessageBox.Show("To activate the user, you must provide details for their profile.")

        Dim user_editor As New form_user_editor
        Dim user_details = user_editor.OpenUserEditor(Me, New User(Val(sel_row.Tag)))
        If Not IsNothing(user_details) Then
            If user_details.Persist() Then
                LoadUserList(_current_search_tag, _current_offset)
            Else
                MsgBox("Failed to add user.", MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub page_navigator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles page_navigator.Load

    End Sub

    Private Sub list_user_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles list_user.DoubleClick

        If button_edit_user.Enabled Then button_edit_user.PerformClick()

    End Sub

    Private Sub list_user_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles list_user.SelectedIndexChanged

        If list_user.SelectedItems.Count < 1 Then
            button_activate_user.Enabled = False
            button_deactivate_user.Enabled = False
            button_edit_user.Enabled = False
            Exit Sub
        End If
        Dim sel_row As ListViewItem = list_user.SelectedItems(0)

        button_activate_user.Enabled = (sel_row.SubItems(3).Text = "Inactive")
        button_deactivate_user.Enabled = Not button_activate_user.Enabled
        button_edit_user.Enabled = button_deactivate_user.Enabled

    End Sub

    Private Sub button_resort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_resort.Click

    End Sub
End Class