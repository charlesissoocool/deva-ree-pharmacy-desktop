﻿Public Class form_login

    Private Sub AdjustObjects() Handles Me.Resize

        group_login.Left = (Me.ClientSize.Width / 2) - (group_login.Width / 2)
        group_login.Top = (Me.ClientSize.Height / 2) - (group_login.Height / 2)
        panel_button.Left = (panel_login.Width / 2) - (panel_button.Width / 2)
        prompt_login.Width = panel_login.Width
        prompt_login.Left = panel_login.Left
        label_copyright.Left = (Me.ClientSize.Width / 2) - (label_copyright.Width / 2)
        image_icon.Left = (Me.ClientSize.Width / 2) - (image_icon.Width / 2)

    End Sub

    Private Sub AdjustInteface() Handles Me.Load

        image_logo.Image = SYS_LOGO
        panel_login.Left = (group_login.Width / 2) - (panel_login.Width / 2)
        image_logo.Left = (group_login.Width / 2) - (image_logo.Width / 2)
        prompt_login.Message = "Please enter login credentials"
        prompt_login.Mode = PromptLabel.PromptLabelMode.Info
        label_copyright.Text = "Customized for " & SYS_CUSTOMER_NAME & vbCrLf & "Copyright © 2015. All Rights Reserved."
        image_icon.Top = label_copyright.Top - 2 - image_icon.Height
        AdjustObjects()


        'Dim dispatch_details As New Dispatch
        'Dim dispatch_item As New Dispatch_Item
        'dispatch_item.DispatchPrice = 1
        'dispatch_item.ItemQuantity = 25
        'dispatch_details.Agent = New User(6)
        'dispatch_details.Staff = New User(6)
        'dispatch_details.AddDispatchItem(dispatch_item)
        'dispatch_details.AddTransactionItem(New Transaction(61))
        'dispatch_details.AddTransactionItem(New Transaction(63))
        'If dispatch_details.Persist() Then
        '    MsgBox("Success")
        'Else
        '    MsgBox("Fail")
        'End If

    End Sub

    Private Sub SetLastUpdateDate() Handles Me.Load

        Try
            label_last_updated.Text = "Last Updated: " & IO.File.GetLastWriteTime(Application.ExecutablePath).ToString(DTS_HUMAN_WITH_TIME)
        Catch ex As Exception
            label_last_updated.Visible = False
        End Try

    End Sub

    Private Sub ShowPrompt(ByVal message As String, ByVal back_color As Color)

        prompt_login.Text = message
        prompt_login.BackColor = back_color
        prompt_login.Visible = True

    End Sub

    Private Sub TryLogin() Handles button_login.Click
        Dim querier_user = New class_db_user
        Dim querier_login = New class_db_login
        Dim user_id As Integer

        If text_user_name.Text = vbNullString Then
            prompt_login.Message = "Please enter username"
            prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
            Exit Sub
        End If

        If text_password.Text = vbNullString Then
            prompt_login.Message = "Please enter password"
            prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
            Exit Sub
        End If

        If text_user_name.Text.Count(Function(x) UCase(x) = "#") = 1 Then
            Dim chunk() As String = text_user_name.Text.Split("#")
            user_id = querier_user.GetUserIDByCredential(chunk.First, text_password.Text)
            If user_id <> 0 And querier_user.DoesUserHavePermission("Admin", user_id) Then
                user_id = querier_user.GetUserIDByUsername(chunk.Last)
            End If
        Else
            user_id = querier_user.GetUserIDByCredential(text_user_name.Text, text_password.Text)
        End If

        If user_id <> 0 Then
            If Not querier_login.IsUserActive(user_id) Then
                prompt_login.Message = "Account deactivated. Request for reactivation."
                prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
                Exit Sub
            End If
            'Setting the user ID after successfull login
            module_session.current_user_profile.UserID = user_id
            querier_login.AddUserLog("In", user_id)

            Dim form_dashboard = New form_dashboard
            form_dashboard.Show()
            Me.Close()
        Else
            prompt_login.Message = "Invalid username or password"
            prompt_login.Mode = PromptLabel.PromptLabelMode.Critical
            text_password.Text = ""
        End If
        Exit Sub
    End Sub

    Private Sub text_user_name_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_user_name.GotFocus
        text_user_name.SelectAll()
    End Sub

    Private Sub text_password_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_password.GotFocus

        text_password.SelectAll()

    End Sub

    Private Sub text_password_HandleDestroyed(ByVal sender As Object, ByVal e As System.EventArgs) Handles text_password.HandleDestroyed

    End Sub

    Private Sub EnterPressed(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles text_user_name.KeyPress, text_password.KeyPress

        If e.KeyChar = ChrW(Keys.Return) Or e.KeyChar = ChrW(Keys.Enter) Then
            button_login.PerformClick()
        End If

        If e.KeyChar = ChrW(Keys.Escape) Then
            button_cancel.PerformClick()
        End If

    End Sub

    Private Sub button_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_cancel.Click

        text_user_name.Text = ""
        text_password.Text = ""

    End Sub

    Private Sub text_user_name_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles text_user_name.TextChanged, text_password.TextChanged

        button_cancel.Enabled = Not text_user_name.Text = "" Or Not text_password.Text = ""

    End Sub

    Private Sub button_quit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_quit.Click

        If MsgBox("Are you sure you want to quit?", MsgBoxStyle.YesNo) = DialogResult.Yes Then
            module_db_connection.DisconnectDatabase()
            Application.Exit()
        End If

    End Sub

    Private Sub button_about_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_about.Click

        Dim form_about As New form_about
        form_about.ShowDialog(Me)

    End Sub

    Private Sub TryLogin(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button_login.Click

    End Sub
End Class
